'use strict';

var ORE = function () {
    this.api_host = Api.HOST;
    this.api_url = this.api_host + Api.VERSION;
};
ORE.prototype.getApiHost = function () {
    return this.api_host;
};
ORE.prototype.getApiUrl = function () {
    return this.api_url;
};
/**
 * ajaxコンストラクタ
 * @constructor
 */
var Ajax = function (url, type, email, password, token_type, token) {
    // 必須
    this.url = url;
    // 基本
    this.type = type;
    this.data_type = Api.DATA_TYPE;
    this.data = new Data( email, password);
    this.timeout = Api.TIMEOUT;
    this.headers = new Headers( token_type, token);
    // 設定
    this.async = Api.ASYNC;
    this.cashe = Api.CASHE;
};
Ajax.prototype.getUrl = function () {
    return this.url;
};
Ajax.prototype.getType = function () {
    return this.type;
};
Ajax.prototype.getDataType = function () {
    return this.data_type;
};
Ajax.prototype.getData = function () {
    return this.data;
};
Ajax.prototype.getTimeout = function () {
    return this.timeout;
};
Ajax.prototype.getHeaders = function () {
    return this.headers;
};
Ajax.prototype.getAsync = function () {
    return this.async;
};
Ajax.prototype.getCashe = function () {
    return this.cashe;
};
Ajax.prototype.done =  function (response) {

};
Ajax.prototype.showError = function () {
    $('#error_message').css('display', 'block');
};
/**
 * トークンオブジェクトをブラウザに保存
 * @param response
 */
Ajax.prototype.saveToken = function (response) {
    var ore_token = new Token( response.staff_id, response.access_token, response.token_type, response.refresh_token, response.expire, response.scope, response.jwt_id);
    window.localStorage.setItem( 'ore_token', ore_token);
};
/**
 * ログイン認証APIをコール
 * @constructor
 */
Ajax.prototype.Authorization = function (auto_login) {
    $.ajax({
        url: this.getUrl(),
        type: this.getType(),
        headers: {
            "Authorization": this.getHeaders().getAuthorization()
        },
        data: {
            grant_type: this.getData().getGrantType(),
            email: this.getData().getEmail(),
            password: this.getData().getPassword(),
            scope: this.getData().getScope()
        },
        dataType: this.getDataType(),
        done: function (response) {
            // 自動でログインチェック時
            if (auto_login) {
                window.localStorage.setItem( 'auto_login', auto_login);
            }
            this.saveToken(response);
            location.href = '/ore/menu.html';
        },
        fail: function () {
            console.log('エラー');
            this.showError();
        }
    });
};
/**
 * ヘッダー情報コンストラクタ
 * @param token_type
 * @param token
 * @constructor
 */
var Headers = function ( token_type, token) {
    this.authorization = token_type + Api.SPACE + token;
    this.content_type = Api.CONTE_TYPE;
};
Headers.prototype.getAuthorization = function () {
    return this.authorization;
};
Headers.prototype.getContentType = function () {
    return this.content_type;
};
/**
 * データ　コンストラクタ
 * @param email
 * @param password
 * @constructor
 */
var Data = function ( email, password) {
    this.grant_type = Api.GRANT_TYPE;
    this.email = email;
    this.password = password;
    this.scope = Api.SCOPE;
};
Data.prototype.getGrantType = function () {
    return this.grant_type;
};
Data.prototype.getEmail = function () {
    return this.email;
};
Data.prototype.getPassword = function () {
    return this.password;
};
Data.prototype.getScope = function () {
    return this.scope;
};
/**
 * トークン　コンストラクタ
 * @param staff_id
 * @param access_token
 * @param token_type
 * @param refresh_token
 * @param expire
 * @param jwt_id
 * @constructor
 */
var Token = function ( staff_id, access_token, token_type, refresh_token, expire, jwt_id) {
    this.staff_id = staff_id;
    this.access_token = access_token;
    this.token_type = token_type;
    this.refresh_token = refresh_token;
    this.expire = expire;
    this.scope = Api.SCOPE;
    this.jwt_id = jwt_id;
};
Token.prototype.getStaffId = function () {
    return this.staff_id;
};
Token.prototype.getAccessToken = function () {
    return this.access_token;
};
Token.prototype.getToken_type = function () {
    return this.token_type;
};
Token.prototype.getRefreshToken = function () {
    return this.refresh_token;
};
Token.prototype.getExpire = function () {
    return this.expire;
};
Token.prototype.getScope = function () {
    return this.scope;
};
Token.prototype.getJwtId = function () {
    return this.jwt_id;
};
Token.prototype.setAccessToken = function (access_token) {
    this.access_token = access_token;
};
