;
(function ($) {
	// スライドメニュー
	$.fn.slidemenu = function (target) {
		var t = $(target);
		var t_w = t.outerWidth(true);
		$(this).on('click', function () {
			if (t.hasClass('on')) {
				t.animate({right: (-1) * t_w}, 500);
			} else {
				t.animate({right: '0'}, 500);
			}
//			$(this).toggleClass('sidemenu-close');
			t.toggleClass('on');
		});
		return this;
	};
	// box スクロール
	$.fn.boxscroll = function (options) {
		var defaults = {
			btn_up: '',
			btn_down: '',
		};
		var opts = $.extend({}, defaults, options);
		var target = $(this);
		var target_h;
		var scrl = 0;
		var animate_flg = false;
		var to;
		var init = function () {
			scrl = 0;
			target_h = target.height();
		}
		var total_h = function () {
			return unit_height() * target.children().length;
		}
		var unit_height = function () {
			return $(target.children()[0]).outerHeight(true);
		}
		$(opts.btn_up).on('click', function () {
			if (!animate_flg) {
				var unit_h = unit_height();
				scrl = target.scrollTop();
				scrl_vol = scrl % unit_h;
				if (scrl_vol < (unit_h / 4))
					scrl_vol += unit_h;
				if ((scrl - scrl_vol) < 0)
					scrl = 0;
				else
					scrl -= scrl_vol;
				animate_flg = true;
				target.animate({scrollTop: scrl}, 300, function () {
					animate_flg = false;
				});
			}
		});
		$(opts.btn_down).on('click', function () {
			if (!animate_flg) {
				var unit_h = unit_height();
				scrl = target.scrollTop();
				scrl_vol = unit_h - (scrl % unit_h);
				if (scrl_vol < (unit_h / 4))
					scrl_vol += unit_h;
				if ((scrl + scrl_vol) > (total_h() - target_h))
					scrl = total_h() - target_h;
				else
					scrl += scrl_vol;
				animate_flg = true;
				target.animate({scrollTop: scrl}, 300, function () {
					animate_flg = false;
				});
			}
		});
		init();
		return this;
	};
	// box 横スクロール
	$.fn.boxscroll_side = function (options) {
		var defaults = {
			btn_prev: '',
			btn_next: '',
		};
		var opts = $.extend({}, defaults, options);
		var target = $(this);
		var target_w;
		var scrl = 0;
		var animate_flg = false;
		var to;
		var init = function () {
			scrl = 0;
			target_w = target.width();
		}
		var total_w = function () {
			return unit_width() * target.children().length;
		}
		var unit_width = function () {
			return $(target.children()[0]).outerWidth(true);
		}
		$(opts.btn_prev).on('click', function () {
			if (!animate_flg) {
				var unit_w = unit_width();
				scrl = target.scrollLeft();
				scrl_vol = scrl % unit_w;
				if (scrl_vol < (unit_w / 4))
					scrl_vol += unit_w;
				if ((scrl - scrl_vol) < 0)
					scrl = 0;
				else
					scrl -= scrl_vol;
				animate_flg = true;
				target.animate({scrollLeft: scrl}, 300, function () {
					animate_flg = false;
				});
			}
		});
		$(opts.btn_next).on('click', function () {
			if (!animate_flg) {
				var unit_w = unit_width();
				scrl = target.scrollLeft() + unit_w;
				animate_flg = true;
				target.animate({scrollLeft: scrl}, 300, function () {
					animate_flg = false;
				});
			}
		});
		init();
		return this;
	};	
	// リスト選択
	$.fn.selectlist = function (options) {
		var defaults = {
			type: 'checkbox', // 'checkbox' or 'radio'
			selected_class: 'selected',
			click_target: '',
			terget: '',
		};
		var opts = $.extend({}, defaults, options);
		var obj = $(this);
		var click = obj.find(opts.click_target)[0] ? obj.find(opts.click_target) : obj;
		var effect = $('<div class="effect-list-select"></div>').appendTo('body').hide();
		obj.on('click', opts.target, function (e) {
			effect.css(
					{
						top: $(this).offset().top + ((effect.height() - $(this).height()) / -2),
						left: $(this).offset().left + ((effect.width() - $(this).width()) / -2)
					}
			).fadeIn(100, function () {
				effect.fadeOut(100)
			});
			if (opts.type == 'radio') {
				obj.find(opts.target).removeClass(opts.selected_class);
				$(this).toggleClass(opts.selected_class).find("input[type=" + opts.type + "]").prop('checked', true);
			} else {
				var cb = $(this).toggleClass(opts.selected_class).find("input[type=" + opts.type + "]");
				cb.prop('checked', !cb.prop('checked'));
			}
			return false;
		});
		return this;
	};
	// おれんちくん helper
	$.fn.ore_helper = function (options) {
		var defaults = {
			duration: 60, // second
		};
		var opts = $.extend({}, defaults, options);
		var duration = opts.duration * 1000;
		var target = $(this);
		var timer_exec = function () {
			target.fadeIn(500);
		}
		var timer = setTimeout(timer_exec, duration);
		$(this).hide();
		$('body').on('keydown mousedown', function () {
//			target.fadeOut(500);
//			clearTimeout(timer);
//			timer = setTimeout(timer_exec, duration);
		});
		return this;
	}
	// スクロール移動
	$.fn.scrollTo = function (options) {
		var defaults = {
		};
		var opts = $.extend({}, defaults, options);
		if ($(this).data('scroll-to') && $($(this).data('scroll-to')).offset()) {
			$("html,body").animate({
				scrollTop: $($(this).data('scroll-to')).offset().top
			});
		}
		return this;
	}
	// BLOCKスクロール
	// ブロックごとにスクロール
	$.fn.block_scroll = function () {
		var list = {};
		var current_index = 0;
		var t = $(this);
		var on_scroll = false;
		var mousewheelevent = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
		var refresh = function () {
			t.each(function (i, elm) {
				list[i] = {
					'offset': $(this).offset().top,
					'height': $(this).outerHeight(true),
					'offset_bottom': $(this).offset().top + $(this).outerHeight(true)
				};
			});
			var wscroll = $(window).scrollTop();
			jQuery.each(list, function (key, val) {
				if (val.offset < wscroll) {
					current_index = Number(key);
				} else {
					return false;
				}
			});
		}
var start_pos = 0;
$(window).scroll(function(e){
			if (on_scroll) {
				return false;
			}
			on_scroll = true;
			var current_pos = $(this).scrollTop();
			var wh = window.innerHeight ? window.innerHeight : $(window).height();
			var wscroll = $(window).scrollTop();
			var scroll = wscroll;
			var current = list[current_index];
console.log('------------------------------');
console.log('start:::'+start_pos);
console.log('currn:::'+current_pos);
  if (current_pos > start_pos) {
    console.log('down');
				            // マウスホイールを下にスクロールしたときの処理を記載
				if (wh > (current.height) ||
						(wh < (current.height) && (wh + wscroll) > current.offset_bottom)) {
					var next = list[++current_index];
					if (typeof next !== "undefined") {
						on_scroll = true;
						$('body').stop().animate({
							scrollTop: next.offset + 'px'
						}, {
							complete: function () {
								    setTimeout(function () {
									on_scroll = false;
									start_pos = $(window).scrollTop();;
								    }, 100);
							}
						});
					} else {
						current_index = current_index - 1;
									on_scroll = false;
					}
				} else {
//					$('body').stop().animate({
//						scrollTop: (wscroll + 100) + 'px'
//					}, {
//						duration: 10,
//						complete: function () {
//							    setTimeout(function () {
//								on_scroll = false;
//							    }, 1);
//						}
//					});
									on_scroll = false;
					start_pos = current_pos;
				}
  } else {
    console.log('up');
				// マウスホイールを上にスクロールしたときの処理を記載
				if (wh > (current.height) ||
						(wh < (current.height) && (wscroll) <= current.offset)) {
					var prev = list[--current_index];
					if (typeof prev !== "undefined") {
						on_scroll = true;
						$('body').stop().animate({
							scrollTop: prev.offset + 'px'
						}, {
							complete: function () {
								    setTimeout(function () {
									on_scroll = false;
									start_pos = $(window).scrollTop();;
								    }, 100);
							}
						});
					} else {
						current_index = 0;
									on_scroll = false;
					}
				} else {
//					$('body').stop().animate({
//						scrollTop: (wscroll - 100) + 'px'
//					}, {
//						duration: 10,
//						complete: function () {
//							setTimeout(function () {
//								on_scroll = false;
//							}, 1);
//						}
//					});
									on_scroll = false;
					start_pos = current_pos;

				}
			}
});
/*
		$(document).on(mousewheelevent, function (e) {
			if (on_scroll) {
				return false;
			}
			e.preventDefault();
			var delta = e.originalEvent.deltaY ? -(e.originalEvent.deltaY) : e.originalEvent.wheelDelta ? e.originalEvent.wheelDelta : -(e.originalEvent.detail);
			var wh = window.innerHeight ? window.innerHeight : $(window).height();
			var wscroll = $(window).scrollTop();
			var scroll = wscroll;
			var current = list[current_index];
			if (delta < 0) {
				            // マウスホイールを下にスクロールしたときの処理を記載
				if (wh > (current.height) ||
						(wh < (current.height) && (wh + wscroll) > current.offset_bottom)) {
					var next = list[++current_index];
					if (typeof next !== "undefined") {
						on_scroll = true;
						$('body').stop().animate({
							scrollTop: next.offset + 'px'
						}, {
							complete: function () {
								    setTimeout(function () {
									on_scroll = false;
								    }, 100);
							}
						});
					} else {
						current_index = current_index - 1;
					}
				} else {
					$('body').stop().animate({
						scrollTop: (wscroll + 100) + 'px'
					}, {
						duration: 10,
						complete: function () {
							    setTimeout(function () {
								on_scroll = false;
							    }, 1);
						}
					});
				}

			} else {
				// マウスホイールを上にスクロールしたときの処理を記載
				if (wh > (current.height) ||
						(wh < (current.height) && (wscroll) <= current.offset)) {
					var prev = list[--current_index];
					if (typeof prev !== "undefined") {
						on_scroll = true;
						$('body').stop().animate({
							scrollTop: prev.offset + 'px'
						}, {
							complete: function () {
								    setTimeout(function () {
									on_scroll = false;
								    }, 100);
							}
						});
					} else {
						current_index = 0;
					}
				} else {
					$('body').stop().animate({
						scrollTop: (wscroll - 100) + 'px'
					}, {
						duration: 10,
						complete: function () {
							setTimeout(function () {
								on_scroll = false;
							}, 1);
						}
					});

				}
			}
		});*/
//		$(window).on('scroll', function (e) {
//			if (!on_scroll) {
//				var wscroll = $(window).scrollTop();
//				jQuery.each(list, function (key, val) {
//					if (val.offset < wscroll) {
//						current_index = Number(key);
//					} else {
//						return false;
//					}
//				});
//			}
//			return false;
//		});
		$(window).on('resize', function () {
			refresh();
		});
		refresh();

	}
})(jQuery);


