/* ラフプラン等の間取り表示イベント等制御
 */
var clickRoom = {}; // namespace

(function($) {

    /* グローバル変数 */
    var API;
 
    /* jsonデータから読み込み
     * {string} str jsonデータ
     * return なし
     */
    clickRoom.loadData = function(str) {
        dragRoom.DRAG.setFocusIndex(null);
 
        convertRoom.importData(str);
        dragRoom.DRAG.fitCanvas();
    };

    /* Canvasでmousedown時に行われる処理
     * {event} evt イベントオブジェクト
     * return このメソッドで処理を行わない場合false
     */
    clickRoom.cvmsDown = function(evt) {
        if (evt.button !== 0) return false;
 
        dragRoom.DRAG.windowResize();

        // ポインタ座標をCanvas座標へ変換
        var cx = evt.pageX - dragRoom.DRAG.cvpos.x + document.getElementById("cvdiv2").scrollLeft + dragRoom.DRAG.startGridX*dragRoom.DRAG.grSep;
        var cy = evt.pageY - dragRoom.DRAG.cvpos.y + document.getElementById("cvdiv2").scrollTop + dragRoom.DRAG.startGridY*dragRoom.DRAG.grSep;

        // ポインタ座標内にアイテムがあるかチェック
 /*
        var openingIdx = dragRoom.DRAG.checkOpeningPx(cx,cy);
        if (openingIdx != null) {
            //alert ("index:" + itemIdx + " click:" + dragRoom.DRAG.itemAr[itemIdx].name);
            dragRoom.DRAG.focusIndex = openingIdx;
            dragRoom.DRAG.focusOpening = true;
            dragRoom.DRAG.draw();
//            roomClick(dragRoom.DRAG.floorNo, openingIdx);
 
            return false;
        }
 */
 
        var itemIdx = dragRoom.DRAG.checkItemPxNotWall(cx, cy);
        if (itemIdx !== null) {
            // 部屋カテゴリ以外のものをクリックした場合は処理させない
            if (dragRoom.DRAG.itemAr[itemIdx] === null || dragRoom.DRAG.itemAr[itemIdx].type >= 90)
                return false;
 
            //alert ("index:" + itemIdx + " click:" + dragRoom.DRAG.itemAr[itemIdx].name);
            dragRoom.DRAG.setFocusIndex(itemIdx);
            dragRoom.DRAG.focusOpening = false;
            dragRoom.DRAG.draw();
            roomClick(dragRoom.DRAG.floorNo, itemIdx);
        }
        return false;
    };

    /* body onload時の処理 */
    $(window).load(function() {
        // canvasのDOM elementとcontext取得
        var canvas = document.getElementById('main-canvas');
        if ( ! canvas || ! canvas.getContext ) { return false; }

        var ctx = canvas.getContext("2d");
        ctx.lineWidth = 1;
        ctx.globalAlpha = 0.7;
        ctx.scale(2,2);
        ctx.globalCompositeOperation = "source-over";

        var $cvdiv = $('#cvdiv2'); // main Canvasのdiv
        if ($cvdiv.size() === 0) return false;
                   
        // canvasサイズ設定
        canvas.width = $cvdiv.width() * 2;
        canvas.height = $cvdiv.height() * 2;
                   
        // 透過canvasサイズ設定
        var canvas2 = document.getElementById('transparency-canvas');
        var ctx2 = canvas2.getContext("2d");
        ctx2.lineWidth = 1;
        ctx2.globalAlpha = 0.7;
        ctx2.scale(2,2);
        ctx2.globalCompositeOperation = "source-over";
                   
        // canvasサイズ設定
        canvas2.width = $cvdiv.width() * 2;
        canvas2.height = $cvdiv.height() * 2;
                   
        // 画面表示
        dragRoom.DRAG = new floorPlan.canv(ctx, canvas.width, canvas.height, 'cvdiv2');
                   
        // 描画初期値修正
        dragRoom.DRAG.grSplit = 2;
        dragRoom.DRAG.grSep = 30;
        dragRoom.DRAG.displayMode = 1;
                   
        dragRoom.DRAG.init();
        dragRoom.DRAG.draw();

        // canvasにイベント付与
        $cvdiv.mousedown(clickRoom.cvmsDown);
        clickRoom.loadData("");
                
        dragRoom.DRAG.draw();

        clickRoom.gamestart = true;

    });


})(jQuery);
