function ore_make_confirm() {
	var html1 = '\
		<div class="modal fade modal-confirm" id="modal-confirm-yn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">\n\
			<div class="modal-dialog" role="document">\n\
				<div class="modal-content">\n\
					<div class="modal-header">\n\
						<h4 class="modal-title">〇〇しますか？</h4>\n\
					</div>\n\
					<div class="modal-body">\n\
						<div class="btn-area">\n\
							<button class="btn btn-green-light btn-check"><span><i class="icon icon-check-large"></i>はい</span></button>\n\
							<button class="btn btn-green-light btn-no"><span><i class="icon icon-x-large"></i>いいえ</span></button>\n\
						</div>\n\
					</div>\n\
				</div>\n\
			</div>\n\
		</div>\n\
		<div class="modal fade modal-confirm" id="modal-confirm-ok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">\n\
			<div class="modal-dialog" role="document">\n\
				<div class="modal-content">\n\
					<div class="modal-header">\n\
						<h4 class="modal-title">登録しました。</h4>\n\
					</div>\n\
					<div class="modal-body">\n\
						<div class="btn-area">\n\
							<button class="btn btn-green-light btn-check"><span><i class="icon icon-check-large"></i>OK</span></button>\n\
						</div>\n\
					</div>\n\
				</div>\n\
			</div>\n\
		</div>\n\
		<div class="modal fade modal-confirm madori" id="modal-confirm-madori-request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">\n\
			<div class="modal-dialog" role="document">\n\
				<div class="modal-content">\n\
					<div class="modal-header">\n\
						<h4 class="modal-title">間取り作成を依頼しますか？</h4>\n\
					</div>\n\
					<div class="modal-body">\n\
						<div class="modal-body-text">依頼後ラフプラン一覧画面に戻ります。</div>\n\
						<div class="btn-area2">\n\
							<button class="btn btn-pink03 btn-check-now" type="button">間取り作成を依頼する</button>\n\
							<button class="btn btn-close02" type="button" id="modal-confirm-madori-request-close"><i class="icon icon-close02"></i>依頼しない</button>\n\
						</div>\n\
					</div>\n\
				</div>\n\
			</div>\n\
		</div>\n\
		<div class="modal fade modal-confirm madori complete" id="modal-confirm-madori-complete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">\n\
			<div class="modal-dialog" role="document">\n\
				<div class="modal-content">\n\
					<div class="modal-header">\n\
						<h4 class="modal-title">間取り作成を依頼しました。</h4>\n\
					</div>\n\
					<div class="modal-body">\n\
						<div class="modal-body-text">弊社よりおススメの間取りをOREにて、ご提案させていただきますのでしばらくお待ちください。</div>\n\
						<div class="btn-area2">\n\
							<button class="btn btn-close02" type="button" id="modal-confirm-madori-complete-close"><i class="icon icon-close02"></i>閉じる</button>\n\
						</div>\n\
					</div>\n\
				</div>\n\
			</div>\n\
		</div>\n\
		<div class="modal fade modal-confirm madori" id="modal-confirm-waku-error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">\n\
			<div class="modal-dialog" role="document">\n\
				<div class="modal-content">\n\
					<div class="modal-header">\n\
						<h4 class="modal-title"></h4>\n\
					</div>\n\
					<div class="modal-body">\n\
						<div class="modal-body-text">物件掲載枠の残りが0件となりました。掲載するのには、別途枠を購入していただく必要があります。<br>一覧画面に戻り、「掲載枠追加購入」より掲載枠を購入ください。</div>\n\
						<div class="btn-area2">\n\
							<button class="btn btn-pink03 btn-check-now" type="button">一覧に戻る</button>\n\
							<button class="btn btn-close02" type="button" id="modal-confirm-madori-request-close"><i class="icon icon-close02"></i>閉じる</button>\n\
						</div>\n\
					</div>\n\
				</div>\n\
			</div>\n\
		</div>';
	

	$('.modal-confirm').html(html1);
	
	$('#modal-confirm-yn .btn-no').click(function () {
		$('#modal-confirm-yn').modal('hide');
	});

	// 間取り依頼
	$('#modal-confirm-madori-request .btn-check-now').click(function () {
		$('#modal-confirm-madori-request').modal('hide');
	});
	$('#modal-confirm-madori-request-close').click(function () {
		$('#modal-confirm-madori-request').modal('hide');
	});

	// 間取り依頼完了
	$('#modal-confirm-madori-complete-close').click(function () {
		$('#modal-confirm-madori-complete').modal('hide');
	});

	// 枠エラー
	$('#modal-confirm-waku-error button').click(function() {
		$('#modal-confirm-waku-error').modal('hide');
	});
}
ore_make_confirm();
