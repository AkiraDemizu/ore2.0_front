class Api {
    /**
     * apiホスト
     * @return {string}
     * @constructor
     */
    static get HOST() {
        return 'https://oreapi.orenoie.co.jp';
    }
    static get VERSION () {
        return '/v2'
    }
    /**
     *
     * @return {string}
     * @constructor
     */
    static get AUTHORIZATION() {
        return '/authorization';
    }
    /**
     * メソッド
     * @return {string}
     * @constructor
     */
    static get POST() {
        return 'POST';
    }
    static get GET() {
        return 'GET';
    }
    static get PUT () {
        return 'PUT';
    }
    static get DELETE () {
        return 'DELETE';
    }

    static get DATA_TYPE() {
        return 'json';
    }
    static get GRANT_TYPE() {
        return "password";
    }
    static get SCOPE () {
        return "oreapi";
    }
    static get TIMEOUT () {
        return 20000;
    }
    static get CONTE_TYPE () {
        return "application/json";
    }
    static get BASIC () {
        return "Basic";
    }
    static get BEARER () {
        return "Bearer";
    }
    static get SPACE () {
        return " ";
    }
    static get OREUSER () {
        return btoa("ORE/Email:ORE/Password");
    }
    /**
     * ajax 非同期通信
     * @return {boolean}
     * @constructor
     */
    static get ASYNC () {
        return true;
    }
    /**
     * リクエストをキャッシュさせない
     * @return {boolean}
     * @constructor
     */
    static get CASHE () {
        return false;
    }
}
