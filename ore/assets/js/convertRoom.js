/* 間取りデータ変換用
 */
var convertRoom = {}; // namespace

(function($) {

    /* グローバル変数 */
    // 部屋情報
    convertRoom.DATA = {
        itemAr: null, // 部屋配列
        openingAr: null,  // 開口部配列
        sinkAr: null,  // シンク配列
        compassNo: 0, // 方角
        grSepMm: 910 // 1グリッド辺りのmm数
    };

    // マッチング用のデータの箱
    convertRoom.MATCH_JSON_DATA = {
        article_info : null
    }

    // マッチング用データ
    convertRoom.matchingJsonData = function(storied, articleId) {
        this.storied = storied; // 何階建てか
        this.id = articleId; // 物件ID
        this.floor_plan_info = null; // (convertRoom.matchingFloorPlanInfoの一覧)
    }
 
    convertRoom.matchingFloorPlanInfo = function(floor, planId) {
        this.unit = "mm"; // 単位
        this.floor = floor; // 何階か
        this.position = {x : 1,  // グリッドの起点から一階間取りの矩形の左端までの座標。これを間取り基準点とする
                         y : 1}; // グリッドの起点から一階間取りの矩形の左端までの座標。これを間取り基準点とする
        this.id = planId; // 間取りID
        this.room_count = 14; // 部屋数
        this.room = []; // 部屋一覧（convertRoom.matchingRoomの一覧）
        this.grid_size = 910; // １のグリッドサイズ
//        this.grid_count = 0; // グリッドの数 階における床　グリッドを12分割したものを1として持つ
        this.brace_wall = []; // 筋交い情報
        this.piller_array = []; // 抜けない柱情報
        this.sink = []; // シンク情報
        this.garege = []; // ガレージ情報　ビルトインはroomに属する
    }
 
    convertRoom.matchingRoom = function(roomId) {
        this.classification = ""; //種類
        this.sub_classification = ""; // 小分類
        this.id = roomId; //部屋のID
        this.position = {x : 1,  // 間取り基準点からの絶対座標　これがroomの基準点
                         y : 1}; // 間取り基準点からの絶対座標　これがroomの基準点
        this.size = {width : 0,  // 矩形の幅
                     height : 0};// 矩形の高さ
        this.center_point = {x : 1,  // roomの基準点からの座標
                             y : 1}; // roomの基準点からの座標
        this.area = 0; // 面積
        this.wall_array = []; // (convertRoom.matchingWallの一覧）
        this.adjacent_room_array = []; // 接している部屋のID
    }

    convertRoom.matchingWall = function(wallId) {
        this.classification = ""; // 壁の種類　share_wallかouter_wall
        this.id = wallId; // 壁のID　部屋ごとにユニーク
        this.wall_info = {wall_flag : -1, // 真壁か大壁かのフラグ
                          inner : "",     // 内壁情報
                          outer : ""};    // 外壁情報
        this.start_point = {x : 1,  // roomの基準点からの座標
                            y : 1}; // roomの基準点からの座標
        this.end_point = {x : 1,  // roomの基準点からの座標
                          y : 1}; // roomの基準点からの座標
        this.wall_element_array = []; // MATCH_DATA_WALL_ELEMENTの一覧
    }

    convertRoom.matchingWallEllement = function(elementId) {
        this.classification = ""; // 壁要素の種類　door or window
        this.sub_classification = ""; // 引き違い戸、片開き戸など
        this.id = elementId; // 要素のID
        this.start_point = {x : 1,  // roomの基準点からの座標
                            y : 1}; // roomの基準点からの座標
        this.end_point = {x : 1,  // roomの基準点からの座標
                          y : 1}; // roomの基準点からの座標
    }

    /* 現在編集している間取りデータをjson形式で出力する
     * {callback} callback コールバックメソッド
     * return 現在編集している間取りデータ(json形式）
     */
    convertRoom.exportData = function(callback) {
        try {
            convertRoom.DATA.itemAr = JSON.parse(JSON.stringify(dragRoom.DRAG.itemAr));
            convertRoom.DATA.openingAr = JSON.parse(JSON.stringify(dragRoom.DRAG.openingAr));
            convertRoom.DATA.sinkAr = JSON.parse(JSON.stringify(dragRoom.DRAG.sinkAr));
            convertRoom.DATA.grSepMm = dragRoom.DRAG.grSepMm;
            convertRoom.DATA.compassNo = dragRoom.DRAG.compassNo;
        }
        catch (e) {
            window.console.log(e);
        }

        var jstr = JSON.stringify( convertRoom.DATA );
//        window.console.log(jstr);
        callback(jstr);
        if (jstr) localStorage.setItem( "roomData" , jstr );

        convertRoom.DATA.itemAr = null;
        convertRoom.DATA.openingAr = null;
        convertRoom.DATA.sinkAr = null;

        return jstr;
    }

    /* 作図ソフトにデータをimportする
     * {string} str 作図文字列データ
     * return なし
     */
    convertRoom.importData = function(str) {
        if(str == '' || str == null) {
            str = '{"itemAr":[],"openingAr":[],"sinkAr":[],"grSepMm":910}';
        }
        convertRoom.DATA = JSON.parse(str);
        dragRoom.DRAG.itemAr = convertRoom.DATA.itemAr;
        dragRoom.DRAG.openingAr = convertRoom.DATA.openingAr;
        dragRoom.DRAG.sinkAr = convertRoom.DATA.sinkAr;
        try {
            if (convertRoom.DATA.grSepMm != null) dragRoom.DRAG.grSepMm = convertRoom.DATA.grSepMm;
        }
        catch (e) {
            dragRoom.DRAG.grSepMm = 910;
        }

        try {
            if (convertRoom.DATA.compassNo != null) {
                dragRoom.DRAG.compassNo = convertRoom.DATA.compassNo;
            }
            else {
                if ($('select[name=compass_orientation] option:selected').is('*')) {
                    var val = parseInt($('select[name=compass_orientation] option:selected').val());
                    dragRoom.DRAG.compassNo = val;
                }
                else {
                    dragRoom.DRAG.compassNo = 0;
                }
            }
        }
        catch (e) {
            if ($('select[name=compass_orientation] option:selected').is('*')) {
                var val = parseInt($('select[name=compass_orientation] option:selected').val());
                dragRoom.DRAG.compassNo = val;
            }
            else {
                dragRoom.DRAG.compassNo = 0;
            }
        }

        $('select[name=grid-size]').val(dragRoom.DRAG.grSepMm).change();
        $('select[name=compass_orientation]').val(dragRoom.DRAG.compassNo).change();

        convertRoom.DATA.itemAr = null;
        convertRoom.DATA.openingAr = null;
        convertRoom.DATA.sinkAr = null;
 
        convertRoom.convertRoomType();// typeが正常になるよう処理（後に除去予定）
        convertRoom.convertRoomName();// nameが正常になるよう処理（調整後除去予定）

        dragRoom.DRAG.setFocus(null);
        dragRoom.DRAG.draw();
    }
 
    /* 文字列から作図用データを出力する
     * {string} str 作図文字列データ
     * return 作図用データ
     */
    convertRoom.convertStrToData = function(str) {
        if(str == '') {
            str = '{"itemAr":[],"openingAr":[],"sinkAr":[],"compassNo":0,"grSepMm":910}';
        }
        return JSON.parse(str);
    }
 
    /* 文字列から作図用データを出力する
     * {string} str 作図データ
     * return 作図文字列データ
     */
    convertRoom.convertDataToStr = function(data) {
        try {
            convertRoom.DATA = data;
        }
        catch (e) {
            window.console.log(e);
        }
 
        var jstr = JSON.stringify( convertRoom.DATA );

        convertRoom.DATA.itemAr = null;
        convertRoom.DATA.openingAr = null;
        convertRoom.DATA.sinkAr = null;
 
        return jstr;
    }
 
    /* 作図データ（json）の方角を編集する
     * {string} str  作図データ
     * {int} compassNo 方角番号
     * return 方角を編集した作図文字列データ
     */
    convertRoom.changeCompassNoJson = function(str, compassNo) {
        if(str == '' || str == null) {// データが空の場合、そのまま返す
            return str;
        }
 
        convertRoom.DATA = JSON.parse(str);
        convertRoom.DATA.compassNo = compassNo;
        var jstr = JSON.stringify( convertRoom.DATA );
 
        convertRoom.DATA.itemAr = null;
        convertRoom.DATA.openingAr = null;
        convertRoom.DATA.sinkAr = null;
 
        return jstr;
    }
 
    /* （テスト用）各部屋のtypeを正常に変換するためのメソッド
     *
     */
    convertRoom.convertRoomType = function() {
        for(var i = 0; i < dragRoom.DRAG.itemAr.length; i++) {
            if (dragRoom.DRAG.itemAr[i] == null) continue;
 
            if (dragRoom.DRAG.itemAr[i].name === "リビング") dragRoom.DRAG.itemAr[i].type = 14;
            if (dragRoom.DRAG.itemAr[i].name === "ダイニング") dragRoom.DRAG.itemAr[i].type = 15;
            if (dragRoom.DRAG.itemAr[i].name === "キッチン") dragRoom.DRAG.itemAr[i].type = 16;
            if (dragRoom.DRAG.itemAr[i].name === "DK") dragRoom.DRAG.itemAr[i].type = 17;
            if (dragRoom.DRAG.itemAr[i].name === "LDK") dragRoom.DRAG.itemAr[i].type = 18;
            if (dragRoom.DRAG.itemAr[i].name === "洋室") dragRoom.DRAG.itemAr[i].type = 6;
            if (dragRoom.DRAG.itemAr[i].name === "和室") dragRoom.DRAG.itemAr[i].type = 5;
            if (dragRoom.DRAG.itemAr[i].name === "床の間") dragRoom.DRAG.itemAr[i].type = 7;
            if (dragRoom.DRAG.itemAr[i].name === "玄関") dragRoom.DRAG.itemAr[i].type = 11;
            if (dragRoom.DRAG.itemAr[i].name === "廊下") dragRoom.DRAG.itemAr[i].type = 12;
            if (dragRoom.DRAG.itemAr[i].name === "階段") dragRoom.DRAG.itemAr[i].type = 13;
            if (dragRoom.DRAG.itemAr[i].name === "クローゼット") dragRoom.DRAG.itemAr[i].type = 8;
            if (dragRoom.DRAG.itemAr[i].name === "押入") dragRoom.DRAG.itemAr[i].type = 9;
            if (dragRoom.DRAG.itemAr[i].name === "物入") dragRoom.DRAG.itemAr[i].type = 10;
            if (dragRoom.DRAG.itemAr[i].name === "洗面脱衣所") dragRoom.DRAG.itemAr[i].type = 1;
            if (dragRoom.DRAG.itemAr[i].name === "トイレ") dragRoom.DRAG.itemAr[i].type = 3;
            if (dragRoom.DRAG.itemAr[i].name === "浴室") dragRoom.DRAG.itemAr[i].type = 2;
            if (dragRoom.DRAG.itemAr[i].name === "ユニットバス") dragRoom.DRAG.itemAr[i].type = 4;
            if (dragRoom.DRAG.itemAr[i].name === "ベランダ") dragRoom.DRAG.itemAr[i].type = 22;
            if (dragRoom.DRAG.itemAr[i].name === "車庫") dragRoom.DRAG.itemAr[i].type = 23;
            if (dragRoom.DRAG.itemAr[i].name === "室内ガレージ") dragRoom.DRAG.itemAr[i].type = 24;
        }
    }
 
    /* （テスト用）各部屋の部屋名を正常に変換するためのメソッド
     *
     */
    convertRoom.convertRoomName = function() {
        for(var i = 0; i < dragRoom.DRAG.itemAr.length; i++) {
            if (dragRoom.DRAG.itemAr[i] == null) continue;
 
            if (dragRoom.DRAG.itemAr[i].type == 14) dragRoom.DRAG.itemAr[i].name = "リビング";
            if (dragRoom.DRAG.itemAr[i].type == 15) dragRoom.DRAG.itemAr[i].name = "ダイニング";
            if (dragRoom.DRAG.itemAr[i].type == 16) dragRoom.DRAG.itemAr[i].name = "キッチン";
            if (dragRoom.DRAG.itemAr[i].type == 17) dragRoom.DRAG.itemAr[i].name = "DK";
            if (dragRoom.DRAG.itemAr[i].type == 18) dragRoom.DRAG.itemAr[i].name = "LDK";
            if (dragRoom.DRAG.itemAr[i].type == 6) dragRoom.DRAG.itemAr[i].name = "洋室";
            if (dragRoom.DRAG.itemAr[i].type == 5) dragRoom.DRAG.itemAr[i].name = "和室";
            if (dragRoom.DRAG.itemAr[i].type == 7) dragRoom.DRAG.itemAr[i].name = "床の間";
            if (dragRoom.DRAG.itemAr[i].type == 11) dragRoom.DRAG.itemAr[i].name = "玄関";
            if (dragRoom.DRAG.itemAr[i].type == 12) dragRoom.DRAG.itemAr[i].name = "廊下";
            if (dragRoom.DRAG.itemAr[i].type == 13) dragRoom.DRAG.itemAr[i].name = "階段";
            if (dragRoom.DRAG.itemAr[i].type == 8) dragRoom.DRAG.itemAr[i].name = "クローゼット";
            if (dragRoom.DRAG.itemAr[i].type == 9) dragRoom.DRAG.itemAr[i].name = "押入";
            if (dragRoom.DRAG.itemAr[i].type == 10) dragRoom.DRAG.itemAr[i].name = "物入";
            if (dragRoom.DRAG.itemAr[i].type == 1) dragRoom.DRAG.itemAr[i].name = "洗面脱衣所";
            if (dragRoom.DRAG.itemAr[i].type == 3) dragRoom.DRAG.itemAr[i].name = "洋式トイレ";
            if (dragRoom.DRAG.itemAr[i].type == 20) dragRoom.DRAG.itemAr[i].name = "和式トイレ";
            if (dragRoom.DRAG.itemAr[i].type == 2) dragRoom.DRAG.itemAr[i].name = "浴室";
            if (dragRoom.DRAG.itemAr[i].type == 4) dragRoom.DRAG.itemAr[i].name = "ユニットバス";
            if (dragRoom.DRAG.itemAr[i].type == 22) dragRoom.DRAG.itemAr[i].name = "ベランダ";
            if (dragRoom.DRAG.itemAr[i].type == 23) dragRoom.DRAG.itemAr[i].name = "車庫";
            if (dragRoom.DRAG.itemAr[i].type == 24) dragRoom.DRAG.itemAr[i].name = "室内ガレージ";
 
            dragRoom.DRAG.itemAr[i].unit = "帖";
        }
    }
 
    /* 部屋のタイプから部屋の種類名を返す
     * {int} type 部屋タイプ
     * return 部屋の種類名
     */
    convertRoom.checkRoomClassification = function(type) {
        switch (type){
            case 14:// リビング
                return "Living";
            case 15:// ダイニング
                return "Dining";
            case 16:// キッチン
                return "Kittchen";
            case 17:// DK
                return "DK";
            case 18:// LDK
                return "LDK";
            case 6:// 洋室
                return "Western style room";
            case 5:// 和室
                return "Japanese style room";
            case 7:// 床の間
                return "Alcove";
            case 11:// 玄関
                return "Entrance";
            case 12:// 廊下
                return "Corridor";
            case 13:// 階段
                return "Stairs";
            case 8:// クローゼット
                return "Closet";
            case 9:// 押入
                return "Armoire";
            case 10:// 物入
                return "Container";
            case 1:// 洗面脱衣所
                return "Washroom";
            case 3:// 洋式トイレ
                return "Toilet";
            case 20:// 和式トイレ
                return "Japanese Style Toilet";
            case 2:// 浴室
                return "Bathroom";
            case 4:// ユニットバス
                return "UnitBath";
            case 22:// ベランダ
                return "Veranda";
            case 23:// 車庫
                return "Garage";
            case 24://　室内ガレージ
                return "Builtin garage";
        }
 
        return "";
    }
 
    /* 部屋のタイプから部屋の種類名を返す（旧バージョン）
     * {int} type 部屋タイプ
     * return 部屋の種類名
     */
    convertRoom.checkRoomClassification2 = function(type) {
        switch (type){
            case 14:// リビング
                return "living";
            case 15:// ダイニング
                return "dining";
            case 16:// キッチン
                return "kitchen";
            case 17:// DK
                return "DK";
            case 18:// LDK
                return "LDK";
            case 6:// 洋室
                return "western_style_room";
            case 5:// 和室
                return "japanese_style_room";
            case 7:// 床の間
                return "alcove";
            case 11:// 玄関
                return "entrance";
            case 12:// 廊下
                return "corridor";
            case 13:// 階段
                return "stairs";
            case 8:// クローゼット
            case 9:// 押入
            case 10:// 物入
                return "storage";
            case 1:// 洗面脱衣所
                return "wash_room";
            case 3:// 洋式トイレ
            case 20:// 和式トイレ
                return "toilet";
            case 2:// 浴室
            case 4:// ユニットバス
                return "bath_room";
 
            case 22:// ベランダ
                return "veranda";
            case 23:// 車庫
                return "garage";
            case 24://　室内ガレージ
                return "builtin_garage";
        }

        return "";
    }
 
    /* 部屋のタイプから部屋の種類名を返す（旧バージョン）
     * {int} type 部屋タイプ
     * return 部屋の種類名
     */
    convertRoom.checkRoomSubClassification = function(type) {
        switch (type){
            case 14:// リビング
                return "リビング";
            case 15:// ダイニング
                return "ダイニング";
            case 16:// キッチン
                return "キッチン";
            case 17:// DK
                return "DK";
            case 18:// LDK
                return "LDK";
            case 6:// 洋室
                return "洋室";
            case 5:// 和室
                return "和室";
            case 7:// 床の間
                return "床の間";
            case 11:// 玄関
                return "玄関";
            case 12:// 廊下
                return "廊下";
            case 13:// 階段
                return "階段";
            case 8:// クローゼット
                return "クローゼット";
            case 9:// 押入
                return "押入";
            case 10:// 物入
                return "物入";
            case 1:// 洗面脱衣所
                return "洗面脱衣所";
            case 3:// 洋式トイレ
            case 20:// 和式トイレ
                return "トイレ";
            case 2:// 浴室
            case 4:// ユニットバス
                return "浴室";
 
            case 22:// ベランダ
                return "ベランダ";
            case 23:// 車庫
                return "車庫";
            case 24://　室内ガレージ
                return "室内ガレージ";
        }
 
        return dragRoom;
    }
 
    /* 開口部のタイプから開口部の名前を返す
     * {int} type 開口部タイプ
     * return 開口部名
     */
    convertRoom.checkOpeningName = function(type) {
        switch (type){
            case 1:// 腰掛窓
                return "腰掛窓";
            case 3:// 片開き窓
                return "片開き窓";
            case 4:// 出窓
                return "出窓";
            case 5:// Fix窓
                return "Fix窓";
            case 19:// 掃き出し窓
                return "掃き出し窓";
            case 21:// 片開き戸
                return "片開き戸";
            case 22:// 両開き戸
                return "両開き戸";
            case 24:// 引き違い戸
                return "引き違い戸";
            case 25:// 引き違い戸(3枚)
                return "引き違い戸(3枚)";
            case 26:// 引き違い戸(4枚)
                return "引き違い戸(4枚)";
            case 27:// 折れ戸
                return "折れ戸";
            case 28:// 親子扉
                return "親子扉";
            case 29:// 片引き戸
                return "片引き戸";
            case 99:// 壁なし
                return "壁なし";
            case 98:// 階段入口
                return "階段入口";
            case 20:// 柱
                return "柱";
        }
 
        return "";
    }
 
    /* 開口部のタイプから開口部のsub_classificationを返す
     * {int} type 開口部タイプ
     * return 開口部のsub_classification
     */
    convertRoom.checkOpeningSubClassification = function(type) {
        switch (type){
            case 1:// 腰掛窓
            case 3:// 片開き窓
            case 4:// 出窓
            case 5:// Fix窓
            case 19:// 掃き出し窓
                return "窓";
            case 21:// 片開き戸
                return "ドア";
            case 22:// 両開き戸
            case 28:// 親子扉
                return "開き戸";
            case 24:// 引き違い戸
            case 25:// 引き違い戸(3枚)
            case 26:// 引き違い戸(4枚)
            case 27:// 折れ戸
            case 29:// 片引き戸
                return "引き違い戸";
            case 99:// 壁なし
            case 98:// 階段入口
                return "開口";
            case 20:// 柱
            return "柱";
        }
 
        return "";
    }
 
    /* マッチング用データ（JSON形式）を返す
     * {return} マッチング用データ（JSON形式）の文字列
     */
    convertRoom.exportJSONData = function() {
        // 全データ四捨五入
        dragRoom.DRAG.roundAllData();
 
        // 間取り情報設定
        convertRoom.MATCH_JSON_DATA.article_info = new convertRoom.matchingJsonData(-1,-1);

        // 1階の情報
        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info = new convertRoom.matchingFloorPlanInfo(1,-1);
 
        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room_count = dragRoom.DRAG.roomCount();// 部屋数
        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.unit = "mm";// 単位
        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.grid_size = dragRoom.DRAG.grSepMm;// １のグリッドサイズ
//        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.grid_count = dragRoom.DRAG.articleAreaCount();// クリッドの数 階における床　グリッドを12分割したものを1として持つ

        var positionValue = dragRoom.DRAG.checkCanvasPosition();
        /// グリッドの起点から一階間取りの矩形の左端までの座標。これを間取り基準点とする
        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.position.x = positionValue.minX;
        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.position.y = positionValue.minY;

        // 部屋情報設定
        for (var roomId = 0; roomId < dragRoom.DRAG.itemAr.length; roomId++) {
            if (dragRoom.DRAG.itemAr[roomId] == null) continue;
            var roomData = new convertRoom.matchingRoom(roomId);
 
            roomData.classification = convertRoom.checkRoomClassification2(dragRoom.DRAG.itemAr[roomId].type);　// 種類
            roomData.sub_classification = convertRoom.checkRoomSubClassification(dragRoom.DRAG.itemAr[roomId].type); // 小分類
 
            roomData.adjacent_room_array = dragRoom.DRAG.checkTouchRoom(roomId);
 
            // 絶対座標
            roomData.position.x = dragRoom.DRAG.itemAr[roomId].x;
            roomData.position.y = dragRoom.DRAG.itemAr[roomId].y;
 
            roomData.size.width = dragRoom.DRAG.itemAr[roomId].width;// 矩形の幅
            roomData.size.height = dragRoom.DRAG.itemAr[roomId].height;// 矩形の高さ
 
            // roomの基準点からの座標
            roomData.center_point.x = dragRoom.DRAG.itemAr[roomId].x + (dragRoom.DRAG.itemAr[roomId].width)/2;
            roomData.center_point.y = dragRoom.DRAG.itemAr[roomId].y + (dragRoom.DRAG.itemAr[roomId].height)/2;

            roomData.area = dragRoom.DRAG.itemAr[roomId].count * 2; // 面積
 
            // 壁情報設定
            var wallInfo = dragRoom.DRAG.calWallInfo(roomId);
            for (var wallId = 0; wallId < wallInfo.length; wallId++) {
                var x = wallInfo[wallId].startPointX;
                var y = wallInfo[wallId].startPointY;
                var width = wallInfo[wallId].endPointX-wallInfo[wallId].startPointX;
                var height = wallInfo[wallId].endPointY-wallInfo[wallId].startPointY;
                if (wallInfo[wallId].startPointX > wallInfo[wallId].endPointX) {
                    x = wallInfo[wallId].endPointX;
                    width = wallInfo[wallId].startPointX-wallInfo[wallId].endPointX;
                }
                if (wallInfo[wallId].startPointY > wallInfo[wallId].endPointY) {
                    y = wallInfo[wallId].endPointY;
                    height = wallInfo[wallId].startPointY-wallInfo[wallId].endPointY;
                }
 
                var wallData = new convertRoom.matchingWall(wallId);
                wallData.classification = wallInfo[wallId].kind; // 壁の種類　share_wallかouter_wallかopen_wall

                wallData.wall_info.wall_flag = -1; // 真壁か大壁かのフラグ
                wallData.wall_info.inner = "内壁の情報"; // 内壁情報
                wallData.wall_info.outer = "外壁の情報"; // 外壁情報
                // roomの基準点からの始点座標
                wallData.start_point.x = wallInfo[wallId].startPointX;
                wallData.start_point.y = wallInfo[wallId].startPointY;
                // roomの基準点からの終点座標
                wallData.end_point.x = wallInfo[wallId].endPointX; // roomの基準点からの座標
                wallData.end_point.y = wallInfo[wallId].endPointY; // roomの基準点からの座標
 
                wallData.start_point.x = Math.round(wallData.start_point.x * 10000 ) / 10000;
                wallData.start_point.y = Math.round(wallData.start_point.y * 10000 ) / 10000;
                wallData.end_point.x = Math.round(wallData.end_point.x * 10000 ) / 10000;
                wallData.end_point.y = Math.round(wallData.end_point.y * 10000 ) / 10000;
 
                // 壁要素情報設定
                var openingListData = dragRoom.DRAG.checkOpeningRectNotPillerList(x, y, width, height);
                for (var i = 0 ; i < openingListData.length; i++) {
                    var openingData = new convertRoom.matchingWallEllement(dragRoom.DRAG.checkOpeningIdWithoutPiller(openingListData[i]));
                    openingData.classification = ( (openingListData[i].type <= 20) ? "window"
                                                  :(openingListData[i].type >= 98) ? "open_wall"
                                                  :"door"); // 壁要素の種類　door or window or open_wall
                    openingData.sub_classification = convertRoom.checkOpeningSubClassification(openingListData[i].type); // 引き違い戸、片開き戸など

                    var difX = 0;
                    var difY = 0;
                    if (dragRoom.DRAG.wallSize == openingListData[i].width) {
                        difX = dragRoom.DRAG.wallSize/2;
                    }
                    else {
                        difY = dragRoom.DRAG.wallSize/2;
                    }
 
                    // roomの基準点からの始点座標
                    openingData.start_point.x = openingListData[i].x + difX + dragRoom.DRAG.itemAr[openingListData[i].roomId].x;
                    openingData.start_point.y = openingListData[i].y + difY + dragRoom.DRAG.itemAr[openingListData[i].roomId].y;
                    // roomの基準点からの終点座標
                    openingData.end_point.x = (openingListData[i].x + openingListData[i].width - difX) + dragRoom.DRAG.itemAr[openingListData[i].roomId].x;
                    openingData.end_point.y = (openingListData[i].y + openingListData[i].height - difY) + dragRoom.DRAG.itemAr[openingListData[i].roomId].y;
 
                    openingData.start_point.x = Math.round(openingData.start_point.x * 10000 ) / 10000;
                    openingData.start_point.y = Math.round(openingData.start_point.y * 10000 ) / 10000;
                    openingData.end_point.x = Math.round(openingData.end_point.x * 10000 ) / 10000;
                    openingData.end_point.y = Math.round(openingData.end_point.y * 10000 ) / 10000;
 
                    wallData.wall_element_array.push(openingData);
                }
 
                // 配列に追加
                roomData.wall_array.push(wallData);
            }
 
            // 配列に追加
            convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room.push(roomData);
        }
 
        // シンクは現在はそのまま格納している
        convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.sink = dragRoom.DRAG.sinkAr; // シンク情報

        var jstr = JSON.stringify(convertRoom.MATCH_JSON_DATA);
        convertRoom.MATCH_JSON_DATA.article_info = null;
 
        return jstr;
    }

    /* マッチング用データ（JSON形式）を読み込む
     * {string} str マッチング用データ（JSON形式）の文字列
     */
    convertRoom.importJSONData = function(str) {
        //TODO: 使用メソッドが未作成のものあり。
        try {
            convertRoom.MATCH_JSON_DATA = JSON.parse(str);
 
            dragRoom.DRAG.grSepMm = convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.grid_size;
 
            // 間取り基準点
            var startX = convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.position.x;
            var startY = convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.position.y;
 
            // 部屋情報設定
            dragRoom.DRAG.itemAr = [];
            dragRoom.DRAG.openingAr = [];
            for (var i = 0; i < convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room.length; i++) {
                var roomData = convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room[i];
                var roomId = roomData.id;
 
                var x = roomData.position.x + startX;
                var y = roomData.position.y + startY;
                var width = roomData.size.width;
                var height = roomData.size.height;
                var name = roomData.sub_classification;
                var type = convertRoom.checkRoomTypeFromClassification(roomData.classification);
 
                var insertRoomData = new floorPlan.item(x, y, width, height, name, type);
 
                insertRoomData.count = roomData.area / 2; // 面積
 
                // 壁情報からdata設定
                dragRoom.DRAG.calRoomData(insertRoomData, convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room.wall_array);
 
                dragRoom.DRAG.itemAr[roomId] = insertRoomData;
 
                for (var j = 0; j < convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room.wall_array.length; j++) {
                    for (var k = 0; k < convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room.wall_array.wall_element_array.length; k++) {
                        // 壁の要素から開口部情報設定
                        var openingData = convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.room.wall_array.wall_element_array[k];
                        var openingX = openingData.start_point.x;
                        var openingY = openingData.start_point.y;
                        var openingWidth = openingData.end_point.x - openingData.start_point.x;
                        var openingHeight = openingData.end_point.y - openingData.start_point.y;
                        var openingName = openingData.sub_classification;
                        var openingType = convertRoom.checkOpeningTypeFromSubClassification(openingData.sub_classification);
 
                        if (openingWidth == 0) {
                            openingWidth = dragRoom.DRAG.wallSize;
                            openingX -= dragRoom.DRAG.wallSize/2;
                        }
                        if (openingHeight == 0) {
                            openingHeight = dragRoom.DRAG.wallSize;
                            openingY -= dragRoom.DRAG.wallSize/2;
                        }
 
                        //TODO: データが重複していないかチェックして追加する必要がある
                        dragRoom.DRAG.openingAr.push(new floorPlan.opening(openingX, openingY, openingWidth, openingHeight, openingName, openingType, roomId));
                    }
                }
            }

            // シンクは現在はそのまま格納している
            dragRoom.DRAG.sinkAr = convertRoom.MATCH_JSON_DATA.article_info.floor_plan_info.sink; // シンク情報
        }
        catch (e) {
            window.console.log(e);
        }
    }

})(jQuery);
