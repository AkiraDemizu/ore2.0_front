/* 間取り作成ソフトのクリックイベント等制御
 */
var dragRoom = {}; // namespace

(function($) {

    /* グローバル変数 */
// drag状態
    dragRoom.drag = {
        now: false, // ドラッグ状態ならtrue
        item: null,  // ドラッグしているアイテムのindex
        point:{// クリックした位置
            x:0,
            y:0
        }
    };

    /* 部屋選択取得メソッド
     * {int} x x座標(px単位)
     * {int} y y座標(px単位)
     * return 生成した部屋情報（生成に失敗した場合はnull）
     */
    function createCheckRoom(x, y) {
        var checkRoom = $('#tab001').find('li.active');

        if (checkRoom.length > 0) {
            var type = parseInt(checkRoom.val());
            var size = 2;
            // typeにより初期サイズを変更する
            if (type == 5 || type == 6 || type == 14 || type == 15 || type == 17 ) {
                size = 12;
            }
            else if (type == 16) {
                size = 9;
            }
            else if (type == 18) {
                size = 24;
            }
            else if (type == 1 || type == 2 || type == 4 || type == 11) {
                size = 4;
            }
            else if (type == 10 || type == 12) {
                size = 1;
            }

            if (size == 0) return null;

            var width = size;
            var height = 1;
            if (size==24) {
                width = (size/6);
                height = 6;
            }
            else if (size%3==0) {
                width = (size/3);
                height = 3;
            }
            else if (size%2==0) {
                width = (size/2);
                height = 2;
            }

            if (width < height) {
                var tmp = width;
                width = height;
                height = tmp;
            }

            var text = checkRoom.children('a').text();

            if (text.indexOf("帖") != -1) text = "";
            if (text.indexOf("和室") != -1 ) {
                size = 12;
                width = 3;
                height = 4;
            }

            return new floorPlan.item(dragRoom.DRAG.setPositionSplitFit(x-width/2), dragRoom.DRAG.setPositionSplitFit(y-height/2),width,height,text,type)
        }

        checkRoom.val();

        return null;
    }

    /* 開口部選択取得メソッド
     * {int} x x座標(px単位)
     * {int} y y座標(px単位)
     * {int} y 部屋のindex
   　* return 生成した開口部情報（生成に失敗した場合はnull）
     */
    function createCheckOpening(x, y, itemIdx) {
        if (checkOpeningDelMode()) return null;
        var checkRoom = $('#tab002').find('li.active');
        if (checkRoom.length > 0) {
            var type = parseInt(checkRoom.val());
            return dragRoom.DRAG.createCheckRoomPositionOpening(x,y,type,itemIdx);
        }

        return null;
    }

    /* シンク選択取得メソッド
     * {int} x x座標(px単位)
     * {int} y y座標(px単位)
     * {int} y 部屋のindex
     * return 生成したシンク情報（生成に失敗した場合はnull）
     */
    function createCheckSink(x, y, itemIdx) {
        var checkSink = $('#tab003').find('li.active');
        if (checkSink.length > 0) {
            var val = parseInt(checkSink.val());
            var sizeType = Math.floor(val / 4);
            var type = val % 4;
            var sink_width = 34/12;
            switch(sizeType) {
                case 0:
                    sink_width = 34/12;
                    break;
                case 1:
                    sink_width = 32/12;
                    break;
                case 2:
                    sink_width = 28/12;
                    break;
                case 3:
                    sink_width = 24/12;
                    break;
            }
            return dragRoom.DRAG.createCheckRoomPositionSink(x,y,type,sink_width,itemIdx);
        }
        return null;
    }

    /* 削除モードかチェック
     * return 開口部削除モードになっている場合true、それ以外が選択されている場合はfalse
     */
    function checkOpeningDelMode() {
        if (dragRoom.DRAG.openingDelMode) return true;

        return false;

    }

    /* Canvasでmousedown時に行われる処理
     * {event} evt イベントオブジェクト
     * return このメソッドで処理を行わない場合false
     */
    dragRoom.cvmsDown = function(evt) {
        if (evt.button != 0) return false;

        // ポインタ座標をCanvas座標へ変換
        var cx = evt.pageX - dragRoom.DRAG.cvpos.x + document.getElementById("cvdiv1").parentNode.scrollLeft;
        var cy = evt.pageY - dragRoom.DRAG.cvpos.y + document.getElementById("cvdiv1").parentNode.scrollTop;

        // poppup-toolsをクリックした場合、mouseイベントを行わない
        var popup = $("#popup-tools");
        if (!popup.is(":hidden")) {
            if (cx >= popup.position().left &&
                cx <= popup.position().left + popup.outerWidth()) {
                if (cy >= popup.position().top &&
                    cy <= popup.position().top + popup.outerHeight() ) {
                    return false;
                }
            }
        }

        if (dragRoom.DRAG.editMode) {
            var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
            if (itemIdx != null && itemIdx != dragRoom.DRAG.focusIndex) {
                dragRoom.DRAG.setFocus(itemIdx);
            }
            else {
                dragRoom.DRAG.focusResize(cx, cy);
            }
            return false;
        }

        if (!dragRoom.drag.now) {

            // 部屋か開口部かチェック
            if (dragRoom.DRAG.checkSelectOpening()) {// 開口部追加
                // ポインタ座標内にドラッグ可能なアイテムがあるかチェック
                var openingIdx = dragRoom.DRAG.checkOpeningPx(cx,cy);
                if (openingIdx == null) {
                    var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
                    if (itemIdx != null) {
                        var addItem = createCheckOpening(cx,cy,itemIdx);
                        if (addItem != null) {
                            openingIdx = dragRoom.DRAG.openingAr.length;
                            dragRoom.DRAG.addOpening(addItem);

                            var canvas = document.getElementById('transparency-canvas');
                            canvas.style.cursor = 'move';

                            // 画面更新
                            dragRoom.DRAG.draw();
                        }
                    }
                }
                // 移動する
                if (openingIdx != null) {
                    if (checkOpeningDelMode()) {
                        dragRoom.DRAG.setFocus(openingIdx);
                        dragRoom.DRAG.focusDelete();
                        return;
                    }
                    dragRoom.drag.now = true;
                    dragRoom.drag.item = openingIdx;
                    dragRoom.drag.point.x = 0;
                    dragRoom.drag.point.y = 0;

                    dragRoom.DRAG.setFocus(openingIdx);
                }
                return;
            }
            else if (dragRoom.DRAG.checkSelectSink()) {// シンク追加
                // ポインタ座標内にドラッグ可能なアイテムがあるかチェック
                var sinkIdx = dragRoom.DRAG.checkSinkPx(cx,cy);
                if (sinkIdx == null) {
                    var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
                    if (itemIdx != null) {
                        var addItem = createCheckSink(cx,cy,itemIdx);
                        if (addItem != null) {
                            sinkIdx = dragRoom.DRAG.sinkAr.length;
                            dragRoom.DRAG.sinkAr.push(addItem);

                            var canvas = document.getElementById('transparency-canvas');
                            canvas.style.cursor = 'move';

                            // 画面更新
                            dragRoom.DRAG.draw();
                        }
                    }
                }
                // 移動する
                if (sinkIdx != null) {
                    dragRoom.drag.now = true;
                    dragRoom.drag.item = sinkIdx;
                    dragRoom.drag.point.x =  cx - (dragRoom.DRAG.itemAr[dragRoom.DRAG.sinkAr[sinkIdx].roomId].x + dragRoom.DRAG.sinkAr[sinkIdx].x)*dragRoom.DRAG.grSep;
                    dragRoom.drag.point.y =  cy - (dragRoom.DRAG.itemAr[dragRoom.DRAG.sinkAr[sinkIdx].roomId].y + dragRoom.DRAG.sinkAr[sinkIdx].y)*dragRoom.DRAG.grSep;

                    dragRoom.DRAG.setFocus(sinkIdx);
                }
                return;
            }

            // ポインタ座標内にドラッグ可能なアイテムがあるかチェック
            var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
            if (itemIdx == null) {
                // ない場合はクリックした位置に追加
                var addItem = createCheckRoom(cx, cy);
                if (addItem == null) {
                    return;
                }
                dragRoom.DRAG.itemAr.push(addItem);
                itemIdx = dragRoom.DRAG.itemAr.length-1;

                var canvas = document.getElementById('transparency-canvas');
                canvas.style.cursor = 'move';

                // 選択解除
                $('#tab001').find('li').removeClass('active');

                // 辺り判定チェック
                if (dragRoom.DRAG.checkItemOverlap(addItem.x, addItem.y, dragRoom.DRAG.itemAr.length-1) != null) {
                    dragRoom.DRAG.itemAr.pop();
                    dragRoom.DRAG.updateInfo();
                    return;
                }

                // 壁内の柱を追加する
                dragRoom.DRAG.createNewRoomPillar(itemIdx);

                dragRoom.DRAG.updateInfo();

                // 画面更新
                dragRoom.DRAG.draw();
            }
            dragRoom.drag.now = true;
            dragRoom.drag.item = itemIdx;
            dragRoom.drag.point.x =  Math.floor(cx/dragRoom.DRAG.grSep)*dragRoom.DRAG.grSep - (dragRoom.DRAG.itemAr[itemIdx].x)*dragRoom.DRAG.grSep;
            dragRoom.drag.point.y =  Math.floor(cy/dragRoom.DRAG.grSep)*dragRoom.DRAG.grSep - (dragRoom.DRAG.itemAr[itemIdx].y)*dragRoom.DRAG.grSep;

            dragRoom.DRAG.setFocus(itemIdx);
        }
        return false;
    };
    /* Canvasでmouseup/mouseleave時に行われる処理
     * {event} evt イベントオブジェクト
     * return このメソッドで処理を行わない場合false
     */
    dragRoom.cvmsUp = function(evt) {
        if (evt.button != 0) return false;

        if (dragRoom.drag.now) {
            // ポインタ座標をCanvas座標へ変換
            var cx = evt.pageX - dragRoom.DRAG.cvpos.x + document.getElementById("cvdiv1").parentNode.scrollLeft;
            var cy = evt.pageY - dragRoom.DRAG.cvpos.y + document.getElementById("cvdiv1").parentNode.scrollTop;
            if (cx < 0) cx = 0;
            if (cx > dragRoom.DRAG.area.w) cx = dragRoom.DRAG.area.w;
            if (cy < 0) cy = 0;
            if (cy > dragRoom.DRAG.area.h) cy = dragRoom.DRAG.area.h;

            dragRoom.drag.now = false;
            dragRoom.drag.item = null;

            // 画面更新
            dragRoom.DRAG.draw();
        }
    };
    /* Canvasでmousemove時に行われる処理
     * {event} evt イベントオブジェクト
     * return このメソッドで処理を行わない場合false
     */
    dragRoom.cvmsMove = function(evt) {

        if (dragRoom.drag.now) {
            if (evt.button != 0) return false;

            // 部屋か開口部かチェック
            if (dragRoom.DRAG.checkSelectOpening()) {// 開口部追加
                var cx = evt.pageX - dragRoom.DRAG.cvpos.x + document.getElementById("cvdiv1").parentNode.scrollLeft - dragRoom.drag.point.x;
                var cy = evt.pageY - dragRoom.DRAG.cvpos.y + document.getElementById("cvdiv1").parentNode.scrollTop - dragRoom.drag.point.y;

                // ポインタ座標内にドラッグ可能なアイテムがあるかチェック
                var updateFlg = false;

                var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
                if (itemIdx != null) {
                    var openingIdx = dragRoom.DRAG.checkOpeningPx(cx,cy,dragRoom.DRAG.focusIndex);
                    if (openingIdx == null) {
                        // アイテムの座標更新
                        dragRoom.DRAG.moveOpening(cx, cy, itemIdx, dragRoom.DRAG.focusIndex);
                        updateFlg = true;
                        // 画面更新
                        dragRoom.DRAG.draw();
                    }
                }

                if (!updateFlg) {
                    dragRoom.DRAG.checkOpeningFlip(cx, cy, dragRoom.drag.item);
                    dragRoom.DRAG.draw();
                }

                return;
            }
            else if (dragRoom.DRAG.checkSelectSink()) {// シンク追加
                var cx = evt.pageX - dragRoom.DRAG.cvpos.x + document.getElementById("cvdiv1").parentNode.scrollLeft;
                var cy = evt.pageY - dragRoom.DRAG.cvpos.y + document.getElementById("cvdiv1").parentNode.scrollTop;

                // ポインタ座標内にドラッグ可能なアイテムがあるかチェック
                var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
                if (itemIdx != null) {
                    cx -= dragRoom.drag.point.x;
                    cy -= dragRoom.drag.point.y;
                    var sinkIdx = dragRoom.DRAG.checkSinkPx(cx,cy,dragRoom.DRAG.focusIndex);
                    if (sinkIdx == null) {
                        // アイテムの座標更新
                        dragRoom.DRAG.sinkAr[dragRoom.drag.item].x = (cx/dragRoom.DRAG.grSep - dragRoom.DRAG.itemAr[itemIdx].x);
                        dragRoom.DRAG.sinkAr[dragRoom.drag.item].y = (cy/dragRoom.DRAG.grSep - dragRoom.DRAG.itemAr[itemIdx].y);
                        dragRoom.DRAG.sinkAr[dragRoom.drag.item].roomId = itemIdx;

                        // 画面更新
                        dragRoom.DRAG.draw();
                    }
                }

                return;
            }

            // ポインタ座標をCanvas座標へ変換
            var cx = dragRoom.DRAG.setPositionSplitFit(evt.pageX - dragRoom.DRAG.cvpos.x + document.getElementById("cvdiv1").parentNode.scrollLeft - dragRoom.drag.point.x);
            var cy = dragRoom.DRAG.setPositionSplitFit(evt.pageY - dragRoom.DRAG.cvpos.y + document.getElementById("cvdiv1").parentNode.scrollTop - dragRoom.drag.point.y);

            // 辺り判定チェック
            if (dragRoom.DRAG.checkItemOverlap(cx, cy, dragRoom.drag.item) != null) {
                return;
            }

            // 画面更新するか判定
            var updSep = 0; // 何px動いたら画面更新するか
            if (Math.abs(cx - dragRoom.DRAG.itemAr[dragRoom.drag.item].x) >= updSep ||
                Math.abs(cy - dragRoom.DRAG.itemAr[dragRoom.drag.item].y) >= updSep) {
                // アイテムの座標更新
                dragRoom.DRAG.itemAr[dragRoom.drag.item].x = cx;
                dragRoom.DRAG.itemAr[dragRoom.drag.item].y = cy;
                // 画面更新
                dragRoom.DRAG.draw();
            }
        }
        else if (dragRoom.DRAG.editMode) {
            var canvas = document.getElementById('transparency-canvas');
            canvas.style.cursor = 'default';
        }
        else {
            var cx = evt.pageX - dragRoom.DRAG.cvpos.x + document.getElementById("cvdiv1").parentNode.scrollLeft;
            var cy = evt.pageY - dragRoom.DRAG.cvpos.y + document.getElementById("cvdiv1").parentNode.scrollTop;

            var canvas = document.getElementById('transparency-canvas');

            // 部屋か開口部かチェック
            if (dragRoom.DRAG.checkSelectOpening()) {// 開口部
                var openingIdx = dragRoom.DRAG.checkOpeningPx(cx,cy);
                if (openingIdx != null) {
                    if (checkOpeningDelMode()) {
                        canvas.style.cursor = 'pointer';
                    }
                    else {
                        canvas.style.cursor = 'move';
                    }
                }
                else {
                    canvas.style.cursor = 'default';

                    var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
                    if (itemIdx != null) {
                        var addItem = createCheckOpening(cx,cy,itemIdx);
                        if (addItem != null) {
                            canvas.style.cursor = 'copy';
                        }
                    }
                }
            }
            else if (dragRoom.DRAG.checkSelectSink()) {// シンク
                var sinkIdx = dragRoom.DRAG.checkSinkPx(cx,cy);
                if (sinkIdx != null) {
                    canvas.style.cursor = 'move';
                }
                else {
                    canvas.style.cursor = 'default';

                    var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
                    if (itemIdx != null) {
                        var addItem = createCheckSink(cx,cy,itemIdx);
                        if (addItem != null) {
                            canvas.style.cursor = 'copy';
                        }
                    }
                }
            }
            else {
                var itemIdx = dragRoom.DRAG.checkItemPx(cx, cy);
                if (itemIdx != null) {
                    canvas.style.cursor = 'move';
                }
                else {
                    canvas.style.cursor = 'default';
                }
            }
        }

        return false;
    };

    /* body onload時の処理 */
    $(window).load(function() {
        // canvasのDOM elementとcontext取得
        var canvas = document.getElementById('main-canvas');
        if ( ! canvas || ! canvas.getContext ) { return false; }
        var ctx;
        var $cvdiv = $('#cvdiv1'); // main Canvasのdiv
        if ($cvdiv.size() == 0) return false;

        // var ctx = canvas.getContext("2d");
        // ctx.lineWidth = 1;
        // ctx.globalAlpha = 0.7;
        // ctx.globalCompositeOperation = "source-over";


        // canvasサイズ設定
        canvas.width = 3600*2;//$cvdiv.width();
        canvas.height = 3600*2;//$cvdiv.height();

        // 透過canvasサイズ設定
        // var canvas2 = document.getElementById('transparency-canvas');
        // var ctx2 = canvas2.getContext("2d");
        // ctx2.lineWidth = 1;
        // ctx2.globalAlpha = 0.7;
        // ctx2.globalCompositeOperation = "source-over";

        // canvasサイズ設定
        // canvas2.width = 3600*2;//$cvdiv.width();
        // canvas2.height = 3600*2;//$cvdiv.height();

        // 画面表示
        dragRoom.DRAG = new floorPlan.canv(ctx, canvas.width, canvas.height, 'cvdiv1');
        dragRoom.DRAG.init();
        dragRoom.DRAG.draw();

        // canvasにイベント付与
        $cvdiv.mousedown(dragRoom.cvmsDown);
        $cvdiv.mouseup(dragRoom.cvmsUp);
        $cvdiv.mouseleave(dragRoom.cvmsUp);
        $cvdiv.mousemove(dragRoom.cvmsMove);

        // // canvas初期化
        // dragRoom.DRAG.init();
        // dragRoom.DRAG.draw();

        dragRoom.gamestart = true;

        // 背景画像変更ボタンのイベント登録
        var $sampleAElement = document.getElementById( "btn-change-back-size1" );
        if ($sampleAElement) {
            $sampleAElement.onmousedown = function ( $event ) {
                if( $event.button == 0 ){
                    $intervalID = setInterval(
                        function(){
                            changeBackSize( 1 );
                        },
                        20);
                }
            };
            $sampleAElement.onmouseup = function ( $event ) {
                if( $event.button == 0 ){
                    clearInterval( $intervalID );
                }
            };
            var $sampleBElement = document.getElementById( "btn-change-back-size0" );
            $sampleBElement.onmousedown = function ( $event ) {
                if( $event.button == 0 ){
                    $intervalID = setInterval(
                        function(){
                            changeBackSize( 0 );
                        },
                        20);
                }
            };
            $sampleBElement.onmouseup = function ( $event ) {
                if( $event.button == 0 ){
                    clearInterval( $intervalID );
                }
            };

            var $btnMoveBack0 = document.getElementById( "btn-move-back-position0" );
            $btnMoveBack0.onmousedown = function ( $event ) {
                if( $event.button == 0 ){
                    $intervalID = setInterval(
                        function(){
                            moveBack( 0 );
                        },
                        20);
                }
            };
            $btnMoveBack0.onmouseup = function ( $event ) {
                if( $event.button == 0 ){
                    clearInterval( $intervalID );
                }
            };
            var $btnMoveBack1 = document.getElementById( "btn-move-back-position1" );
            $btnMoveBack1.onmousedown = function ( $event ) {
                if( $event.button == 0 ){
                    $intervalID = setInterval(
                        function(){
                            moveBack( 1 );
                        },
                        20);
                }
            };
            $btnMoveBack1.onmouseup = function ( $event ) {
                if( $event.button == 0 ){
                    clearInterval( $intervalID );
                }
            };
            var $btnMoveBack2 = document.getElementById( "btn-move-back-position2" );
            $btnMoveBack2.onmousedown = function ( $event ) {
                if( $event.button == 0 ){
                    $intervalID = setInterval(
                        function(){
                            moveBack( 2 );
                        },
                        20);
                }
            };
            $btnMoveBack2.onmouseup = function ( $event ) {
                if( $event.button == 0 ){
                    clearInterval( $intervalID );
                }
            };
            var $btnMoveBack3 = document.getElementById( "btn-move-back-position3" );
            $btnMoveBack3.onmousedown = function ( $event ) {
                if( $event.button == 0 ){
                    $intervalID = setInterval(
                        function(){
                            moveBack( 3 );
                        },
                        20);
                }
            };
            $btnMoveBack3.onmouseup = function ( $event ) {
                if( $event.button == 0 ){
                    clearInterval( $intervalID );
                }
            }

        }

    });


})(jQuery);
