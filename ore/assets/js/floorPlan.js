/* 間取りデータの表示・編集を行う
 */
var floorPlan = {}; // namespace

(function ($) {

    /** 部屋クラス
     * {int} x 部屋の左上x座標 (グリッド単位)
     * {int} y 部屋の左上y座標 (グリッド単位）
     * {int} width 部屋の幅 　（グリッド単位）
     * {int} height 部屋の高さ （グリッド単位）
     * {string} name 部屋の名前
     * {string} type 部屋のタイプ
     */
    floorPlan.item = function (x, y, width, height, name, type) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.data = []; // 二次元配列
        for (var i = 0; i < this.width * dragRoom.DRAG.grSplitMax; i++) {
            this.data[i] = [];
            for (var j = 0; j < this.height * dragRoom.DRAG.grSplitMax; j++) {
                this.data[i][j] = 1;
            }
        }
        this.count = width * height / 2;
        this.type = type;
        this.unit = "帖";
        this.name = name;
        this.roomChange = -1;
        this.WallBool = true;
    };

    /** 開口部クラス
     * {int} x 開口部の左上x座標 (グリッド単位：部屋の左上起点)
     * {int} y 開口部の左上y座標 (グリッド単位：部屋の左上起点）
     * {int} width 開口部の幅 　（グリッド単位）
     * {int} height 開口部の高さ （グリッド単位）
     * {string} name 開口部の名前
     * {string} type 開口部のタイプ
     * {string} roomId 開口部が取り付けられている部屋id
     */
    floorPlan.opening = function (x, y, width, height, name, type, roomId) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.displayWidth = width;
        this.displayHeight = height;
        this.type = type;
        this.unit = "帖";
        this.name = name;
        this.roomId = roomId;
        this.flip = false;
        this.hFlip = false;
        this.openingChange = -1;
    };

    /** シンククラス
     * {int} x シンクの左上x座標 (グリッド単位：部屋の左上起点)
     * {int} y シンクの左上y座標 (グリッド単位：部屋の左上起点）
     * {int} width シンクの幅 　（グリッド単位）
     * {int} height シンクの高さ （グリッド単位）
     * {string} type シンクのタイプ
     * {string} room シンクが取り付けられている部屋id
     */
    floorPlan.sink = function (x, y, width, height, type, roomId, angle, sink_width) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.sink_width = sink_width;
        this.type = type;
        this.unit = "grid";
        this.roomId = roomId;
        this.angle = angle;
    };

    /** Canvasクラス
     * {canvas context} ctx 処理対象となるcontext
     * {int} w 表示領域の幅（px）
     * {int} h 表示領域の高さ（px）
     * {string} name 呼び出し元のcanvas名
     */
    floorPlan.canv = function (ctx, w, h, name) {
        this.canvDivName = name;
        this.ctx = ctx; // 処理対象context
        var canvas2 = document.getElementById('transparency-canvas');
        this.ctx2 = canvas2.getContext("2d");
        this.area = {w: w, h: h};  // 表示エリア
        this.cvpos = {x: 0, y: 0};  // ブラウザ上のCanvas左上座標
        this.grMax = 60; // グリッド最大数
        this.grSep = 60;  // グリッド間隔(px)
        this.grSepUnit = 0.5;  // グリッド間隔単位(帖/グリッド)
        this.grSepMm = 910;  // グリッド間隔単位(mm/グリッド)
        this.grSplit = 1; // グリッドの分割数
        this.grSplitMax = 12; // グリッド分割の最大数
        this.grWidth = 0.5; // グリッド線の太さ
        this.grXAr = []; // グリッドのX座標を格納した配列
        this.grRcv = false; // canvasの右端がグリッドと重なるならtrue
        this.grYAr = []; // グリッドのY座標を格納した配列
        this.grBcv = false; // canvasの下端がグリッドと重なるならtrue
        this.wallSize = 0.1; // 壁サイズ
        this.itemAr = []; // 部屋格納配列
        this.openingAr = []; // 開口部格納配列
        this.sinkAr = []; // シンク格納配列
        this.focusIndex = null; // 現在フォーカスがあるindex
        this.focusIndexAr = []; // 現在フォーカスがあるindexの配列
        this.editFinishIndexAr = []; // 編集が完了したindexの配列
        this.editMode = false; // 編集モードが否か
        this.openingDelMode = false; // 開口部削除モードか否か
        this.type2Img = new Image();
        this.type5Img = new Image();
        this.type11Img = new Image();
        this.flooringImg = new Image();
        this.closetImg = new Image();
        this.oshiireImg = new Image();
        this.tokonomaImg = new Image();
        this.toiletImg = new Image();
        this.toilet2Img = new Image();
        this.verandaImg = new Image();
        this.garageImg = new Image();
        this.shakoImg = new Image();
        this.ubImg = new Image();
        this.roadImg = new Image();
        this.typeKaidanImg = new Image();
        this.typeKaidanRightImg = new Image();
        this.domaImg = new Image();
        this.nayaImg = new Image();
        this.engawaImg = new Image();
        this.tenpoImg = new Image();
        this.niwaImg = new Image();
        this.sinkImage2550 = new Image();
        this.sinkImage2400 = new Image();
        this.sinkImage2100 = new Image();
        this.sinkImage1800 = new Image();
        this.backgroundImage = new Image();
        this.editIconImg = new Image();
        this.editFinishIconImg = new Image();
        // 背景に置く画像の位置・サイズ
        this.backgroundImagePositionX = 0;
        this.backgroundImagePositionY = 0;
        this.backgroundImageWidth = 100;
        this.backgroundImageHeight = 100;
        this.madoriAlpha = 1.0;// 間取りの描画色
        // シンクサイズ
        this.sinkWidth = 34 / 12;   // シンクの幅(グリッド)
        this.sinkDepth = 8 / 12;    // シンクの奥行き(グリッド)
        this.sinkMarginLR = 12 / 12; // シンクの通路幅（グリッド）
        this.sinkMarginBottom = 18 / 12;   // シンクの人の立つ位置（グリッド）

        this.floorNo = 1;
        this.displayMode = 0;
        this.startGridX = 0;
        this.startGridY = 0;

        this.compassNo = 0;// 方角番号

        /* ブラウザ上のCanvas左上座標を求める */
        var $cvdiv = $('#' + name);
        this.cvpos.x = $cvdiv.offset().left;
        this.cvpos.y = $cvdiv.offset().top;

        /* グリッド座標計算 */
        this.initGrid = function () {
            this.grXAr = [];
            this.grYAr = [];
            var gcntx = 0;  // 縦線本数
            this.grBcv = false;

            if (this.area.w % this.grSep) {
                gcntx = Math.floor(this.area.w / this.grSep);
            } else {
                // canvasの右端がグリッドと重なる場合
                gcntx = this.area.w / this.grSep - 1;
                this.grRcv = true;
            }
            for (var i = 0; i < gcntx; i++) {
                this.grXAr[i] = this.grSep * i;
            }

            var gcnty = 0;  // 横線本数
            if (this.area.h % this.grSep) {
                gcnty = Math.floor(this.area.h / this.grSep);
            } else {
                // canvasの下端がグリッドと重なる場合
                gcnty = this.area.h / this.grSep - 1;
                this.grBcv = true;
            }
            for (var i = 0; i < gcnty; i++) {
                this.grYAr[i] = this.grSep * i;
            }
        };

        /** 初期処理
         * return なし
         */
        this.init = function () {
            this.initGrid();

            // アイテム初期配置
            this.itemAr = [];

            // 浴室画像初期読み込み
            this.type2Img.src = '/ore/assets/images/floor/yokushitsu.png';
            // this.type2Img.on('load', function (){
            //     dragRoom.DRAG.draw();
            // });

            // 和室画像初期読み込み
            this.type5Img.src = '/ore/assets/images/floor/tatami.png';
            if (this.grSep < 60) {
                this.type5Img.src = '/ore/assets/images/floor/tatami_mini.png';
            }
            // this.type5Img.on();
            // this.type5Img.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 玄関画像初期読み込み
            this.type11Img.src = '/ore/assets/images/floor/genkan.png';
            if (this.grSep < 60) {
                this.type11Img.src = '/ore/assets/images/floor/genkan.png';
            }
            // this.type11Img.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // フローリング画像初期読み込み
            this.flooringImg.src = '/ore/assets/images/floor/ldk.png';
            // this.flooringImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // クローゼット画像初期読み込み
            this.closetImg.src = '/ore/assets/images/floor/closet.png';
            // this.closetImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 押し入れ/物入れ画像初期読み込み
            this.oshiireImg.src = '/ore/assets/images/floor/oshiire.png';
            // this.oshiireImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 床の間画像初期読み込み
            this.tokonomaImg.src = '/ore/assets/images/floor/tokonoma.png';
            // this.tokonomaImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 洋式トイレ画像初期読み込み
            this.toiletImg.src = '/ore/assets/images/floor/toilet1.png';
            // this.toiletImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 和式トイレ画像初期読み込み
            this.toilet2Img.src = '/ore/assets/images/floor/toilet2.png';
            // this.toilet2Img.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // ベランダ画像初期読み込み
            this.verandaImg.src = '/ore/assets/images/floor/veranda.png';
            // this.verandaImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 室内ガレージ画像初期読み込み
            this.garageImg.src = '/ore/assets/images/floor/garage.png';
            // this.garageImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 車庫画像初期読み込み
            this.shakoImg.src = '/ore/assets/images/floor/shako.png';
            // this.shakoImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // UB画像初期読み込み
            this.ubImg.src = '/ore/assets/images/floor/ub.png';
            // this.ubImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 廊下画像初期読み込み
            this.roadImg.src = '/ore/assets/images/floor/rouka.png';
            // this.roadImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 階段画像初期読み込み
            this.typeKaidanImg.src = '/ore/assets/images/floor/kaidan_tate.png';
            // this.typeKaidanImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            this.typeKaidanRightImg.src = '/ore/assets/images/floor/kaidan_yoko.png';
            // this.typeKaidanRightImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 土間
            this.domaImg.src = '/ore/assets/images/floor/doma.png';
            // this.domaImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 納戸
            this.nayaImg.src = '/ore/assets/images/floor/naya.png';
            // this.nayaImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 縁側
            this.engawaImg.src = '/ore/assets/images/floor/engawa.png';
            // this.engawaImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 店舗
            this.tenpoImg.src = '/ore/assets/images/floor/tenpo.png';
            // this.tenpoImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 中庭
            this.niwaImg.src = '/ore/assets/images/floor/nakaniwa.png';
            // this.niwaImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // シンク画像初期読み込み
            this.sinkImage2550.src = '/ore/assets/images/floor/sink2550.png';
            // this.sinkImage2550.onload = function () {
            //     dragRoom.DRAG.draw();
            // };
            this.sinkImage2400.src = '/ore/assets/images/floor/sink2400.png';
            // this.sinkImage2400.onload = function () {
            //     dragRoom.DRAG.draw();
            // };
            this.sinkImage2100.src = '/ore/assets/images/floor/sink2100.png';
            // this.sinkImage2100.onload = function () {
            //     dragRoom.DRAG.draw();
            // };
            this.sinkImage1800.src = '/ore/assets/images/floor/sink1800.png';
            // this.sinkImage1800.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 編集中の丸アイコン
            this.editIconImg.src = '/ore/assets/images/icon-option-edit.png';
            // this.editIconImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // };

            // 編集中の丸アイコン
            this.editFinishIconImg.src = '/ore/assets/images/icon-option-finish.png';
            // this.editFinishIconImg.onload = function () {
            //     dragRoom.DRAG.draw();
            // }
        };

        /** 画面を初期化
         * return なし
         */
        this.blank = function () {
            // 画面をクリア
            // this.ctx.clearRect(0, 0, this.area.w, this.area.h);
            this.ctx2.clearRect(0, 0, this.area.w, this.area.h);

            // 背景画像描画
            // this.ctx.drawImage(this.backgroundImage, this.backgroundImagePositionX * this.grSep / 60,
            //     this.backgroundImagePositionY * this.grSep / 60, this.backgroundImageWidth * this.grSep / 60,
            //     this.backgroundImageWidth / this.backgroundImage.width * this.backgroundImage.height * this.grSep / 60);

            // グリッド表示
            this.ctx2.save();
            this.ctx2.globalAlpha = 0.5;
            if (this.displayMode === 0) {
                this.ctx2.strokeStyle = "#000033";
            }
            else {
                this.ctx2.globalAlpha = 1.0;
                this.ctx2.strokeStyle = "#ffffff";
            }
            this.ctx2.lineWidth = this.grWidth;
            this.ctx2.beginPath();
            for (var i = 0; i < this.grXAr.length; i++) {

                // this.ctx.beginPath();
                this.ctx2.moveTo(this.grXAr[i] + this.grSep, 0);
                this.ctx2.lineTo(this.grXAr[i] + this.grSep, this.area.h);
                // this.ctx.stroke();

                if (this.grSplit !== 1) {
                    if (this.displayMode === 0) {
                        this.ctx2.strokeStyle = "#000055";
                    }
                    else {
                        this.ctx2.strokeStyle = "#ffffff";
                    }
                    this.ctx2.lineWidth = this.grWidth / 2;

                    for (var j = 1; j < this.grSplit; j++) {
                        // this.ctx.beginPath();
                        this.ctx2.moveTo(this.grXAr[i] + this.grSep * j / this.grSplit, 0);
                        this.ctx2.lineTo(this.grXAr[i] + this.grSep * j / this.grSplit, this.area.h);
                        // this.ctx.stroke();
                    }
                    if (this.displayMode === 0) {
                        this.ctx2.strokeStyle = "#000033";
                    }
                    else {
                        this.ctx2.strokeStyle = "#ffffff";
                    }
                    this.ctx2.lineWidth = this.grWidth;
                }
            }
            for (var i = 0; i < this.grYAr.length; i++) {
                // this.ctx.beginPath();
                this.ctx2.moveTo(0, this.grYAr[i] + this.grSep);
                this.ctx2.lineTo(this.area.w, this.grYAr[i] + this.grSep);
                // this.ctx.stroke();

                if (this.grSplit !== 1) {
                    if (this.displayMode === 0) {
                        this.ctx2.strokeStyle = "#000055";
                    }
                    else {
                        this.ctx2.strokeStyle = "#ffffff";
                    }
                    this.ctx2.lineWidth = this.grWidth / 2;

                    for (var j = 1; j < this.grSplit; j++) {
                        // this.ctx.beginPath();
                        this.ctx2.moveTo(0, this.grYAr[i] + this.grSep * j / this.grSplit);
                        this.ctx2.lineTo(this.area.w, this.grYAr[i] + this.grSep * j / this.grSplit);
                        // this.ctx2.stroke();
                    }

                    if (this.displayMode === 0) {
                        this.ctx2.strokeStyle = "#000033";
                    }
                    else {
                        this.ctx2.strokeStyle = "#ffffff";
                    }
                    this.ctx2.lineWidth = this.grWidth;
                }
            }
            this.ctx2.stroke();
            this.ctx2.restore();
        };

        /** 画面サイズ変更時にcanvasを調整する
         * return なし
         */
        this.windowResize = function () {
            var canvas = document.getElementById('main-canvas');
            var canvas2 = document.getElementById('transparency-canvas');
            var $cvdiv = $('#' + this.canvDivName); // main Canvasのdiv

            if (this.canvDivName === "cvdiv2") {
                var cvWidth = $cvdiv.width();
                var cvHeight = $cvdiv.height();
                $cvdiv.css('overflow', 'auto');

                // divの範囲で間取りが収まるかチェック
                var checkValue = this.checkCanvasPosition();
                if (cvWidth < (checkValue.maxX - this.startGridX + 1) * this.grSep) {
                    cvWidth = (checkValue.maxX - this.startGridX + 1) * this.grSep;
                }
                if (cvHeight < (checkValue.maxY - this.startGridY + 1) * this.grSep) {
                    cvHeight = (checkValue.maxY - this.startGridY + 1) * this.grSep;
                }

                canvas.width = cvWidth * 2;
                canvas.height = cvHeight * 2;
                canvas.style.width = cvWidth + "px";
                canvas.style.height = cvHeight + "px";

                // canvasサイズ設定
                canvas2.width = cvWidth * 2;
                canvas2.height = cvHeight * 2;
                canvas2.style.width = cvWidth + "px";
                canvas2.style.height = cvHeight + "px";

                this.area = {w: canvas.width, h: canvas.height};

                this.cvpos.x = $cvdiv.offset().left;
                this.cvpos.y = $cvdiv.offset().top;
//        window.console.log (this.cvpos.x + "," + this.cvpos.y);

                // this.ctx.scale(2, 2);
                this.ctx2.scale(2, 2);
                this.initGrid();
                this.draw();
                return;
            }

            var scrollLeft = document.getElementById(this.canvDivName).parentNode.scrollLeft;
            var scrollTop = document.getElementById(this.canvDivName).parentNode.scrollTop;

            document.getElementById(this.canvDivName).parentNode.scrollLeft = 0;
            document.getElementById(this.canvDivName).parentNode.scrollTop = 0;

            canvas.width = this.grMax * this.grSep * 2;//$cvdiv.width();
            canvas.height = this.grMax * this.grSep * 2;//$cvdiv.height();
            canvas.style.width = canvas.width + "px";
            canvas.style.height = canvas.height + "px";

            // var canvas2 = document.getElementById('transparency-canvas');
            // canvasサイズ設定
            canvas2.width = this.grMax * this.grSep * 2;//$cvdiv.width();
            canvas2.height = this.grMax * this.grSep * 2;//$cvdiv.height();
            canvas2.style.width = canvas.width + "px";
            canvas2.style.height = canvas.height + "px";

            // canvasサイズ設定
            this.area = {w: canvas.width, h: canvas.height};

            this.cvpos.x = $cvdiv.offset().left;
            this.cvpos.y = $cvdiv.offset().top;

            this.scrollAdjustRoom();

            this.draw();
        };

        /** 画面サイズ変更時にスクロール位置を調整する
         * return なし
         */
        this.scrollAdjustRoom = function () {
            // 間取り上で一番左上の位置をチェック
            var scrollLeft = (this.grMax - 17) * this.grSep;
            var scrollTop = (this.grMax - 15) * this.grSep;

            if (this.itemAr.length > 0) {
                var checkValue = this.checkCanvasPosition();
                if (checkValue.minX < 1) checkValue.minX = 1;
                if (checkValue.minY < 1) checkValue.minY = 1;

                scrollLeft = (checkValue.minX - 1) * this.grSep;
                scrollTop = (checkValue.minY - 1) * this.grSep;
            }

            document.getElementById(this.canvDivName).parentNode.scrollLeft = scrollLeft;
            document.getElementById(this.canvDivName).parentNode.scrollTop = scrollTop;

        };

        /** フォーカスセット
         * {int} index セットするid
         * return なし
         */
        this.setFocus = function (index) {
            this.focusIndex = index;
            this.focusIndexAr = [index];
            if (index === null) this.focusIndexAr = [];
            this.updateInfo();
            this.draw();

            /*
    if (index == null) {
        $('select[name=change-select-tool]').hide();
    }
    else {
        $('select[name=change-select-tool]').show();
    }

    if (this.checkSelectSink()) {
        $('select[name=change-select-tool] optgroup[id=select-sink] option').val(""+this.sinkAr[index].type);
    }
    else if (!this.checkSelectOpening()) {
        $('select[name=change-select-tool]').val(""+this.itemAr[index].type);
    }
  */
        };

        /** フォーカスセット（プラン専用）
         * {int} index セットするid
         * return なし
         */
        this.setFocusIndex = function (index) {
            this.focusIndex = index;
            this.focusIndexAr = [];

            if (index === null) return;

            this.focusIndexAr.push(index);

            // 玄関、もしくは廊下の場合、配列を作成
            if (false) {
                if ((this.itemAr[index].type === 11 || this.itemAr[index].type === 12) &&
                    this.displayMode === 1) {
                    var nowType = this.itemAr[index].type;
                    var touchHallList = this.checkTouchType(index, nowType + 23 - 2 * nowType);
                    if (touchHallList.length > 0) {
                        // 接している玄関・廊下含めた配列を作成
                        for (var j = 0; j < touchHallList.length; j++) {
                            this.focusIndexAr.push(touchHallList[j]);
                        }
                    }
                }
            }
        };

        /** 一番若いキッチン（LDK,DK,キッチン）かリビング、ダイニングにフォーカスセット（ない場合は一番若い部屋にフォーカス）
         * return なし
         */
        this.setFocusLDK = function () {
            var index = -1;
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] === null) continue;
                if (index < 0) index = i;
                if (this.itemAr[i].type >= 14 && this.itemAr[i].type <= 18) {
                    index = i;
                    break;
                }
            }

            this.focusIndex = index;
            this.focusIndexAr = [index];
            if (index === null) this.focusIndexAr = [];
            this.updateInfo();
            this.draw();
        };

        /** 編集が完了した部屋番号セット（プラン専用）
         * {int} index セットするid
         * return なし
         */
        this.setEditFinishIndex = function (index) {
            if (index === null) {
                this.editFinishIndexAr = [];
            }
            else if (this.editFinishIndexAr.indexOf(index) < 0) {
                this.editFinishIndexAr.push(index);
            }
            this.draw();
        };

        /** 編集モードセット
         * {int} flg 編集モードフラグ
         * return なし
         */
        this.setEditMode = function (flg) {
            this.editMode = flg;
            this.draw();
        };

        /** 開口部削除モードセット
         * {boolean} flg 開口部削除モードフラグ
         * return なし
         */
        this.setOpeningDelMode = function (flg) {
            this.openingDelMode = flg;
            this.draw();
        };

        /** 背景画像セット
         * {string} file セットする画像のパス
         * return なし
         */
        this.setBackgroundImage = function (file) {
            this.backgroundImage.src = file;
            this.backgroundImage.onload = function () {
                dragRoom.DRAG.backgroundImageWidth = dragRoom.DRAG.backgroundImage.width;
                dragRoom.DRAG.backgroundImageHeight = dragRoom.DRAG.backgroundImage.height;
                dragRoom.DRAG.draw();
            }
        };

        /** フォーカスしているアイテムをリサイズ
         * {int} x x座標（px単位）
         * {int} y y座標（px単位）
         * return なし
         */
        this.focusResize = function (x, y) {
            if (this.focusIndex !== null) {
                var nowPositionItem = this.checkItemPx(x, y);

                var positionX = Math.floor(Math.floor((x - this.itemAr[this.focusIndex].x * this.grSep) / this.grSep * this.grSplitMax) / (this.grSplitMax / this.grSplit)) * (this.grSplitMax / this.grSplit);
                var positionY = Math.floor(Math.floor((y - this.itemAr[this.focusIndex].y * this.grSep) / this.grSep * this.grSplitMax) / (this.grSplitMax / this.grSplit)) * (this.grSplitMax / this.grSplit);

                if (nowPositionItem === this.focusIndex) {
                    this.focusAddGrid(positionX, positionY);
                }
                else if (nowPositionItem === null) {
                    // 追加
                    this.focusAddGrid(positionX, positionY);
                }

                // 何帖か再集計
                this.itemAr[this.focusIndex].count = 0;
                this.itemAr[this.focusIndex].width = 0;
                this.itemAr[this.focusIndex].height = 0;
                var minX = this.itemAr[this.focusIndex].data.length;
                var minY = this.itemAr[this.focusIndex].data[0].length;

                for (var i = 0; i < this.itemAr[this.focusIndex].data.length; i++) {
                    for (var j = 0; j < this.itemAr[this.focusIndex].data[i].length; j++) {
                        if (this.itemAr[this.focusIndex].data[i][j] === 1) {
                            this.itemAr[this.focusIndex].count += this.grSepUnit / (this.grSplitMax * this.grSplitMax);
                            if (this.itemAr[this.focusIndex].width * this.grSplitMax < i + 1) {
                                this.itemAr[this.focusIndex].width = (i + 1) / this.grSplitMax;
                            }
                            if (this.itemAr[this.focusIndex].height * this.grSplitMax < j + 1) {
                                this.itemAr[this.focusIndex].height = (j + 1) / this.grSplitMax;
                            }
                            if (minX > i) {
                                minX = i;
                            }
                            if (minY > j) {
                                minY = j;
                            }
                        }
                    }
                }

                // count四捨五入
                this.itemAr[this.focusIndex].count = Math.round(this.itemAr[this.focusIndex].count * 10000) / 10000;

                // 配列除去
                if (minX >= this.grSplitMax) {
                    this.itemAr[this.focusIndex].x += Math.floor(minX / this.grSplitMax);
                    this.itemAr[this.focusIndex].width -= Math.floor(minX / this.grSplitMax);
                    this.itemAr[this.focusIndex].data.splice(0, Math.floor(minX / this.grSplitMax) * this.grSplitMax);
                    // 開口部の位置ずらし
                    for (var i = 0; i < this.openingAr.length; i++) {
                        if (this.openingAr[i].roomId === this.focusIndex) {
                            this.openingAr[i].x -= Math.floor(minX / this.grSplitMax);
                        }
                    }
                }
                if (minY >= this.grSplitMax) {
                    this.itemAr[this.focusIndex].y += Math.floor(minY / this.grSplitMax);
                    this.itemAr[this.focusIndex].height -= Math.floor(minY / this.grSplitMax);
                    for (var i = 0; i < this.itemAr[this.focusIndex].data.length; i++) {
                        this.itemAr[this.focusIndex].data[i].splice(0, Math.floor(minY / this.grSplitMax) * this.grSplitMax);
                    }
                    // 開口部の位置ずらし
                    for (var i = 0; i < this.openingAr.length; i++) {
                        if (this.openingAr[i].roomId === this.focusIndex) {
                            this.openingAr[i].y -= Math.floor(minY / this.grSplitMax);
                        }
                    }
                }
                this.updateInfo();
                this.draw();
            }
        };

        /** フォーカスしているアイテムを移動
         * {int} vectorX x座標の移動数（グリッド単位）
         * {int} vectorY y座標の移動数（グリッド単位）
         * return なし
         */
        this.focusModeGrid = function (vectorX, vectorY) {
            vectorX /= this.grSplit;
            vectorY /= this.grSplit;

            if (this.focusIndex !== null && !this.checkSelectOpening()) {
                if (vectorX === 0 && vectorY === 0) return;
                if (this.itemAr[this.focusIndex].x + vectorX < 0) return;
                if (this.itemAr[this.focusIndex].y + vectorY < 0) return;

                // 辺り判定チェック
                if (this.checkItemOverlap(this.itemAr[this.focusIndex].x + vectorX, this.itemAr[this.focusIndex].y + vectorY, this.focusIndex) !== null) {
                    return;
                }

                // アイテムの座標更新
                this.itemAr[this.focusIndex].x += vectorX;
                this.itemAr[this.focusIndex].y += vectorY;

                // 画面更新
                this.draw();
            }
        };

        /** フォーカスしているアイテムのグリッド追加（チェック込み）
         * {int} positionX x座標（data単位、フォーカスのx起点）
         * {int} positionY y座標（data単位、フォーカスのy起点）
         * return なし
         */
        this.focusAddGrid = function (positionX, positionY) {
            if (this.focusIndex !== null) {
                if (positionX < -1 * this.grSplitMax / this.grSplit || positionY < -1 * this.grSplitMax / this.grSplit) {
                    return;
                }
                if (positionX >= this.itemAr[this.focusIndex].width * this.grSplitMax + 1 ||
                    positionY >= this.itemAr[this.focusIndex].height * this.grSplitMax + 1) {
                    return;
                }
                if (positionX === this.itemAr[this.focusIndex].width * this.grSplitMax + 1 &&
                    positionY === this.itemAr[this.focusIndex].height * this.grSplitMax + 1) {
                    return;
                }

                // 追加予定の範囲に別の部屋がないかチェック
                var x = this.itemAr[this.focusIndex].x + 1.0 * positionX / this.grSplitMax;
                var y = this.itemAr[this.focusIndex].y + 1.0 * positionY / this.grSplitMax;
                for (var i = 1; i < this.grSplitMax / this.grSplit; i++) {
                    for (var j = 1; j < this.grSplitMax / this.grSplit; j++) {
                        if (this.checkItemPxNotWall((x + i / this.grSplitMax) * this.grSep, (y + j / this.grSplitMax) * this.grSep, this.focusIndex) !== null) return;
                    }
                }


                if (positionX < 0) {
                    if (positionY >= 0) {
                        if (this.checkGridContact(positionX, positionY) > 0 ||
                            this.checkGridContact(positionX + (this.grSplitMax / this.grSplit - 1), positionY) > 0 ||
                            this.checkGridContact(positionX, positionY + (this.grSplitMax / this.grSplit - 1)) > 0 ||
                            this.checkGridContact(positionX + (this.grSplitMax / this.grSplit - 1), positionY + (this.grSplitMax / this.grSplit - 1)) > 0) {
                            this.itemAr[this.focusIndex].x -= 1 / this.grSplit;
                            this.itemAr[this.focusIndex].width += 1 / this.grSplit;
                            for (var i = 0; i < this.grSplitMax / this.grSplit; i++) {
                                this.itemAr[this.focusIndex].data.unshift([]);
                            }
                            // 開口部の位置ずらし
                            for (var i = 0; i < this.openingAr.length; i++) {
                                if (this.openingAr[i].roomId === this.focusIndex) {
                                    this.openingAr[i].x += 1 / this.grSplit;
                                }
                            }

                            this.setFocusItemData(0, positionY, 1);
                        }
                    }
                }
                else if (positionY < 0) {
                    if (this.checkGridContact(positionX, positionY) > 0 ||
                        this.checkGridContact(positionX, positionY + (this.grSplitMax / this.grSplit - 1)) > 0 ||
                        this.checkGridContact(positionX + (this.grSplitMax / this.grSplit - 1), positionY) > 0 ||
                        this.checkGridContact(positionX + (this.grSplitMax / this.grSplit - 1), positionY + (this.grSplitMax / this.grSplit - 1)) > 0) {
                        this.itemAr[this.focusIndex].y -= 1 / this.grSplit;
                        this.itemAr[this.focusIndex].height += 1 / this.grSplit;
                        for (var i = 0; i < this.itemAr[this.focusIndex].data.length; i++) {
                            for (var j = 0; j < this.grSplitMax / this.grSplit; j++) {
                                this.itemAr[this.focusIndex].data[i].unshift(0);
                            }
                        }

                        // 開口部の位置ずらし
                        for (var i = 0; i < this.openingAr.length; i++) {
                            if (this.openingAr[i].roomId === this.focusIndex) {
                                this.openingAr[i].y += 1 / this.grSplit;
                            }
                        }

                        this.setFocusItemData(positionX, 0, 1);
                    }
                }
                else {
                    while (positionX >= this.itemAr[this.focusIndex].data.length) {// 配列追加
                        var addAr = [];
                        for (var k = 0; k < this.itemAr[this.focusIndex].data[0].length; k++) {
                            addAr[k] = 0;
                        }
                        this.itemAr[this.focusIndex].data[this.itemAr[this.focusIndex].data.length] = addAr;
                    }

                    if (this.itemAr[this.focusIndex].data[positionX][positionY] === 1) {
                        // 削除
                        if (this.itemAr[this.focusIndex].count > Math.round(0.5 / (this.grSplit * this.grSplit) * 10000) / 10000) {

                            if (this.checkDelPart(positionX, positionY)) {
                                this.setFocusItemData(positionX, positionY, 0);

                                // 関連する開口部も削除
                                for (var i = 0; i < this.openingAr.length; i++) {
                                    if (this.openingAr[i].roomId === this.focusIndex) {
                                        if (this.openingAr[i].x >= positionX && this.openingAr[i].x < positionX + 1) {
                                            if (this.openingAr[i].y >= positionY && this.openingAr[i].y < positionY + 1) {
                                                this.openingAr.splice(i, 1);
                                                i--;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        // 追加
                        if (this.checkGridContact(positionX, positionY) > 0) {
                            this.setFocusItemData(positionX, positionY, 1);
                        }
                        else if (this.checkGridContact(positionX + this.grSplitMax / this.grSplit, positionY + this.grSplitMax / this.grSplit) > 0) {
                            this.setFocusItemData(positionX, positionY, 1);
                        }
                    }
                }
            }
        };

        /** 現在フォーカス中のアイテムを右回転する
         * return なし
         */
        this.focusRotate = function () {
            if (this.checkSelectSink()) {
                this.sinkAr[this.focusIndex].angle = (this.sinkAr[this.focusIndex].angle + 90) % 360;
                var tmp = this.sinkAr[this.focusIndex].width;
                this.sinkAr[this.focusIndex].width = this.sinkAr[this.focusIndex].height;
                this.sinkAr[this.focusIndex].height = tmp;
                this.draw();
            }
            else if (!this.rotateRoom(this.focusIndex)) {
                window.console.log("error:" + this.itemAr[this.focusIndex].name + "の右回転失敗");
            }
        };

        /** 現在フォーカス中のアイテムにデータをセットする
         * {int} positionX x座標（data単位、フォーカスのx起点）
         * {int} positionY y座標（data単位、フォーカスのy起点）
         * {int} value データにセットする値
         * return なし
         */
        this.setFocusItemData = function (positionX, positionY, value) {
            if (this.focusIndex !== null) {
                for (var i = 0; i < this.grSplitMax / this.grSplit; i++) {
                    for (var j = 0; j < this.grSplitMax / this.grSplit; j++) {
                        // x座標を伸ばす
                        while (this.itemAr[this.focusIndex].data.length <= positionX + i) {
                            var addAr = [];
                            for (var k = 0; k < this.itemAr[this.focusIndex].data[0].length; k++) {
                                addAr[k] = 0;
                            }

                            this.itemAr[this.focusIndex].data.push(addAr);
                        }

                        this.itemAr[this.focusIndex].data[positionX + i][positionY + j] = value;
                    }
                }

                // 範囲内の開口部除去
                for (var i = this.openingAr.length - 1; i >= 0; i--) {
                    if (value === 1 && this.openingAr[i].type !== 20) continue;

                    try {
                        var x = positionX / this.grSplitMax + this.itemAr[this.focusIndex].x;
                        var y = positionY / this.grSplitMax + this.itemAr[this.focusIndex].y;

                        if (!(x >= this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x + this.openingAr[i].width ||
                                x + 1 / this.grSplit <= this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x ||
                                y >= this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y + this.openingAr[i].height ||
                                y + 1 / this.grSplit <= this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y)) {
                            // 重なる場合、除去
                            this.openingAr.splice(i, 1);
                        }
                    } catch (e) {
                        window.console.log(e);
                    }
                }

                // 柱の除去・追加
                var createOpeningAr = [];
                for (var i = 0; i < 4; i++) {
                    var x = Math.floor(i / 2) * (this.grSplitMax / this.grSplit - 1) + positionX;
                    var y = (i % 2) * (this.grSplitMax / this.grSplit - 1) + positionY;
                    var isCheck = true;

                    // 上下左右検索
                    for (var j = 0; j < 4; j++) {
                        var pX = Math.floor(j / 2);
                        var pY = (j % 2);

                        if (pX === pY) {
                            if (pX === 0) pX = -1;
                            if (pX === 1) {
                                pX = 0;
                                pY = -1;
                            }
                        }

                        try {
                            var checkValue = this.itemAr[this.focusIndex].data[x + pX][y + pY];

                            if ((value === 1 && value !== checkValue) ||
                                (value !== 1 && 1 === checkValue)) {
                                isCheck = false;
                            }
                        }
                        catch (e) {
                            if (value === 1) {
                                isCheck = false;
                            }
                        }

                        if (!isCheck) break;
                    }

                    var setPositionX = (this.itemAr[this.focusIndex].x + positionX / this.grSplitMax + Math.floor(i / 2) / this.grSplit) * this.grSep;
                    var setPositionY = (this.itemAr[this.focusIndex].y + positionY / this.grSplitMax + (i % 2) / this.grSplit) * this.grSep;

                    // 四捨五入
                    setPositionX = Math.round(setPositionX * 10000) / 10000;
                    setPositionY = Math.round(setPositionY * 10000) / 10000;

                    if (!isCheck) {
                        // 柱追加
//                window.console.log("create:"+setPositionX+","+setPositionY);
                        var newOpening = this.createCheckRoomPositionOpening(setPositionX, setPositionY, 20, this.focusIndex);
                        createOpeningAr.push([x, y, newOpening]);
                        this.addOpening(newOpening);
                    }
                    else {
                        // 柱削除
//                window.console.log("del:"+setPositionX+","+setPositionY);
                        var positionOpeningIndex = this.checkOpeningPx(setPositionX, setPositionY);

                        while (positionOpeningIndex !== null) {
                            if (this.openingAr[positionOpeningIndex].type !== 20) {
                                positionOpeningIndex = this.checkOpeningPx(setPositionX, setPositionY, positionOpeningIndex);
                            }
                            else {
                                break;
                            }
                        }

                        if (positionOpeningIndex !== null) {
                            if (this.openingAr[positionOpeningIndex].type === 20) {
                                // 削除
                                this.openingAr.splice(positionOpeningIndex, 1);
                            }
                        }
                    }
                }

                // 半畳間隔じゃないもので端に配置されていない場合は、削除
                /*
        for (var i = 0; i < createOpeningAr.length; i++) {

            // 上下検索
            var checkCount = 0;
            for (var k = 0; k < 2; k++) {
                var pX = 0;
                var pY = k * 2 - 1;

                try {
                    var checkValue = this.itemAr[this.focusIndex].data[createOpeningAr[i][0]+pX][createOpeningAr[i][1]+pY];

                    if ((value == 1 && value != checkValue) ||
                        (value != 1 && 1 == checkValue)) {
                        checkCount++;
                    }
                }
                catch(e) {
                    if (value == 1) {
                        checkCount++;
                    }
                }
            }

            if (checkCount == 0) {
                // y方向の柱をチェック
                var pillerAr = this.checkWallPiller((this.itemAr[createOpeningAr[i][2].roomId]+createOpeningAr[i][2].x+createOpeningAr[i][2].width/2)*this.grSep,-1,this.focusIndex);

                var ue = -1;
                var sita = -1;
                // 直近の柱二つを取得
                for (var j = 0; j < pillerAr.length; j++) {
                    if (pillerAr.indexOf(createOpeningAr[i][2]) == j) continue;
                    var dif = createOpeningAr[i][2].y - pillerAr[j].y;
                    if (dif == 0) {}
                    else if (dif > 0) {
                        if (ue == -1) ue = j;
                        else if (pillerAr[ue].y  < pillerAr[j].y) ue = j;
                    }
                    else {
                        if (sita == -1) sita = j;
                        else if (pillerAr[sita].y  > pillerAr[j].y) sita = j;
                    }
                }

                if (sita != -1 && ue != -1 && pillerAr[sita].y - pillerAr[ue].y <= 1) {
  window.console.log("check:"+(pillerAr[sita].y - pillerAr[ue].y));

                    // 削除
                    var delId = this.openingAr.indexOf(createOpeningAr[i][2]);
                    this.openingAr.splice(delId,1);
                    window.console.log("del:"+pillerAr[sita].y +"?"+ pillerAr[ue].y);
                    continue;
                }
            }

            // 左右検索
            var checkCount = 0;
            for (var k = 0; k < 2; k++) {
                var pX = k * 2 - 1;
                var pY = 0;

                try {
                    var checkValue = this.itemAr[this.focusIndex].data[createOpeningAr[i][0]+pX][createOpeningAr[i][1]+pY];

                    if ((value == 1 && value != checkValue) ||
                        (value != 1 && 1 == checkValue)) {
                        checkCount++;
                    }
                }
                catch(e) {
                    if (value == 1) {
                        checkCount++;
                    }
                }
            }

            if (checkCount == 0) {
                // x方向の柱をチェック
                var pillerAr = this.checkWallPiller(-1,(this.itemAr[createOpeningAr[i][2].roomId]+createOpeningAr[i][2].y+createOpeningAr[i][2].height/2)*this.grSep,this.focusIndex);

                var migi = -1;
                var hidari = -1;
                // 直近の柱二つを取得
                for (var j = 0; j < pillerAr.length; j++) {
                    if (pillerAr.indexOf(createOpeningAr[i][2]) == j) continue;
                    var dif = createOpeningAr[i][2].x - pillerAr[j].x;
                    if (dif == 0) {}
                    else if (dif > 0) {
                        if (hidari == -1) hidari = j;
                        else if (pillerAr[hidari].x  < pillerAr[j].x) hidari = j;
                    }
                    else {
                        if (migi == -1) migi = j;
                        else if (pillerAr[migi].x  > pillerAr[j].x) migi = j;
                    }
                }

                if (migi != -1 && hidari != -1 && pillerAr[migi].x - pillerAr[hidari].x <= 1) {
                    // 削除
                    var delId = this.openingAr.indexOf(createOpeningAr[i][2]);
                    this.openingAr.splice(delId,1);
                    window.console.log("del:"+pillerAr[migi].x  +"?"+  pillerAr[hidari].x);
                    continue;
                }
            }
        }
    */
            }
        };

        /** 指定した部屋の柱抜き処理（壁を除く）
         * {int} roomId 部屋ID
         * return なし
         */
        this.ventPillar = function (roomId) {
            // 全く同じ位置に柱がある場合、除去
            for (var i = 0; i < this.openingAr.length; i++) {
                for (var j = this.openingAr.length - 1; j > i; j--) {
                    if (this.openingAr[i].type === this.openingAr[j].type &&
                        this.openingAr[i].roomId === this.openingAr[j].roomId) {

                        // 四捨五入してxとy座標が一致する場合
                        this.openingAr[i].x = Math.round(this.openingAr[i].x * 10000) / 10000;
                        this.openingAr[i].y = Math.round(this.openingAr[i].y * 10000) / 10000;
                        this.openingAr[j].x = Math.round(this.openingAr[j].x * 10000) / 10000;
                        this.openingAr[j].y = Math.round(this.openingAr[j].y * 10000) / 10000;

                        if (this.openingAr[i].x === this.openingAr[j].x &&
                            this.openingAr[i].y === this.openingAr[j].y) {
                            this.openingAr.splice(j, 1);
                        }
                    }
                }
            }

            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i] === null) continue;
                if (this.openingAr[i].type !== 20) continue;
                if (this.itemAr[this.openingAr[i].roomId] === null) continue;

                // 壁に接しているかチェック
                if (this.checkRoomWallOpening(roomId, i)) continue;

                // 左右方向チェック
                var checkLeftRight = this.checkWallPiller(-1, (this.itemAr[this.openingAr[i].roomId].y + this.openingAr[i].y + this.openingAr[i].height / 2) * this.grSep, roomId);

                var checkStartIndex = 0;
                for (var j = 1; j < checkLeftRight.length; j++) {
                    var checkStartX = checkLeftRight[checkStartIndex].x + this.itemAr[checkLeftRight[checkStartIndex].roomId].x;
                    var checkNowX = checkLeftRight[j].x + this.itemAr[checkLeftRight[j].roomId].x;

                    if (checkStartX < Math.round((checkNowX - 3) * 10000) / 10000) {
                        if (checkStartIndex < j - 1) {
                            // 間の柱を除去
                            for (var k = (checkStartIndex + 1); k < (j - 1); k++) {
                                var index = this.openingAr.indexOf(checkLeftRight[k]);
                                if (index >= 0 && !this.checkRoomWallOpening(roomId, index)) {
                                    this.openingAr[index] = null;
                                }
                            }
                        }
                        checkStartIndex = j - 1;
                    }
                }
                if (checkStartIndex < checkLeftRight.length - 2) {
                    // 間の柱を除去
                    for (var k = (checkStartIndex + 1); k < (checkLeftRight.length - 1); k++) {
                        var index = this.openingAr.indexOf(checkLeftRight[k]);
                        if (index >= 0 && !this.checkRoomWallOpening(roomId, index)) {
                            this.openingAr[index] = null;
                        }
                    }
                }

                if (this.openingAr[i] === null) continue;

                // 上下方向チェック
                var checkUpDown = this.checkWallPiller((this.itemAr[this.openingAr[i].roomId].x + this.openingAr[i].x + this.openingAr[i].width / 2) * this.grSep, -1, roomId);

                var checkStartIndex = 0;
                for (var j = 1; j < checkUpDown.length; j++) {
                    var checkStartY = checkUpDown[checkStartIndex].y + this.itemAr[checkUpDown[checkStartIndex].roomId].y;
                    var checkNowY = checkUpDown[j].y + this.itemAr[checkUpDown[j].roomId].y;

                    if (checkStartY < Math.round((checkNowY - 3) * 10000) / 10000) {
                        if (checkStartIndex < j - 1) {
                            // 間の柱を除去
                            for (var k = (checkStartIndex + 1); k < (j - 1); k++) {
                                var index = this.openingAr.indexOf(checkUpDown[k]);
                                if (index >= 0 && !this.checkRoomWallOpening(roomId, index))
                                    this.openingAr[index] = null;
                            }
                        }
                        checkStartIndex = j - 1;
                    }
                }

                if (checkStartIndex < checkUpDown.length - 2) {
                    // 間の柱を除去
                    for (var k = (checkStartIndex + 1); k < (j - 1); k++) {
                        var index = this.openingAr.indexOf(checkUpDown[k]);
                        if (index >= 0 && !this.checkRoomWallOpening(roomId, index))
                            this.openingAr[index] = null;
                    }
                }
            }

            // null除去
            for (var i = this.openingAr.length - 1; i >= 0; i--) {
                if (this.openingAr[i] === null) this.openingAr.splice(i, 1);
            }
        };

        /** 指定した部屋の壁にある柱を取得
         * {int} positionX x座標（px単位）  （-1を指定した場合、何でも値が入るものとする）
         * {int} positionY y座標（px単位）  （-1を指定した場合、何でも値が入るものとする）
         * {int} roomId 部屋ID
         * return 柱情報の配列
         */
        this.checkWallPiller = function (x, y, roomId) {
            // 四捨五入
            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;

            var pillerAr = [];

            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i] === null) continue;
                if (this.openingAr[i].type !== 20) continue;
                if (this.itemAr[this.openingAr[i].roomId] === null) continue;

                var openingX = Math.round((this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x + this.openingAr[i].width / 2) * this.grSep * 10000) / 10000;
                var openingY = Math.round((this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y + this.openingAr[i].height / 2) * this.grSep * 10000) / 10000;

                // 部屋の範囲内かチェック
                var checkRoom = this.openingAr[i].roomId;
                if (this.checkItemPxWithRoom(openingX, openingY, roomId)) {
                    if (x !== -1) {
                        // x軸方向をチェック
                        if (openingX !== x) {
                            continue;
                        }
                    }
                    if (y !== -1) {
                        // y軸方向をチェック
                        if (openingY !== y) {
                            continue;
                        }
                    }

                    // 範囲内のため、配列に追加
                    pillerAr.push(this.openingAr[i]);
                }
            }

            // 左上から昇順ソート
            pillerAr.sort(function (a, b) {
                var openingXa = Math.round((a.x + dragRoom.DRAG.itemAr[a.roomId].x) * 10000) / 10000;
                var openingYa = Math.round((a.y + dragRoom.DRAG.itemAr[a.roomId].y) * 10000) / 10000;

                var openingXb = Math.round((b.x + dragRoom.DRAG.itemAr[b.roomId].x) * 10000) / 10000;
                var openingYb = Math.round((b.y + dragRoom.DRAG.itemAr[b.roomId].y) * 10000) / 10000;

                if (openingXa < openingXb || openingYa < openingYb) return -1;
                if (openingXa > openingXb || openingYa > openingYb) return 1;
                return 0;
            });
            return pillerAr;
        };

        /** 指定した部屋の壁に開口部があるかチェック
         * {int} roomId 部屋ID
         * {int} openingId 開口部ID
         * return 指定した部屋の壁に開口部がある場合、true　ない場合、false
         */
        this.checkRoomWallOpening = function (roomId, openingId) {
            if (this.openingAr[openingId] === null) return false;
            if (this.itemAr[this.openingAr[openingId].roomId] === null) return false;
            var openingX = (this.openingAr[openingId].x + this.itemAr[this.openingAr[openingId].roomId].x) * this.grSep;
            var openingY = (this.openingAr[openingId].y + this.itemAr[this.openingAr[openingId].roomId].y) * this.grSep;
            var openingWidth = this.openingAr[openingId].width * this.grSep;
            var openingHeight = this.openingAr[openingId].height * this.grSep;

            // 開口部が部屋の範囲内にあるかどうかをチェック
            if (!this.checkItemPxWithRoom(openingX + openingWidth / 2, openingY + openingHeight / 2, roomId)) return false;

            // 4点が全て部屋の範囲内であれば、壁でないと判定する

            // 部屋の範囲内かチェック
            if (this.checkItemPxWithRoomNotWall(openingX, openingY, roomId) &&
                this.checkItemPxWithRoomNotWall(openingX + openingWidth, openingY, roomId) &&
                this.checkItemPxWithRoomNotWall(openingX, openingY + openingHeight, roomId) &&
                this.checkItemPxWithRoomNotWall(openingX + openingWidth, openingY + openingHeight, roomId)) {

                return false;
            }
            return true;
        };

        /** 指定位置のグリッド同士の接点を返す
         * {int} positionX x座標（data単位、フォーカスのx起点）
         * {int} positionY y座標（data単位、フォーカスのy起点）
         * {int} roomId 部屋のindex(nullの場合はフォーカスを使用)
         * return dataの指定位置の上下左右のデータが1の数
         */
        this.checkGridContact = function (positionX, positionY, roomId) {
            if (roomId === undefined || roomId === null) roomId = this.focusIndex;

            var count = 0;
            for (var i = 0; i < 4; i++) {
                var x = positionX + Math.floor(-1 * i / 2 + 1);
                var y = positionY;
                if (x === positionX) {
                    var y = positionY + (1 - 2 * (i % 2));
                }

                if (x >= 0 && y >= 0 && this.itemAr[roomId].data.length > x) {
                    if (this.itemAr[roomId].data[x][y] === 1) {
                        count++;
                    }
                }
            }
            return count;
        };

        /** dataの指定位置が削除可能かチェック
         * {int} positionX x座標（data単位、フォーカスのx起点）
         * {int} positionY y座標（data単位、フォーカスのy起点）
         * return dataの指定位置が削除可能ならばtrue、不可能ならばfalse
         */
        this.checkDelPart = function (positionX, positionY) {
            var data = $.extend(true, {}, this.itemAr[this.focusIndex].data);

            var checkCount = 0;
            var changeCount = 0;

            if (positionX < 0 || positionY < 0 || positionX >= data.length || positionY >= data[positionX].length) {
                return false;
            }

            if (data[positionX][positionY] === 1) {
                for (var i = 0; i < this.grSplitMax / this.grSplit; i++) {
                    for (var j = 0; j < this.grSplitMax / this.grSplit; j++) {
                        try {
                            if (data[positionX + i][positionY + j] === 1) {
                                data[positionX + i][positionY + j] = 2;
                                changeCount++;
                            }
                        }
                        catch (e) {
                        }
                    }
                }

                if (checkCount === 0) {
                    // 上
                    checkCount = this.checkPartCount(positionX, positionY - 1, data);
                }
                if (checkCount === 0) {
                    // 下
                    checkCount = this.checkPartCount(positionX, positionY + this.grSplitMax / this.grSplit + 1, data);
                }
                if (checkCount === 0) {
                    // 左
                    checkCount = this.checkPartCount(positionX - 1, positionY, data);
                }
                if (checkCount === 0) {
                    // 右
                    checkCount = this.checkPartCount(positionX + this.grSplitMax / this.grSplit + 1, positionY, data);
                }
            }

            if (checkCount >= this.itemAr[this.focusIndex].count * 2 * this.grSplitMax * this.grSplitMax - 1 - changeCount) {
                return true;
            }

            return false;
        };

        /** 指定位置のグリッド同士の接する数を返す
         * {int} positionX x座標（data単位、フォーカスのx起点）
         * {int} positionY y座標（data単位、フォーカスのy起点）
         * {int} data チェック中のdata情報
         * return 接する数
         */
        this.checkPartCount = function (positionX, positionY, data) {

            var checkCount = 0;

            if (positionX < 0 || positionY < 0 || positionX >= data.length) {
                return 0;
            }

            if (data[positionX] === undefined || positionY >= data[positionX].length) {
                return 0;
            }

            if (data[positionX][positionY] === 1) {
                data[positionX][positionY] = 2;
                checkCount++;

                // 上
                checkCount += this.checkPartCount(positionX, positionY - 1, data);
                // 下
                checkCount += this.checkPartCount(positionX, positionY + 1, data);
                // 右
                checkCount += this.checkPartCount(positionX - 1, positionY, data);
                // 左
                checkCount += this.checkPartCount(positionX + 1, positionY, data);
            }

            return checkCount;
        };

        /** フォーカスがあるアイテムを除去
         * return なし
         */
        this.focusDelete = function () {
            if (this.focusIndex !== null) {
                if (this.checkSelectOpening()) {
                    this.openingAr.splice(this.focusIndex, 1);
                }
                else if (this.checkSelectSink()) {
                    this.sinkAr.splice(this.focusIndex, 1);
                }
                else {
                    this.itemAr.splice(this.focusIndex, 1);
                    for (var i = this.openingAr.length - 1; i >= 0; i--) {
                        if (this.openingAr[i].roomId === this.focusIndex) {
                            this.openingAr.splice(i, 1);
                        }
                        else if (this.openingAr[i].roomId > this.focusIndex) {
                            this.openingAr[i].roomId -= 1;
                        }
                    }
                    for (var i = this.sinkAr.length - 1; i >= 0; i--) {
                        if (this.sinkAr[i].roomId === this.focusIndex) {
                            this.sinkAr.splice(i, 1);
                        }
                        else if (this.sinkAr[i].roomId > this.focusIndex) {
                            this.sinkAr[i].roomId -= 1;
                        }
                    }
                }
                this.focusIndex = null;
                this.updateInfo();
                this.editMode = false;
                this.draw();
            }
        };

        /** フォーカスがある開口部を反転する
         * return なし
         */
        this.focusFlip = function (val) {
            if (this.focusIndex != null) {
                if (this.checkSelectOpening()) {
                    if ((this.openingAr[this.focusIndex].width == this.wallSize && val === 0) ||
                        (this.openingAr[this.focusIndex].height == this.wallSize && val !== 0)) {
                        this.openingAr[this.focusIndex].flip = !this.openingAr[this.focusIndex].flip;
                    }
                    else {
                        this.openingAr[this.focusIndex].hFlip = !this.openingAr[this.focusIndex].hFlip;
                    }
                }
            }

            this.draw();
        };

        /** 現在開口部を選択しているか否か
         * return 開口部編集を選択している場合はtrue それ以外はfalse
         */
        this.checkSelectOpening = function () {
            if ($('#tab002').hasClass('active')) {
                return true;
            }
            return false;
        };

        /** 現在シンクを選択しているか否か
         * return シンク編集を選択している場合はtrue それ以外はfalse
         */
        this.checkSelectSink = function () {
            if ($('#tab003').hasClass('active')) {
                return true;
            }
            return false;
        };

        /** 画面表示
         * return なし
         */
        this.draw = function () {
            var isSelectOpening = this.checkSelectOpening();
            var isSelectSink = this.checkSelectSink();

            // 画面を初期化
            this.blank();
            // アイテム配置
            for (var i = 0; i < this.itemAr.length; i++) {
                this.drawRoom(i);
            }

            // フォーカスを最前面に再描画
            if (this.focusIndex != null && !(isSelectOpening || isSelectSink)) {
                for (var i = 0; i < this.focusIndexAr.length; i++) {
                    this.focusIndex = this.focusIndexAr[i];
                    this.drawRoom(this.focusIndex);
                }
                this.focusIndex = this.focusIndexAr[0];
            }

            // シンク描画
            if (this.sinkAr != null) {
                for (var i = 0; i < this.sinkAr.length; i++) {
                    this.drawSink(i);
                }
            }

            // 開口部描画
            for (var i = 0; i < this.openingAr.length; i++) {
                this.drawOpening(i);
            }

            // 部屋のオプションを描画
            if (this.displayMode == 1) {
                // フォーカス時
                if (this.focusIndex != null && !(isSelectOpening || isSelectSink)) {
                    for (var i = 0; i < this.focusIndexAr.length; i++) {
                        this.focusIndex = this.focusIndexAr[i];
                        this.drawRoomOption(this.focusIndex);
                    }
                    this.focusIndex = this.focusIndexAr[0];
                }

                // 未フォーカス時
                for (var i = 0; i < this.itemAr.length; i++) {
                    if (this.focusIndexAr.indexOf(i) < 0) this.drawRoomOption(i);
                }
            }
            else if (this.displayMode == 0) {
                // フォーカス時
                if (this.focusIndex != null && !(isSelectOpening || isSelectSink)) {
                    for (var i = 0; i < this.focusIndexAr.length; i++) {
                        this.focusIndex = this.focusIndexAr[i];
                        this.drawRoomOption(this.focusIndex);
                    }
                    this.focusIndex = this.focusIndexAr[0];
                }
            }

            // ツールボックス表示切り替え
            var popup = document.getElementById('popup-tools');
            if (popup) {
                if (this.focusIndex != null && !dragRoom.drag.now) {
                    popup.style.display = 'block';
                    if (isSelectSink) {
                        popup.style.left = (this.itemAr[this.sinkAr[this.focusIndex].roomId].x + this.sinkAr[this.focusIndex].x) * this.grSep - $('#popup-tools').width() - 20 + 'px';
                        popup.style.top = (this.itemAr[this.sinkAr[this.focusIndex].roomId].y + this.sinkAr[this.focusIndex].y) * this.grSep - $('#popup-tools').height() - 20 + 'px';
                        popup.style.width = '';
                    }
                    else if (isSelectOpening) {
                        popup.style.left = (this.itemAr[this.openingAr[this.focusIndex].roomId].x + this.openingAr[this.focusIndex].x) * this.grSep - $('#popup-tools').width() - 20 + 'px';
                        popup.style.top = (this.itemAr[this.openingAr[this.focusIndex].roomId].y + this.openingAr[this.focusIndex].y) * this.grSep - $('#popup-tools').height() - 20 + 'px';
                        popup.style.width = '246px';
                    }
                    else {
                        popup.style.left = this.itemAr[this.focusIndex].x * this.grSep - $('#popup-tools').width() - 20 + 'px';
                        popup.style.top = this.itemAr[this.focusIndex].y * this.grSep - $('#popup-tools').height() - 20 + 'px';
                        popup.style.width = '';
                    }
                }
                else {
                    popup.style.display = 'none';
                }
            }
        };

        /** 部屋を描く
         * {int} index 部屋番号
         * return なし
         */
        this.drawRoom = function (index) {
            if (this.itemAr[index] == null) return;
            if (this.itemAr[index].width <= 0.0001 && this.itemAr[index].height <= 0.0001) return;

            var x = this.itemAr[index].x * this.grSep - this.startGridX * this.grSep;
            var y = this.itemAr[index].y * this.grSep - this.startGridY * this.grSep;
            var width = this.itemAr[index].width * this.grSep;
            var height = this.itemAr[index].height * this.grSep;

            this.ctx2.save();
            this.ctx2.globalAlpha = this.madoriAlpha;

            // 部屋タイプにより背景色を変える
            if (this.itemAr[index].type == 2) {// 浴室
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.type2Img, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 4) {
                // UB

                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.ubImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 13) {// 階段
                if (width > height) {
                    //背景画像とその繰り返し方法を指定する
                    var pattern = this.ctx2.createPattern(this.typeKaidanRightImg, 'repeat');
                    //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                    this.ctx2.fillStyle = pattern;
                }
                else {
                    //背景画像とその繰り返し方法を指定する
                    var pattern = this.ctx2.createPattern(this.typeKaidanImg, 'repeat');
                    //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                    this.ctx2.fillStyle = pattern;
                }

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 1 || this.itemAr[index].type == 3) {
                // 洋式トイレ・洗面脱衣所

                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.toiletImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 20) {
                // 和式トイレ

                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.toilet2Img, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type >= 9 && this.itemAr[index].type <= 10) {
                // 押入・物入

                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.oshiireImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 7) {
                // 床の間

                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.tokonomaImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 8) {
                // クローゼット

                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.closetImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 5) {// 和室
                // 畳

                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.type5Img, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 11) {// 玄関
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.type11Img, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 22) {
                // ベランダ
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.verandaImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 23) {
                // 車庫
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.shakoImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 24) {
                // 室内ガレージ
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.garageImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 12) {
                // 廊下
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.roadImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 30) {
                // 土間
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.domaImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 31) {
                // 納戸
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.nayaImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 32) {
                // 縁側
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.engawaImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 33) {
                // 店舗
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.tenpoImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else if (this.itemAr[index].type == 91) {
                // 吹き抜け
                this.ctx2.fillStyle = '#FFF';
            }
            else if (this.itemAr[index].type == 92) {
                // 中庭
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.niwaImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }
            else {
                // フローリング
//        this.ctx2.fillStyle = '#FFF5E0';
                //背景画像とその繰り返し方法を指定する
                var pattern = this.ctx2.createPattern(this.flooringImg, 'repeat');
                //上で指定した背景パターン内容を塗りつぶしスタイルに代入する
                this.ctx2.fillStyle = pattern;

                this.ctx2.translate(x, y);
                x = 0;
                y = 0;
            }

            if (!this.checkSelectOpening()) {
                this.ctx2.strokeStyle = '#666';
            }
            else {
                this.ctx2.strokeStyle = '#888';
            }
            this.ctx2.lineCap = "square";
            this.ctx2.lineWidth = this.wallSize * this.grSep;

            if (this.focusIndex == index && !(this.checkSelectOpening() || this.checkSelectSink())) {
                if (this.displayMode == 0) {
                    this.ctx2.strokeStyle = 'red';
                }
                else {
                    this.ctx2.strokeStyle = '#e3439d';
                }
            }
            if (this.editMode) {
                if (this.focusIndex == index && !(this.checkSelectOpening() || this.checkSelectSink())) {
                    this.ctx2.strokeStyle = '#666';
                    this.ctx2.fillStyle = 'red';
                }
                else {
                    this.ctx2.strokeStyle = '#DDD';
                }
            }


            this.ctx2.beginPath();
            for (var i = 0; i < this.itemAr[index].data.length; i++) {
                for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                    if (this.itemAr[index].data[i][j] == 1) {
                        var positionX = x + i * this.grSep / this.grSplitMax;
                        var positionY = y + j * this.grSep / this.grSplitMax;
                        this.ctx2.moveTo(positionX, positionY);
                        this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                        this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                        this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY);
                        this.ctx2.closePath();
                    }
                }
            }
//    this.ctx2.stroke();
            this.ctx2.fill();

            if (this.itemAr[index].type == 91) {
                this.ctx2.lineWidth = this.grWidth * 4;

                var startX1 = -1;
                var startY1 = -1;
                var endX1 = -1;
                var endY1 = -1;
                var startX2 = -1;
                var startY2 = -1;
                var endX2 = -1;
                var endY2 = -1;

                for (var i = 0; i < this.itemAr[index].data.length; i++) {
                    for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                        if (this.itemAr[index].data[i][j] == 1) {
                            var positionX = x + i * this.grSep / this.grSplitMax;
                            var positionY = y + j * this.grSep / this.grSplitMax;
                            // 吹き抜けの場合は対角線上に線を引く
                            if (width * j == height * i ||
                                ((j == height / (this.grSep / this.grSplitMax) - 1)
                                    && (i == width / (this.grSep / this.grSplitMax) - 1))) {
                                // 左斜め上
                                if (startX1 == -1) {
                                    startX1 = positionX;
                                    startY1 = positionY;
                                }
                                endX1 = positionX + this.grSep / this.grSplitMax;
                                endY1 = positionY + this.grSep / this.grSplitMax;
                            }
                            if (width * (height / (this.grSep / this.grSplitMax) - j) == height * i ||
                                (i == 0 && (j == height / (this.grSep / this.grSplitMax) - 1) ) ||
                                (j == 0 && (i == width / (this.grSep / this.grSplitMax) - 1) )) {
                                // 右斜め上
                                if (startX2 == -1) {
                                    startX2 = positionX;
                                    startY2 = positionY + this.grSep / this.grSplitMax;
                                }
                                endX2 = positionX + this.grSep / this.grSplitMax;
                                endY2 = positionY;
                            }
                        }
                    }
                }
                this.ctx2.beginPath();
                if (startX1 >= 0) {
                    this.ctx2.moveTo(startX1, startY1);
                    this.ctx2.lineTo(endX1, endY1);
                }
                if (startX2 >= 0) {
                    this.ctx2.moveTo(startX2, startY2);
                    this.ctx2.lineTo(endX2, endY2);
                }
                this.ctx2.stroke();
            }

            this.ctx2.lineWidth = this.wallSize * this.grSep;

            // 周りの枠描画
            for (var i = 0; i < this.itemAr[index].data.length; i++) {
                for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                    var positionX = x + i * this.grSep / this.grSplitMax;
                    var positionY = y + j * this.grSep / this.grSplitMax;

                    this.ctx2.beginPath();
                    if (this.itemAr[index].data[i][j] != 1) {
                        if (i > 0 && this.itemAr[index].data[i - 1][j] == 1) {
                            // 左描画
                            this.ctx2.moveTo(positionX, positionY);
                            this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                        }
                        if (j > 0 && this.itemAr[index].data[i][j - 1] == 1) {
                            // 上描画
                            this.ctx2.moveTo(positionX, positionY);
                            this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY);
                        }
                        if (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1][j] == 1) {
                            // 右描画
                            this.ctx2.moveTo(positionX + this.grSep / this.grSplitMax, positionY);
                            this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                        }
                        if (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] == 1) {
                            // 下描画
                            this.ctx2.moveTo(positionX, positionY + this.grSep / this.grSplitMax);
                            this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                        }
                    }
                    else {
                        if (i === 0 || (i > 0 && this.itemAr[index].data[i - 1].length <= j)) {
                            // 左描画
                            this.ctx2.moveTo(positionX, positionY);
                            this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                        }
                        if (j === 0) {
                            // 上描画
                            this.ctx2.moveTo(positionX, positionY);
                            this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY);
                        }
                        if (i == this.itemAr[index].width * this.grSplitMax - 1 ||
                            i == this.itemAr[index].data.length - 1 ||
                            (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1].length <= j)) {
                            // 右描画
                            this.ctx2.moveTo(positionX + this.grSep / this.grSplitMax, positionY);
                            this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                        }
                        if (j == this.itemAr[index].height * this.grSplitMax - 1 || j == this.itemAr[index].data[i].length - 1) {
                            // 下描画

                            this.ctx2.moveTo(positionX, positionY + this.grSep / this.grSplitMax);
                            this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                        }
                    }
                    this.ctx2.stroke();
                }
            }

            // 現在の面積表示
            if (this.itemAr[index].name !== "廊下" || this.displayMode === 1) {
                this.ctx2.fillStyle = 'black';
                if (this.displayMode === 1) {

                }
                else if (this.editMode) {
                    if (this.focusIndex === index && !(this.checkSelectOpening() || this.checkSelectSink())) {
                        this.ctx2.fillStyle = 'white';
                    }
                    else {
                        this.ctx2.fillStyle = 'gray';
                    }
                }
                else if (this.focusIndex === index && !(this.checkSelectOpening() || this.checkSelectSink())) {
                    this.ctx2.fillStyle = 'red';
                }

                var value = this.itemAr[index].count;
                var message = this.itemAr[index].name + "（" + value + this.itemAr[index].unit + "）";
                if (this.displayMode === 1 &&
                    (this.itemAr[index].type <= 4 ||
                        (this.itemAr[index].type >= 7 && this.itemAr[index].type <= 13) || this.itemAr[index].type === 20)) {
                    message = this.itemAr[index].name;
                }

                if (this.displayMode === 0) {
                    this.ctx2.font = "12px Arial";
                }
                else {
                    this.ctx2.font = "10px Arial";
                }
                this.ctx2.textAlign = 'center';

                this.ctx2.strokeStyle = 'white';
                this.ctx2.lineWidth = 1;
                this.ctx2.strokeText(message, x + width / 2, y + height / 2);
                this.ctx2.fillText(message, x + width / 2, y + height / 2);
            }

            this.ctx2.restore();
        };

        /** 部屋のオプションを描く
         * {int} index 部屋番号
         * return なし
         */
        this.drawRoomOption = function (index) {
            if (this.itemAr[index] == null) return;
            if (this.itemAr[index].width <= 0.0001 && this.itemAr[index].height <= 0.0001) return;

            // 部屋カテゴリに入らないものはオプションを描画しない
            if (this.itemAr[index].type >= 90) return;

            var x = this.itemAr[index].x * this.grSep - this.startGridX * this.grSep;
            var y = this.itemAr[index].y * this.grSep - this.startGridY * this.grSep;
            var width = this.itemAr[index].width * this.grSep;
            var height = this.itemAr[index].height * this.grSep;

            this.ctx2.save();
            this.ctx2.globalAlpha = this.madoriAlpha;
            this.ctx2.lineCap = "square";
            this.ctx2.lineWidth = this.wallSize * this.grSep;

            if (this.focusIndex == index && !(this.checkSelectOpening() || this.checkSelectSink())) {
                // ピンク色で描画
                this.ctx2.fillStyle = 'red';
                this.ctx2.strokeStyle = '#e3439d';

                // 周りの枠描画
                for (var i = 0; i < this.itemAr[index].data.length; i++) {
                    for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                        var positionX = x + i * this.grSep / this.grSplitMax;
                        var positionY = y + j * this.grSep / this.grSplitMax;

                        this.ctx2.beginPath();
                        if (this.itemAr[index].data[i][j] !== 1) {
                            if (i > 0 && this.itemAr[index].data[i - 1][j] === 1) {
                                // 左描画
                                this.ctx2.moveTo(positionX, positionY);
                                this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                            }
                            if (j > 0 && this.itemAr[index].data[i][j - 1] === 1) {
                                // 上描画
                                this.ctx2.moveTo(positionX, positionY);
                                this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY);
                            }
                            if (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1][j] === 1) {
                                // 右描画
                                this.ctx2.moveTo(positionX + this.grSep / this.grSplitMax, positionY);
                                this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                            }
                            if (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] === 1) {
                                // 下描画
                                this.ctx2.moveTo(positionX, positionY + this.grSep / this.grSplitMax);
                                this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                            }
                        }
                        else {
                            if (i === 0 || (i > 0 && this.itemAr[index].data[i - 1].length <= j)) {
                                // 左描画
                                this.ctx2.moveTo(positionX, positionY);
                                this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                            }
                            if (j === 0) {
                                // 上描画
                                this.ctx2.moveTo(positionX, positionY);
                                this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY);
                            }
                            if (i === this.itemAr[index].width * this.grSplitMax - 1 ||
                                i === this.itemAr[index].data.length - 1 ||
                                (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1].length <= j)) {
                                // 右描画
                                this.ctx2.moveTo(positionX + this.grSep / this.grSplitMax, positionY);
                                this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                            }
                            if (j === this.itemAr[index].height * this.grSplitMax - 1 || j === this.itemAr[index].data[i].length - 1) {
                                // 下描画
                                this.ctx2.moveTo(positionX, positionY + this.grSep / this.grSplitMax);
                                this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                            }
                        }
                        this.ctx2.stroke();
                    }
                }

                this.ctx2.globalAlpha = 0.3;

                this.ctx2.beginPath();
                for (var i = 0; i < this.itemAr[index].data.length; i++) {
                    for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                        if (this.itemAr[index].data[i][j] === 1 && (i % 3 + j % 3) === 2) {
                            var positionX = x + i * this.grSep / this.grSplitMax;
                            var positionY = y + j * this.grSep / this.grSplitMax;
                            this.ctx2.moveTo(positionX + this.grSep / this.grSplitMax, positionY);
                            this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                        }
                    }
                }
                this.ctx2.stroke();

                this.ctx2.globalAlpha = 1.0;

                // 編集中の丸描画
                if (this.displayMode == 1) {
                    this.ctx2.drawImage(this.editIconImg, x + width / 2 - 15, y + height / 2 - 15, 30, 30);
                }
            }

            if ((this.focusIndexAr.indexOf(index) < 0 || this.checkSelectOpening() || this.checkSelectSink()) && this.displayMode === 1) {
                // 丸を文字の上に描画
//        if (this.itemAr[index].type != 12 || this.checkTouchType(index,11).length == 0) {
                if (this.editFinishIndexAr.indexOf(index) < 0) {
                    this.ctx2.strokeStyle = "#beff01";
                    this.ctx2.beginPath();
                    this.ctx2.arc(x + width / 2, y + height / 2, 15, 0, Math.PI * 2, false);
                    this.ctx2.stroke();
                }
                else {
                    this.ctx2.strokeStyle = '#26B1FF';
                    // 周りの枠描画
                    for (var i = 0; i < this.itemAr[index].data.length; i++) {
                        for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                            var positionX = x + i * this.grSep / this.grSplitMax;
                            var positionY = y + j * this.grSep / this.grSplitMax;

                            this.ctx2.beginPath();
                            if (this.itemAr[index].data[i][j] !== 1) {
                                if (i > 0 && this.itemAr[index].data[i - 1][j] === 1) {
                                    // 左描画
                                    this.ctx2.moveTo(positionX, positionY);
                                    this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                                }
                                if (j > 0 && this.itemAr[index].data[i][j - 1] === 1) {
                                    // 上描画
                                    this.ctx2.moveTo(positionX, positionY);
                                    this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY);
                                }
                                if (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1][j] === 1) {
                                    // 右描画
                                    this.ctx2.moveTo(positionX + this.grSep / this.grSplitMax, positionY);
                                    this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                                }
                                if (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] === 1) {
                                    // 下描画
                                    this.ctx2.moveTo(positionX, positionY + this.grSep / this.grSplitMax);
                                    this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                                }
                            }
                            else {
                                if (i === 0 || (i > 0 && this.itemAr[index].data[i - 1].length <= j)) {
                                    // 左描画
                                    this.ctx2.moveTo(positionX, positionY);
                                    this.ctx2.lineTo(positionX, positionY + this.grSep / this.grSplitMax);
                                }
                                if (j === 0) {
                                    // 上描画
                                    this.ctx2.moveTo(positionX, positionY);
                                    this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY);
                                }
                                if (i === this.itemAr[index].width * this.grSplitMax - 1 ||
                                    i === this.itemAr[index].data.length - 1 ||
                                    (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1].length <= j)) {
                                    // 右描画
                                    this.ctx2.moveTo(positionX + this.grSep / this.grSplitMax, positionY);
                                    this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                                }
                                if (j === this.itemAr[index].height * this.grSplitMax - 1 ||
                                    j === this.itemAr[index].data[i].length - 1) {
                                    // 下描画
                                    this.ctx2.moveTo(positionX, positionY + this.grSep / this.grSplitMax);
                                    this.ctx2.lineTo(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax);
                                }
                            }
                            this.ctx2.stroke();
                        }
                    }
                    this.ctx2.drawImage(this.editFinishIconImg, x + width / 2 - 15, y + height / 2 - 15, 30, 30);
                }
            }
            this.ctx2.restore();
        };

        /** 開口部を描く
         * {int} index 開口部番号
         * return なし
         */
        this.drawOpening = function (index) {
            if (this.openingAr[index] == null) return;
            if (this.itemAr[this.openingAr[index].roomId] == null) return;

            var x = (this.itemAr[this.openingAr[index].roomId].x + this.openingAr[index].x) * this.grSep - this.startGridX * this.grSep;
            var y = (this.itemAr[this.openingAr[index].roomId].y + this.openingAr[index].y) * this.grSep - this.startGridY * this.grSep;
            var width = this.openingAr[index].width * this.grSep;
            var height = this.openingAr[index].height * this.grSep;

            this.openingAr[index].displayWidth = width;
            this.openingAr[index].displayHeight = height;

            this.ctx2.save();
            this.ctx2.globalAlpha = this.madoriAlpha;
            this.ctx2.lineWidth = 1.0;
            this.ctx2.fillStyle = 'white';
            this.ctx2.strokeStyle = 'white';

            if (this.openingAr[index].type <= 20) {
                this.ctx2.strokeStyle = 'black';
            }
            if (this.openingAr[index].type === 20) {// 柱
                this.ctx2.fillStyle = 'black';
            }

            if (this.focusIndex == index && this.checkSelectOpening()) {
                this.ctx2.fillStyle = 'red';
            }

            if (this.openingDelMode) {
                this.ctx2.fillStyle = 'red';
            }

            if (this.openingAr[index].type === 20) {
                // 柱の場合
            }
            else if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                x += this.wallSize / 2 * this.grSep;
                width -= this.wallSize * this.grSep;
                // 反転設定
                if (this.openingAr[index].hFlip) {
                    this.ctx2.transform(-1, 0, 0, 1, x * 2 + width, 0);
                }
            }
            else {
                y += this.wallSize / 2 * this.grSep;
                height -= this.wallSize * this.grSep;

                // 反転設定
                if (this.openingAr[index].hFlip) {
                    this.ctx2.transform(1, 0, 0, -1, 0, y * 2 + height);
                }
            }

            // 一部開口部は先に描画
            if (this.openingAr[index].type === 3) {// 片開き窓
                this.ctx2.strokeStyle = 'black';

                this.ctx2.beginPath();
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    if (!this.openingAr[index].flip) {
                        this.ctx2.arc(x, y, width, 0, -(Math.PI / 2), true);
                        this.openingAr[index].displayHeight = -1 * width;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x, y + width);
                        this.ctx2.arc(x, y, width, (Math.PI / 2), 0, true);
                        this.openingAr[index].displayHeight = width;
                    }
                }
                else {// 右向き
                    if (!this.openingAr[index].flip) {
                        this.ctx2.arc(x, y, height, (Math.PI / 2), 0, true);
                        this.openingAr[index].displayWidth = height;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x - height, y);
                        this.ctx2.arc(x, y, height, Math.PI, (Math.PI / 2), true);
                        this.openingAr[index].displayWidth = -1 * height;
                    }
                }
                this.ctx2.lineTo(x, y);

                this.ctx2.stroke();
                this.ctx2.globalAlpha = this.madoriAlpha / 3;
                this.ctx2.fill();

                this.ctx2.globalAlpha = this.madoriAlpha;
            }

            this.ctx2.beginPath();
            this.ctx2.moveTo(x, y);
            this.ctx2.lineTo(x, y + height);
            this.ctx2.lineTo(x + width, y + height);
            this.ctx2.lineTo(x + width, y);
            this.ctx2.closePath();
            this.ctx2.stroke();
            this.ctx2.fill();

            if (this.openingAr[index].type === 1 || this.openingAr[index].type === 18 || this.openingAr[index].type === 19 || this.openingAr[index].type === 24) {// 引違窓/掃き出し窓/引き違い戸(2枚)/腰掛け窓
                this.ctx2.strokeStyle = 'black';
                var centerLineDiff = 0;
                if (this.openingAr[index].type === 18) centerLineDiff = 8;

                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width / 2, y - centerLineDiff);
                    this.ctx2.lineTo(x + width / 2, y + height + centerLineDiff);
                    this.ctx2.stroke();

                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x, y + height / 4);
                    this.ctx2.lineTo(x + width * 3 / 5, y + height / 4);
                    this.ctx2.stroke();

                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width, y + height * 3 / 4);
                    this.ctx2.lineTo(x + width * 2 / 5, y + height * 3 / 4);
                    this.ctx2.stroke();
                }
                else {// 右向き
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x - centerLineDiff, y + height / 2);
                    this.ctx2.lineTo(x + width + centerLineDiff, y + height / 2);
                    this.ctx2.stroke();

                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width / 4, y);
                    this.ctx2.lineTo(x + width / 4, y + height * 3 / 5);
                    this.ctx2.stroke();

                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width * 3 / 4, y + height);
                    this.ctx2.lineTo(x + width * 3 / 4, y + height * 2 / 5);
                    this.ctx2.stroke();
                }
            }
            else if (this.openingAr[index].type === 25 || this.openingAr[index].type === 26) {// 引き違い戸(3枚/4枚)
                this.ctx2.strokeStyle = 'black';
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    // 中央線
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width / 2, y);
                    this.ctx2.lineTo(x + width / 2, y + height);
                    this.ctx2.stroke();

                    var value = this.openingAr[index].type - 25 + 3;
                    for (var i = 1; i < value + 1; i++) {
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width * (i - 1) / (value), y + height * i / (value + 1));

                        var lineWidth = x + width * i / (value) + value;
                        if (i === value) lineWidth = x + width * i / (value);
                        this.ctx2.lineTo(lineWidth, y + height * i / (value + 1));
                        this.ctx2.stroke();
                    }
                }
                else {// 右向き
                    // 中央線
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x, y + height / 2);
                    this.ctx2.lineTo(x + width, y + height / 2);
                    this.ctx2.stroke();

                    var value = this.openingAr[index].type - 25 + 3;
                    for (var i = 1; i < value + 1; i++) {
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width * i / (value + 1), y + height * (i - 1) / (value));

                        var lineHeight = y + height * i / (value) + value;
                        if (i === value) lineHeight = y + height * i / (value);
                        this.ctx2.lineTo(x + width * i / (value + 1), lineHeight);
                        this.ctx2.stroke();
                    }
                }
            }
            else if (this.openingAr[index].type === 4) {// 出窓
                this.ctx2.strokeStyle = 'black';

                this.ctx2.beginPath();
                if (Math.abs(height) === this.wallSize * this.grSep) {// 上向き
                    if (this.openingAr[index].flip) {
                        this.ctx2.moveTo(x + width, y + height);
                        this.ctx2.lineTo(x + width - this.grSep / 4, y + this.grSep / 4 + height);
                        this.ctx2.lineTo(x + this.grSep / 4, y + this.grSep / 4 + height);
                        this.ctx2.lineTo(x, y + height);
                        this.openingAr[index].displayHeight = this.grSep / 4 + height;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x + this.grSep / 4, y - this.grSep / 4);
                        this.ctx2.lineTo(x + width - this.grSep / 4, y - this.grSep / 4);
                        this.ctx2.lineTo(x + width, y);
                        this.openingAr[index].displayHeight = -1 * this.grSep / 4;
                    }
                }
                else {// 右向き
                    if (this.openingAr[index].flip) {
                        this.ctx2.moveTo(x, y + height);
                        this.ctx2.lineTo(x - this.grSep / 4, y + height - this.grSep / 4);
                        this.ctx2.lineTo(x - this.grSep / 4, y + this.grSep / 4);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayWidth = -1 * this.grSep / 4;
                    }
                    else {
                        this.ctx2.moveTo(x + width, y);
                        this.ctx2.lineTo(x + this.grSep / 4 + width, y + this.grSep / 4);
                        this.ctx2.lineTo(x + this.grSep / 4 + width, y + height - this.grSep / 4);
                        this.ctx2.lineTo(x + width, y + height);

                        this.openingAr[index].displayWidth = this.grSep / 4 + width;
                    }
                }

                this.ctx2.stroke();
                this.ctx2.globalAlpha = this.madoriAlpha / 3;
                this.ctx2.fill();
                this.ctx2.globalAlpha = this.madoriAlpha;
            }
            else if (this.openingAr[index].type === 5) {// Fix窓
                this.ctx2.strokeStyle = 'black';

                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x, y + height / 2);
                    this.ctx2.lineTo(x + width, y + height / 2);
                    this.ctx2.stroke();
                }
                else {// 右向き
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width / 2, y);
                    this.ctx2.lineTo(x + width / 2, y + height);
                    this.ctx2.stroke();
                }
            }
            else if (this.openingAr[index].type === 27) {// 折れ戸
                this.ctx2.strokeStyle = 'black';

                this.ctx2.beginPath();
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    if (!this.openingAr[index].flip) {
//                this.ctx2.lineTo(x+width/2, y-width/3);
                        this.ctx2.lineTo(x + width / 3, y);
                        this.ctx2.lineTo(x + width / 6, y + width / 3);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayHeight = -1 * width / 3;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x + width / 6, y - width / 3);
//                this.ctx2.lineTo(x+width/2, y+width/3);
                        this.ctx2.lineTo(x + width / 3, y);
                        this.openingAr[index].displayHeight = width / 3;
                    }
                }
                else {// 右向き
                    if (!this.openingAr[index].flip) {
//                this.ctx2.lineTo(x+height/3, y+height/2);
                        this.ctx2.lineTo(x, y + height / 3);
                        this.ctx2.lineTo(x - height / 3, y + height / 6);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayWidth = height / 3;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x + height / 3, y + height / 6);
//                this.ctx2.lineTo(x-height/3, y+height/2);
                        this.ctx2.lineTo(x, y + height / 3);
                        this.openingAr[index].displayWidth = -1 * height / 3;
                    }
                }

                this.ctx2.stroke();
                this.ctx2.globalAlpha = this.madoriAlpha / 3;
                this.ctx2.fill();
                this.ctx2.globalAlpha = this.madoriAlpha;
            }
            else if (this.openingAr[index].type === 30) {// 両折れ戸
                this.ctx2.strokeStyle = 'black';

                this.ctx2.beginPath();
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    if (this.openingAr[index].flip) {
                        this.ctx2.lineTo(x + width / 3, y);
                        this.ctx2.lineTo(x + width / 6, y + width / 3);
                        this.ctx2.lineTo(x, y);
                        this.ctx2.moveTo(x + width, y);
                        this.ctx2.lineTo(x + width * 5 / 6, y + width / 3);
                        this.ctx2.lineTo(x + width * 2 / 3, y);

                        this.openingAr[index].displayHeight = -1 * width / 3;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x + width / 6, y - width / 3);
                        this.ctx2.lineTo(x + width / 3, y);
                        this.ctx2.moveTo(x + width * 2 / 3, y);
                        this.ctx2.lineTo(x + width * 5 / 6, y - width / 3);
                        this.ctx2.lineTo(x + width, y);
                        this.openingAr[index].displayHeight = width / 3;
                    }
                }
                else {// 右向き
                    if (this.openingAr[index].flip) {
                        this.ctx2.lineTo(x, y + height / 3);
                        this.ctx2.lineTo(x - height / 3, y + height / 6);
                        this.ctx2.lineTo(x, y);
                        this.ctx2.moveTo(x, y + height);
                        this.ctx2.lineTo(x - height / 3, y + height * 5 / 6);
                        this.ctx2.lineTo(x, y + height * 2 / 3);
                        this.openingAr[index].displayWidth = height / 3;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x + height / 3, y + height / 6);
                        this.ctx2.lineTo(x, y + height / 3);
                        this.ctx2.moveTo(x, y + height * 2 / 3);
                        this.ctx2.lineTo(x + height / 3, y + height * 5 / 6);
                        this.ctx2.lineTo(x, y + height);
                        this.openingAr[index].displayWidth = -1 * height / 3;
                    }
                }

                this.ctx2.stroke();
                this.ctx2.globalAlpha = this.madoriAlpha / 3;
                this.ctx2.fill();
                this.ctx2.globalAlpha = this.madoriAlpha;
            }
            else if (this.openingAr[index].type === 21) {// 片開きドア
                this.ctx2.strokeStyle = 'black';

                this.ctx2.beginPath();
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    if (!this.openingAr[index].flip) {
                        if (y % this.grSep !== 0) {
                            this.ctx2.moveTo(x + width, y + height);
                            this.ctx2.lineTo(x + width, y);
                        }
                        this.ctx2.arc(x, y, width, 0, -(Math.PI / 2), true);
                        this.ctx2.lineTo(x, y);
                        if (y % this.grSep !== 0) {
                            this.ctx2.lineTo(x, y + height);
                        }
                        this.openingAr[index].displayHeight = -1 * width;
                    }
                    else {
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x, y + width);
                        this.ctx2.arc(x, y, width, (Math.PI / 2), 0, true);
                        this.openingAr[index].displayHeight = width;

                    }
                }
                else {// 右向き
                    if (!this.openingAr[index].flip) {
                        this.ctx2.arc(x, y, height, (Math.PI / 2), 0, true);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayWidth = height;
                    }
                    else {
                        if (x % this.grSep === 0) {
                            this.ctx2.moveTo(x, y);
                        }
                        else {
                            this.ctx2.moveTo(x + width, y);
                            this.ctx2.lineTo(x, y);
                        }
                        this.ctx2.lineTo(x - height, y);
                        this.ctx2.arc(x, y, height, Math.PI, (Math.PI / 2), true);
                        if (x % this.grSep !== 0) {
                            this.ctx2.lineTo(x + width, y + height);
                        }
                        this.openingAr[index].displayWidth = -1 * height;
                    }
                }

                this.ctx2.stroke();
                this.ctx2.globalAlpha = this.madoriAlpha / 3;
                this.ctx2.fill();
                this.ctx2.globalAlpha = this.madoriAlpha;
            }
            else if (this.openingAr[index].type === 22) {// 両開きドア
                this.ctx2.strokeStyle = 'black';

                this.ctx2.beginPath();
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    this.ctx2.moveTo(x + width, y);
                    if (!this.openingAr[index].flip) {
                        if (y % this.grSep !== 0) {
                            this.ctx2.moveTo(x + width, y + height);
                            this.ctx2.lineTo(x + width, y);
                        }
                        this.ctx2.arc(x + width, y, width / 2, -(Math.PI / 2), -(Math.PI), true);
                        this.ctx2.arc(x, y, width / 2, 0, -(Math.PI / 2), true);
                        this.ctx2.lineTo(x, y);
                        if (y % this.grSep !== 0) {
                            this.ctx2.lineTo(x, y + height);
                        }
                        this.openingAr[index].displayHeight = -1 * width / 2;
                    }
                    else {
                        this.ctx2.arc(x + width, y, width / 2, (Math.PI / 2), (Math.PI), false);
                        this.ctx2.arc(x, y, width / 2, 0, (Math.PI / 2), false);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayHeight = width / 2;
                    }
                }
                else {// 右向き
                    this.ctx2.moveTo(x, y + height);
                    if (!this.openingAr[index].flip) {
                        this.ctx2.arc(x, y + height, height / 2, 0, -(Math.PI / 2), true);
                        this.ctx2.arc(x, y, height / 2, (Math.PI / 2), 0, true);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayWidth = height / 2;
                    }
                    else {
                        if (x % this.grSep !== 0) {
                            this.ctx2.moveTo(x + width, y + height);
                            this.ctx2.lineTo(x, y + height);
                        }
                        this.ctx2.arc(x, y + height, height / 2, Math.PI, -(Math.PI / 2), false);
                        this.ctx2.arc(x, y, height / 2, (Math.PI / 2), Math.PI, false);
                        if (x % this.grSep !== 0) {
                            this.ctx2.lineTo(x + width, y);
                        }
                        else {
                            this.ctx2.lineTo(x, y);
                        }
                        this.openingAr[index].displayWidth = -1 * height / 2;
                    }
                }
                this.ctx2.stroke();
                this.ctx2.globalAlpha = this.madoriAlpha / 3;
                this.ctx2.fill();
                this.ctx2.globalAlpha = this.madoriAlpha;
            }
            else if (this.openingAr[index].type === 28) {// 親子扉
                this.ctx2.strokeStyle = 'black';

                this.ctx2.beginPath();
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    this.ctx2.moveTo(x + width, y);
                    if (!this.openingAr[index].flip) {
                        if (y % this.grSep !== 0) {
                            this.ctx2.moveTo(x + width, y + height);
                            this.ctx2.lineTo(x + width, y);
                        }
                        this.ctx2.arc(x + width, y, width * 2 / 3, -(Math.PI / 2), -(Math.PI), true);
                        this.ctx2.arc(x, y, width / 3, 0, -(Math.PI / 2), true);
                        this.ctx2.lineTo(x, y);
                        if (y % this.grSep !== 0) {
                            this.ctx2.lineTo(x, y + height);
                        }
                        this.openingAr[index].displayHeight = -1 * width * 2 / 3;
                    }
                    else {
                        this.ctx2.arc(x + width, y, width * 2 / 3, (Math.PI / 2), (Math.PI), false);
                        this.ctx2.arc(x, y, width / 3, 0, (Math.PI / 2), false);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayHeight = width * 2 / 3;
                    }
                }
                else {// 右向き
                    this.ctx2.moveTo(x, y + height);
                    if (!this.openingAr[index].flip) {
                        this.ctx2.arc(x, y + height, height * 2 / 3, 0, -(Math.PI / 2), true);
                        this.ctx2.arc(x, y, height / 3, (Math.PI / 2), 0, true);
                        this.ctx2.lineTo(x, y);
                        this.openingAr[index].displayWidth = height * 2 / 3;
                    }
                    else {
                        if (x % this.grSep !== 0) {
                            this.ctx2.moveTo(x + width, y + height);
                            this.ctx2.lineTo(x, y + height);
                        }
                        this.ctx2.arc(x, y + height, height * 2 / 3, Math.PI, -(Math.PI / 2), false);
                        this.ctx2.arc(x, y, height / 3, (Math.PI / 2), Math.PI, false);
                        if (x % this.grSep !== 0) {
                            this.ctx2.lineTo(x + width, y);
                        }
                        else {
                            this.ctx2.lineTo(x, y);
                        }
                        this.openingAr[index].displayWidth = -1 * height * 2 / 3;
                    }
                }
                this.ctx2.stroke();
                this.ctx2.globalAlpha = this.madoriAlpha / 3;
                this.ctx2.fill();
                this.ctx2.globalAlpha = this.madoriAlpha;
            }
            else if (this.openingAr[index].type === 29) {// 片引き戸
                this.ctx2.strokeStyle = 'black';

                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    if (!this.openingAr[index].flip) {
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x + width / 2, y);
                        this.ctx2.lineTo(x + width / 2, y + height / 3);
                        this.ctx2.lineTo(x, y + height / 3);
                        this.ctx2.stroke();

                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width / 2, y + height * 2 / 3);
                        this.ctx2.lineTo(x + width, y + height * 2 / 3);
                        this.ctx2.stroke();

                        this.ctx2.setLineDash([this.grSep / 12, this.grSep / 12]);
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width / 2, y + height * 2 / 3);
                        this.ctx2.lineTo(x, y + height * 2 / 3);
                        this.ctx2.stroke();
                        this.ctx2.setLineDash([]);

                    }
                    else {
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x, y + height * 2 / 3);
                        this.ctx2.lineTo(x + width / 2, y + height * 2 / 3);
                        this.ctx2.lineTo(x + width / 2, y + height);
                        this.ctx2.lineTo(x, y + height);
                        this.ctx2.stroke();

                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width / 2, y + height / 3);
                        this.ctx2.lineTo(x + width, y + height / 3);
                        this.ctx2.stroke();

                        this.ctx2.setLineDash([this.grSep / 12, this.grSep / 12]);
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width / 2, y + height / 3);
                        this.ctx2.lineTo(x, y + height / 3);
                        this.ctx2.stroke();
                        this.ctx2.setLineDash([]);
                    }
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width * 3 / 4 - 4, y - 8 + 4);
                    this.ctx2.lineTo(x + width * 3 / 4, y - 8);
                    this.ctx2.lineTo(x + width * 3 / 4, y + height + 8);
                    this.ctx2.lineTo(x + width * 3 / 4 + 4, y + height + 8 - 4);
                    this.ctx2.stroke();
                }
                else {// 右向き
                    if (this.openingAr[index].flip) {
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x, y);
                        this.ctx2.lineTo(x, y + height / 2);
                        this.ctx2.lineTo(x + width / 3, y + height / 2);
                        this.ctx2.lineTo(x + width / 3, y);
                        this.ctx2.stroke();

                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width * 2 / 3, y + height / 2);
                        this.ctx2.lineTo(x + width * 2 / 3, y + height);
                        this.ctx2.stroke();

                        this.ctx2.setLineDash([this.grSep / 12, this.grSep / 12]);
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width * 2 / 3, y + height / 2);
                        this.ctx2.lineTo(x + width * 2 / 3, y);
                        this.ctx2.stroke();
                        this.ctx2.setLineDash([]);

                    }
                    else {
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width * 2 / 3, y);
                        this.ctx2.lineTo(x + width * 2 / 3, y + height / 2);
                        this.ctx2.lineTo(x + width, y + height / 2);
                        this.ctx2.lineTo(x + width, y);
                        this.ctx2.stroke();

                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width / 3, y + height / 2);
                        this.ctx2.lineTo(x + width / 3, y + height);
                        this.ctx2.stroke();

                        this.ctx2.setLineDash([this.grSep / 12, this.grSep / 12]);
                        this.ctx2.beginPath();
                        this.ctx2.moveTo(x + width / 3, y + height / 2);
                        this.ctx2.lineTo(x + width / 3, y);
                        this.ctx2.stroke();
                        this.ctx2.setLineDash([]);
                    }
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x - 8 + 4, y + height * 3 / 4 - 4);
                    this.ctx2.lineTo(x - 8, y + height * 3 / 4);
                    this.ctx2.lineTo(x + width + 8, y + height * 3 / 4);
                    this.ctx2.lineTo(x + width + 8 - 4, y + height * 3 / 4 + 4);
                    this.ctx2.stroke();
                }
            }
            if (this.openingAr[index].type === 98) {
                // 階段入口
                this.ctx2.font = "13px Arial Bold";
                this.ctx2.textAlign = 'center';
                this.ctx2.fillStyle = 'black';

                if (this.openingAr[index].width == this.wallSize) {
                    if (this.checkItemPxNotWall(x, y) == this.openingAr[index].roomId) {
                        this.ctx2.fillText("←", x + width / 2, y + height / 2);
                    }
                    else {
                        this.ctx2.fillText("→", x + width / 2, y + height / 2);
                    }
                }
                else {
                    if (this.checkItemPxNotWall(x, y) == this.openingAr[index].roomId) {
                        this.ctx2.fillText("↑", x + width / 2, y + height / 2);
                    }
                    else {
                        this.ctx2.fillText("↓", x + width / 2, y + height / 2);
                    }
                }
            }
            if (this.openingAr[index].type === 19 ||
                (this.openingAr[index].type > 20 && this.openingAr[index].type < 90 && this.openingAr[index].type !== 29)) {
                // 掃き出し窓・ドアの場合（片引き戸は除く）
                if (Math.abs(height) == this.wallSize * this.grSep) {// 上向き
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x + width / 2 - 4, y - 8 + 4);
                    this.ctx2.lineTo(x + width / 2, y - 8);
                    this.ctx2.lineTo(x + width / 2, y + height + 8);
                    this.ctx2.lineTo(x + width / 2 + 4, y + height + 8 - 4);
                    this.ctx2.stroke();
                }
                else { // 右向き
                    this.ctx2.beginPath();
                    this.ctx2.moveTo(x - 8 + 4, y + height / 2 - 4);
                    this.ctx2.lineTo(x - 8, y + height / 2);
                    this.ctx2.lineTo(x + width + 8, y + height / 2);
                    this.ctx2.lineTo(x + width + 8 - 4, y + height / 2 + 4);
                    this.ctx2.stroke();
                }
            }

            this.ctx2.restore();
        };

        /** シンクを描く
         * {int} index シンク番号
         * return なし
         */
        this.drawSink = function (index) {
            if (this.itemAr[this.sinkAr[index].roomId] == null) return;

            var x = (this.itemAr[this.sinkAr[index].roomId].x + this.sinkAr[index].x) * this.grSep - this.startGridX * this.grSep;
            var y = (this.itemAr[this.sinkAr[index].roomId].y + this.sinkAr[index].y) * this.grSep - this.startGridY * this.grSep;
            var width = this.sinkAr[index].width * this.grSep;
            var height = this.sinkAr[index].height * this.grSep;

            if (this.sinkAr[index].unit === "mm") {
                // mm単位のシンクの場合
                x = (this.itemAr[this.sinkAr[index].roomId].x + this.sinkAr[index].x / this.grSepMm) * this.grSep - this.startGridX * this.grSep;
                y = (this.itemAr[this.sinkAr[index].roomId].y + this.sinkAr[index].y / this.grSepMm) * this.grSep - this.startGridY * this.grSep;
                width /= this.grSepMm;
                height /= this.grSepMm;
            }

            this.ctx2.save();
            this.ctx2.globalAlpha = this.madoriAlpha;
            this.ctx2.lineWidth = 1.0;
            this.ctx2.fillStyle = 'white';
            this.ctx2.strokeStyle = 'black';

            if (this.focusIndex == index && this.checkSelectSink()) {
                this.ctx2.fillStyle = 'red';
                this.ctx2.strokeStyle = 'red';
                // 範囲
                this.ctx2.strokeRect(x, y, width, height);
            }

            var sinkImage = this.sinkImage2550;
            var sinkW = this.sinkWidth;
            if (this.sinkAr[index].sink_width) {
                sinkW = this.sinkAr[index].sink_width;

                if (sinkW <= 24 / 12) sinkImage = this.sinkImage1800;
                else if (sinkW <= 28 / 12) sinkImage = this.sinkImage2100;
                else if (sinkW <= 32 / 12) sinkImage = this.sinkImage2400;
            }

            if (this.sinkAr[index].type == 0) {
                // I型(シンク左側にスペースあり)
                if (this.sinkAr[index].angle == 0) {
                    x += this.sinkMarginLR * this.grSep;
                    width = sinkW * this.grSep;
                    height = this.sinkDepth * this.grSep;
                }
                if (this.sinkAr[index].angle == 90) {
                    x += this.sinkMarginBottom * this.grSep;
                    y += this.sinkMarginLR * this.grSep;
                    width = this.sinkDepth * this.grSep;
                    height = sinkW * this.grSep;
                }
                if (this.sinkAr[index].angle == 180) {
                    y += this.sinkMarginBottom * this.grSep;
                    width = sinkW * this.grSep;
                    height = this.sinkDepth * this.grSep;
                }
                if (this.sinkAr[index].angle == 270) {
                    width = this.sinkDepth * this.grSep;
                    height = sinkW * this.grSep;
                }
            }
            else if (this.sinkAr[index].type == 1) {
                // I型(シンク右側にスペースあり)
                if (this.sinkAr[index].angle == 0) {
                    width = sinkW * this.grSep;
                    height = this.sinkDepth * this.grSep;
                }
                if (this.sinkAr[index].angle == 90) {
                    x += this.sinkMarginBottom * this.grSep;
                    width = this.sinkDepth * this.grSep;
                    height = sinkW * this.grSep;
                }
                if (this.sinkAr[index].angle == 180) {
                    x += this.sinkMarginLR * this.grSep;
                    y += this.sinkMarginBottom * this.grSep;
                    width = sinkW * this.grSep;
                    height = this.sinkDepth * this.grSep;
                }
                if (this.sinkAr[index].angle == 270) {
                    y += this.sinkMarginLR * this.grSep;
                    width = this.sinkDepth * this.grSep;
                    height = sinkW * this.grSep;
                }
            }
            else if (this.sinkAr[index].type == 2) {
                // I型(壁付け：隙間なし)
                if (this.sinkAr[index].angle % 180 == 0) {
                    width = sinkW * this.grSep;
                    height = this.sinkDepth * this.grSep;
                }
                else {
                    width = this.sinkDepth * this.grSep;
                    height = sinkW * this.grSep;
                }
            }
            else if (this.sinkAr[index].type == 3) {
                // I型（センターキッチン）
                if (this.sinkAr[index].angle == 0) {
                    x += this.sinkMarginLR * this.grSep;
                    width = sinkW * this.grSep;
                    height = this.sinkDepth * this.grSep;
                }
                if (this.sinkAr[index].angle == 90) {
                    x += this.sinkMarginBottom * this.grSep;
                    y += this.sinkMarginLR * this.grSep;
                    width = this.sinkDepth * this.grSep;
                    height = sinkW * this.grSep;
                }
                if (this.sinkAr[index].angle == 180) {
                    x += this.sinkMarginLR * this.grSep;
                    y += this.sinkMarginBottom * this.grSep;
                    width = sinkW * this.grSep;
                    height = this.sinkDepth * this.grSep;
                }
                if (this.sinkAr[index].angle == 270) {
                    y += this.sinkMarginLR * this.grSep;
                    width = this.sinkDepth * this.grSep;
                    height = sinkW * this.grSep;
                }
            }
//    this.ctx2.fillRect(x,y,width,height);
//    this.ctx2.strokeRect(x,y,width,height);

            this.ctx2.translate(x, y);
            this.ctx2.rotate(this.sinkAr[index].angle / 180 * Math.PI);

            if (this.sinkAr[index].angle == 0) {
                this.ctx2.drawImage(sinkImage, 0, 0, width, height);
            }
            else if (this.sinkAr[index].angle == 90) {
                this.ctx2.drawImage(sinkImage, 0, -width, height, width);
            }
            else if (this.sinkAr[index].angle == 180) {
                this.ctx2.drawImage(sinkImage, -width, -height, width, height);
            }
            else if (this.sinkAr[index].angle == 270) {
                this.ctx2.drawImage(sinkImage, -height, 0, height, width);
            }

            this.ctx2.restore();
        };

        /** アイテムが指定した座標内にあるか判定
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * return アイテムがあればthis.itemArのindex アイテムが無ければnull
         */
        this.checkItemPx = function (x, y, index) {
            /*
    x -= this.startGridX * this.grSep;
    y -= this.startGridY * this.grSep;
  */

            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;

//    window.console.log(x+","+y);
            var rtn = null;

            for (var i = 0; i < this.itemAr.length; i++) {
                if (i == index) continue;
                if (this.itemAr[i] == null) continue;

                if (x >= (this.itemAr[i].x - this.wallSize / 2) * this.grSep &&
                    y >= (this.itemAr[i].y - this.wallSize / 2) * this.grSep &&
                    x <= (this.itemAr[i].x + this.itemAr[i].width + this.wallSize) * this.grSep &&
                    y <= (this.itemAr[i].y + this.itemAr[i].height + this.wallSize) * this.grSep) {
                    if (this.checkWallPosition(x, y, i) != null) {
                        rtn = i;
                        break;
                    }
                }
            }

            return rtn;
        };

        /** 指定した座標が指定した部屋内にあるかチェック
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} index 部屋番号
         * return 部屋内にあればtrue、なければfalse
         */
        this.checkItemPxWithRoom = function (x, y, index) {
            if (this.itemAr[index] == null) return false;

            if (x >= (this.itemAr[index].x - this.wallSize / 2) * this.grSep &&
                y >= (this.itemAr[index].y - this.wallSize / 2) * this.grSep &&
                x <= (this.itemAr[index].x + this.itemAr[index].width + this.wallSize) * this.grSep &&
                y <= (this.itemAr[index].y + this.itemAr[index].height + this.wallSize) * this.grSep) {

                if (this.checkWallPosition(x, y, index) != null) {
                    return true;
                }
            }

            return false;
        };

        /** 指定した座標が指定した部屋内にあるかチェック（壁は含めない）
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} index 部屋番号
         * return 部屋内にあればtrue、なければfalse
         */
        this.checkItemPxWithRoomNotWall = function (x, y, index) {
            if (this.itemAr[index] == null) return false;

            if (x >= this.itemAr[index].x * this.grSep &&
                y >= this.itemAr[index].y * this.grSep &&
                x <= (this.itemAr[index].x + this.itemAr[index].width) * this.grSep &&
                y <= (this.itemAr[index].y + this.itemAr[index].height) * this.grSep) {

                var positionX = Math.floor((x - this.itemAr[index].x * this.grSep) / this.grSep * this.grSplitMax);
                var positionY = Math.floor((y - this.itemAr[index].y * this.grSep) / this.grSep * this.grSplitMax);

                try {
                    if (this.itemAr[index].data[positionX][positionY] == 1) {
                        return true;
                    }
                }
                catch (e) {

                }
            }
            return false;
        };

        /** 指定した範囲に部屋があるか（バグありのため、現在未使用）
         * {int} x x座標(グリッド単位)
         * {int} y y座標(グリッド単位)
         * {int} width 幅(グリッド単位)
         * {int} height 高さ(グリッド単位)
         * {int} notCheckIndex チェックしない部屋
         * return  範囲に接する部屋の一覧
         */
        this.checkItemRect = function (x, y, width, height, notCheckIndex) {
            var output = [];

            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;
            width = Math.round(width * 10000) / 10000;
            height = Math.round(height * 10000) / 10000;

            if (width == 0) {
                x += 0.05;
                height -= 0.1;
            }
            if (height == 0) {
                y += 0.05;
                width -= 0.1;
            }

            for (var i = 0; i < this.itemAr.length; i++) {
                if (i == notCheckIndex) continue;
                if (this.itemAr[i] == null) continue;

                var distanceX = Math.abs((x + width / 2) - (this.itemAr[i].x + this.itemAr[i].width / 2));
                var distanceY = Math.abs((y + height / 2) - (this.itemAr[i].y + this.itemAr[i].height / 2));

                if (distanceX <= (width + this.itemAr[i].width) / 2 &&
                    distanceY <= (height + this.itemAr[i].height) / 2) {
                    // データが接しているかチェック

                    var positionX1 = Math.floor((x - this.itemAr[i].x) * this.grSplitMax);
                    var positionY1 = Math.floor((y - this.itemAr[i].y) * this.grSplitMax);

                    var positionX2 = Math.floor((x + width - this.itemAr[i].x) * this.grSplitMax);
                    var positionY2 = Math.floor((y + height - this.itemAr[i].y) * this.grSplitMax);

                    if (this.checkItemDataPosition(positionX1, positionY1, i) ||
                        this.checkItemDataPosition(positionX2, positionY2, i)) {
                        output.push(this.itemAr[i]);
                    }
                }
            }

            return output;
        };

        /** 枠線分はみ出した座標をチェック
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} i 部屋のindex
         * return あればdata座標を返す 無ければnull
         */
        this.checkWallPosition = function (x, y, i) {
            var gridX = this.setPositionFit(x);
            var gridY = this.setPositionFit(y);

            // 左上の座標
            var checkX = Math.floor(Math.floor((x - this.itemAr[i].x * this.grSep) / this.grSep * this.grSplitMax) / (this.grSplitMax / this.grSplit)) * (this.grSplitMax / this.grSplit);
            var checkY = Math.floor(Math.floor((y - this.itemAr[i].y * this.grSep) / this.grSep * this.grSplitMax) / (this.grSplitMax / this.grSplit)) * (this.grSplitMax / this.grSplit);

            /*
    var checkX = Math.floor((x-this.itemAr[i].x*this.grSep)/(this.grSep / this.grSplitMax));
    var checkY = Math.floor((y-this.itemAr[i].y*this.grSep)/(this.grSep / this.grSplitMax));
 */

            if (this.checkItemDataPosition(checkX, checkY, i)) {
                return [checkX, checkY];
            }

            /*
    var positionX = x - gridX * this.grSep;
    var positionY = y - gridY * this.grSep;
  */

            var positionX = x - this.itemAr[i].x * this.grSep - checkX * (this.grSep / this.grSplitMax);
            var positionY = y - this.itemAr[i].y * this.grSep - checkY * (this.grSep / this.grSplitMax);

//  window.console.log(positionX+","+positionY);

            if (positionX >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize * this.grSep) {
                // 右チェック
                if (this.checkItemDataPosition(checkX + 1, checkY, i)) {
                    return [checkX + 1, checkY];
                }
            }
            if (positionX <= this.wallSize * this.grSep) {
                // 左チェック
                if (this.checkItemDataPosition(checkX - 1, checkY, i)) {
                    return [checkX - 1, checkY];
                }
            }
            if (positionY >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize * this.grSep) {
                // 下チェック
                if (this.checkItemDataPosition(checkX, checkY + 1, i)) {
                    return [checkX, checkY + 1];
                }
                else {
                    if (positionX >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize * this.grSep) {
                        // 右下チェック
                        if (this.checkItemDataPosition(checkX + 1, checkY + 1, i)) {
                            return [checkX + 1, checkY + 1];
                        }
                    }
                    if (positionX <= this.wallSize * this.grSep) {
                        // 左下チェック
                        if (this.checkItemDataPosition(checkX - 1, checkY + 1, i)) {
                            return [checkX - 1, checkY + 1];
                        }
                    }
                }
            }
            if (positionY <= this.wallSize * this.grSep) {
                // 上チェック
                if (this.checkItemDataPosition(checkX, checkY - 1, i)) {
                    return [checkX, checkY - 1];
                }
                else {
                    if (positionX >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize * this.grSep) {// 右上
                        if (this.checkItemDataPosition(checkX + 1, checkY - 1, i)) {
                            return [checkX + 1, checkY - 1];
                        }
                    }
                    if (positionX <= this.wallSize * this.grSep) {// 左上
                        if (this.checkItemDataPosition(checkX - 1, checkY - 1, i)) {
                            return [checkX - 1, checkY - 1];
                        }
                    }
                }
            }

            return null;
        };

        /** アイテムが指定した場所のフラグがONか否か
         * {int} checkX x座標(アイテムの左上基点、data単位)
         * {int} checkY y座標(アイテムの左上基点、data単位)
         * {int} index 部屋のindex
         * return あればtrue 無ければfalse
         */
        this.checkItemDataPosition = function (checkX, checkY, index) {
            if (this.itemAr[index] == null) return false;

            if (checkX >= 0 && checkY >= 0 &&
                checkX < this.itemAr[index].data.length &&
                checkY < this.itemAr[index].data[checkX].length) {
                if (this.itemAr[index].data[checkX][checkY] == 1) {
                    return true;
                }
            }
            return false;
        };

        /** アイテムが指定した座標内にあるか判定(壁なし)
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * return アイテムがあればthis.itemArのindex アイテムが無ければnull
         */
        this.checkItemPxNotWall = function (x, y, exclusionIndex) {
            var rtn = null;
            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;

            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;

                if (x >= this.itemAr[i].x * this.grSep &&
                    y >= this.itemAr[i].y * this.grSep &&
                    x < (this.itemAr[i].x + this.itemAr[i].width) * this.grSep &&
                    y < (this.itemAr[i].y + this.itemAr[i].height) * this.grSep) {
                    var positionX = Math.floor((x - this.itemAr[i].x * this.grSep) / this.grSep * this.grSplitMax);
                    var positionY = Math.floor((y - this.itemAr[i].y * this.grSep) / this.grSep * this.grSplitMax);

                    if (this.itemAr[i].data[positionX][positionY] == 1) {
                        if (exclusionIndex != i) {
                            rtn = i;
                            break;
                        }
                    }
                }
            }

            return rtn;
        };

        /** 指定したアイテムが指定した座標内にあるか判定
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} checkIndex チェックするアイテムのindex
         * return 座標内にあればtrue, なければfalse
         *
         */
        this.checkItemPxWithIndex = function (x, y, checkIndex) {
            try {

                if (x >= this.itemAr[checkIndex].x * this.grSep - this.wallSize / 2 * this.grSep &&
                    y >= this.itemAr[checkIndex].y * this.grSep - this.wallSize / 2 * this.grSep &&
                    x <= (this.itemAr[checkIndex].x + this.itemAr[checkIndex].width) * this.grSep + this.wallSize / 2 * this.grSep &&
                    y <= (this.itemAr[checkIndex].y + this.itemAr[checkIndex].height) * this.grSep + this.wallSize / 2 * this.grSep) {

                    var positionX = Math.floor((x - this.itemAr[checkIndex].x * this.grSep) / this.grSep * this.grSplitMax);
                    var positionY = Math.floor((y - this.itemAr[checkIndex].y * this.grSep) / this.grSep * this.grSplitMax);

//            window.console.log("ok:"+positionX+","+positionY);

                    if (positionX < 0) positionX = 0;
                    if (positionY < 0) positionY = 0;
                    if (positionX > this.itemAr[checkIndex].data.length) positionX = this.itemAr[checkIndex].data.length - 1;
                    if (positionY > this.itemAr[checkIndex].data[positionX].length) positionY = this.itemAr[checkIndex].data[positionX].length - 1;

//            window.console.log("change:"+positionX+","+positionY);

                    if (this.itemAr[checkIndex].data[positionX][positionY] == 1) {
                        return true;
                    }
                    else if (positionX != 0 && this.itemAr[checkIndex].data[positionX - 1][positionY] == 1) {
                        return true;
                    }
                    else if (positionY != 0 && this.itemAr[checkIndex].data[positionX][positionY - 1] == 1) {
                        return true;
                    }
                    else if (positionX != this.itemAr[checkIndex].data.length - 1 &&
                        this.itemAr[checkIndex].data[positionX + 1][positionY] == 1) {
                        return true;
                    }
                    else if (positionY != this.itemAr[checkIndex].data[positionX].length - 1 &&
                        this.itemAr[checkIndex].data[positionX][positionY + 1] == 1) {
                        return true;
                    }
                }
            }
            catch (e) {
                window.console.log(e);
            }

            return false;
        };

        /** 現在動かしているアイテムが重なっていないかチェック
         * {int} x x座標(グリッド単位)
         * {int} y y座標(グリッド単位)
         * {int} index 現在動かしている座標のindex
         * return アイテムがあればthis.itemArのindex アイテムが無ければnull
         */
        this.checkItemOverlap = function (x, y, index) {

            var rtn = null;
            for (var i = 0; i < this.itemAr[index].data.length; i++) {
                for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                    if (this.itemAr[index].data[i][j] == 1) {
                        rtn = this.checkItemPxNotWall((x + i / this.grSplitMax) * this.grSep + 1, (y + j / this.grSplitMax) * this.grSep + 1, index);
                        if (rtn != null) break;
                    }
                }
                if (rtn != null) break;
            }

            return rtn;
        };

        /** 開口部が生成できるか否か
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {string} type 開口部のtype
         * {int} index 部屋のindex
         * return  生成できればobject 無ければnull
         */
        this.createCheckRoomPositionOpening = function (x, y, type, index) {

            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;

            var gridX = dragRoom.DRAG.setPositionFit(x);// グリッド単位
            var gridY = dragRoom.DRAG.setPositionFit(y);// グリッド単位
            var positionX = gridX - this.itemAr[index].x; // 部屋を起点としたx座標（グリッド単位）
            var positionY = gridY - this.itemAr[index].y; // 部屋を起点としたy座標（グリッド単位）
            var text = "";

            var position = this.checkWallPosition(x, y, index);

            if (position != null) {
                positionX = position[0];
                positionY = position[1];
                gridX = positionX / this.grSplitMax + this.itemAr[index].x;
                gridY = positionY / this.grSplitMax + this.itemAr[index].y;

                // data内のどの位置か
                var gPositionX = x - gridX * this.grSep;
                var gPositionY = y - gridY * this.grSep;

                if (type == 20) {
                    var x = positionX / this.grSplitMax - this.wallSize / 2;
                    var y = positionY / this.grSplitMax - this.wallSize / 2;

                    if (positionX % (this.grSplitMax / this.grSplit) != 0) {
                        x = Math.ceil(positionX / (this.grSplitMax / this.grSplit)) / this.grSplit - this.wallSize / 2;
                    }
                    if (positionY % (this.grSplitMax / this.grSplit) != 0) {
                        y = Math.ceil(positionY / (this.grSplitMax / this.grSplit)) / this.grSplit - this.wallSize / 2;
                    }

                    if (this.checkOpening(x + this.itemAr[index].x, y + this.itemAr[index].y) == null ||
                        this.checkOpening(x + this.itemAr[index].x + this.wallSize, y + this.itemAr[index].y + this.wallSize) == null) { // 配置場所に開口部がなければ
                        // 柱追加
                        return new floorPlan.opening(x, y, this.wallSize, this.wallSize, text, type, index);
                    }
                    return null;
                }

//        window.console.log("p:" + positionX + "," + positionY);
//        window.console.log("g:" + gPositionX + "," + gPositionY);

                try {
                    if (gPositionX >= -this.wallSize * this.grSep && gPositionX <= this.wallSize * this.grSep) {// 左
                        if (this.itemAr[index].data[positionX][positionY + this.grSplitMax - 1] == 1 && (positionX == 0 || this.itemAr[index].data[positionX - 1][positionY] != 1)) {

                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller(positionX / this.grSplitMax - this.wallSize / 2 + this.itemAr[index].x, positionY / this.grSplitMax + this.itemAr[index].y, this.wallSize, 1) == null) {
                                return new floorPlan.opening(positionX / this.grSplitMax - this.wallSize / 2,
                                    positionY / this.grSplitMax,
                                    this.wallSize, 1, text, type, index);
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
                try {
                    if (gPositionY >= -this.wallSize * this.grSep && gPositionY <= this.wallSize * this.grSep) {// 上
//            window.console.log(positionX+this.grSplitMax-1 + "," + positionY + ":" + this.itemAr[index].data[positionX+this.grSplitMax-1]);
                        if (this.itemAr[index].data[positionX + this.grSplitMax - 1][positionY] == 1 && (positionY == 0 || this.itemAr[index].data[positionX][positionY - 1] != 1)) {
                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller(positionX / this.grSplitMax + this.itemAr[index].x, positionY / this.grSplitMax - this.wallSize / 2 + this.itemAr[index].y, 1, this.wallSize) == null) {
                                return new floorPlan.opening(positionX / this.grSplitMax, positionY / this.grSplitMax - this.wallSize / 2, 1, this.wallSize, text, type, index);
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
                try {
                    if ((gPositionX <= this.grSep / this.grSplitMax + this.wallSize * this.grSep && gPositionX >= this.grSep / this.grSplitMax - this.wallSize * this.grSep) ||
                        (gPositionX <= this.grSep / (this.grSplitMax / this.grSplit) + this.wallSize / 2 * this.grSep && gPositionX >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize / 2 * this.grSep)) {// 右
                        if (this.itemAr[index].data[positionX][positionY + this.grSplitMax - 1] == 1 && (this.itemAr[index].data.length == positionX + 1 || this.itemAr[index].data[positionX + 1][positionY] != 1 || (positionX % this.grSplit == 0 && this.itemAr[index].data[positionX][positionY + this.grSplit - 1] != 1))) {
                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller((positionX + 1) / this.grSplitMax - this.wallSize / 2 + this.itemAr[index].x, positionY / this.grSplitMax + this.itemAr[index].y, this.wallSize, 1) == null) {
                                return new floorPlan.opening((positionX + 1) / this.grSplitMax - this.wallSize / 2, positionY / this.grSplitMax, this.wallSize, 1, text, type, index);
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
                try {
                    if ((gPositionY >= this.grSep / this.grSplitMax - this.wallSize * this.grSep && gPositionY <= this.grSep / this.grSplitMax + this.wallSize * this.grSep) ||
                        (gPositionY >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize * this.grSep && gPositionY <= this.grSep / (this.grSplitMax / this.grSplit) + this.wallSize * this.grSep)) {// 下
                        if (this.itemAr[index].data[positionX + this.grSplitMax - 1][positionY] == 1 && (this.itemAr[index].data[positionX].length == positionY + 1 || this.itemAr[index].data[positionX][positionY + 1] != 1 || (positionY % this.grSplit == 0 && this.itemAr[index].data[positionX][positionY + this.grSplit - 1] != 1))) {
                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller(positionX / this.grSplitMax + this.itemAr[index].x, (positionY + 1) / this.grSplitMax - this.wallSize / 2 + this.itemAr[index].y, 1, this.wallSize) == null) {
                                return new floorPlan.opening(positionX / this.grSplitMax, (positionY + 1) / this.grSplitMax - this.wallSize / 2, 1, this.wallSize, text, type, index);
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
            }

            return null;
        };

        /** シンクが生成できるか否か
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {string} type シンクのtype
         * {int} index 部屋のindex
         * return  生成できればobject 無ければnull
         */
        this.createCheckRoomPositionSink = function (x, y, type, sink_width, index) {
            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;

            var gridX = dragRoom.DRAG.setPositionFit(x);// グリッド単位
            var gridY = dragRoom.DRAG.setPositionFit(y);// グリッド単位
            var positionX = gridX - this.itemAr[index].x; // 部屋を起点としたx座標（グリッド単位）
            var positionY = gridY - this.itemAr[index].y; // 部屋を起点としたy座標（グリッド単位）

            var width = sink_width + this.sinkMarginLR;
            var height = this.sinkDepth + this.sinkMarginBottom;

            if (type == 2) {
                width = sink_width;
                height = this.sinkDepth;
            }
            else if (type == 3) {
                width = sink_width + this.sinkMarginLR * 2;
                height = this.sinkDepth + this.sinkMarginBottom;
            }

            return new floorPlan.sink(positionX, positionY, width, height, type, index, 0, sink_width);
        };

        /** 開口部追加（柱以外の場合は開口部の両端に柱を追加する）
         * {floorPlan.opening} 追加する開口部
         * return なし
         */
        this.addOpening = function (editOpening) {
            if (editOpening != null) {
                this.openingAr.push(editOpening);
                if (editOpening.type != 20) {
                    // 両端に柱を追加
                    if (editOpening.width == this.wallSize) {
                        var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y) * this.grSep, 20, editOpening.roomId);
                        this.addOpening(newOpening1);

                        var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height) * this.grSep, 20, editOpening.roomId);
                        this.addOpening(newOpening2);
                    }
                    else {
                        var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, editOpening.roomId);
                        this.addOpening(newOpening1);

                        var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, editOpening.roomId);
                        this.addOpening(newOpening2);
                    }
                }
            }
        };

        /** 開口部移動
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} roomIndex 部屋のindex
         * {int} openingIndex 部屋のindex
         * return 開口部が移動できる場合true、移動できない場合はfalse
         */
        this.moveOpening = function (x, y, roomIndex, openingIndex) {
            var gridX = dragRoom.DRAG.setPositionFit(x);// グリッド単位
            var gridY = dragRoom.DRAG.setPositionFit(y);// グリッド単位
            var positionX = gridX - this.itemAr[roomIndex].x; // 部屋を起点としたx座標（グリッド単位）
            var positionY = gridY - this.itemAr[roomIndex].y; // 部屋を起点としたy座標（グリッド単位）
            var size = this.openingAr[openingIndex].width;
            if (this.openingAr[openingIndex].width == this.wallSize) size = this.openingAr[openingIndex].height;

//    size = 1;

            var position = this.checkWallPosition(x, y, roomIndex);

            if (position != null) {
                positionX = position[0];
                positionY = position[1];
                gridX = positionX / this.grSplitMax + this.itemAr[roomIndex].x;
                gridY = positionY / this.grSplitMax + this.itemAr[roomIndex].y;


                // グリッド内のどの位置か
                var gPositionX = x - gridX * this.grSep;
                var gPositionY = y - gridY * this.grSep;

                if (this.openingAr[openingIndex].type == 20) {
                    var x = positionX / this.grSplitMax - this.wallSize / 2;
                    var y = positionY / this.grSplitMax - this.wallSize / 2;

                    if (positionX % (this.grSplitMax / this.grSplit) != 0) {
                        x = Math.ceil(positionX / this.grSplitMax) - this.wallSize / 2;
                    }
                    if (positionY % (this.grSplitMax / this.grSplit) != 0) {
                        y = Math.ceil(positionY / this.grSplitMax) - this.wallSize / 2;
                    }

                    if (this.checkOpening(x + this.itemAr[roomIndex].x, y + this.itemAr[roomIndex].y) == null ||
                        this.checkOpening(x + this.itemAr[roomIndex].x + this.wallSize, y + this.itemAr[roomIndex].y + this.wallSize) == null) { // 配置場所に開口部がなければ
                        // 柱移動
                        this.openingAr[openingIndex].x = x;
                        this.openingAr[openingIndex].y = y;
                        this.openingAr[openingIndex].roomId = roomIndex;

                        return true;
                    }

                    return false;
                }

//        window.console.log(gPositionX+","+gPositionY);

                try {
                    if (gPositionX >= -this.wallSize * this.grSep && gPositionX <= this.wallSize * this.grSep) {// 左
                        if (this.itemAr[roomIndex].data[positionX][positionY + Math.floor(this.grSplitMax * size) - 1] == 1 && (positionX == 0 || this.itemAr[roomIndex].data[positionX - 1][positionY] != 1)) {

                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller(positionX / this.grSplitMax - this.wallSize / 2 + this.itemAr[roomIndex].x, positionY / this.grSplitMax + this.itemAr[roomIndex].y, this.wallSize, 1, openingIndex) == null) {

                                this.openingAr[openingIndex].x = positionX / this.grSplitMax - this.wallSize / 2;
                                this.openingAr[openingIndex].y = positionY / this.grSplitMax;
                                this.openingAr[openingIndex].width = this.wallSize;
                                this.openingAr[openingIndex].height = size;
                                this.openingAr[openingIndex].roomId = roomIndex;

                                // 範囲内の柱除去
                                var editOpening = this.openingAr[openingIndex];
                                this.removePillarRect(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, editOpening.width, editOpening.height);
                                this.focusIndex = this.openingAr.indexOf(editOpening);

                                // 両端に柱追加
                                var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening1);

                                var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening2);

                                return true;
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
                try {
                    if (gPositionY >= -this.wallSize * this.grSep && gPositionY <= this.wallSize * this.grSep) {// 上

                        if (this.itemAr[roomIndex].data[positionX + Math.floor(this.grSplitMax * size) - 1][positionY] == 1 && (positionY == 0 || this.itemAr[roomIndex].data[positionX][positionY - 1] != 1)) {
                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller(positionX / this.grSplitMax + this.itemAr[roomIndex].x, positionY / this.grSplitMax - this.wallSize / 2 + this.itemAr[roomIndex].y, 1, this.wallSize, openingIndex) == null) {
                                this.openingAr[openingIndex].x = positionX / this.grSplitMax;
                                this.openingAr[openingIndex].y = positionY / this.grSplitMax - this.wallSize / 2;
                                this.openingAr[openingIndex].width = size;
                                this.openingAr[openingIndex].height = this.wallSize;
                                this.openingAr[openingIndex].roomId = roomIndex;

                                // 範囲内の柱除去
                                var editOpening = this.openingAr[openingIndex];
                                this.removePillarRect(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, editOpening.width, editOpening.height);
                                this.focusIndex = this.openingAr.indexOf(editOpening);

                                // 両端に柱追加
                                var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening1);

                                var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening2);

                                return true;
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
                try {
                    if ((gPositionX <= this.grSep / this.grSplitMax + this.wallSize * this.grSep && gPositionX >= this.grSep / this.grSplitMax - this.wallSize * this.grSep) ||
                        (gPositionX <= this.grSep / (this.grSplitMax / this.grSplit) + this.wallSize * this.grSep && gPositionX >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize * this.grSep)) {// 右
                        if (this.itemAr[roomIndex].data[positionX][positionY + Math.floor(this.grSplitMax * size) - 1] == 1 && (this.itemAr[roomIndex].data.length == positionX + 1 || this.itemAr[roomIndex].data[positionX + 1][positionY] != 1 || (positionX % this.grSplit == 0 && this.itemAr[roomIndex].data[positionX][positionY + this.grSplit - 1] != 1))) {
                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller((positionX + 1) / this.grSplitMax - this.wallSize / 2 + this.itemAr[roomIndex].x, positionY / this.grSplitMax + this.itemAr[roomIndex].y, this.wallSize, 1, openingIndex) == null) {

                                this.openingAr[openingIndex].x = (positionX + 1) / this.grSplitMax - this.wallSize / 2;
                                this.openingAr[openingIndex].y = positionY / this.grSplitMax;
                                this.openingAr[openingIndex].width = this.wallSize;
                                this.openingAr[openingIndex].height = size;
                                this.openingAr[openingIndex].roomId = roomIndex;

                                // 範囲内の柱除去
                                var editOpening = this.openingAr[openingIndex];
                                this.removePillarRect(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, editOpening.width, editOpening.height);
                                this.focusIndex = this.openingAr.indexOf(editOpening);

                                // 両端に柱追加
                                var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening1);

                                var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening2);

                                return true;
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
                try {
                    if ((gPositionY >= this.grSep / this.grSplitMax - this.wallSize * this.grSep && gPositionY <= this.grSep / this.grSplitMax + this.wallSize * this.grSep) ||
                        (gPositionY >= this.grSep / (this.grSplitMax / this.grSplit) - this.wallSize * this.grSep && gPositionY <= this.grSep / (this.grSplitMax / this.grSplit) + this.wallSize * this.grSep)) {// 下
                        if (this.itemAr[roomIndex].data[positionX + Math.floor(this.grSplitMax * size) - 1][positionY] == 1 && (this.itemAr[roomIndex].data[positionX].length == positionY + 1 || this.itemAr[roomIndex].data[positionX][positionY + 1] != 1 || (positionY % this.grSplit == 0 && this.itemAr[roomIndex].data[positionX][positionY + this.grSplit - 1] != 1))) {
                            // 配置場所に柱以外の開口部がなければ
                            if (this.checkOpeningRectNotPiller(positionX / this.grSplitMax + this.itemAr[roomIndex].x, (positionY + 1) / this.grSplitMax - this.wallSize / 2 + this.itemAr[roomIndex].y, 1, this.wallSize, openingIndex) == null) {

                                this.openingAr[openingIndex].x = positionX / this.grSplitMax;
                                this.openingAr[openingIndex].y = (positionY + 1) / this.grSplitMax - this.wallSize / 2;
                                this.openingAr[openingIndex].width = size;
                                this.openingAr[openingIndex].height = this.wallSize;
                                this.openingAr[openingIndex].roomId = roomIndex;

                                // 範囲内の柱除去
                                var editOpening = this.openingAr[openingIndex];
                                this.removePillarRect(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, editOpening.width, editOpening.height);
                                this.focusIndex = this.openingAr.indexOf(editOpening);

                                // 両端に柱追加
                                var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening1);

                                var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, roomIndex);
                                this.addOpening(newOpening2);

                                return true;
                            }
                        }
                    }
                }
                catch (e) {
                    window.console.log(e);
                }
            }

            // 移動しない場合は座標位置によって、向きを変更させる
            this.checkOpeningFlip(x, y, openingIndex);

            return false;
        };

        /** 指定した座標に応じて、開口部を反転させる
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} openingIndex 部屋のindex
         * return なし
         */
        this.checkOpeningFlip = function (x, y, openingIndex) {
            if (this.openingAr[openingIndex].width == this.wallSize) {
                // 左右
                if ((this.itemAr[this.openingAr[openingIndex].roomId].x + this.openingAr[openingIndex].x) * this.grSep > x) {
                    // 左
                    this.openingAr[openingIndex].flip = true;
                }
                else {
                    // 右
                    this.openingAr[openingIndex].flip = false;
                }
            }
            else {
                // 上下
                if ((this.itemAr[this.openingAr[openingIndex].roomId].y + this.openingAr[openingIndex].y) * this.grSep > y) {
                    // 上
                    this.openingAr[openingIndex].flip = false;
                }
                else {
                    // 下
                    this.openingAr[openingIndex].flip = true;
                }
            }
        };

        /** 指定した座標に開口部があるか
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} notCheckIndex チェックしない開口部
         * return  存在すれば開口部のindexを返す 無ければnull
         */
        this.checkOpeningPx = function (x, y, notCheckIndex) {
            /*
    x -= this.startGridX*this.grSep;
    y -= this.startGridY*this.grSep;
 */
            for (var i = 0; i < this.openingAr.length; i++) {
                if (i == notCheckIndex) continue;
                var roomIdx = this.openingAr[i].roomId;
                if (this.itemAr[roomIdx] == null) continue;

                var gOpeningX = (this.itemAr[roomIdx].x + this.openingAr[i].x) * this.grSep;// px単位
                var gOpeningY = (this.itemAr[roomIdx].y + this.openingAr[i].y) * this.grSep;// px単位

                /*
        if (x >= gOpeningX && x <= gOpeningX + this.openingAr[i].width*this.grSep) {
            if (y >= gOpeningY && y <= gOpeningY + this.openingAr[i].height*this.grSep) {
                return i;
            }
        }
 */

                if ((this.openingAr[i].displayWidth >= 0 &&
                        x >= gOpeningX &&
                        x <= gOpeningX + this.openingAr[i].displayWidth) ||
                    (this.openingAr[i].displayWidth < 0 &&
                        x >= gOpeningX + this.openingAr[i].displayWidth &&
                        x <= gOpeningX + this.openingAr[i].width * this.grSep )) {
                    if ((this.openingAr[i].displayHeight >= 0 &&
                            y >= gOpeningY &&
                            y <= gOpeningY + this.openingAr[i].displayHeight) ||
                        (this.openingAr[i].displayHeight < 0 &&
                            y >= gOpeningY + this.openingAr[i].displayHeight &&
                            y <= gOpeningY + this.openingAr[i].height * this.grSep )) {
//                 window.console.log("ok:" + this.openingAr[i].displayWidth + "," + this.openingAr[i].displayHeight);
                        return i;
                    }
                }
            }

            return null;
        };

        /** 指定した座標に開口部があるか（柱を除く）
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} notCheckIndex チェックしない開口部
         * return  存在すれば開口部のindexを返す 無ければnull
         */
        this.checkOpeningPxNotPiller = function (x, y, notCheckIndex) {
            for (var i = 0; i < this.openingAr.length; i++) {
                if (i == notCheckIndex) continue;
                if (this.openingAr[i].type == 20) continue;

                var roomIdx = this.openingAr[i].roomId;

                var gOpeningX = (this.itemAr[roomIdx].x + this.openingAr[i].x) * this.grSep;// px単位
                var gOpeningY = (this.itemAr[roomIdx].y + this.openingAr[i].y) * this.grSep;// px単位

                if ((this.openingAr[i].displayWidth >= 0 &&
                        x >= gOpeningX &&
                        x <= gOpeningX + this.openingAr[i].displayWidth) ||
                    (this.openingAr[i].displayWidth < 0 &&
                        x >= gOpeningX + this.openingAr[i].displayWidth &&
                        x <= gOpeningX + this.openingAr[i].width * this.grSep )) {
                    if ((this.openingAr[i].displayHeight >= 0 &&
                            y >= gOpeningY &&
                            y <= gOpeningY + this.openingAr[i].displayHeight) ||
                        (this.openingAr[i].displayHeight < 0 &&
                            y >= gOpeningY + this.openingAr[i].displayHeight &&
                            y <= gOpeningY + this.openingAr[i].height * this.grSep )) {

                        return i;
                    }
                }
            }

            return null;
        };

        /** 指定した範囲に開口部があるか（柱を除く
         * {int} x x座標(グリッド単位)
         * {int} y y座標(グリッド単位)
         * {int} width 幅(グリッド単位)
         * {int} height 高さ(グリッド単位)
         * {int} notCheckIndex チェックしない開口部
         * return  存在すれば開口部のindexを返す 無ければnull
         */
        this.checkOpeningRectNotPiller = function (x, y, width, height, notCheckIndex) {

            for (var i = 0; i < this.openingAr.length; i++) {
                if (i == notCheckIndex) continue;
                if (this.openingAr[i].type == 20) continue;

                var roomIdx = this.openingAr[i].roomId;

                if (x + width > this.openingAr[i].x + this.itemAr[roomIdx].x + 1 / 12 &&
                    x < this.openingAr[i].x + this.itemAr[roomIdx].x + this.openingAr[i].width - 1 / 12) {
                    if (y + height > this.openingAr[i].y + this.itemAr[roomIdx].y + 1 / 12 &&
                        y < this.openingAr[i].y + this.itemAr[roomIdx].y + this.openingAr[i].height - 1 / 12) {
                        return i;
                    }
                }
            }

            return null;
        };

        /** 指定した範囲に開口部があるか（柱を除く
         * {int} x x座標(グリッド単位)
         * {int} y y座標(グリッド単位)
         * {int} width 幅(グリッド単位)
         * {int} height 高さ(グリッド単位)
         * {int} notCheckIndex チェックしない開口部
         * return  範囲内の開口部の一覧
         */
        this.checkOpeningRectNotPillerList = function (x, y, width, height, notCheckIndex) {
            var output = [];

            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;
            width = Math.round(width * 10000) / 10000;
            height = Math.round(height * 10000) / 10000;

            for (var i = 0; i < this.openingAr.length; i++) {
                if (i == notCheckIndex) continue;
                if (this.openingAr[i].type == 20) continue;

                var roomIdx = this.openingAr[i].roomId;

                var centerX = this.itemAr[roomIdx].x + this.openingAr[i].x + this.openingAr[i].width / 2;
                var centerY = this.itemAr[roomIdx].y + this.openingAr[i].y + this.openingAr[i].height / 2;

                if (x <= centerX && x + width >= centerX &&
                    y <= centerY && y + height >= centerY) {
                    output.push(this.openingAr[i]);
                }
            }

            return output;
        };

        /** 指定した座標に開口部があるか
         * {int} x x座標(グリッド単位)
         * {int} y y座標(グリッド単位)
         * {int} notCheckIndex チェックしない開口部
         * return  存在すれば開口部のindexを返す 無ければnull
         */
        this.checkOpening = function (x, y, notCheckIndex) {

            for (var i = 0; i < this.openingAr.length; i++) {
                if (i == notCheckIndex) continue;

                var roomIdx = this.openingAr[i].roomId;

                if (x >= this.openingAr[i].x + this.itemAr[roomIdx].x &&
                    x <= this.openingAr[i].x + this.itemAr[roomIdx].x + this.openingAr[i].width) {
                    if (y >= this.openingAr[i].y + this.itemAr[roomIdx].y &&
                        y <= this.openingAr[i].y + this.itemAr[roomIdx].y + this.openingAr[i].height) {
                        return i;
                    }
                }
            }

            return null;
        };

        /** 指定した座標にシンクがあるか
         * {int} x x座標(px単位)
         * {int} y y座標(px単位)
         * {int} notCheckIndex チェックしないシンク
         * return  存在すればシンクのindexを返す 無ければnull
         */
        this.checkSinkPx = function (x, y, notCheckIndex) {

            for (var i = 0; i < this.sinkAr.length; i++) {
                if (i == notCheckIndex) continue;
                var roomIdx = this.sinkAr[i].roomId;
                if (this.itemAr[roomIdx] == null) continue;

                var gSinkX = (this.itemAr[roomIdx].x + this.sinkAr[i].x) * this.grSep;// px単位
                var gSinkY = (this.itemAr[roomIdx].y + this.sinkAr[i].y) * this.grSep;// px単位
                var gSinkWidth = this.sinkAr[i].width * this.grSep;
                var gSinkHeight = this.sinkAr[i].height * this.grSep;

                if (x >= gSinkX && x <= gSinkX + gSinkWidth) {
                    if (y >= gSinkY && y <= gSinkY + gSinkHeight) {
                        return i;
                    }
                }
            }

            return null;
        };

        /** 開口部の大きさを変更する
         * {int} 追加する値(grid)
         * return なし
         */
        this.resizeOpening = function (vector) {
            vector = 1.0 * vector / this.grSplit;

            if (this.focusIndex != null && this.checkSelectOpening()) {
                var editOpening = this.openingAr[this.focusIndex];
                if (editOpening.width == this.wallSize) {
                    var size = Math.floor((editOpening.height + vector) * this.grSplit) / this.grSplit;
                    if (size > 0) {
                        if (vector < 0) {
                            editOpening.height = size;

                            // 両端に柱を追加
                            var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y) * this.grSep, 20, editOpening.roomId);
                            this.addOpening(newOpening1);

                            var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height) * this.grSep, 20, editOpening.roomId);
                            this.addOpening(newOpening2);
                        }
                        else {
                            var x = (this.itemAr[editOpening.roomId].x + editOpening.x + this.wallSize / 2) * this.grSep;
                            var y = (this.itemAr[editOpening.roomId].y + editOpening.y + size) * this.grSep;

                            if (this.checkItemPxWithRoom(x, y, editOpening.roomId) &&
                                (this.checkOpeningRectNotPiller(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, editOpening.width, size, this.focusIndex) == null)) {
                                editOpening.height = size;

                                // 範囲内の柱除去
                                this.removePillarRect(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, editOpening.width, editOpening.height);
                                this.focusIndex = this.openingAr.indexOf(editOpening);

                                // 両端に柱を追加
                                var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y) * this.grSep, 20, editOpening.roomId);
                                this.addOpening(newOpening1);

                                var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width / 2) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height) * this.grSep, 20, editOpening.roomId);
                                this.addOpening(newOpening2);
                            }
                        }
                    }
                }
                else {
                    var size = Math.floor((editOpening.width + vector) * this.grSplit) / this.grSplit;
                    if (size > 0) {
                        if (vector < 0) {
                            editOpening.width = size;

                            // 両端に柱を追加
                            var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, editOpening.roomId);
                            this.addOpening(newOpening1);

                            var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, editOpening.roomId);
                            this.addOpening(newOpening2);
                        }
                        else {
                            var x = (this.itemAr[editOpening.roomId].x + editOpening.x + size) * this.grSep;
                            var y = (this.itemAr[editOpening.roomId].y + editOpening.y + this.wallSize / 2) * this.grSep;

                            if (this.checkItemPxWithRoom(x, y, editOpening.roomId) &&
                                (this.checkOpeningRectNotPiller(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, size, editOpening.height, this.focusIndex) == null)) {
                                editOpening.width = size;

                                // 範囲内の柱除去
                                this.removePillarRect(editOpening.x + this.itemAr[editOpening.roomId].x, editOpening.y + this.itemAr[editOpening.roomId].y, editOpening.width, editOpening.height);
                                this.focusIndex = this.openingAr.indexOf(editOpening);

                                // 両端に柱を追加
                                var newOpening1 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, editOpening.roomId);
                                this.addOpening(newOpening1);

                                var newOpening2 = this.createCheckRoomPositionOpening((editOpening.x + this.itemAr[editOpening.roomId].x + editOpening.width) * this.grSep, (editOpening.y + this.itemAr[editOpening.roomId].y + editOpening.height / 2) * this.grSep, 20, editOpening.roomId);
                                this.addOpening(newOpening2);
                            }
                        }
                    }
                }
            }
            this.draw();
        };

        /** アイテムの座標をグリッドに合わせてフィットさせる
         * {int} value(px)
         * return 変換後の値(グリッド単位）
         */
        this.setPositionFit = function (value) {
            return Math.floor(value / this.grSep);
        };

        /** アイテムの座標をグリッドに合わせてフィットさせる(分割対応)
         * {int} value(px)
         * return 変換後の値(グリッド単位）
         */
        this.setPositionSplitFit = function (value) {
            return Math.floor(value / this.grSep * this.grSplit) / this.grSplit;
        };

        /** 追加している情報の更新
         * return なし
         */
        this.updateInfo = function () {
            var infoText = "<table>";
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;

                if (this.focusIndex == i && !this.checkSelectOpening() && !this.checkSelectSink()) {
                    infoText += "<tr class='red-mark'><td>" + this.itemAr[i].name + "</td><td>" + this.itemAr[i].count + this.itemAr[i].unit + "</td></tr>";
                }
                else {
                    infoText += "<tr><td>" + this.itemAr[i].name + "</td><td>" + this.itemAr[i].count + this.itemAr[i].unit + "</td></tr>";
                }
            }
            infoText += "</table>";

            $(".info-list").html(infoText);

            $(".info-list tr").on('click', function () {
                if (!dragRoom.DRAG.checkSelectOpening() && !dragRoom.DRAG.checkSelectSink()) {
                    var index = $(this).index();
                    dragRoom.DRAG.setFocus(index);
                }
            });
        };

        /** ドアが全部屋にあるかチェック
         * return エラーメッセージ
         */
        this.checkDoor = function () {
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].type >= 90) continue;// 部屋カテゴリじゃないものは除く
                var flg = false;
                for (var j = 0; j < this.openingAr.length; j++) {
                    if (this.openingAr[j].type == 20 || this.openingAr[j].type < 19) continue;// 窓/柱の場合、skip

                    if (i != this.openingAr[j].roomId) {
                        // 部屋内にドアがあるかチェック
                        if (this.checkRoomWallOpening(i, j)) {
                            flg = true;
                            break;
                        }
                    }
                    else {
                        flg = true;
                        break;
                    }
                }
                if (!flg) {
                    return this.itemAr[i].name + "にドアがありません。";
                }
            }
            return "全部屋にドアが設置されています。";
        };

        /** ドアが全部屋にあるかチェック(保存時用)
         * return エラーメッセージ|true
         */
        this.checkDoorSave = function () {
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].type >= 90) continue;// 部屋カテゴリじゃないものは除く
                var flg = false;
                for (var j = 0; j < this.openingAr.length; j++) {
                    if (this.openingAr[j].type == 20 || this.openingAr[j].type < 19) continue;// 窓/柱の場合、skip

                    if (i != this.openingAr[j].roomId) {
                        // 部屋内にドアがあるかチェック
                        if (this.checkRoomWallOpening(i, j)) {
                            flg = true;
                            break;
                        }
                    }
                    else {
                        flg = true;
                        break;
                    }
                }
                if (!flg) {
                    return this.itemAr[i].name + "にドアがありません。";
                }
            }
            return true;
        };

        /** キッチンが複数ないかチェック
         * return エラーメッセージ
         */
        this.checkKitchen = function () {
            var flg = false;
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].type >= 16 && this.itemAr[i].type <= 18) {
                    if (flg) return "キッチンが複数あります。";
                    flg = true;
                }
            }
            return "　";
        };

        /** 指定範囲内の柱を除去
         * {int} x 左上x座標　 (グリッド単位)
         * {int} y 左上y座標 　(グリッド単位）
         * {int} width 幅 　 （グリッド単位）
         * {int} height 高さ （グリッド単位）
         * return なし
         */
        this.removePillarRect = function (x, y, width, height) {
            for (var i = this.openingAr.length - 1; i >= 0; i--) {
                if (this.openingAr[i].type != 20) continue;

                if (x < this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x + this.openingAr[i].width / 2 &&
                    x + width > this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x + this.openingAr[i].width / 2 &&
                    y < this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y + this.openingAr[i].height / 2 &&
                    y + height > this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y + this.openingAr[i].height / 2) {
                    // 重なる場合、除去
                    this.openingAr.splice(i, 1);
                }
            }
        };

        /** 新規追加した部屋の壁内に柱を設置（半間おき）
         * {int} roomId 新規追加した部屋番号
         * return なし
         */
        this.createNewRoomPillar = function (roomId) {

            for (var i = 0; i <= this.itemAr[roomId].width; i++) {
                for (var j = 0; j <= this.itemAr[roomId].height; j++) {
                    if (i % this.itemAr[roomId].width == 0 || j % this.itemAr[roomId].height == 0) {
                        var positionX = (this.itemAr[roomId].x + i) * this.grSep;
                        var positionY = (this.itemAr[roomId].y + j) * this.grSep;

                        // 四捨五入
                        positionX = Math.round(positionX * 10000) / 10000;
                        positionY = Math.round(positionY * 10000) / 10000;

                        // この部屋の内部であれば
                        if (this.checkItemPxWithRoom(positionX, positionY, roomId)) {
                            var newOpening = this.createCheckRoomPositionOpening(positionX, positionY, 20, roomId);
                            this.addOpening(newOpening);
                        }
                    }
                }
            }
        };

        /** 全部屋の壁内に柱を設置
         * return なし
         */
        this.createPillar = function () {
            for (var index = 0; index < this.itemAr.length; index++) {
                var x = this.itemAr[index].x * this.grSep;
                var y = this.itemAr[index].y * this.grSep;
                var width = this.itemAr[index].width * this.grSep;
                var height = this.itemAr[index].height * this.grSep;

                // 部屋の角に柱を設置
                for (var i = 0; i < this.itemAr[index].data.length; i++) {
                    for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                        var positionX = x + i * this.grSep / this.grSplitMax;
                        var positionY = y + j * this.grSep / this.grSplitMax;

                        if (this.itemAr[index].data[i][j] != 1) {
                            if (i > 0 && this.itemAr[index].data[i - 1][j] == 1) {
                                // 左
                                if (j > 0 && this.itemAr[index].data[i][j - 1] == 1) {
                                    // 上
                                    var newOpening = this.createCheckRoomPositionOpening(positionX, positionY, 20, index);
                                    this.addOpening(newOpening);
                                }
                                if (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] == 1) {
                                    // 下
                                    var newOpening = this.createCheckRoomPositionOpening(positionX, positionY + this.grSep / this.grSplitMax, 20, index);
                                    this.addOpening(newOpening);
                                }
                            }
                            if (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1][j] == 1) {
                                // 右
                                if (j > 0 && this.itemAr[index].data[i][j - 1] == 1) {
                                    // 上
                                    var newOpening = this.createCheckRoomPositionOpening(positionX + this.grSep / this.grSplitMax, positionY, 20, index);
                                    this.addOpening(newOpening);
                                }
                                if (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] == 1) {
                                    // 下
                                    var newOpening = this.createCheckRoomPositionOpening(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax, 20, index);
                                    this.addOpening(newOpening);
                                }
                            }
                        }
                        else {
                            if (i == 0 || (i > 0 && this.itemAr[index].data[i - 1].length <= j) ||
                                (i > 0 && this.itemAr[index].data[i - 1].length > j && this.itemAr[index].data[i - 1][j] != 1)) {
                                // 左
                                if (j == 0 || (j > 0 && this.itemAr[index].data[i][j - 1] != 1)) {
                                    // 上
                                    var newOpening = this.createCheckRoomPositionOpening(positionX, positionY, 20, index);
                                    this.addOpening(newOpening);
                                }
                                if (j == this.itemAr[index].height * this.grSplitMax - 1 ||
                                    j == this.itemAr[index].data[i].length - 1 ||
                                    (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] != 1 )) {
                                    // 下
                                    var newOpening = this.createCheckRoomPositionOpening(positionX, positionY + this.grSep / this.grSplitMax, 20, index);
                                    this.addOpening(newOpening);
                                }

                            }
                            if (i == this.itemAr[index].width * this.grSplitMax - 1 ||
                                i == this.itemAr[index].data.length - 1 ||
                                (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1].length <= j) ||
                                (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1].length > j && this.itemAr[index].data[i + 1][j] != 1)) {
                                // 右
                                if (j == 0 || (j > 0 && this.itemAr[index].data[i][j - 1] != 1)) {
                                    // 上
                                    var newOpening = this.createCheckRoomPositionOpening(positionX + this.grSep / this.grSplitMax, positionY, 20, index);
                                    this.addOpening(newOpening);
                                }
                                if (j == this.itemAr[index].height * this.grSplitMax - 1 ||
                                    j == this.itemAr[index].data[i].length - 1 ||
                                    (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] != 1 )) {
                                    // 下
                                    var newOpening = this.createCheckRoomPositionOpening(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax, 20, index);
                                    this.addOpening(newOpening);
                                }
                            }
                        }
                    }
                }
            }

            // 柱以外の開口部の両端に柱を設置
            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i].type == 20) continue;

                var roomId = this.openingAr[i].roomId;

                var x1 = this.openingAr[i].x + this.itemAr[roomId].x;
                var y1 = this.openingAr[i].y + this.itemAr[roomId].y;
                var x2 = x1;
                var y2 = y1;

                if (this.openingAr[i].width > this.openingAr[i].height) {
                    y1 += this.openingAr[i].height / 2;
                    y2 += this.openingAr[i].height / 2;
                    x2 += this.openingAr[i].width;
                }
                else {
                    x1 += this.openingAr[i].width / 2;
                    x2 += this.openingAr[i].width / 2;
                    y2 += this.openingAr[i].height;
                }

                x1 *= this.grSep;
                y1 *= this.grSep;
                x2 *= this.grSep;
                y2 *= this.grSep;

                var newOpening1 = this.createCheckRoomPositionOpening(x1, y1, 20, roomId);
                this.addOpening(newOpening1);

                var newOpening2 = this.createCheckRoomPositionOpening(x2, y2, 20, roomId);
                this.addOpening(newOpening2);
            }

            this.draw();
        };

        /** 間取りが一致するかチェック
         * {string} 比較データ1
         * {string} 比較データ2
         * {bool} 回転させる場合false,回転させない場合true
         * return 回転等させてデータが一致する場合はtrue, 一致しない場合はfalse
         */
        this.checkMatchRoom = function (dataStr1, dataStr2, notRotate) {
            if (dataStr1 == dataStr2) return true;

            if (dataStr1 === "" || dataStr2 === "") return false;

            var data1 = JSON.parse(dataStr1);
            var data2 = JSON.parse(dataStr2);

            // 重複している開口部データがある場合、除去
            for (var i = 0; i < data1.openingAr.length; i++) {
                for (var j = data1.openingAr.length - 1; j > i; j--) {
                    if (data1.openingAr[i].type === data1.openingAr[j].type &&
                        data1.openingAr[i].roomId === data1.openingAr[j].roomId) {

                        // 四捨五入してxとy座標が一致する場合
                        data1.openingAr[i].x = Math.round(data1.openingAr[i].x * 10000) / 10000;
                        data1.openingAr[i].y = Math.round(data1.openingAr[i].y * 10000) / 10000;
                        data1.openingAr[j].x = Math.round(data1.openingAr[j].x * 10000) / 10000;
                        data1.openingAr[j].y = Math.round(data1.openingAr[j].y * 10000) / 10000;

                        if (data1.openingAr[i].x == data1.openingAr[j].x &&
                            data1.openingAr[i].y == data1.openingAr[j].y) {
                            data1.openingAr.splice(j, 1);
                        }
                    }
                }
            }
            for (var i = 0; i < data2.openingAr.length; i++) {
                for (var j = data2.openingAr.length - 1; j > i; j--) {
                    if (data2.openingAr[i].type === data2.openingAr[j].type &&
                        data2.openingAr[i].roomId === data2.openingAr[j].roomId) {

                        // 四捨五入してxとy座標が一致する場合
                        data2.openingAr[i].x = Math.round(data2.openingAr[i].x * 10000) / 10000;
                        data2.openingAr[i].y = Math.round(data2.openingAr[i].y * 10000) / 10000;
                        data2.openingAr[j].x = Math.round(data2.openingAr[j].x * 10000) / 10000;
                        data2.openingAr[j].y = Math.round(data2.openingAr[j].y * 10000) / 10000;

                        if (data2.openingAr[i].x == data2.openingAr[j].x &&
                            data2.openingAr[i].y == data2.openingAr[j].y) {
                            data2.openingAr.splice(j, 1);
                        }
                    }
                }
            }

            dataStr2 = JSON.stringify(data2);

            // 部屋と開口部の数が一致するかチェック
            if (data1.itemAr.length != data2.itemAr.length || data1.openingAr.length != data2.openingAr.length || data1.sinkAr.length != data2.sinkAr.length) {
                window.console.log("ng1:" + data1.itemAr.length + "vs" + data2.itemAr.length + ":" + data1.openingAr.length + "vs" + data2.openingAr.length);
                return false;
            }

            // 回転させてチェック
            var rotateCount = 4;
            if (notRotate) rotateCount = 1;
            for (var rotate = 0; rotate < rotateCount; rotate++) {
                // データを初期状態に戻す
                data2 = JSON.parse(dataStr2);

                // 回転処理を行うため、一度canvasにデータをインポートする
                convertRoom.importData(dataStr2);

                for (var count = 0; count < rotate; count++) this.rotateRoom(0);

                // データを読み込み直す
                data2 = JSON.parse(convertRoom.exportData(function (e) {
                }));

                // 部屋が一致するかチェック
                for (var i = data1.itemAr.length - 1; i >= 0; i--) {
                    for (var j = data2.itemAr.length - 1; j >= 0; j--) {
                        // dataのサイズを同じにする
                        var maxLength1 = data1.itemAr[i].data.length;
                        if (maxLength1 < data2.itemAr[j].data.length) {
                            maxLength1 = data2.itemAr[j].data.length;
                        }
                        while (maxLength1 >= data1.itemAr[i].data.length) data1.itemAr[i].data.push([]);
                        while (maxLength1 >= data2.itemAr[j].data.length) data2.itemAr[j].data.push([]);
                        for (var m = 0; m < maxLength1; m++) {
                            var maxLength2 = 0;
                            maxLength2 = data1.itemAr[i].data[m].length;
                            if (maxLength2 < data2.itemAr[j].data[m].length) {
                                maxLength2 = data2.itemAr[j].data[m].length;
                            }
                            while (maxLength2 > data1.itemAr[i].data[m].length) data1.itemAr[i].data[m].push(0);
                            while (maxLength2 > data2.itemAr[j].data[m].length) data2.itemAr[j].data[m].push(0);
                            for (var n = 0; n < maxLength2; n++) {
                                try {
                                    if (data1.itemAr[i].data[m][n] != 1) data1.itemAr[i].data[m][n] = 0;
                                    if (data2.itemAr[j].data[m][n] != 1) data2.itemAr[j].data[m][n] = 0;
                                }
                                catch (e) {
                                }
                            }
                        }

                        var object1String = JSON.stringify(data1.itemAr[i]);
                        var object2String = JSON.stringify(data2.itemAr[j]);

                        if (object1String === object2String) {
                            data2.itemAr.splice(j, 1);
                            break;
                        }
                    }
                }

                if (data2.itemAr.length > 0) {
                    continue;
                }

                // 開口部が一致するかチェック
                for (var i = data1.openingAr.length - 1; i >= 0; i--) {
                    for (var j = data2.openingAr.length - 1; j >= 0; j--) {

                        var object1String = JSON.stringify(data1.openingAr[i]);
                        var object2String = JSON.stringify(data2.openingAr[j]);

                        if (object1String === object2String) {
                            data2.openingAr.splice(j, 1);
                            break;
                        }
                    }
                }

                if (data2.openingAr.length > 0) {
                    continue;
                }

                // シンクが一致するかチェック
                for (var i = data1.sinkAr.length - 1; i >= 0; i--) {
                    for (var j = data2.sinkAr.length - 1; j >= 0; j--) {

                        var object1String = JSON.stringify(data1.sinkAr[i]);
                        var object2String = JSON.stringify(data2.sinkAr[j]);

                        if (object1String === object2String) {
                            data2.sinkAr.splice(j, 1);
                            break;
                        }
                    }
                }

                if (data2.sinkAr.length > 0) {
                    continue;
                }

                return true;
            }

            return false;
        };

        /** 開口部一覧を取得
         * {int} チェックする部屋ID
         * return 開口部の配列一覧（roomIDをチェック部屋IDに変えたもの）
         */
        this.checkTouchOpening = function (index) {
            var touchOpening = [];

            if (this.itemAr[index] == null) return null;

            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i] == null) continue;
                if (this.itemAr[this.openingAr[i].roomId] == null) continue;

                if (this.openingAr[i].roomId == index) {
                    // そのままのデータをpush
                    var newOpening = JSON.parse(JSON.stringify(this.openingAr[i]));
                    newOpening.roomId = 0;
                    touchOpening.push(newOpening);
//            window.console.log("sonomama:"+this.openingAr[i]);
                    continue;
                }

                var x = this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x;
                var y = this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y;
                var width = this.openingAr[i].width;
                var height = this.openingAr[i].height;

                if (this.checkItemPxWithRoom((x + width / 2) * this.grSep, (y + height / 2) * this.grSep, index)) {
                    // indexを起点としたデータに変換してpush
                    var newOpening = JSON.parse(JSON.stringify(this.openingAr[i]));

                    newOpening.x = x - this.itemAr[index].x;
                    newOpening.y = y - this.itemAr[index].y;
                    newOpening.roomId = 0;

                    touchOpening.push(newOpening);
// window.console.log("new:"+newOpening);

                    continue;
                }
            }

            return touchOpening;
        };

        /** 開口部一覧を取得
         * {array} チェックする部屋IDリスト
         * return 開口部の配列一覧（roomIDをチェック部屋IDに変えたもの）
         */
        this.checkTouchOpeningMulti = function (indexList) {
            var touchOpening = [];

            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i] == null) continue;

                var index = indexList.indexOf(this.openingAr[i].roomId);
                if (index >= 0) {
                    // そのままのデータをpush
                    var newOpening = JSON.parse(JSON.stringify(this.openingAr[i]));
                    newOpening.roomId = index;
                    touchOpening.push(newOpening);
                    continue;
                }

                var x = this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x;
                var y = this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y;
                var width = this.openingAr[i].width;
                var height = this.openingAr[i].height;

                for (var j = 0; j < indexList.length; j++) {
                    var index = indexList[j];
                    if (this.itemAr[index] == null) continue;

                    if (this.checkItemPxWithRoom((x + width / 2) * this.grSep, (y + height / 2) * this.grSep, index)) {
                        // indexを起点としたデータに変換してpush
                        var newOpening = JSON.parse(JSON.stringify(this.openingAr[i]));

                        newOpening.x = x - this.itemAr[index].x;
                        newOpening.y = y - this.itemAr[index].y;
                        newOpening.roomId = j;

                        touchOpening.push(newOpening);

                        continue;
                    }
                }
            }

            return touchOpening;
        };

        /** シンク一覧を取得
         * {int} チェックする部屋ID
         * return 開口部の配列一覧（roomIDをチェック部屋IDに変えたもの）
         */
        this.checkTouchSink = function (index) {
            var touchSink = [];

            if (this.itemAr[index] == null) return null;

            for (var i = 0; i < this.sinkAr.length; i++) {
                if (this.sinkAr[i] == null) continue;

                if (this.sinkAr[i].roomId == index) {
                    // そのままのデータをpush
                    var newSink = JSON.parse(JSON.stringify(this.sinkAr[i]));
                    newSink.roomId = 0;
                    touchSink.push(newSink);
                    continue;
                }
            }

            return touchSink;
        };

        /** 指定したtypeが一致するものと接するかチェック
         * {int} index チェックする部屋ID
         * {int} checkType 指定type
         * return 指定したtypeと接する配列の一覧を取得
         */
        this.checkTouchType = function (index, checkType) {
            var touchList = this.checkTouchRoom(index);
            var touchCheckTypeAr = [];

            for (var i = 0; i < touchList.length; i++) {
                if (this.itemAr[touchList[i]].type == checkType) touchCheckTypeAr.push(touchList[i]);
            }

            return touchCheckTypeAr;
        };

        /** 接する部屋一覧を取得
         * {int} チェックする部屋ID
         * return 接する部屋の配列を取得
         */
        this.checkTouchRoom = function (index) {
            var touchRoomAr = [];

            for (var k = 0; k < this.itemAr.length; k++) {
                if (index == k) continue;
                if (this.itemAr[k] == null) continue;

                var distanceX = Math.abs((this.itemAr[k].x + this.itemAr[k].width / 2) - (this.itemAr[index].x + this.itemAr[index].width / 2));
                var distanceY = Math.abs((this.itemAr[k].y + this.itemAr[k].height / 2) - (this.itemAr[index].y + this.itemAr[index].height / 2));

                if (distanceX <= (this.itemAr[k].width + this.itemAr[index].width) / 2 &&
                    distanceY <= (this.itemAr[k].height + this.itemAr[index].height) / 2 &&
                    !(distanceX == (this.itemAr[k].width + this.itemAr[index].width) / 2 &&
                        distanceY == (this.itemAr[k].height + this.itemAr[index].height) / 2)) {

                    // 周りの枠をチェック
                    var x = this.itemAr[index].x * this.grSep;
                    var y = this.itemAr[index].y * this.grSep;
                    var width = this.itemAr[index].width * this.grSep;
                    var height = this.itemAr[index].height * this.grSep;

                    var endFlg = false;

                    for (var i = 0; i < this.itemAr[index].data.length; i++) {
                        for (var j = 0; j < this.itemAr[index].data[i].length; j++) {
                            var positionX = x + i * this.grSep / this.grSplitMax;
                            var positionY = y + j * this.grSep / this.grSplitMax;

                            // 角はチェックさせない
                            if (this.checkGridContact(i, j, index) == 2) continue;

                            if (this.itemAr[index].data[i][j] != 1) {
                                if (i > 0 && this.itemAr[index].data[i - 1][j] == 1) {
                                    // 左
                                    if (this.checkGridContact(i - 1, j, index) != 2 &&
                                        this.checkItemPxWithRoom(positionX, positionY + this.grSep / this.grSplitMax / 2, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                                if (j > 0 && this.itemAr[index].data[i][j - 1] == 1) {
                                    // 上
                                    if (this.checkGridContact(i, j - 1, index) != 2 &&
                                        this.checkItemPxWithRoom(positionX + this.grSep / this.grSplitMax / 2, positionY, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                                if (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1][j] == 1) {
                                    // 右
                                    if (this.checkGridContact(i + 1, j, index) != 2 &&
                                        this.checkItemPxWithRoom(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax / 2, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                                if (j < this.itemAr[index].data[i].length - 1 && this.itemAr[index].data[i][j + 1] == 1) {
                                    // 下
                                    if (this.checkGridContact(i, j + 1, index) != 2 &&
                                        this.checkItemPxWithRoom(positionX + this.grSep / this.grSplitMax / 2, positionY + this.grSep / this.grSplitMax, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                            }
                            else {
                                if (i == 0 || (i > 0 && this.itemAr[index].data[i - 1].length <= j)) {
                                    // 左
                                    if (this.checkItemPxWithRoom(positionX, positionY + this.grSep / this.grSplitMax / 2, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                                if (j == 0) {
                                    // 上
                                    if (this.checkItemPxWithRoom(positionX + this.grSep / this.grSplitMax / 2, positionY, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                                if (i == this.itemAr[index].width * this.grSplitMax - 1 ||
                                    i == this.itemAr[index].data.length - 1 ||
                                    (i < this.itemAr[index].data.length - 1 && this.itemAr[index].data[i + 1].length <= j)) {
                                    // 右
                                    if (this.checkItemPxWithRoom(positionX + this.grSep / this.grSplitMax, positionY + this.grSep / this.grSplitMax / 2, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                                if (j == this.itemAr[index].height * this.grSplitMax - 1 || j == this.itemAr[index].data[i].length - 1) {
                                    // 下
                                    if (this.checkItemPxWithRoom(positionX + this.grSep / this.grSplitMax / 2, positionY + this.grSep / this.grSplitMax, k)) {
                                        endFlg = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (endFlg) {
                            touchRoomAr.push(k);
                            break;
                        }
                    }
                }
            }

            return touchRoomAr;
        };

        /** キッチンに結合可能な部屋の数をチェックする
         * return キッチンに結合可能な部屋の数
         */
        this.checkEnableTouchKitchen = function () {
            var count = 0;

            for (var i = this.itemAr.length - 1; i >= 0; i--) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].name === "キッチン" || this.itemAr[i].name === "DK" || this.itemAr[i].name === "LDK") {

                    var checkTouchRoom = this.checkTouchRoom(i);

                    for (var l = 0; l < checkTouchRoom.length; l++) {
                        j = checkTouchRoom[l];

                        if (i == j) continue;
                        if (this.itemAr[j] == null) continue;

                        if (this.itemAr[j].name === "リビング" || this.itemAr[j].name === "洋室" ||
                            this.itemAr[j].name === "ダイニング" || this.itemAr[j].name === "和室" ||
                            this.itemAr[j].name === "床の間" || this.itemAr[j].name === "クローゼット" ||
                            this.itemAr[j].name === "物入" || this.itemAr[j].name === "押入") {
                            count++;
                        }
                    }
                }
            }
            return count;
        };

        /** キッチンの部屋データを取得する
         * {string} data
         * return キッチンの部屋データ（json文字列）
         */
        this.checkKitchenData = function (importData) {
            var index = 0;
            convertRoom.importData(importData);
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].name === "キッチン" || this.itemAr[i].name === "DK" || this.itemAr[i].name === "LDK") {
                    var opening = this.checkTouchOpening(i);

                    dragRoom.DRAG.openingAr = opening;
                    dragRoom.DRAG.itemAr = [this.itemAr[i]];

                    return convertRoom.exportData(function (e) {
                    });
                }
            }

            return "";
        };

        /** キッチンデータが一致するかチェック
         * {string} 比較データ1
         * {string} 比較データ2
         * return データが一致する場合はtrue, 一致しない場合はfalse
         */
        this.checkMatchKitchen = function (dataStr1, dataStr2) {
            var kitchenStr1 = this.checkKitchenData(dataStr1);
            var kitchenStr2 = this.checkKitchenData(dataStr2);

            return this.checkMatchRoom(kitchenStr1, kitchenStr2, true);
        };

        /** 指定した部屋typeの数を取得
         * {string} data
         * {int} type
         * return 指定した部屋typeの数
         */
        this.checkTypeCount = function (importData, type) {
            var count = 0;
            var data = convertRoom.convertStrToData(importData);
            for (var i = 0; i < data.itemAr.length; i++) {
                if (data.itemAr[i] == null) continue;
                if (data.itemAr[i].type == type) {
                    count++;
                }
            }

            return count;
        };

        /** 指定した部屋typeの座標位置（グリッド単位）を取得
         * {string} data
         * {int} type
         * return 指定した部屋typeの座標位置（グリッド単位）
         */
        this.checkTypePosition = function (importData, type) {
            var position = [];
            var data = convertRoom.convertStrToData(importData);
            for (var i = 0; i < data.itemAr.length; i++) {
                if (data.itemAr[i] == null) continue;
                if (data.itemAr[i].type == type) {
                    position.push([data.itemAr[i].x, data.itemAr[i].y]);
                }
            }

            return position;
        };

        /** UBの数が一致するかチェック
         * {string} 比較データ1
         * {string} 比較データ2
         * return データの数が一致する場合はtrue, 一致しない場合はfalse
         */
        this.checkMatchUB = function (dataStr1, dataStr2) {
            return (this.checkTypeCount(dataStr1, 4) == this.checkTypeCount(dataStr2, 4));
        };

        /** 洋室が一致するかチェック（※数と座標位置のみチェック）
         * {string} 比較データ1
         * {string} 比較データ2
         * return データが一致する場合はtrue, 一致しない場合はfalse
         */
        this.checkMatchBR = function (dataStr1, dataStr2) {
            if ((this.checkTypeCount(dataStr1, 6) != this.checkTypeCount(dataStr2, 6))) {
                return false;
            }

            // 部屋のx,y座標が一致するかチェック
            var position1 = this.checkTypePosition(dataStr1, 6);
            var position2 = this.checkTypePosition(dataStr2, 6);

            for (var i = 0; i < position1.length; i++) {
                if (position1[i][0] != position2[i][0] ||
                    position1[i][1] != position2[i][1]) return false;
            }

            return true;
        };

        /** 論理間取り（キッチンの面積を広げる）
         * {int} position 結合するキッチンNo
         * return なし
         */
        this.convertRoomBigKitchen = function (position) {

            for (var i = this.itemAr.length - 1; i >= 0; i--) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].name === "キッチン" || this.itemAr[i].name === "DK" || this.itemAr[i].name === "LDK") {
                    // 近接するitemを検索
                    var checkTouchRoom = this.checkTouchRoom(i);

                    for (var l = 0; l < checkTouchRoom.length; l++) {
                        j = checkTouchRoom[l];

                        if (i == j) continue;
                        if (this.itemAr[j] == null) continue;

                        if (this.itemAr[j].name === "リビング" || this.itemAr[j].name === "洋室" ||
                            this.itemAr[j].name === "ダイニング" || this.itemAr[j].name === "和室" ||
                            this.itemAr[j].name === "床の間" || this.itemAr[j].name === "クローゼット" ||
                            this.itemAr[j].name === "物入" || this.itemAr[j].name === "押入") {

                            if (position != null && position != 0) {
                                position--;
                                continue;
                            }

                            var baseX = (this.itemAr[j].x - this.itemAr[i].x) * this.grSplitMax;
                            var baseY = (this.itemAr[j].y - this.itemAr[i].y) * this.grSplitMax;

                            if (baseX < 0) {
                                this.itemAr[i].x += baseX / this.grSplitMax;
                                for (var p = 0; p < this.openingAr.length; p++) {
                                    if (this.openingAr[p].roomId == i)
                                        this.openingAr[p].x -= baseX / this.grSplitMax;
                                }
                            }
                            while (baseX < 0) {
                                this.itemAr[i].data.unshift([]);
                                ++baseX;
                            }
                            if (baseY < 0) {
                                this.itemAr[i].y += baseY / this.grSplitMax;
                                for (var p = 0; p < this.openingAr.length; p++) {
                                    if (this.openingAr[p].roomId == i)
                                        this.openingAr[p].y -= baseY / this.grSplitMax;
                                }
                            }
                            while (baseY < 0) {
                                for (var p = 0; p < this.itemAr[i].data.length; p++)
                                    this.itemAr[i].data[p].unshift(0);
                                ++baseY;
                            }

                            for (var k = 0; k < this.itemAr[j].data.length; k++) {
                                for (var l = 0; l < this.itemAr[j].data[k].length; l++) {
                                    var insertX = baseX + k;
                                    var insertY = baseY + l;
                                    while (insertX >= this.itemAr[i].data.length)
                                        this.itemAr[i].data[this.itemAr[i].data.length] = [];
                                    try {
                                        this.itemAr[i].data[insertX][insertY] = this.itemAr[j].data[k][l];
                                    }
                                    catch (e) {
                                        window.console.log(e);
                                    }
                                }
                            }

                            //count,widthとheight再計算
                            var width = 0;
                            var height = 0;
                            this.itemAr[i].count = 0;
                            for (var k = 0; k < this.itemAr[i].data.length; k++) {
                                for (var l = 0; l < this.itemAr[i].data[k].length; l++) {
                                    if (this.itemAr[i].data[k][l] == 1) {
                                        this.itemAr[i].count += this.grSepUnit / (this.grSplitMax * this.grSplitMax);
                                        if (width < k) width = k;
                                        if (height < l) height = l;
                                    }
                                }
                            }
                            this.itemAr[i].width = (width + 1) / this.grSplitMax;
                            this.itemAr[i].height = (height + 1) / this.grSplitMax;
                            // count四捨五入
                            this.itemAr[i].count = Math.round(this.itemAr[i].count * 10000) / 10000;

                            // 結合したitem内にある開口部を移動
                            for (var k = this.openingAr.length - 1; k >= 0; k--) {
                                if (this.openingAr[k].roomId == j) {
                                    this.openingAr[k].roomId = i;
                                    this.openingAr[k].x += baseX / this.grSplitMax;
                                    this.openingAr[k].y += baseY / this.grSplitMax;
                                }
                            }

                            // 開口部チェック
                            for (var k = 0; k < this.openingAr.length; k++) {
                                if (this.openingAr[k].roomId == i) {
                                    if (this.openingAr[k].type != 20) { // 柱でないとき
                                        var x = (this.itemAr[this.openingAr[k].roomId].x + this.openingAr[k].x) * this.grSep;
                                        var y = (this.itemAr[this.openingAr[k].roomId].y + this.openingAr[k].y) * this.grSep;

                                        var x2 = x + (this.openingAr[k].width - this.wallSize) * this.grSep;
                                        var y2 = y + (this.openingAr[k].height - this.wallSize) * this.grSep;

                                        if (!(this.checkItemPxNotWall(x - this.wallSize * this.grSep, y, j) != i ||
                                                this.checkItemPxNotWall(x + this.wallSize * this.grSep, y, j) != i ||
                                                this.checkItemPxNotWall(x, y + this.wallSize * this.grSep, j) != i ||
                                                this.checkItemPxNotWall(x, y - this.wallSize * this.grSep, j) != i) ||
                                            !(this.checkItemPxNotWall(x2 - this.wallSize * this.grSep, y2, j) != i ||
                                                this.checkItemPxNotWall(x2 + this.wallSize * this.grSep, y2, j) != i ||
                                                this.checkItemPxNotWall(x2, y2 + this.wallSize * this.grSep, j) != i ||
                                                this.checkItemPxNotWall(x2, y2 - this.wallSize * this.grSep, j) != i) ||
                                            !(this.checkItemPxNotWall(x + this.openingAr[k].width / 2 * this.grSep - this.wallSize / 2 * this.grSep,
                                                y + this.openingAr[k].height / 2 * this.grSep, j) != i ||
                                                this.checkItemPxNotWall(x + this.openingAr[k].width / 2 * this.grSep + this.wallSize / 2 * this.grSep,
                                                    y + this.openingAr[k].height / 2 * this.grSep, j) != i ||
                                                this.checkItemPxNotWall(x + this.openingAr[k].width / 2 * this.grSep,
                                                    y + this.openingAr[k].height / 2 * this.grSep + this.wallSize / 2 * this.grSep, j) != i ||
                                                this.checkItemPxNotWall(x + this.openingAr[k].width / 2 * this.grSep,
                                                    y + this.openingAr[k].height / 2 * this.grSep - this.wallSize / 2 * this.grSep, j) != i)
                                        ) {
                                            this.openingAr[k].type = 20;

                                            var addX = this.openingAr[k].x;
                                            var addY = this.openingAr[k].y;

                                            if (this.openingAr[k].width == this.wallSize) {
                                                addY += this.openingAr[k].height - this.wallSize / 2;
                                                this.openingAr[k].y -= this.wallSize / 2;
                                                this.openingAr[k].height = this.wallSize;
                                            }
                                            else {
                                                addX += this.openingAr[k].width - this.wallSize / 2;
                                                this.openingAr[k].x -= this.wallSize / 2;
                                                this.openingAr[k].width = this.wallSize;
                                            }

                                            // 1つ追加
                                            var addOpening = new floorPlan.opening(addX, addY, this.wallSize, this.wallSize, "柱", 20, i);
                                            this.addOpening(addOpening);
                                        }
                                    }
                                }
                            }

                            this.itemAr[i].name = "LDK";
                            if (this.itemAr[i].roomChange <= 0 || this.itemAr[i].roomChange == null) this.itemAr[i].roomChange = this.itemAr[i].type;
                            this.itemAr[i].type = 18;

                            // 結合したアイテムを削除
                            this.itemAr[j] = null;

                            // キッチンの柱抜き処理
                            this.ventPillar(i);

                            break;
                        }
                    }
                }
            }

            this.draw();
        };

        /** 論理間取り（浴室をUBに変更）
         * return なし
         */
        this.convertRoomUB = function () {
            for (var i = this.itemAr.length - 1; i >= 0; i--) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].name === "浴室") {
                    this.itemAr[i].name = "ユニットバス";
                    this.itemAr[i].roomChange = this.itemAr[i].type;
                    this.itemAr[i].type = 4;
                }
            }
            this.draw();
        };

        /** 論理間取り（和室を洋室に変更）
         * return なし
         */
        this.convertRoomBR = function (val) {
            var binaryNum = val.toString(2);

            var position = 0;
            for (var i = this.itemAr.length - 1; i >= 0; i--) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].type == 5) {
                    position++;
                    if (binaryNum.length < position) break;

                    if (parseInt(binaryNum.charAt(binaryNum.length - position)) == 1) {
                        this.itemAr[i].name = "洋室";
                        this.itemAr[i].roomChange = this.itemAr[i].type;
                        this.itemAr[i].type = 6;
                    }
                }
            }

            this.draw();
        };

        /** 論理間取りの変更チェック（壁抜きの数）
         * {string} str1 変更前のjsonデータ
         * {string} str2 変更後のjsonデータ
         * return 壁抜きを行った数
         */
        this.checkWallVentCount = function (str1, str2) {
            var changeCount = 0;

            var data1 = convertRoom.convertStrToData(str1);
            var data2 = convertRoom.convertStrToData(str2);

            var position = 0;
            while (true) {
                if (data1.itemAr.length <= position || data2.itemAr.length <= position) break;

                if (data1.itemAr[position] != data2.itemAr[position] &&
                    (data1.itemAr[position] == null || data2.itemAr[position] == null)) {
                    changeCount++;
                }

                position++;
            }

            changeCount += Math.abs(data1.itemAr.length - data2.itemAr.length);

            return changeCount;
        };

        /** 論理間取りの変更チェック（壁抜きしたものに含まれている和室の数）
         * {string} str1 変更前のjsonデータ
         * {string} str2 変更後のjsonデータ
         * return 和室を壁抜きした数
         */
        this.checkWallVentWashituCount = function (str1, str2) {
            var changeWashitsuCount = 0;

            var data1 = convertRoom.convertStrToData(str1);
            var data2 = convertRoom.convertStrToData(str2);

            var position = 0;
            while (true) {
                if (data1.itemAr.length <= position || data2.itemAr.length <= position) break;

                if (data1.itemAr[position] != data2.itemAr[position] &&
                    ((data1.itemAr[position] == null && data2.itemAr[position].type == 5) ||
                        (data2.itemAr[position] == null && data1.itemAr[position].type == 5))) {

                    changeWashitsuCount++;
                }

                position++;
            }

            return changeWashitsuCount;
        };

        /** 論理間取りの変更チェック（浴室→UB変更数）
         * {string} str1 変更前のjsonデータ
         * {string} str2 変更後のjsonデータ
         * return 浴室からUBに変更した数
         */
        this.checkChangeUBCount = function (str1, str2) {
            var changeCount = 0;

            var data1 = convertRoom.convertStrToData(str1);
            var data2 = convertRoom.convertStrToData(str2);

            var position = 0;
            while (true) {
                if (data1.itemAr.length <= position || data2.itemAr.length <= position) break;

                if (data1.itemAr[position] != null && data2.itemAr[position] != null) {
                    if (data1.itemAr[position].type == 2 && data2.itemAr[position].type == 4) {
                        changeCount++;
                    }
                }

                position++;
            }

            return changeCount;
        };

        /** 論理間取りの変更チェック（和室→洋室変更数）
         * {string} str1 変更前のjsonデータ
         * {string} str2 変更後のjsonデータ
         * return 和室から洋室に変更した数
         */
        this.checkChangeBRCount = function (str1, str2) {
            var changeCount = 0;

            var data1 = convertRoom.convertStrToData(str1);
            var data2 = convertRoom.convertStrToData(str2);

            var position = 0;
            while (true) {
                if (data1.itemAr.length <= position || data2.itemAr.length <= position) break;

                if (data1.itemAr[position] != null && data2.itemAr[position] != null) {
                    if (data1.itemAr[position].type == 5 && data2.itemAr[position].type == 6) {
                        changeCount++;
                    }
                }

                position++;
            }

            return changeCount;
        };

        /** シンクの位置変更
         * {int} position 位置（左上:0 右上:1 左下:2 右下:3）
         * {int} angle シンクの角度(度)
         * {int} type シンクの種類（0:I型(左側にスペース) 1:I型(右側にスペース) 2:I型(壁付け) 3:I型（センターキッチン））
         * return 指定位置にシンクが入る場合はtrue、入らない場合はfalse
         */
        this.convertKitchenSink = function (position, angle, type) {
            if (this.sinkAr == null) this.sinkAr = [];

            this.draw();

            // 角度を指定
            if (type == 0) {
                if (position == 0) angle = 180;
                if (position == 1) angle = 270;
                if (position == 2) angle = 90;
                if (position == 3) angle = 0;
            }
            if (type == 1) {
                if (position == 0) angle = 90;
                if (position == 1) angle = 180;
                if (position == 2) angle = 0;
                if (position == 3) angle = 270;
            }
            if (type == 3) {
                if (position == 0) {
                    angle = ((angle + 90) % 180) + 90;
                }
                if (position == 1) {
                    angle = (angle % 180) + 180;
                }
                if (position == 2) {
                    angle = (angle % 180);
                }
                if (position == 3) {
                    angle = (360 - (angle % 180)) % 360;
                }
            }

            for (var i = this.itemAr.length - 1; i >= 0; i--) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].name === "キッチン" || this.itemAr[i].name === "DK" || this.itemAr[i].name === "LDK") {
                    var sinkNo = -1;
                    for (var j = 0; j < this.sinkAr.length; j++) {
                        if (this.sinkAr[j].roomId == i) {
                            sinkNo = j;
                            break;
                        }
                    }

                    var x = 0;
                    var y = 0;
                    var width = this.sinkWidth + this.sinkMarginLR;
                    var height = this.sinkDepth + this.sinkMarginBottom;

                    if (type == 2) {
                        width = this.sinkWidth;
                        height = this.sinkDepth;
                    }
                    else if (type == 3) {
                        width = this.sinkWidth + this.sinkMarginLR * 2;
                        height = this.sinkDepth + this.sinkMarginBottom;
                    }

                    if (angle % 180 != 0) {
                        width = this.sinkDepth + this.sinkMarginBottom;
                        height = this.sinkWidth + this.sinkMarginLR;

                        if (type == 2) {
                            width = this.sinkDepth;
                            height = this.sinkWidth;
                        }
                        else if (type == 3) {
                            width = this.sinkDepth + this.sinkMarginBottom;
                            height = this.sinkWidth + this.sinkMarginLR * 2;
                        }
                    }

                    // 部屋にシンクが入るかチェック
                    var sinkP = this.checkSinkPosition(width, height, type, angle, position, i);

                    if (sinkP == null) return false;

                    if (sinkNo < 0) {
                        // 新規作成
                        this.sinkAr.push(new floorPlan.sink(x, y, width, height, type, i, angle, 34 / 12));
                        sinkNo = this.sinkAr.length - 1;
                    }

                    this.sinkAr[sinkNo].x = Math.round(sinkP[0] * 10000) / 10000;
                    this.sinkAr[sinkNo].y = Math.round(sinkP[1] * 10000) / 10000;
                    this.sinkAr[sinkNo].width = Math.round(width * 10000) / 10000;
                    this.sinkAr[sinkNo].height = Math.round(height * 10000) / 10000;

                    this.sinkAr[sinkNo].type = type;
                    this.sinkAr[sinkNo].angle = angle;
                }
            }
            this.draw();

            return true;
        };

        /** シンクを配置する場所取得
         * {int} width シンクの幅 　 （グリッド単位）
         * {int} height シンクの高さ （グリッド単位）
         * {int} type シンクの種類（0:I型(左側にスペース) 1:I型(右側にスペース) 2:I型(壁付け) 3:I型（センターキッチン））
         * {int} angle シンクの角度（度）
         * {int} position 配置する場所（0:左上、1:右上、2:左下、3:右下）
         * {int} roomId 基準の部屋番号
         * return シンクが配置できる場合true、配置できない場合false
         */
        this.checkSinkPosition = function (width, height, type, angle, position, roomId) {
            if (this.itemAr[roomId].width <= width - this.wallSize) return null;
            if (this.itemAr[roomId].height <= height - this.wallSize) return null;

            var x = 0;
            var y = 0;

            var endFlg = false;

            if (position == 0) {// 左上
                if (this.checkSetSink(x, y, width, height, roomId, type, angle)) {
                    endFlg = true;
                }
                else {
                    // ずらして確認
                    if (this.itemAr[roomId].data.length == 0 ||
                        this.itemAr[roomId].data[0].length == 0 ||
                        this.itemAr[roomId].data[0][0] != 1) {

                        if (type >= 2 && width < height) {
                            // 下検索
                            for (var i = 0; i < this.itemAr[roomId].height * this.grSplitMax; i++) {
                                try {
                                    if (this.itemAr[roomId].data[0][i] == 1) {
                                        if (this.checkSetSink(x, y + i / this.grSplitMax, width, height, roomId, type, angle)) {
                                            y += i / this.grSplitMax;
                                            endFlg = true;
                                        }
                                        break;
                                    }
                                }
                                catch (e) {
                                }
                            }
                        }

                        if (type >= 2 && width > height) {
                            // 右検索
                            if (!endFlg) {
                                for (var i = 0; i < this.itemAr[roomId].width * this.grSplitMax; i++) {
                                    try {
                                        if (this.itemAr[roomId].data[i][0] == 1) {
                                            if (this.checkSetSink(x + i / this.grSplitMax, y, width, height, roomId, type, angle)) {
                                                x += i / this.grSplitMax;
                                                endFlg = true;
                                            }
                                            break;
                                        }
                                    }
                                    catch (e) {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (position == 1) {// 右上
                x = this.itemAr[roomId].width - width;

                if (this.checkSetSink(x, y, width, height, roomId, type, angle)) {
                    endFlg = true;
                }
                else {
                    // ずらして確認
                    if (this.itemAr[roomId].data.length < this.itemAr[roomId].width * this.grSplitMax ||
                        this.itemAr[roomId].data[this.itemAr[roomId].width * this.grSplitMax - 1].length == 0 ||
                        this.itemAr[roomId].data[this.itemAr[roomId].width * this.grSplitMax - 1][0] != 1) {

                        if (type >= 2 && width < height) {
                            // 下検索
                            for (var i = 0; i < this.itemAr[roomId].height * this.grSplitMax; i++) {
                                try {
                                    if (this.itemAr[roomId].data[this.itemAr[roomId].width * this.grSplitMax - 1][i] == 1) {
                                        if (this.checkSetSink(x, y + i / this.grSplitMax, width, height, roomId, type, angle)) {
                                            y += i / this.grSplitMax;
                                            endFlg = true;
                                        }
                                        break;
                                    }
                                }
                                catch (e) {

                                }
                            }
                        }

                        if (type >= 2 && width < height) {
                            // 左検索
                            if (!endFlg) {
                                for (var i = this.itemAr[roomId].width * this.grSplitMax - 1; i >= 0; i--) {
                                    try {
                                        if (this.itemAr[roomId].data[i][0] == 1) {
                                            if (this.checkSetSink(x - i / this.grSplitMax, y, width, height, roomId, type, angle)) {
                                                x -= i / this.grSplitMax;
                                                endFlg = true;
                                            }
                                            break;
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (position == 2) {// 左下
                y = this.itemAr[roomId].height - height;

                if (this.checkSetSink(x, y, width, height, roomId, type, angle)) {
                    endFlg = true;
                }
                /*
        else {
            // ずらして確認
            if (this.itemAr[roomId].data.length == 0 ||
                this.itemAr[roomId].data[0].length < this.itemAr[roomId].height*this.grSplitMax ||
                this.itemAr[roomId].data[0][this.itemAr[roomId].height*this.grSplitMax-1] != 1) {

                // 上検索
                for (var i = this.itemAr[roomId].height*this.grSplitMax-1; i >= 0; i-- ) {
                    try {
                        if (this.itemAr[roomId].data[0][i] == 1) {
                            if (this.checkSetSink(x-i/this.grSplitMax,y,width,height,roomId,type,angle)) {
                                y -= i/this.grSplitMax;
                                endFlg = true;
                            }
                            break;
                        }
                    }
                    catch(e) {
                    }
                }

                // 右検索
                if (!endFlg) {
                    for (var i = 0; i < this.itemAr[roomId].width*this.grSplitMax; i++ ) {
                        try {
                            if (this.itemAr[roomId].data[i][this.itemAr[roomId].height*this.grSplitMax-1] == 1) {
                                if (this.checkSetSink(x+i/this.grSplitMax,y,width,height,roomId,type,angle)) {
                                    x += i/this.grSplitMax;
                                    endFlg = true;
                                }
                                break;
                            }
                        }
                        catch(e) {
                        }
                    }
                }
            }
        }*/
            }
            else if (position == 3) {// 右下
                x = this.itemAr[roomId].width - width;
                y = this.itemAr[roomId].height - height;

                if (this.checkSetSink(x, y, width, height, roomId, type, angle)) {
                    endFlg = true;
                }
                /*
        else {
            // ずらして確認
            if (this.itemAr[roomId].data.length < this.itemAr[roomId].width*this.grSplitMax ||
                this.itemAr[roomId].data[this.itemAr[roomId].width*this.grSplitMax-1].length < this.itemAr[roomId].height*this.grSplitMax ||
                this.itemAr[roomId].data[this.itemAr[roomId].width*this.grSplitMax-1][this.itemAr[roomId].height*this.grSplitMax-1] != 1) {

                // 上検索
                for (var i = this.itemAr[roomId].height*this.grSplitMax-1; i >= 0; i-- ) {
                    try {
                        if (this.itemAr[roomId].data[this.itemAr[roomId].width*this.grSplitMax-1][i] == 1) {
                            if (this.checkSetSink(x-i/this.grSplitMax,y,width,height,roomId,type,angle)) {
                                y -= i/this.grSplitMax;
                                endFlg = true;
                            }
                            break;
                        }
                    }
                    catch(e) {
                    }
                }

                // 左検索
                if (!endFlg) {
                    for (var i = this.itemAr[roomId].width*this.grSplitMax-1; i >= 0; i-- ) {
                        try {
                            if (this.itemAr[roomId].data[i][this.itemAr[roomId].height*this.grSplitMax-1] == 1) {
                                if (this.checkSetSink(x-i/this.grSplitMax,y,width,height,roomId,type,angle)) {
                                    x -= i/this.grSplitMax;
                                    endFlg = true;
                                }
                                break;
                            }
                        }
                        catch(e) {
                        }
                    }
                }
            }
        }*/
            }

            if (!endFlg) return null;

            return [x, y];
        };

        /** シンクを配置できるかチェック
         * {int} x 左上x座標　 (グリッド単位：部屋の左上起点)
         * {int} y 左上y座標 　(グリッド単位：部屋の左上起点）
         * {int} width シンクの幅 　 （グリッド単位）
         * {int} height シンクの高さ （グリッド単位）
         * {int} roomId 基準の部屋番号
         * {int} type シンクの種類（0:I型(左側にスペース) 1:I型(右側にスペース) 2:I型(壁付け) 3:I型（センターキッチン））
         * {int} angle シンクの角度(度)
         * return シンクが配置できる場合true、配置できない場合false
         */
        this.checkSetSink = function (x, y, width, height, roomId, type, angle) {

            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;
            width = Math.round(width * 10000) / 10000;
            height = Math.round(height * 10000) / 10000;

            // 部屋の内側かチェック
            if (x <= this.itemAr[roomId].width &&
                y <= this.itemAr[roomId].height &&
                x + width <= this.itemAr[roomId].x + this.itemAr[roomId].width &&
                y + height <= this.itemAr[roomId].y + this.itemAr[roomId].height) {

                for (var i = 0; i <= width * this.grSplitMax; i++) {
                    for (var j = 0; j <= height * this.grSplitMax; j++) {
                        var nowRoom = this.checkItemPxNotWall((x + i / this.grSplitMax + this.itemAr[roomId].x) * this.grSep, (y + j / this.grSplitMax + this.itemAr[roomId].y) * this.grSep);
                        if (nowRoom != roomId) {
                            return false;
                        }
                    }
                }

                return this.checkScopeOpening(x, y, width, height, roomId, type, angle);
            }

            return false;
        };

        /** 開口部が範囲内にあるかチェック
         * {int} x 左上x座標　 (グリッド単位：部屋の左上起点)
         * {int} y 左上y座標 　(グリッド単位：部屋の左上起点）
         * {int} width 幅 　 （グリッド単位）
         * {int} height 高さ （グリッド単位）
         * {int} roomId 基準の部屋番号
         * {int} type シンクの種類（0:I型(左側にスペース) 1:I型(右側にスペース) 2:I型(壁付け) 3:I型（センターキッチン））
         * {int} angle シンクの角度(度)
         * return 開口部が範囲内にある場合true、ない場合false
         */
        this.checkScopeOpening = function (x, y, width, height, roomId, type, angle) {
            var positionX = this.itemAr[roomId].x + x;
            var positionY = this.itemAr[roomId].y + y;

            var sinkPositionX = 0;
            var sinkPositionY = 0;
            var sinkWidth = width;
            var sinkHeight = height;

            if (angle == 0) {
                if (type == 0) sinkPositionX = this.sinkMarginLR;
                sinkWidth = this.sinkWidth;
                sinkHeight = this.sinkDepth;
            }
            else if (angle == 90) {
                sinkPositionX = this.sinkMarginBottom;
                if (type == 0) sinkPositionY = this.sinkMarginLR;
                sinkWidth = this.sinkDepth;
                sinkHeight = this.sinkWidth;
            }
            else if (angle == 180) {
                if (type == 1) sinkPositionX = this.sinkMarginLR;
                sinkPositionY = this.sinkMarginBottom;
                sinkWidth = this.sinkWidth;
                sinkHeight = this.sinkDepth;
            }
            else if (angle == 270) {
                if (type == 1) sinkPositionY = this.sinkMarginLR;
                sinkWidth = this.sinkDepth;
                sinkHeight = this.sinkWidth;
            }

            if (type == 2) {
                sinkPositionX = 0;
                sinkPositionY = 0;
            }

            sinkPositionX += this.itemAr[roomId].x + x;
            sinkPositionY += this.itemAr[roomId].y + y;

            // 四捨五入
            positionX = Math.round(positionX * 10000) / 10000;
            positionY = Math.round(positionY * 10000) / 10000;
            sinkPositionX = Math.round(sinkPositionX * 10000) / 10000;
            sinkPositionY = Math.round(sinkPositionY * 10000) / 10000;
            sinkWidth = Math.round(sinkWidth * 10000) / 10000;
            sinkHeight = Math.round(sinkHeight * 10000) / 10000;

            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i] == null) continue;
                if (this.itemAr[this.openingAr[i].roomId] == null) continue;

                // 柱チェック
                if (this.openingAr[i].type == 20 &&
                    positionX < this.openingAr[i].x + this.openingAr[i].width / 2 + this.itemAr[this.openingAr[i].roomId].x &&
                    positionY < this.openingAr[i].y + this.openingAr[i].height / 2 + this.itemAr[this.openingAr[i].roomId].y &&
                    positionX + width > this.openingAr[i].x + this.openingAr[i].width / 2 + this.itemAr[this.openingAr[i].roomId].x &&
                    positionY + height > this.openingAr[i].y + this.openingAr[i].height / 2 + this.itemAr[this.openingAr[i].roomId].y) {
                    return false;
                }

                // 開口部チェック（シンクとドア・掃き出し窓が接しているかチェック）
                if (this.openingAr[i].type != 20 && this.openingAr[i].type >= 19 &&
                    (sinkPositionX <= this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x + this.openingAr[i].width &&
                        sinkPositionY <= this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y + this.openingAr[i].height) &&
                    (sinkPositionX + sinkWidth >= this.openingAr[i].x + this.itemAr[this.openingAr[i].roomId].x &&
                        sinkPositionY + sinkHeight >= this.openingAr[i].y + this.itemAr[this.openingAr[i].roomId].y)) {
                    return false;
                }

                var gOpeningX = this.itemAr[this.openingAr[i].roomId].x + this.openingAr[i].x;// グリッド単位
                var gOpeningY = this.itemAr[this.openingAr[i].roomId].y + this.openingAr[i].y;// グリッド単位

                if (this.openingAr[i].type != 20 && this.openingAr[i].type >= 19 &&
                    ((this.openingAr[i].displayWidth >= 0 &&
                        positionX + width >= gOpeningX &&
                        positionX <= gOpeningX + this.openingAr[i].displayWidth / this.grSep) ||
                        (this.openingAr[i].displayWidth < 0 &&
                            positionX + width >= gOpeningX + this.openingAr[i].displayWidth / this.grSep &&
                            positionX <= gOpeningX + this.openingAr[i].width ))) {
                    if ((this.openingAr[i].displayHeight >= 0 &&
                            positionY + height >= gOpeningY &&
                            positionY <= gOpeningY + this.openingAr[i].displayHeight / this.grSep) ||
                        (this.openingAr[i].displayHeight < 0 &&
                            positionY + height >= gOpeningY + this.openingAr[i].displayHeight / this.grSep &&
                            positionY <= gOpeningY + this.openingAr[i].height )) {
//                  window.console.log("ok:" + this.openingAr[i].displayWidth + "," + this.openingAr[i].displayHeight);
                        return false;
                    }
                }
            }

            return true;
        };

        /** 間取りの総面積取得
         * return 間取りの総面積(1data単位)
         */
        this.articleAreaCount = function () {
            var areaCount = 0;
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;
                areaCount += this.itemAr[i].count * 2;
            }

            areaCount *= 12 * 12;

            // 四捨五入
            return Math.round(areaCount * 10000) / 10000;
        };


        /** 平米数計算
         * {int} str 間取りのjsonデータ
         * {int} index 部屋のindex（nullの場合、部屋全てで計算）
         * return 指定した部屋の平米数(m2)
         */
        this.calSquareMeterWithJSON = function (str, index) {
            var data = convertRoom.convertStrToData(str);

            var squareMeterCount = 0;
            for (var i = 0; i < data.itemAr.length; i++) {
                if (index == null || i == index) {
                    if (data.itemAr[i] == null) continue;
                    if (data.itemAr[i].type >= 90) continue;// 部屋カテゴリじゃないものは除く

                    // 計算
                    squareMeterCount += data.itemAr[i].count * (data.grSepMm * data.grSepMm * 2 / 1000000);
                }
            }

            // 四捨五入
            return Math.round(squareMeterCount * 10000) / 10000;
        };

        /** 平米数計算
         * {int} index 部屋のindex（nullの場合、部屋全てで計算）
         * return 指定した部屋の平米数(m2)
         */
        this.calSquareMeter = function (index) {
            var squareMeterCount = 0;
            for (var i = 0; i < this.itemAr.length; i++) {
                if (index == null || i == index) {
                    if (this.itemAr[i] == null) continue;
                    if (this.itemAr[i].type >= 90) continue;// 部屋カテゴリじゃないものは除く

                    // 計算
                    squareMeterCount += this.itemAr[i].count * (this.grSepMm * this.grSepMm * 2 / 1000000);
                }
            }

            // 四捨五入
            return Math.round(squareMeterCount * 10000) / 10000;
        };

        /** 外周計算
         * {int} index 部屋のindex（nullの場合、部屋全てで計算）
         * return 指定した部屋の外周(グリッド/12)
         */
        this.calOuterWall = function (index) {
            var outerWallGridCount = 0;

            for (var i = 0; i < this.itemAr.length; i++) {
                if (index == null || i == index) {
                    if (this.itemAr[i] == null) continue;
                    if (this.itemAr[i].type >= 90) continue;// 部屋カテゴリじゃないものは除く

                    var wallInfo = this.calWallInfo(i);
                    for (var wallId = 0; wallId < wallInfo.length; wallId++) {
                        if (wallInfo[wallId].kind === "outer_wall" || i == index) {
                            var distance = Math.sqrt(Math.pow((wallInfo[wallId].endPointX - wallInfo[wallId].startPointX), 2) +
                                Math.pow((wallInfo[wallId].endPointY - wallInfo[wallId].startPointY), 2));
                            outerWallGridCount += distance * 12;
                        }
                    }
                }
            }

            // 四捨五入
            return Math.round(outerWallGridCount * 10000) / 10000;

        };

        /** 壁の面積計算
         * {int} index 部屋のindex（nullの場合、部屋全てで計算）
         * return 指定した部屋の壁面積(グリッド/12の2乗)
         */
        this.calWallArea = function (index) {

            var outerWallGridCount = this.calOuterWall(index);  // 外周（グリッド/12単位）
            var wallHeight = 2600 / this.grSepMm * 12;  //壁の高さ（グリッド/12単位）

            var wallArea = outerWallGridCount * wallHeight;

            // 四捨五入
            return Math.round(wallArea * 10000) / 10000;
        };

        /** チェックするデータ部分が壁かチェック
         * {int} roomId 部屋のindex
         * {int} x （data単位）
         * {int} y （data単位）
         * return 壁の場合true,それ以外であればfalse
         */
        this.checkDataWall = function (roomId, x, y) {
            try {
                if (this.itemAr[roomId].data[x][y] != 1) return false;
            }
            catch (e) {
                return false;
            }

            if (this.checkGridContact(x, y, roomId) == 0) return false;
            if (this.checkGridContact(x, y, roomId) == 4) return false;

            return true;
        };

        /** 壁の始点・終点を計算
         * {int} roomId 部屋のindex
         * return 壁の始点・終点・壁の種類の一覧
         */
        this.calWallInfo = function (roomId) {
            var wallList = [];

            var startX = -1;
            var startY = -1;

            // 始点検索
            if (this.itemAr[roomId].data[0][0] != 1) {
                // 下方向検索
                for (var i = 0; i < this.itemAr[roomId].data[0].length; i++) {
                    if (this.itemAr[roomId].data[0][i] == 1) {
                        startX = 0;
                        startY = i;
                        break;
                    }
                }
                if (startX == -1 || startY == -1) {
                    // 右方向検索
                    for (var i = 0; i < this.itemAr[roomId].data.length; i++) {
                        if (this.itemAr[roomId].data[i][0] == 1) {
                            startX = i;
                            startY = 0;
                            break;
                        }
                    }
                }

                if (startX == -1 || startY == -1) {
                    window.console.log("エラー：壁の始点が見つかりませんでした。");
                    return wallList;
                }
            }
            else {
                startX = startY = 0;
            }

            // 現在チェック中の始点
            var nowStartX = startX;
            var nowStartY = startY;

            // 検索方向
            var vectorX = 1;
            var vectorY = 0;

            while (true) {
                var checkPositionX = nowStartX;
                var checkPositionY = nowStartY;
                while (true) {
                    if (checkPositionX + vectorX < 0 || checkPositionY + vectorY < 0 ||
                        this.itemAr[roomId].data.length <= checkPositionX + vectorX ||
                        this.itemAr[roomId].data[checkPositionX + vectorX].length <= checkPositionY + vectorY) {
                        // 範囲外の場合、検索終了
                        break;
                    }

                    // 検索位置をずらす
                    checkPositionX += vectorX;
                    checkPositionY += vectorY;

                    if (this.itemAr[roomId].data[checkPositionX][checkPositionY] != 1) {
                        // 部屋外の場合、検索終了
                        checkPositionX -= vectorX;
                        checkPositionY -= vectorY;
                        break;
                    }
                    else {
                        // 壁上にいるかチェック
                        if (!this.checkDataWall(roomId, checkPositionX, checkPositionY)) {
                            break;
                        }
                    }
                }

                // 次の検索方向を調べる
                if (vectorX == 0) {
                    // 左右方向
                    vectorX = 1;
                    vectorY = 0;

                    // 壁上にいるかチェック
                    if (this.checkDataWall(roomId, checkPositionX + vectorX, checkPositionY + vectorY)) {
                    }
                    else if (this.checkDataWall(roomId, checkPositionX - vectorX, checkPositionY - vectorY)) {
                        vectorX = -1;
                    }
                    else {
                        window.console.log("エラー：壁検索時に異常が発生しました。データをお確かめください。");
                        return wallList;
                    }
                }
                else {
                    // 上下方向
                    vectorX = 0;
                    vectorY = 1;

                    // 壁上にいるかチェック
                    if (this.checkDataWall(roomId, checkPositionX + vectorX, checkPositionY + vectorY)) {
                    }
                    else if (this.checkDataWall(roomId, checkPositionX - vectorX, checkPositionY - vectorY)) {
                        vectorY = -1;
                    }
                    else {
                        window.console.log("エラー：壁検索時に異常が発生しました。データをお確かめください。");
                        return wallList;
                    }
                }

                if (nowStartX == checkPositionX && nowStartY == checkPositionY) {
                    window.console.log("エラー：壁検索時に異常が発生しました。データをお確かめください。");
                    return wallList;
                }

                // 出力値調整
                var startPointX = nowStartX;
                var startPointY = nowStartY;
                var endPointX = checkPositionX;
                var endPointY = checkPositionY;

                var wallData = this.calWallKind(startPointX, startPointY, endPointX, endPointY, roomId);
                wallList = wallList.concat(wallData);

                nowStartX = checkPositionX;
                nowStartY = checkPositionY;

                if (nowStartX == startX && nowStartY == startY) break;
            }

            return wallList;
        };

        /** 壁の始点・終点から壁の種類属性を追加
         * {int} startPointX 始点のx座標（data単位）
         * {int} startPointY 始点のy座標（data単位）
         * {int} endPointX 終点のx座標（data単位）
         * {int} endPointY 終点のy座標（data単位）
         * {int} roomId 部屋のindex
         * return 壁の始点・終点・壁の種類の情報
         */
        this.calWallKind = function (startPointX, startPointY, endPointX, endPointY, roomId) {
            wallList = [];

            if (startPointX % 2 != 0 || startPointX % 3 != 0) {
                startPointX += 1;
            }
            if (startPointY % 2 != 0 || startPointY % 3 != 0) {
                startPointY += 1;
            }
            if (endPointX % 2 != 0 || endPointX % 3 != 0) {
                endPointX += 1;
            }
            if (endPointY % 2 != 0 || endPointY % 3 != 0) {
                endPointY += 1;
            }

            var x = startPointX / 12;
            var y = startPointY / 12;
            var width = (endPointX - startPointX) / 12;
            var height = (endPointY - startPointY) / 12;

            var reverseFlg = false;

            if (startPointX > endPointX) {
                x = endPointX / 12;
                width = (startPointX - endPointX) / 12;
                reverseFlg = true;
            }
            if (startPointY > endPointY) {
                y = endPointY / 12;
                height = (startPointY - endPointY) / 12;
                reverseFlg = true;
            }

            x += this.itemAr[roomId].x;
            y += this.itemAr[roomId].y;

            var checkTouchItem = dragRoom.DRAG.checkItemWall(x, y, width, height, roomId);

            if (checkTouchItem.length > 0) {
                if (checkTouchItem == 1) {
                    if (x == checkTouchItem[0].startPointX &&
                        y == checkTouchItem[0].startPointY &&
                        width == (checkTouchItem[0].endPointX - checkTouchItem[0].startPointX) &&
                        height == (checkTouchItem[0].endPointY - checkTouchItem[0].startPointX)) {
                        wallList.push(this.createStartEndPointWithData(startPointX, startPointY, endPointX, endPointY, roomId, "share_wall"));
                        return wallList;
                    }
                }

                var nowX = x;
                var nowY = y;

                for (var i = 0; i < checkTouchItem.length; i++) {
                    if (nowX < checkTouchItem[i].startPointX || nowY < checkTouchItem[i].startPointY) {
                        if (!reverseFlg) {
                            wallList.push({
                                startPointX: nowX,
                                startPointY: nowY,
                                endPointX: checkTouchItem[i].startPointX,
                                endPointY: checkTouchItem[i].startPointY,
                                kind: "outer_wall"
                            });
                        }
                        else {
                            wallList.unshift({
                                startPointX: checkTouchItem[i].startPointX,
                                startPointY: checkTouchItem[i].startPointY,
                                endPointX: nowX,
                                endPointY: nowY,
                                kind: "outer_wall"
                            });
                        }
                    }

                    nowX = checkTouchItem[i].startPointX;
                    nowY = checkTouchItem[i].startPointY;

                    if (!reverseFlg) {
                        wallList.push({
                            startPointX: nowX,
                            startPointY: nowY,
                            endPointX: checkTouchItem[i].endPointX,
                            endPointY: checkTouchItem[i].endPointY,
                            kind: "share_wall"
                        });
                    }
                    else {
                        wallList.unshift({
                            startPointX: checkTouchItem[i].endPointX,
                            startPointY: checkTouchItem[i].endPointY,
                            endPointX: nowX,
                            endPointY: nowY,
                            kind: "share_wall"
                        });
                    }

                    nowX = checkTouchItem[i].endPointX;
                    nowY = checkTouchItem[i].endPointY;
                }

                if (width > (nowX - x) || height > (nowY - y)) {
                    if (!reverseFlg) {
                        wallList.push({
                            startPointX: nowX,
                            startPointY: nowY,
                            endPointX: x + width,
                            endPointY: y + height,
                            kind: "outer_wall"
                        });
                    }
                    else {
                        wallList.unshift({
                            startPointX: x + width,
                            startPointY: y + height,
                            endPointX: nowX,
                            endPointY: nowY,
                            kind: "outer_wall"
                        });
                    }
                }
            }
            else {
                wallList.push(this.createStartEndPointWithData(startPointX, startPointY, endPointX, endPointY, roomId, "outer_wall"));
            }
            return wallList;
        };


        /** 指定した壁要素の範囲に部屋に接する壁の始点・終点座標取得
         * {int} x x座標(グリッド単位)
         * {int} y y座標(グリッド単位)
         * {int} width 幅(グリッド単位)
         * {int} height 高さ(グリッド単位)
         * {int} roomIndex チェックしない部屋index
         * return  範囲に接する始点・終点座標（グリッド単位）
         */
        this.checkItemWall = function (x, y, width, height, notCheckIndex) {
            var output = [];

            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;
            width = Math.round(width * 10000) / 10000;
            height = Math.round(height * 10000) / 10000;

            for (var i = 0; i < this.itemAr.length; i++) {
                if (i == notCheckIndex) continue;
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].type >= 90) continue;// 部屋カテゴリじゃないものは除く

                var distanceX = Math.abs((x + width / 2) - (this.itemAr[i].x + this.itemAr[i].width / 2));
                var distanceY = Math.abs((y + height / 2) - (this.itemAr[i].y + this.itemAr[i].height / 2));

                if (distanceX <= (width + this.itemAr[i].width) / 2 &&
                    distanceY <= (height + this.itemAr[i].height) / 2 &&
                    !(distanceX == (width + this.itemAr[i].width) / 2 &&
                        distanceY == (height + this.itemAr[i].height) / 2)) {
                    // データが接しているかチェック

                    var positionX1 = Math.floor((x - this.itemAr[i].x) * this.grSplitMax);
                    var positionY1 = Math.floor((y - this.itemAr[i].y) * this.grSplitMax);

                    var positionX2 = Math.floor((x + width - this.itemAr[i].x) * this.grSplitMax);
                    var positionY2 = Math.floor((y + height - this.itemAr[i].y) * this.grSplitMax);

                    if (width === 0) {
                        if (positionX1 < 0) continue;

                        // 下方向に検索
                        var startY = -1;
                        for (var j = positionY1; j <= positionY2; j++) {
                            if (this.checkItemDataPosition(positionX1, j, i) || this.checkItemDataPosition(positionX1 - 1, j, i)) {
                                if (startY < 0) {
                                    startY = j;
                                }
                            }
                            else {
                                if (startY >= 0) {
                                    var addData = this.createStartEndPointWithData(positionX1, startY, positionX1, j - 1, i);
                                    if (!(addData.startPointX == addData.endPointX && addData.startPointY == addData.endPointY)) {
                                        output.push(addData);
                                        startY = -1;
                                    }
                                }
                            }
                        }
                        if (startY != -1) {
                            var addData = this.createStartEndPointWithData(positionX1, startY, positionX1, positionY2, i);
                            if (!(addData.startPointX == addData.endPointX && addData.startPointY == addData.endPointY)) {
                                output.push(addData);
                            }
                        }
                    }
                    else if (height == 0) {
                        if (positionY1 < 0) continue;

                        // 右方向に検索
                        var startX = -1;
                        for (var j = positionX1; j <= positionX2; j++) {
                            if (this.checkItemDataPosition(j, positionY1, i) || this.checkItemDataPosition(j, positionY1 - 1, i)) {
                                if (startX < 0) {
                                    startX = j;
                                }
                            }
                            else {
                                if (startX >= 0) {
                                    var addData = this.createStartEndPointWithData(startX, positionY1, j - 1, positionY1, i);
                                    if (!(addData.startPointX == addData.endPointX && addData.startPointY == addData.endPointY)) {
                                        output.push(addData);
                                        startX = -1;
                                    }
                                }
                            }
                        }
                        if (startX != -1) {
                            var addData = this.createStartEndPointWithData(startX, positionY1, positionX2, positionY1, i);
                            if (!(addData.startPointX == addData.endPointX && addData.startPointY == addData.endPointY)) {
                                output.push(addData);
                            }
                        }
                    }
                }
            }

            // 連結データを結合
            for (var i = output.length - 1; i > 0; i--) {
                if (output[i].startPointX == output[i - 1].endPointX && output[i].startPointY == output[i - 1].endPointY) {
                    output[i - 1].endPointX = output[i].endPointX;
                    output[i - 1].endPointY = output[i].endPointY;
                    output.splice(i, 1);
                }
                else if (output[i - 1].startPointX == output[i].endPointX && output[i - 1].startPointY == output[i].endPointY) {
                    output[i - 1].startPointX = output[i].startPointX;
                    output[i - 1].startPointX = output[i].startPointX;
                    output.splice(i, 1);
                }
            }

            // 重複除去
            for (var i = output.length - 1; i >= 0; i--) {
                for (var j = 0; j < i; j++) {
                    if (output[i].startPointX == output[j].startPointX &&
                        output[i].startPointY == output[j].startPointY &&
                        output[i].endPointX == output[j].endPointX &&
                        output[i].endPointY == output[j].endPointY) {
                        output.splice(i, 1);
                        break;
                    }
                }
            }

            return output;
        };

        /** 始点・終点データをdata単位からグリッド単位へ変換
         * {int} startX 開始x座標(data単位)
         * {int} startY 開始y座標(data単位)
         * {int} endX 終了x座標(data単位)
         * {int} endY 終了y座標(data単位)
         * {int} roomId dataの部屋index
         * {string} kind 壁の種類
         * return グリッド単位の始点終点データ
         */
        this.createStartEndPointWithData = function (startX, startY, endX, endY, roomId, kind) {
            var startPointX = startX;
            var startPointY = startY;
            var endPointX = endX;
            var endPointY = endY;

            if (startPointX % 2 != 0 || startPointX % 3 != 0) {
                startPointX += 1;
            }
            if (startPointY % 2 != 0 || startPointY % 3 != 0) {
                startPointY += 1;
            }
            if (endPointX % 2 != 0 || endPointX % 3 != 0) {
                endPointX += 1;
            }
            if (endPointY % 2 != 0 || endPointY % 3 != 0) {
                endPointY += 1;
            }

            return {
                startPointX: startPointX / 12 + this.itemAr[roomId].x,
                startPointY: startPointY / 12 + this.itemAr[roomId].y,
                endPointX: endPointX / 12 + this.itemAr[roomId].x,
                endPointY: endPointY / 12 + this.itemAr[roomId].y,
                kind: kind
            };
        }


        /** 部屋右回転
         * {int} 部屋番号
         * retrun 回転できた場合true、回転処理に失敗した場合false
         */
        this.rotateRoom = function (roomId) {
            if (roomId == null) return false;
            var item = this.itemAr[roomId];
            if (item == null) return false;

            var tmpData = [];
            for (var i = 0; i < item.data.length; i++) {
                for (var j = 0; j < item.data[i].length; j++) {
                    while (tmpData.length <= (Math.floor(item.height * this.grSplitMax) - 1 - j)) tmpData.push([]);
                    try {
                        tmpData[Math.floor(item.height * this.grSplitMax) - 1 - j][i] = item.data[i][j];
                    }
                    catch (e) {
                    }
                }
            }

            var tmpWidth = item.width;
            item.width = item.height;
            item.height = tmpWidth;

            var beforeData = item.data;
            item.data = tmpData;
            // 回転させた際に他の部屋と重なるかチェック
            if (this.checkItemOverlap(item.x, item.y, roomId) != null) {
                // データを戻す
                item.data = beforeData;
                item.height = item.width;
                item.width = tmpWidth;

                return false;
            }

            // 開口部を回転
            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i].roomId == roomId) {
                    var oTmpX = this.openingAr[i].x;

                    this.openingAr[i].x = item.width - this.openingAr[i].y;
                    this.openingAr[i].y = oTmpX;
                    this.openingAr[i].x -= this.openingAr[i].height;

                    // 四捨五入
                    this.openingAr[i].x = Math.round(this.openingAr[i].x * 10000) / 10000;
                    this.openingAr[i].y = Math.round(this.openingAr[i].y * 10000) / 10000;

                    if (this.openingAr[i].width < this.openingAr[i].height) {
                        this.openingAr[i].flip = !this.openingAr[i].flip;
                        this.openingAr[i].hFlip = !this.openingAr[i].hFlip;
                    }

                    var oTmpWidth = this.openingAr[i].width;
                    this.openingAr[i].width = this.openingAr[i].height;
                    this.openingAr[i].height = oTmpWidth;
                }
            }

            // シンクを回転
            for (var i = 0; i < this.sinkAr.length; i++) {
                if (this.sinkAr[i].roomId == roomId) {
                    var oTmpX = this.sinkAr[i].x;

                    this.sinkAr[i].x = item.width - this.sinkAr[i].y - this.sinkAr[i].height;
                    this.sinkAr[i].y = oTmpX;

                    // 四捨五入
                    this.sinkAr[i].x = Math.round(this.sinkAr[i].x * 10000) / 10000;
                    this.sinkAr[i].y = Math.round(this.sinkAr[i].y * 10000) / 10000;

                    this.sinkAr[i].angle += 90;
                    this.sinkAr[i].angle %= 360;

                    var oTmpWidth = this.sinkAr[i].width;
                    this.sinkAr[i].width = this.sinkAr[i].height;
                    this.sinkAr[i].height = oTmpWidth;
                }
            }

            this.draw();

            return true;
        }

        /* 空データを除いた部屋の数取得
  * return 部屋の数
  */
        this.roomCount = function () {
            var count = 0;
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] != null) count++;
            }
            return count;
        };

        /** 柱を除いた開口部のID取得（ドア、窓、柱、開口でそれぞれ計測する）
         * {object} obj 開口部情報
         * return 開口部ID
         */
        this.checkOpeningIdWithoutPiller = function (obj) {
            var index = this.openingAr.indexOf(obj);
            var type = obj.type;

            if (index < 0) return index;

            var output = 0;

            for (var i = 0; i < index; i++) {
                if (type < 20 && this.openingAr[i].type < 20) output++;
                else if (type == 20 && this.openingAr[i].type == 20) output++;
                else if (type > 20 && type < 98 && this.openingAr[i].type > 20 && this.openingAr[i].type < 98) output++;
                else if (type >= 98 && this.openingAr[i].type >= 98) output++;
            }

            return output;
        };

        /** canvasのサイズを中身に合わせて変更する
         * return なし
         */
        this.fitCanvas = function () {
            this.grMax = 60;
            var checkValue = this.checkCanvasPosition();

            if (checkValue.minX < 1) checkValue.minX = 1;
            if (checkValue.minY < 1) checkValue.minY = 1;

            if (checkValue.maxX - checkValue.minX < checkValue.maxY - checkValue.minY) {
                this.grMax = checkValue.maxY - checkValue.minY + 2;
            }
            else {
                this.grMax = checkValue.maxX - checkValue.minX + 2;
            }

            this.grMax /= 2;

            this.startGridX = checkValue.minX - 1;
            this.startGridY = checkValue.minY - 1;

            dragRoom.DRAG.windowResize();
        };

        /** canvasのサイズを中身に合わせて変更する（画像に出力するため中央寄せにする）
         * return なし
         */
        this.fitCanvasImage = function () {
            var checkValue = this.checkCanvasPosition();

            if (checkValue.maxX - checkValue.minX < checkValue.maxY - checkValue.minY) {
                this.grMax = checkValue.maxY - checkValue.minY + 2;
                this.startGridX = checkValue.minX - (this.grMax - (checkValue.maxX - checkValue.minX)) / 2;
                this.startGridY = checkValue.minY - 1;
            }
            else {
                this.grMax = checkValue.maxX - checkValue.minX + 2;
                this.startGridX = checkValue.minX - 1;
                this.startGridY = checkValue.minY - (this.grMax - (checkValue.maxY - checkValue.minY)) / 2;
            }

            this.grMax /= 2;

            window.console.log(this.startGridX + "," + this.startGridY + ":" + this.grMax);

            dragRoom.DRAG.windowResize();
        };

        /** canvas上での表示範囲
         * return {最小値(x,y),最大値(x,y)} （グリッド単位）
         */
        this.checkCanvasPosition = function () {
            var minX = this.grMax;
            var minY = this.grMax;
            var maxX = 0;
            var maxY = 0;

            for (var i = this.itemAr.length - 1; i >= 0; i--) {
                if (this.itemAr[i] == null) continue;
                if (this.itemAr[i].x < minX) minX = this.itemAr[i].x;
                if (this.itemAr[i].y < minY) minY = this.itemAr[i].y;

                if (this.itemAr[i].x + this.itemAr[i].width > maxX)
                    maxX = this.itemAr[i].x + this.itemAr[i].width;
                if (this.itemAr[i].y + this.itemAr[i].height > maxY)
                    maxY = this.itemAr[i].y + this.itemAr[i].height;
            }

            return {minX: minX, minY: minY, maxX: maxX, maxY: maxY};
        };

        /** 全データ四捨五入
         */
        this.roundAllData = function () {
            for (var i = 0; i < this.itemAr.length; i++) {
                if (this.itemAr[i] == null) continue;
                this.itemAr[i].x = Math.round(this.itemAr[i].x * 10000) / 10000;
                this.itemAr[i].y = Math.round(this.itemAr[i].y * 10000) / 10000;
                this.itemAr[i].width = Math.round(this.itemAr[i].width * 10000) / 10000;
                this.itemAr[i].height = Math.round(this.itemAr[i].height * 10000) / 10000;
            }

            for (var i = 0; i < this.openingAr.length; i++) {
                if (this.openingAr[i] == null) continue;
                this.openingAr[i].x = Math.round(this.openingAr[i].x * 10000) / 10000;
                this.openingAr[i].y = Math.round(this.openingAr[i].y * 10000) / 10000;
                this.openingAr[i].width = Math.round(this.openingAr[i].width * 10000) / 10000;
                this.openingAr[i].height = Math.round(this.openingAr[i].height * 10000) / 10000;
            }
        }

    }
})(jQuery);
