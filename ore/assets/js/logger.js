// 例1
// var logger = new Logger(LoggerLevel.DEBUG);
// logger.debug("debug");
//
// 例2
// OreLog.info("info");
var LoggerLevel={
  ALL:-99,
  DEBUG:-1,
  INFO:1,
  WARN:2,
  ERROR:3,
  OFF:99
};
var Logger=function(level){
  var self=this;
  self.level=isNaN(level) ? LoggerLevel.INFO : level;
  self.make();
};
Logger.prototype.make=function(){
  var self=this;
  for(var key in console){
    var l=LoggerLevel[key.toUpperCase()];
    if(!l){l=LoggerLevel.OFF};
    if(self.level<=l){
      if(console[key].bind){
        Logger.prototype[key]=(
          function(k){
            return console[k].bind(console);
          }
        )(key);
      }else if(console[key].apply){
        Logger.prototype[key]=(
          function(k){
            return console[k].apply(console, arguments);
          }
        )(key);
      }
    }else{
      Logger.prototype[key]=function(){};
    }
  }
};

var OreLog = new Logger(LoggerLevel.DEBUG);
