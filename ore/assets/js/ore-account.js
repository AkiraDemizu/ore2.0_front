var OreAccount = function() {
	OreLog.info('OreAccount');
	this.return_url = location.href;
	this.success_str = '元のページに戻る';
	this.failure_str = '元のページに戻る';
	this.url = 'https://linkpt.cardservice.co.jp/cgi-bin/credit/order.cgi';
}
OreAccount.prototype = {
	pay: function(shopping_item) {
		var self = this;
		OreLog.debug('shopping_item', shopping_item);
		OreUtil.ajax('zeus_payments', {shopping_item:shopping_item}, function(data){
			OreLog.info('pay ajax終了');
			OreLog.debug('data', data);

			data.success_url = self.return_url;
			data.failure_url = self.return_url;
			data.success_str = self.success_str;
			data.failure_str = self.failure_str;

			OreUtil.postForm(self.url, data);
		}, 'POST');
	},
	buySheet: function() {
		this.pay(1);
	},
	buyWaku: function() {
		this.pay(2);
	},
	buyKamei: function() {
		this.pay(10);
	}
}
