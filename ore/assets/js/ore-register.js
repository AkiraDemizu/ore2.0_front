var OreRegister = function(api, $form){
	this.api = api;
	this.$form = $form;
	this.marge_data = {};
	this.after_func = null;
	this.test_data = [];
	this.images = {};
	this.data = {};
};
OreRegister.prototype = {
	load: function(house_id) {
		var dfd = $.Deferred();
		OreLog.debug('load 開始');
		// TODO:デバッグ用にクエリ引数からパラメータを復元できるようにする
		if (typeof OreStrage !== 'undefined' && Object.keys(OreUtil.getParams).length === 0) {
			OreUtil.getParams = OreStrage.getParams;
		}
		for (var i in OreUtil.getParams) {
			this.id = OreUtil.getParams[i];
		}
		if(this.id === undefined && house_id !== undefined){
			this.id = house_id;
		}

		var self = this;
		OreUtil.ajax(this.api+'/'+this.id, {}, function(data) {
			OreLog.info("データ取得");
			OreLog.debug(data);
			self.data = data.content;

			var obj = data.content;
			OreLog.debug(obj);
			for (var name in obj) {
				var input = self.$form.find('[name="'+name+'"]');
				var type = '';
				if (input.prop('tagName') === 'INPUT') {
					type = input.prop('type');
				} else if (input.prop('tagName') === 'SPAN') {
					type = 'span';
				}
				
				//OreLog.debug(input);
				//OreLog.debug(type);
				switch(type){
					case 'radio':
						if (obj[name]) {
							input.val([obj[name]]);
						}
						break;
					case 'span':
						if (!obj[name]) {
							input.parents('td').hide();
							input.text('');
						} else {
							var select = input.data('select');
							if (select) {
								var s = select.split(',');
								input.prepend(s[obj[name]]);
							} else {
								input.prepend(obj[name]);
							}
						}
						break;
					default:
						if (obj[name]) {
							input.val(obj[name]);
						}
				}
			}
			// 出力先を持たせて伝搬させる
			obj.$pref = self.$pref;
			obj.$city = self.$city;

			OreLog.debug('load 終了');
			dfd.resolve(obj);
		});
		return dfd.promise();
    },
    preCheck: function (key, value) {
    	var dfd = $.Deferred();

		var ajax_param = {
			type: 'page',
			page: 1,
			size: 10,
			where: '[{"field":"'+key+'","ope":"equal","value":"'+value+'"}]'
		};

		OreUtil.ajax(this.api, ajax_param, function(data) {
			OreLog.debug(data);

			if (data.totalElements > 0) {
				dfd.reject(data);
			} else {
				dfd.resolve();
			}
		});

    	return dfd.promise();
    },
    process: function (method) {
		var dfd = $.Deferred();
		var param = {};
		$(this.$form.serializeArray()).each(function(i,v){
			if (v.value !== '') {
				param[v.name] = v.value;
			}
		});
		OreLog.debug(param);
		
		// テストデータがあれば挿入
		for (var i in this.test_data) {
			if (typeof(param[i]) === 'undefined') {
				param[i] = this.test_data[i];
			}
		}
		OreLog.debug(param);

		// ログインスタッフは自動で挿入
		if (this.api === 'clients') {
			param['staff_id'] = OreUtil.loginStaffId;
		}

		// マージデータで上書き
		for (var data in this.marge_data) {
			param[data] = this.marge_data[data];
		}

		// エスケープ
		for (var p in param) {
			param[p] = OreUtil.escHtml(param[p]);
		}

		OreLog.debug('param');
		OreLog.debug(param);

		// id=0で新規登録
		var api = this.api;
		if (this.id > 0) {
			api += '/' + this.id;
		}
		OreLog.debug('method = '+this.method);
		OreLog.debug('url='+api);

		var self = this;
		OreUtil.ajax(api, param, function(data) {
			OreLog.info('登録が完了した');
			OreLog.debug(data);
			if (self.after_func !== null) {
				self.after_func(data.pkey);
			}
			dfd.resolve({key:data.pkey,images:self.images});
		}, method, {error_func:function(data) {
			dfd.reject(data);
		}});

		return dfd.promise();
    },
    test: function(obj) {
    	this.test_data = obj;
		for (var name in obj) {
			var input = this.$form.find('[name="'+name+'"]');
			var type = input.prop('type');
			//OreLog.debug(input);
			switch(type){
				case 'radio':
					input.val([obj[name]]);
					break;
				default:
					input.val(obj[name]);
			}
		}
    },
    regist: function () {
		return this.process('POST');
    },
    edit: function () {
		return this.process('PUT');
    },
    delete: function () {
		var dfd = $.Deferred();
		var self = this;
		OreUtil.ajax(this.api+'/'+this.id, {}, function(data) {
			OreLog.info('削除が完了した');
			OreLog.debug(data);
			dfd.resolve(data.pkey);
		}, 'DELETE');
		return dfd.promise();
    },
    marge: function (data) {
		this.marge_data = data;
    },
    addAfter: function (func) {
		this.after_func = func;
    },
    makeOptionPrefCity: function ($pref, $city) {
    	this.$pref = $pref;
    	this.$city = $city;
		OreUtil.ajax('prefs', {}, function(prefs) {
			OreLog.debug(prefs);
			$pref.empty();
			$pref.append($('<option>').val('').text('都道府県'));
			for (var pref of prefs) {
				var $option = $('<option>').val(pref.id).text(pref.name);
				$pref.append($option);
			}

			// 都道府県リストが更新された
			$pref.change(function () {
				$city.empty();
				$city.append($('<option>').val('').text('市区町村'));
				var pref_id = $(this).find('option:selected').val();
				if (pref_id === '') return;
				OreUtil.ajax('prefs/'+pref_id+'/cities', {}, function(cities) {
					OreLog.debug(cities);
					for (var city of cities) {
						var $option = $('<option>').val(city.city_code).text(city.city_name);
						$city.append($option);
					}
				});
			})
		});
    },
    getPrefCityName: function () {
		return this.$pref.find('option:selected').text() + this.$city.find('option:selected').text();
	},
	editWithImage: function () {
		this.process('PUT')
		.done(function(data) {
			var address = 'houses/'+data.key+'/images';
			for (var id in data.images) {
				var form = new FormData();
				form.append('image', data.images[id]);
				OreUtil.ajax(address, form, function(response) {
					OreLog.info('画像登録が完了した');
					OreLog.debug(response);
				}, 'POST');
			}
		});
	},
	deleteImage: function (key) {
		var self = this;
		OreLog.info(key+'を削除します');
		OreUtil.ajax(this.api+'/'+this.id+'/images/'+key, {}, function(data) {
			OreLog.info('画像削除が完了した:'+key);
		}, 'DELETE');
    }
};

var uploadImage = function (data) {
	var dfds = [];	// Deferredオブジェクト保存用
	OreLog.debug(data);
	OreLog.info("uploadImage 開始");

	$.each(data.images, function(i, image) {
		var d = new $.Deferred();
		var form = new FormData();
		form.append('image', image);
		OreUtil.ajax('houses/'+data.key+'/images', form, function(res) {
			OreLog.info('画像登録が完了した');
			OreLog.debug(res);
			d.resolve(data);
		}, 'POST', {chained:true});
		dfds.push(d);
	});
	if (dfds.length > 0) {
		return $.when.apply($, dfds)
		.then(function(data) {
			OreLog.debug('modal hide');
			var dfd = new $.Deferred();
			dfd.resolve(data);
			OreUtil.$apiModal.hide();
			return dfd.promise();
		});
	} else {
		var d = new $.Deferred();
		d.resolve(data);
		return d.promise();
	}
};

var loadOptionCity = function (data) {
	var dfd = $.Deferred();
	OreLog.debug(data);
	OreLog.debug("loadOptionCity 開始");

	OreUtil.ajax('prefs/'+data.pref_id+'/cities', {}, function(cities) {
		OreLog.debug(cities);
		data.$city.empty();
		data.$city.append($('<option>').val('').text('市区町村'));
		for (var city of cities) {
			var $option = $('<option>').val(city.city_code).text(city.city_name);
			data.$city.append($option);
		}

		OreLog.debug("loadOptionPref 終わり");
		dfd.resolve(data);
	});

	return dfd.promise();
};

var loadImage = function (data) {
	var dfd = $.Deferred();
	OreLog.debug(data);
	OreLog.debug("loadImage 開始");

	// サーバ上の画像データを追加
	data.server_images = [];
	var url = 'houses/'+data.id+'/images';
	OreLog.debug(url);
	OreUtil.ajax(url, {}, function(response) {
		OreLog.debug(response);
	
		for (var cnt of response.content) {
			data.server_images.push({url:cnt.image,id:cnt.id});
		}

		OreLog.debug("loadImage 終わり");
		dfd.resolve(data);
	});

	return dfd.promise();
};
