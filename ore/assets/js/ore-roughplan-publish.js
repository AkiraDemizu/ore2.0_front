var OrePublish = function() {
	OreLog.info('OrePublish');
	this.data = {};
	this.outputData = {};
	this.input = {};
	this.floor_cache = {};
}
OrePublish.prototype = {
	// 物件掲載用
	load: function(id) {
		OreLog.info('物件情報ロード開始');
		var self = this;
		OreUtil.ajax('houses/' + id, {}, function(data) {
			self.data = data.content;
			OreLog.info('物件情報ロード終了');
		}, 'get');
	},

	// 物件登録用
	setData: function(data) {
		this.data = data;
	},
	setFloorCache: function(floor_cache) {
		this.floor_cache = floor_cache;	
	},
	validate: function(rough_plan_id) {
		OreLog.info('validate');
		var dfd = $.Deferred();
		// アラート
		var alert = new ore_alert('.ore-alert');
		alert.hide();
		alert.clear_msg();

		var error = false;
		if (!$('.stage2 input[name="privacy_agree"]').prop('checked')) {
			alert.set_message('掲載規約が 同意されていません。');
			error = true;
		}
		if (!$('.stage2 input[name="price_agree"]').prop('checked')) {
			alert.set_message('金額変動の可能性について 同意されていません。');
			error = true;
		}
		var params = {
			rough_plan_id: rough_plan_id,
			title: OreValidation.modifyTrim($('.stage2 input[name="title"]').val()),
			description: OreValidation.modifyTrim($('.stage2 textarea[name="description"]').val()),
			trade_aspect: OreValidation.modifyTrim($('.stage2 select[name="trade_aspect"]').val()),
			renovation_title: OreValidation.modifyTrim($('.stage2 input[name="renovation_title"]').val()),
			renovation_description: OreValidation.modifyTrim($('.stage2 textarea[name="renovation_description"]').val()),
		};
		if (params.title === '') {
			alert.set_message('タイトルが 設定されていません。');
			error = true;
		}
		if (params.description === '') {
			alert.set_message('アピールポイントが 設定されていません。');
			error = true;
		}
		if (params.trade_aspect === '') {
			alert.set_message('取引態様が 設定されていません。');
			error = true;
		}
		if (params.renovation_title === '') {
			alert.set_message('リノベーションのキャッチコピーが 設定されていません。');
			error = true;
		}
		if (params.renovation_description === '') {
			alert.set_message('リノベーションのアピールポイントが 設定されていません。');
			error = true;
		}
		
		// 入力チェック
		if (params.title.length > 255) {
			alert.set_message('タイトルは 255文字以内にしてください。');
			error = true;
		}
		if (params.renovation_title.length > 255) {
			alert.set_message('リノベーションのキャッチコピーは 255文字以内にしてください。');
			error = true;
		}

		// 画像説明文をチェック
		$.each($('.stage2 .thumbnails img'), function(i) {
			var $this = $(this);
			var caption = $this.data('caption');
			if (typeof caption === 'undefined') {
				return;
			}
			OreLog.debug('caption', $this.data('caption'));
			if (caption.length > 100) {
				alert.set_message((i+1)+'枚目の画像の説明文を 100文字以内にしてください。');
				error = true;
			}
		});

		if (error) {
			var $box = $('#bukken-keisai');
			var $alert = $('.ore-alert')

			var offset = $box.position();
			var h0 = $box.height();
			var h1 = $alert.outerHeight();
			alert.set_position(offset.top + (h0 - h1) / 2 + 'px', '150px');
			alert.show();
			dfd.reject();
		} else {
			dfd.resolve();
		}

		return dfd.promise();
	},
	do: function(rough_plan_id) {
		var dfd = $.Deferred();

		var params = {
			rough_plan_id: rough_plan_id,
			title: OreValidation.modifyTrim($('.stage2 input[name="title"]').val()),
			description: OreValidation.modifyTrim($('.stage2 textarea[name="description"]').val()),
			trade_aspect: OreValidation.modifyTrim($('.stage2 select[name="trade_aspect"]').val()),
			renovation_title: OreValidation.modifyTrim($('.stage2 input[name="renovation_title"]').val()),
			renovation_description: OreValidation.modifyTrim($('.stage2 textarea[name="renovation_description"]').val()),
		};

		OreLog.debug('params', params);
		OreUtil.ajax('confirm_public_houses', params, function(data){
			dfd.resolve();
		}, 'post', {chained:true, error_func:function(err, defaultError) {
			if (err.status === 400) {
				$('#modal-confirm-waku-error .btn-close02').hide();
				$('#modal-confirm-waku-error .btn-check-now').click(function() {
					location.href = '/ore/publish/plan_list.html';
				});
				$('#modal-confirm-waku-error').modal('show');
			} else {
				defaultError();
			}
		}});

		return dfd.promise();
	},

	// 物件登録掲載共通
	// データの出力先の定義
	setOutputData: function(output) {
		this.outputData = output;	
	},
	show: function() {
		OreLog.info('OrePublish show');
		OreLog.debug('data', this.data);
		
		function showProcess(output, data) {
			$.each(output, function(i, option) {
				if (typeof option.func === 'function') {
					// modsに関数が代入されたときそれを実行
					option.func(option.$target, data);
				} else {
					// modsを解析してvalを書き換え
					var keys = option.keys.split(',');
					var val = '';
					$.each(keys, function(j, key) {
						var d = Function('data', 'return data.'+key+';')(data);
						if (typeof d === 'undefined') {
							OreLog.warn(option.keys, 'is undefined');
						}
						val += (d === null) ? '' : d;
					});

					var mods = [];
					if (typeof option.mods !== 'undefined') {
						mods = option.mods.split(',');
					}
					OreLog.debug('val', val);
					$.each(mods, function(j, mod) {
						if (mod === 'manyen') {
							val = OreUtil.formatNumMan(val, true);
						}
						if (mod === 'yen') {
							val = OreUtil.formatNum(val);
						}
						if (mod === 'address') {
							var idx = val.search(/[0-9０-９]/);
							if (idx >= 0) {
								val = val.substring(0, idx);
							}
						}
					});

					var select = [];
					if (typeof option.select !== 'undefined') {
						val = option.select[val];
					}
					// 入力がない項目は-
					val = val === '' ? '-' : val;

					// 出力先にアウトプット
					option.$target.text(val);
				}
			});
		}

		// 物件情報出力
		showProcess(this.outputData, this.data);

		// マップ
		var data = this.data;
		var map_url = 'https://maps.google.co.jp/maps?output=embed&t=m&z=16';
		if (data.map_longitude && data.map_latitude && $('#detail-map')[0]) {
			//map_url += '&ll='+data.map_latitude+','+data.map_longitude;
			var address = data.pref+data.city+data.village_section;
			var idx = address.search(/[0-9０-９]/);
			if (idx >= 0) {
				address = address.substring(0, idx);
			}
			map_url += '&q='+address;
			OreLog.debug('map_url', map_url);
			$('#detail-map')[0].contentDocument.location.replace(map_url);
		}

		// オプションリストを表示
		var $target = $('.planning-wrap .option-list');
		$target.empty();
		$.each(data.options, function(i, option) {
			$target.append('<li>'+option.name+'</li>');
		});

		// パース画像をロード
		if (typeof data.pers_list !== 'undefined') {
			if (data.pers_list.length > 0) {
				$('.renovation-image-main img').prop('src', data.pers_list[0]);
				$target = $('.renovation-image-thumbnail .row');
				$target.empty();
				$.each(data.pers_list, function(i, pers) {
					$target.append('<li class="col-xs-6"><img src="'+pers+'" alt=""></li>');
				});
				$target.find('img').bind("load",function(){
					$(this).parent().css({height:$(this).height()});
				});
				$target.find('img').click(function() {
					$('.renovation-image-main img').prop('src', $(this).prop('src'));
				});
			}
		}

		// 間取りを表示
		this.showMadori();
	},
	// 間取り表示
	showMadori: function() {
		var self = this;
		function showProcess() {
			OreLog.info('OrePublish showMadori show');
			var data = self.data;
			var floor_cache = self.floor_cache;
			OreLog.debug('data', data);
			OreLog.debug('floor_cache', floor_cache);
			var len = data.floor_plans.length;
			var $images = $('.regist-list-images').empty();
			for(var i = 0; i < len; i++) {

				var floor_data = floor_cache['plan' + data.floor_plans[i].floor_plan_id].data;
				if (floor_data.floor === 0) {
					continue;
				}

				// 画像を挿入
				$('#madori-'+i+'f-before').prop('src', floor_data.before.image);
				$('#madori-'+i+'f-after').prop('src', floor_data.after.image);

				// タブ制御
				$('#madori-'+i+'f-tab').show();
			}
		}
		if (Object.keys(this.floor_cache).length <= 0) {
			// キャッシュが保存されていないのでAPIからロード
			var dfds = [];	// Deferredオブジェクト保存用
			// 間取り画像をロード
			$.each(self.data.floor_plans, function(i, floor_plan) {
				OreLog.debug('floor_plan', floor_plan);
				var d = new $.Deferred();
				OreUtil.ajax('floor_plans/' + floor_plan.floor_plan_id, {}, function(res) {
					OreLog.debug('res', floor_plan.floor_plan_id, res);
					// 必要な画像を保存する
					var cache = {};
					cache.data = res;
					self.floor_cache['plan' + floor_plan.floor_plan_id] = cache;
					d.resolve();
				});
				dfds.push(d);
			})
			$.when.apply($, dfds).done(showProcess);
		} else {
			// キャッシュからロード
			showProcess();
		}
	}
}

var OreImage = function() {
	OreLog.info('OreImage');
	this.$main = {};
	this.$regist = {};
	this.$edit = {};
	this.$subViewParent = {};
	this.$currentSubView = {};
	this.initFunc = null;
	this.readonlyFunc = null;
}
OreImage.prototype = {
	changeCurrent: function($current) {
		// サムネイル遷移前にキャプションを保存
		this.saveCaption();
		this.$currentSubView.parent().removeClass('on');
		
		this.$currentSubView = $current;

		// キャプションをロード
		this.loadCaption();
		this.$currentSubView.parent().addClass('on');
	},
	setMain: function($caption, $regist, $edit) {
		this.$mainCaption = $caption;
		this.$regist = $regist;
		this.$edit = $edit;
		OreLog.info(this, 'setMainCaption');
	},
	setSubViewParent: function($viewParent) {
		OreLog.info(this, 'setSubViewList');
		OreLog.debug('$viewList', $viewParent);
		this.$subViewParent = $viewParent;
		this.$currentSubView = $(this.$subViewParent.find('img')[0]);
		this.$currentSubView.parent().addClass('on');
	},
	setInitFunc: function(func) {
		this.initFunc = func;
	},
	setReadonlyFunc: function(func) {
		this.readonlyFunc = func;
	},
	showView: function(src) {
		OreLog.info(this, 'showView');

		// ファイルから表示
		var $targetView = this.$edit.find('img');
		if (src instanceof File) {
			var reader = new FileReader();
			var self = this;
			reader.onload = function() {
				$targetView.prop('src', reader.result);
				self.$regist.hide();
				self.$edit.show();
			}
			reader.readAsDataURL(src);
		} else {
			$targetView.prop('src', src);
			this.$regist.hide();
			this.$edit.show();
		}
	},
	showSubView: function(src) {
		OreLog.info(this, 'showSubView');

		// 画像表示
		if (src instanceof File) {
			var reader = new FileReader();
			var self = this;
			reader.onload = function() {
				self.$currentSubView.prop('src', reader.result);
				self.$currentSubView.data('src', src);
			}
			reader.readAsDataURL(src);
		} else {
			this.$currentSubView.prop('src', src);
			this.$currentSubView.data('src', src);
		}
	},
	deleteCurrent: function() {
		OreLog.info(this, 'deleteCurrent');

		// 画像削除
		this.$currentSubView.prop('src', '/ore/assets/images/dummy/before.png');
		this.$currentSubView.removeData();

		// メインキャプション削除
		this.$mainCaption.val('');
		// メイン画面表示入れ替え
		this.$edit.hide();
		this.$regist.show();
	},
	saveCaption: function() {
		OreLog.info(this, 'saveCaption');
		var caption = this.$mainCaption.val();
		OreLog.debug('caption', caption);
		this.$currentSubView.data('caption', caption);
	},
	loadCaption: function() {
		OreLog.info(this, 'loadCaption');
		var caption =  this.$currentSubView.data('caption');
		OreLog.debug('caption', caption);
		this.$mainCaption.val(caption);
	},
	save: function(id) {
		OreLog.info(this, 'save');
		var dfd = new $.Deferred();
		// 最新のキャプション変更は保存されていないため忘れずに保存
		this.saveCaption();
		var address = 'houses/'+id+'/images/list';
		var form = new FormData();
		var nodata = true;
		// 画像を保存
		$.each(this.$subViewParent.find('img'), function(i) {
			var $this = $(this);
			var src = $this.data('src');
			var caption = $this.data('caption');

			if (typeof src === 'undefined') {
				return;
			}
			OreLog.debug('src', $this.data('src'));
			form.append('images['+i+'][title]', caption);
			form.append('images['+i+'][image]', src);
			nodata = false;
		});
		OreLog.debug(form);
		OreLog.debug('nodata', nodata);
		OreUtil.ajax(address, form, function(res) {
			OreLog.info('画像登録が完了した');
			dfd.resolve();
		}, nodata ? 'DELETE' : 'POST', {error_func:function(res) {
			OreLog.info('画像登録が失敗した');
			OreLog.debug('res', res);
			var message = '';
			if (res.status === 400) {
				// アラート
				var alert = new ore_alert('.ore-alert');
				alert.hide();
				alert.clear_msg();

				alert.set_message(JSON.parse(res.responseText).details[0].messages);
				
				var $box = $('#bukken-keisai');
				var $alert = $('.ore-alert')

				var offset = $box.position();
				var h0 = $box.height();
				var h1 = $alert.outerHeight();
				alert.set_position(offset.top + (h0 - h1) / 2 + 'px', '150px');
				alert.show();
			}
			dfd.reject();
		}});
		return dfd.promise();
	},
	load: function(id) {
		OreLog.info(this, 'load');
		var address = 'houses/'+id+'/images';
		var dfd = new $.Deferred();
		// 画像をロード
		var self = this;
		OreUtil.ajax(address, {}, function(res) {
			OreLog.info('画像のロードが完了した');
			OreLog.debug('res', res);
			
			if (res.content.length <= 0) {
				return;
			}

			// サムネイル表示
			var $targets = self.$subViewParent.find('img');
			$.each(res.content, function(i) {
				OreLog.debug('image', this.image);
				OreLog.debug('title', this.title);
				$($targets[i]).prop('src', this.image);
				$($targets[i]).data('caption', this.title);
				$($targets[i]).data('src', this.image);
			});
			
			// メイン画像表示
			self.showView(res.content[0].image);
			self.loadCaption();

			dfd.resolve();
		});
		return dfd.promise();
	},
	delete: function(id) {
		OreLog.info(this, 'delete');
		var address = 'rough_plans/'+id+'/images/';
		var dfds = [];	// Deferredオブジェクト保存用
		// 画像を削除
		$.each(this.imageIDList, function() {
			var d = new $.Deferred();
			var imageID = this;
			OreUtil.ajax(address + imageID, {}, function(res) {
				OreLog.info('画像の削除が完了した');
				d.resolve();
			}, 'DELETE');
			dfds.push(d);
		});
		return $.when.apply($, dfds);
	}
}
