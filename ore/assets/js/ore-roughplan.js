function ore_plan_init(plan_type,house_id, client_id, publish, image) {
	jQuery.extend({
		stringify : function stringify(obj) {
			var t = typeof (obj);
			if (t != "object" || obj === null) {
				// simple data type
				if (t == "string") obj = '"' + obj + '"';
				return String(obj);
			} else {
				// recurse array or object
				var n, v, json = [], arr = (obj && obj.constructor == Array);

				for (n in obj) {
					v = obj[n];
					t = typeof(v);
					if (obj.hasOwnProperty(n)) {
						if (t === 'string' || t === 'number') v = '"' + v + '"'; else if (t == "object" && v !== null) v = jQuery.stringify(v);
						json.push((arr ? "" : '"' + n + '":') + String(v));
					}
				}
				return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
			}
		}
	});

	var rough_plan_id = null;
	var taste_id = null;
	var selected_floor_plans = null;
	var floor_cache;
	var viewMode = '';
	var floor_search_results = $.stringify([]);
	var readOnly = false;
	var client = null;

	var init_mng = [false,false];
	var stageCount;
	if (plan_type === 0) {
		stageCount = 7;
	} else {
		stageCount = 4;
	}
	var alert;
	var containerHeight;
	var madori_obj = null;
	var house_data = {};

	loadPreference()
	.then(loadTastes)
	.then(loadHouseInfo)
	.done(function() {
		if (typeof OreUtil.getParams.rough_plan_id !== 'undefined') {
			OreLog.info('ラフプランロード開始');
			if (typeof OreUtil.getParams.mode === 'undefined') {
				location.href = '/ore/404.html';
				return;
			}
			viewMode = OreUtil.getParams.mode;
			var modes = ['edit', 'confirm', 'print', 'doc'];
			if ($.inArray(viewMode,modes) < 0) {
				location.href = '/ore/404.html';
				return;
			}

			loadRoughplan();
			$('.request-madori').hide();
			return;
		}
		if (plan_type === 0) {
			OreUtil.ajax('clients/' + OreUtil.getParams.client_id, {}, function(data) {
				OreUtil.$apiModal.hide();
				client = data.content;
				init_mng[0] = true;
				if (init_mng[0] && init_mng[1]) {
					ore_plan_init2();
				}
			}, 'get', {chained: true});
			return;
		}
		OreUtil.$apiModal.hide();
		init_mng[0] = true;
		if (init_mng[0] && init_mng[1]) {
			ore_plan_init2();
		}
	});

	$(function(){
		init_mng[1] = true;
		if (init_mng[0] && init_mng[1]) {
			ore_plan_init2();
		}
	});

	function loadPreference() {
		var dfd = $.Deferred();
		OreLog.info('こだわりロード開始');
		OreUtil.ajax('houses/' + OreUtil.getParams.house_id + '/preference', {}, function(data) {
			$('.kodawari-list input[type="radio"],.kodawari-list input[type="checkbox"],.parking-type-select input[type="radio"]').removeClass('show').attr('disabled', 'disabled');
			$('.kodawari-list input[type="radio"]').each(function(){
				var $this = $(this);
				var val = $this.val();
				for (var i in data) {
					if (data[i] == val) {
						$this.addClass('show').removeAttr('disabled');
						break;
					}
				}
			});
			$('.kodawari-list input[type="checkbox"]').each(function(){
				var $this = $(this);
				var val = $this.val();
				for (var i in data) {
					if (data[i] == val) {
						$this.addClass('show').removeAttr('disabled');
						break;
					}
				}
			});
			$('.parking-type-select input[type="radio"]').each(function(){
				var $this = $(this);
				var val = $this.val();
				for (var i in data) {
					if (data[i] == val) {
						$this.addClass('show').removeAttr('disabled');
						break;
					}
				}
			});
			OreLog.info('こだわりロード終了');
			dfd.resolve();
		}, 'get', {chained: true});
		return dfd.promise();
	}
	function loadTastes() {
		var dfd = $.Deferred();
		OreLog.info('tastesロード開始');
		OreUtil.ajax('tastes', {
			type: 'page',
			page: 1,
			orderby: '[-id]',
			size: 100
		}, function(data) {
			var $tastes = $('.taste-list .row').empty();
			var color = 'beige';
			if (plan_type === 0) {
				color = 'green';
			}
			for(var i in data) {
				var $div = $('<div/>').addClass('col-xs-3');
				var $list = $('<div/>').addClass('list-image');
				// こだわりリスト中のradioの数だけずらす
				var labelNum = $('.kodawari-list input[type="radio"]').length + Number(i);
				$list.append($('<label for="rd_'+labelNum+'"/>').append($('<img/>').attr('src', data[i].image)));
				$list.append('<a class="btn-enlarge image-preview" href="javascript:void(0);"></a>');
				$div.append($list);
				$div.append('<input class="deco-cb deco-rb-'+ color + '" type="radio" name="taste_id" value="' + data[i].id + '">');
				$div.append('<label>' + data[i].name + '</label>');
				$tastes.append($div);
			}
			OreLog.info('tastesロード終了');
			dfd.resolve();
		}, 'get', {chained: true});
		return dfd.promise();
	}

	function loadHouseInfo() {
		var dfd = $.Deferred();
		OreLog.info('物件情報ロード開始');
		OreUtil.ajax('houses/' + OreUtil.getParams.house_id, {}, function(data) {
			OreLog.debug('houses', data);
			var selector = '.stage2 ';
			if (plan_type === 0) {
				selector = '.stage3 '
			}
			$(selector + '.house-val').each(function () {
				var $this = $(this);
				var field = $this.data('field');
				var val = data.content[field];
				if (typeof val === 'undefined') {
					console.log('API:' + field + 'がありません。');// debug
					return;
				}
				if ($this.hasClass('year')) {
					$this.text(val + '年');
				} else if ($this.hasClass('format-num')) {
					$this.text(OreUtil.formatNum(val));
				} else if ($this.hasClass('yen')) {
					$this.text(OreUtil.formatNum(val) + '円');
				} else {
					$this.text(val);
				}
			});
			$(selector + '.blih-floor span').text(OreUtil.formatMadori(data.content));
			house_data = data.content;
			OreLog.info('物件情報ロード終了');
			dfd.resolve();
		}, 'get', {chained: true});
		return dfd.promise();
	}

	function loadRoughplan() {
		var dfd = $.Deferred();
		rough_plan_id = OreUtil.getParams.rough_plan_id;
		OreUtil.ajax('rough_plans/' + rough_plan_id, {}, function(data) {
			OreLog.info('ラフプランロード終了');
			OreLog.debug('data', data);
			if (viewMode === 'edit') {
				if (plan_type === 0 && data.complete !== 0) {
					viewMode = 'confirm';
				} else if (plan_type === 1 && data.status !== 0) {
					viewMode = 'confirm';
				}

			}
			//データ表示
			if (plan_type === 0) {
				$('input[name="repaid_amount"]').val(data.repaid_amount);
				$('input[name="own_fund"]').val(data.own_fund);
			} else {
				$('input[name="offer_price"]').val(OreUtil.formatNumMan(data.offer_price,true));
			}
			$('input[name="interest_rate"]').val(data.interest_rate);
			$('input[name="repayment_years"]').val(data.repayment_years);
			$('input[name="taste_id"]').each(function(){
				var $this = $(this);
				if ($this.val() == data.taste_id) {
					$this.prop('checked', true)
				}
			});
			taste_id = data.taste_id;

			var preference = [];
			if (data.preference) {
				preference = $.parseJSON(data.preference);
			}
			$('.kodawari-list input[type="radio"]').each(function(){
				var $this = $(this);
				if ($.inArray($this.val(), preference) >= 0) {
					$this.prop('checked', true);
				}
			});
			$('.kodawari-list input[type="checkbox"]').each(function(){
				var $this = $(this);
				if ($.inArray($this.val(), preference) >= 0) {
					$this.prop('checked', true);
				}
			});
			$('.parking-type-select input[type="radio"]').each(function(){
				var $this = $(this);
				if ($.inArray($this.val(), preference) >= 0) {
					$this.prop('checked', true);
					$('#parking-ex-label').text($this.data('display-val'));
				}
			});
			var params = {
				plan_type: plan_type,
				house_id : house_id,
				num: 3
			};
			if (plan_type === 0) {
				params.repaid_amount = data.repaid_amount;
				params.own_fund = data.own_fund;
			} else {
				params.offer_price = data.offer_price;
			}
			params.interest_rate = data.interest_rate;
			params.repayment_years = data.repayment_years;
			params.preference = data.preference;
			$('.stage1').data('params', params);
			selected_floor_plans = data.floor_plans;
			floor_search_results = data.floor_search_results;

			var rough_plan = data;
			show_madori({floor_plans: $.parseJSON(data.floor_search_results)}, function(){
				if (plan_type === 0) {
					showStep5(rough_plan);
					if (viewMode !== 'edit') {
						showConfirm(rough_plan);
						$('.stage3 input[type="checkbox"]').prop('checked', true);
					}
				} else {
					showConfirm(rough_plan);
					if (viewMode !== 'edit') {
						$('.stage2 input[type="checkbox"]').prop('checked', true);
					}
				}
				init_mng[0] = true;
				if (init_mng[0] && init_mng[1]) {
					OreUtil.$apiModal.hide();
					ore_plan_init2();
				}
			});
			dfd.resolve();
		}, 'get', {chained: true});
		return dfd.promise();
	}

	function getRestNum() {
		if (plan_type === 0) {
			if (OreUtil.contractName === 'パートナー') {
				$('.planning-nokori-kaisu').html('印刷可能回数は<span class="large">無制限</span>です。').data('count', 99);
			} else {
				OreUtil.ajax('frame/print', {}, function (data) {
					$('.planning-nokori-kaisu .large').text(data.rest_count).data('count', data.rest_count);
				}, 'get', {async: true});
			}
		} else {
			OreUtil.ajax('frame/publish', {}, function(data){
				$('.header-info-item.waku').html('掲載残り：' + data.rest_count + '枠<small>（' + data.total_count + '枠中）</small>');
				$('.confirm-msg.waku .large').text(data.rest_count);
				$('.confirm-msg.price .large').text(OreUtil.formatNum(data.unit_price));
				if (data.rest_count <= 0) {
					$('.confirm-msg.waku').hide();
					$('.confirm-msg.price').show();
				} else {
					$('.confirm-msg.waku').show();
					$('.confirm-msg.price').hide();
				}
			}, 'get', {async:true});
		}
	}
	function ore_plan_init2() {
		//確認の物件詳細リンク
		$('.bukken-list-item .btn-area a').attr('href', '/ore/master/property_view.html?blank=true&house_id=' + house_id);
		if (plan_type === 0) {
			$('.btn-send-fax-later').attr('href', '/ore/planning/plan_list.html?client_id=' + client_id);
			if (viewMode === '') {
				$('input[name="own_fund"]').val(client.own_fund);
				$('input[name="interest_rate"]').val(client.interest_rate);
				$('input[name="repayment_years"]').val(client.repaiment_year);
			}
		}


		//テイストに追加されたラジオボタン用
		setupCheckbox();

		// アラート
		alert = new ore_alert('.ore-alert');

		// 間取り
		madori_obj = new madori();

		// スライドメニュー
		$('.sidemenu-btn').slidemenu('#sidemenu');

		//残り回数
		getRestNum();

		$('.stage1 .madori-list .row').css({'min-height': '336px'});
		var headerHeight = $('.header').outerHeight() - 16;
		$('#header-dummy').height(headerHeight);
		$(window).resize(function() {
			var height = $(window).height();
			var offset = $('.container').offset();
			$('#sidemenu-container').css({height:height + 'px',left:offset.left - 281 + 1224 + 'px'});
			containerHeight = height;
			if (plan_type === 0) {
				// canvas初期化;
				dragRoom.DRAG.windowResize();
			}
		}).trigger('resize');

		//step1
		// キッチン選択
		var current = 0;
		$('input[name="kitchen-type"]').on('click', function () {
			OreLog.debug($(this).val());
			OreLog.debug('current', current);
			if (current === $(this).val()) {
				$(this).prop('checked', false);
				current = 0;
			} else {
				current = $(this).val();
			}
		});
		// 駐車場
		$('#parking-ex').on('change', function (e) {
			if ($(this).prop('checked')) {
				$('#modal-parkign').modal('show');
			} else {
				$('.parking-type-select input[type="radio"]').prop('checked', false);
				$('#parking-ex-label').text('駐車場を増設したい。');
			}
			e.stopPropagation();
		});
		// 駐車場選択
		$('input[name=parking-type]').on('click', function () {
			$('#parking-ex-label').text($(this).data('display-val'));
			$('#modal-parkign').modal('hide');
		});


		// GO
		$('#btn-go, #btn-madori').on('click', function () {
			if (readOnly) {
				return false;
			}
			checkStage1();
		});
		//step4
		// 横スクロール
		$('#select-plan').boxscroll_side({btn_prev: '#select-plan-prev', btn_next: '#select-plan-next'});

		// modal 画像プレビュー
		$('body').on('click', '.image-preview', function () {
			if ($(this).siblings('label').length) {
				OreLog.debug('labelあり');
				$('#modal-image-view .image img').attr('src', $(this).siblings('label').find('img').attr('src'));
			} else {
				OreLog.debug('labelなし');
				$('#modal-image-view .image img').attr('src', $(this).siblings('img').attr('src'));
			}
			
			$('#modal-image-view').modal('show');
			return false;
		});
		// modal 画像プレビュー
		$('body').on('click', '.image-preview-with-compass', function () {
			$('#modal-image-view-with-compass .image img').attr('src', $(this).siblings('img').attr('src'));
			
			OreLog.debug($(this).next());
			if ($(this).next().length > 0) {
				$('#modal-image-view-with-compass .compass02').remove();
				$('#modal-image-view-with-compass .modal-content').append($(this).next().clone());
			}
			$('#modal-image-view-with-compass').modal('show');
			return false;
		});

		//最初からやり直す
		$('.main').on('click', '.scroll-top', function(){
			changeStage(1,0,function(){});
		});
		//気に入った間取りがない
		$('.main').on('click', '.request-madori', function(){
			$('#modal-confirm-madori-request').modal('show');
		});
		$('#modal-confirm-madori-request .btn-check-now').click(function () {
			// TODO:ここに依頼処理を実装

			var params = {
				house_id : house_id,
			};
			params.taste_id = $('input[name="taste_id"]:checked').val();
			if (plan_type === 0) {
				params.repaid_amount = $('input[name="repaid_amount"]').val();
				params.own_fund = $('input[name="own_fund"]').val();
			} else {
				params.offer_price = String($('input[name="offer_price"]').val() * 10000);
			}
			params.interest_rate = $('input[name="interest_rate"]').val();
			params.repayment_years = $('input[name="repayment_years"]').val();
			var preference = [];
			$('.kodawari-list input[type="checkbox"].show:checked').each(function(){
				preference.push($(this).val());
			});
			$('.kodawari-list input[type="radio"].show:checked').each(function(){
				preference.push($(this).val());
			});
			if ($('#parking-ex').is(':checked')) {
				var $opt = $('.parking-type-select input[type="radio"].show:checked');
				if (typeof $opt.get(0) !== 'undefined') {
					preference.push($opt.val());
				}
			}
			params.preference = $.stringify(preference);
			OreLog.debug('request/floor_plan params', params);
			var dfd = new $.Deferred();
			OreUtil.ajax('request/floor_plan', params, function(res) {
				OreLog.debug('res', res);
				dfd.resolve(res);
			},'POST');
			dfd.promise()
			.done(function(data) {
				//data.result = 2;
				if (data.result === 2) {
					// 依頼処理中におすすめ間取りが作成されたためリロード
					$('#btn-go, #btn-madori').click();
				} else {
					// 普通はこっち
					$('#modal-confirm-madori-complete').modal('show');
				}
			});
		});
		$('#modal-confirm-madori-complete-close').click(function () {
			if (plan_type === 0) {
				location.href = '/ore/planning/plan_list.html?client_id='+client_id;
			} else {
				location.href = '/ore/publish/plan_list.html';
			}
		});
		//間取りクリック
		$('.plan-container-inner.stage1').on('click', '.madori-select', function() {
			OreLog.info('間取りクリック');
			OreLog.debug('data', $(this).data('floor_plans'));
			if (readOnly) {
				return false;
			}
			madoriClicked($(this).data('floor_plans'));
		});

		if (plan_type === 0) {
			//step5
			// サブウィンドウ
			$('.option-check').on('click', '.close-sub-window', function () {
				$($(this).attr('href')).hide();
				return false;
			});
			$('.option-check').on('click', '.open-sub-window', function () {
				$($(this).attr('href')).show();
				return false;
			});

			// 確認画面表示
			$('#btn-roughplan-conf').on('click', function () {
				if (readOnly) {
					return false;
				}
				OreUtil.ajax('rough_plans/' + rough_plan_id, {}, function (data) {
					showConfirm(data);
					changeStage(2, 3, function () {
						OreUtil.$apiModal.hide();
					}, 'get', {chained: true});
				});
			});

			//確認画面
			$('.stage3 .box-item-body-utext a').click(function () {
				var stage = $(this).data('stage');
				changeStage(3, stage, function () {
				})

			});

			// ラフプラン確定
			$('#roughplan-kakutei').on('click', function () {
				if (readOnly) {
					return false;
				}
				checkConfirm();
			});

			// 印刷
			$('.stage4 .btn-print').click(function () {
				OreLog.info('印刷する');
				var count = (OreUtil.contractName === 'パートナー') ? 99 : $('.planning-nokori-kaisu .large').data('count');
				//var count = 0;
				OreLog.debug('rest_count', count);
				if (count > 0) {
					OreUtil.ajax('rough_plans_pdf', {rough_plan_id: rough_plan_id}, function(data) {
						var $a = $('<a/>');
						$a.attr('href', data.file_path).attr('target', '_blank');
						$a.get(0).click();
						OreUtil.ajax('frame/print', {}, function (data) {
							if (OreUtil.contractName !== 'パートナー') {
								$('.planning-nokori-kaisu .large').text(data.rest_count).data('count', data.rest_count);
							}
						});
					}, 'post', {sync:true});
				} else {
					// 印刷回数が足りないので購入する
					var ore_account = new OreAccount();
					ore_account.buySheet();
				}
			});
			//確認書類へ
			$('#btn-roughplan-kakutei').click(function () {
				changeStage(4, 5, function () {
				});
			});

			// ファイルフォーム
			$('.file-kakunin-shorui').on('change', function () {
				var file = $(this).prop('files')[0];
				if (file != null) {
					$($(this).data('label-id')).empty().text(file.name);
				}
			});
			$('.file-upload-btn a').click(function () {
				sendFile();
			});
			$('.btn-send-fax').click(function () {
				$('.stage6 .planning-wrap').hide();
				$('.stage6 .planning-wrap.complete-fax').show();
				changeStage(5, 6, function(){});
			});

			//最終画面
			$('.btn-planning-list').attr('href','/ore/planning/plan_list.html?client_id=' + client_id);
		} else {
			$('#bukken-keisai').click(function(){
				if (readOnly) {
					return false;
				}

				// 掲載処理を実行
				image.saveCaption();
				publish.validate(rough_plan_id)
				.then(function() {
					return image.save(house_id)
				})
				.then(function() {
					return publish.do(rough_plan_id);
				})
				.done(function() {
					setReadOnly();
					changeStage(2, 3, function() {
						OreUtil.$apiModal.hide();
					});
				});
			});
			$('.stage2 .box-item-body-utext a').click(function(){
				changeStage(2, 1, function () {});
			});

			// 画像周り初期化
			image.initFunc();
		}
		$('.plan-container-inner.stage0').show();

		function confirm_init() {
			madori_obj.init();
			if (plan_type === 0) {
				dragRoom.DRAG.windowResize();
			}
		}

		if (viewMode === 'confirm') {
			setReadOnly();
			if (plan_type === 0) {
				changeStage(0, 3, confirm_init);
			} else {
				changeStage(0, 2, confirm_init);
			}
		} else if (viewMode === 'print') {
			setReadOnly();
			changeStage(0, 4, confirm_init);
		} else if (viewMode === 'doc') {
			setReadOnly();
			changeStage(0, 5, confirm_init);
		} else if (viewMode === 'edit') {
			if (plan_type === 0) {
				changeStage(0, 2, confirm_init);
			} else {
				changeStage(0, 2, confirm_init);
			}
		}
	}
	function changeStage(from, to, callback) {
		var duration = 500;
		var $to = $('.plan-container-inner.stage' + to);


		if (from < to) {
			for(var i= to + 1; i < stageCount;i++) {
				$('.plan-container-inner.stage' + i).hide();
			}
			for(var i = from; i <=to; i++) {
				$('.plan-container-inner.stage' + i).show();
			}
			$to.show();
		} else {
		}
		var offset = $to.offset().top - 50;
		if (to === 0){
			offset = 0;
		}
		$('body').animate({scrollTop: offset}, duration, 'swing', function () {
			callback();
		});
	}
	function checkStage1() {
		var params = {
			plan_type: plan_type,
			house_id : house_id,
			num: 3
		};
		alert.hide();
		alert.clear_msg();
		var error = false;
		var $taste = $('input[name="taste_id"]:checked');
		var interest_rate, repayment_years;
		if (typeof $taste.get(0) === 'undefined') {
			alert.set_message('テイストが 選択されていません。');
			error = true;
		}
		taste_id = $taste.val();
		var valids = [];
		if (plan_type === 0) {
			valids.push({
				$elm:$('input[name="repaid_amount"]'),
				field:'repaid_amount',
				mods:[{type: 'trim'}, {type: 'hankaku_num'}],
				valids: [{type: 'required'}, {type: 'integer'}, {type: 'limit', max: 10000000000, min: 0}]
			});
			valids.push({
				$elm:$('input[name="own_fund"]'),
				field:'own_fund',
				mods:[{type: 'trim'}, {type: 'hankaku_num'}],
				valids: [{type: 'required'}, {type: 'integer'}, {type: 'limit', max: 10000000000, min: 0}]
			});

		} else {
			valids.push({
				$elm:$('input[name="offer_price"]'),
				field:'offer_price',
				mods:[{type: 'trim'}, {type: 'hankaku_num'}, {type: 'to_yen'}],
				valids: [{type: 'required'}, {type: 'integer'}, {type: 'limit', max: 10000000000, min: 0}]
			});
		}
		valids.push({
			$elm:$('input[name="interest_rate"]'),
			field:'interest_rate',
			mods:[{type: 'trim'}, {type: 'hankaku_num'}],
			valids: [{type: 'required'}, {type: 'number'}, {type: 'limit', max: 20, le: 0}]
		});
		valids.push({
			$elm:$('input[name="repayment_years"]'),
			field:'repayment_years',
			mods:[{type: 'trim'}, {type: 'hankaku_num'}],
			valids: [{type: 'required'}, {type: 'integer'}, {type: 'limit', max: 100, le: 0}]
		});
		var check = OreValidation.validate(valids);
		if (!check) {
			alert.set_message('入力項目に 不備があります。');
			error = true;
		} else {
			for (var i in check) {
				params[i] = check[i];
			}
		}
		if (error) {
			var $box;
			if (plan_type === 0) {
				$box = $('#btn-go');
			} else {
				$box = $('#btn-madori');
			}
			var $alert = $('.ore-alert')

			var offset = $box.position();
			var h0 = $box.height();
			var h1 = $alert.outerHeight();
			alert.set_position(offset.top + (h0 - h1) / 2 + 'px', '140px');
			alert.show();
			return;
		}
		var preference = [];
		$('.kodawari-list input[type="checkbox"].show:checked').each(function(){
			preference.push($(this).val());
		});
		$('.kodawari-list input[type="radio"].show:checked').each(function(){
			preference.push($(this).val());
		});
		if ($('#parking-ex').is(':checked')) {
			var $opt = $('.parking-type-select input[type="radio"].show:checked');
			if (typeof $opt.get(0) !== 'undefined') {
				preference.push($opt.val());
			}
		}

		params.preference = $.stringify(preference);
		OreUtil.ajax('floor_plans', params, function(data) {
			$('.stage1').data('params', params);
			show_madori(data);
			floor_search_results = $.stringify(data.floor_plans);
		},'post', {chained:true});

	}

	//間取り
	function show_madori(data, isInit) {
		OreLog.info('show_madori');
		OreLog.debug('data', data);
		var len = data.floor_plans.length;
		var plans = data.floor_plans;
		if (len === 0) {
			$('.stage1 .madori-list .row').empty();
			changeStage(0, 1, function() {
				setTimeout(function() {
					OreUtil.$apiModal.hide();
					alert.clear_msg();
					alert.set_message('間取りがありません。<br><small class="scroll-top">最初からやり直す...</small>');
					var $box = $('.plan-container-inner.stage1 .planning-item');
					var $alert = $('.ore-alert')
					var offset = $box.position();
					var h0 = $box.height();
					var w0 = $box.width();
					var h1 = $alert.outerHeight();
					var w1 = $alert.outerWidth();
					var h2 = $('.plan-container-inner.stage1 h1').height();
					alert.set_position(offset.top + (h0 - h1 - h2) / 2 + h2 + 100 + 'px', offset.left + (w0 - w1) / 2 + 10 + 'px');
					alert.show();
				}, 500);

			});
			return;
		}
		floor_cache = {};
		for (var i =0; i<len; i++ ) {
			for (var j in data.floor_plans[i]) {
				var plan = data.floor_plans[i][j];
				if (typeof plan.recommend_flg !== 'undefined') {
					continue;
				}
				if (typeof floor_cache['plan' + plan.floor_plan_id] !== 'undefined') {
					continue;
				}
				floor_cache['plan' + plan.floor_plan_id] = {id:plan.floor_plan_id, code:plan.floor_code};
			}
		}

		var dfds = [];	// Deferredオブジェクト保存用
		$.each(floor_cache, function(i, cache) {
			var d = new $.Deferred();
			OreUtil.ajax('floor_plans/' + cache.id, {}, function(res) {
				cache.data = res;
				d.resolve();
			}, 'get', {chained:true});
			dfds.push(d);
		});
		$.when.apply($, dfds)
		.done(function() {
			OreLog.debug('floor_cache', floor_cache);
			var $madoris = $('.stage1 .madori-list .row').empty();
			for (var i=0; i< len; i++) {
				var $div = $('<div/>').addClass('col-xs-4 madori-list-item');
				var madoris = plans[i];
				var madoriCount = madoris.length;
				var $inner = $('<div class="madori-list-item-innerbox">');
				$div.append($inner);
				for (var j=0; j<madoriCount; j++ ) {
					var name = 'roughplan' + i + '-' + j;
					var floor = madoris[j];
					if (typeof floor.recommend_flg !== 'undefined') {
						if (Number(floor.recommend_flg) === 1) {
							$inner.addClass('osusume');
						}
					} else {
						var madoriData = floor_cache['plan' + floor.floor_plan_id].data;
						if (madoriData.floor === 0) {
							continue;
						}
						var $madoriWrap = $('<div/>').addClass('madori-list-item-image').css({position:'relative'});
						$madoriWrap.append('<div class="madori-list-item-label-wrap"><span class="madori-list-item-label">' + madoriData.floor + 'F</span></div>')
						var $imgContainer = $('<div/>').css({'min-height': '328px', 'min-width': '310px'});
						$imgContainer.append($('<img/>').attr('src', madoriData.after.image).attr('width', 328));
						$madoriWrap.append($imgContainer);
						var $a = $('<a class="btn-enlarge madori-preview" href="javascript:void(0);"></a>');
						$a.data('madori', madoriData);
						$madoriWrap.append($a);
						if (madoriData.after.compass >= 0) {
							var deg = OreUtil.compassNumtoDeg(madoriData.after.compass);
							OreLog.debug('deg', deg);
							var $compass = $('<div class="compass02"><div class="compass-needle" style="transform: rotateZ('+deg+'deg);"></div></div>');
							$madoriWrap.append($compass);
						}
						$madoriWrap.append($('<div class="effect-list-select-l"></div>'));
						$inner.append($madoriWrap);
					}
				}
				var $btn = $('<button type="button" class="btn btn-orange2 madori-select">この間取りを選択する</button>');
				$btn.data('floor_plans',madoris);
				$div.append($('<div class="btn-area"></div>').append($btn));
				$madoris.append($div);
			}
			if (isInit) {
				$('.madori-list-item').css({opacity:1});
				$('.madori-list-item .effect-list-select-l').hide();
				isInit();
				return;
			}
			madori_obj.init();
			changeStage(0,1, function(){
				OreUtil.$apiModal.hide();
				madori_obj.show_list(0);
			});			
		});

		if (typeof data.request_status !== 'undefined') {
			if (data.request_status !== 0) {
				$('.request-madori').hide();
			} else {
				$('.request-madori').show();
			}
		}
	}
	// modal 間取りプレビュー
	$('.stage1').on('click', '.madori-preview', function () {
		var $header = $('#modal-madori-view .modal-header .row').empty();
		var $container = $('#modal-madori-view .modal-body .row').empty();
		var madoriData = $(this).data('madori');
		var floor = madoriData.floor;

		var $div = $('<div>').addClass('col-xs-6');
		$div.append($('<div/>').addClass('modal-madori-title').text(floor + 'F ビフォー'));
		$header.append($div);
		$div = $('<div>').addClass('col-xs-6');
		$div.append($('<div/>').addClass('modal-madori-img').append($('<img/>').attr('src', madoriData.before.image).attr('width', 505)));
		if (madoriData.before.compass >= 0) {
			var deg = OreUtil.compassNumtoDeg(madoriData.before.compass);
			$div.append($('<div class="compass02"><div class="compass-needle" style="transform: rotateZ('+deg+'deg);"></div></div>'));
		}
		$container.append($div);

		$div = $('<div>').addClass('col-xs-6');
		$div.append($('<div/>').addClass('modal-madori-title').text(floor + 'F アフター'));
		$header.append($div);
		$div = $('<div>').addClass('col-xs-6');
		$div.append($('<div/>').addClass('modal-madori-img').append($('<img/>').attr('src', madoriData.after.image).attr('width', 505)));
		if (madoriData.after.compass >= 0) {
			var deg = OreUtil.compassNumtoDeg(madoriData.after.compass);
			$div.append($('<div class="compass02"><div class="compass-needle" style="transform: rotateZ('+deg+'deg);"></div></div>'));
		}
		$container.append($div);
		$('#modal-madori-view').modal('show');
		return false;
	});
	function madoriClicked(plans) {
		OreLog.info('madoriClicked');
		OreLog.debug('plans', plans);
		var data = $('.stage1').data('params');
		selected_floor_plans = [];
		$.each(plans, function(k, v) {
			if (typeof v.recommend_flg === 'undefined') {
				selected_floor_plans.push(v);
			}
		})
		var params = {
			plan_type: plan_type,
			house_id : house_id,
			taste_id: taste_id,
			preference: data.preference,
			floor_plans: $.stringify(selected_floor_plans),
			interest_rate: data.interest_rate,
			repayment_years: data.repayment_years
		};
		if (plan_type === 0) {
			params.client_id = client_id;
			params.repaid_amount = data.repaid_amount;
			params.own_fund = data.own_fund;
		} else {
			params.offer_price = data.offer_price;

		}
		params.floor_search_results = floor_search_results;
		OreLog.info('post rough_plans');
		OreLog.debug('params', params);
		OreUtil.ajax('rough_plans', params, function(data) {
			rough_plan_id = data.rough_plan_id;
			if (plan_type !== 0) {
				OreUtil.ajax('rough_plans/' + rough_plan_id, {}, function(data) {
					showConfirm(data);
					changeStage(1,2,function() {
						showStep5();
					});
				}, 'get', {chained:true});
				return;
			}
			changeStage(1,2,function() {
				showStep5();
			});
		}, 'post', {chained:true});
	}
	//Step5
	function showStep5() {
		OreLog.info('showStep5');
		if (madori_obj !== null) {
			madori_obj.clicked = false;
		}
		if (plan_type !== 0) {
			return;
		}

		roomClick = roomClickInstance;
		updateModalOptionDetails = updateModalOptionDetailsInstance;
		var $ul = $('.plan-view-tab ul').empty();
		var len = selected_floor_plans.length;
		var plans;
		var plansRoom = [];
		var plansOther = [];
		var nums;
		var numsRoom = [];
		var numsOther = [];
		getPlanMoney();
		for (var i=0; i<len; i++) {
			var plan = selected_floor_plans[i];
			if (plan.floor_code == 0) {
				plansOther.push(plan);
				numsOther.push(i);
			} else {
				plansRoom.push(plan);
				numsRoom.push(i);
			}
		}
		plans = plansRoom.concat(plansOther);
		nums = numsRoom.concat(numsOther);
		len = plans.length;
		for (var i=0; i<len; i++) {
			var plan = plans[i];
			var $li = $('<li>').attr('role', 'presentation').addClass('number' + nums[i]);
			if (i === 0) {
				$li.addClass('active');
			}
			var $a = $('<a/>').attr('href', '#tab001').attr('aria-controls', 'tab001').attr('role', 'tab').attr('data-toggle', 'tab');
			if (plan.floor_code == 0) {
				$a.text('外装・その他');
			} else {
				$a.text(floor_cache['plan' + plan.floor_plan_id].data.floor +  'F');
			}
			$a.data('id', plan.floor_plan_id).data('number', nums[i]);
			$li.append($a);
			$ul.append($li);
		}
		$('.stage2').data('plans', selected_floor_plans);
		$('.stage2 .plan-view-tab').on('click', 'a', function() {
			var $this = $(this);
			changeMadori($this.data('id'), $this.data('number'));
		});
		changeMadori(plans[0].floor_plan_id, nums[0]);

		/* クリックイベント初期化
		 */
		function initClickEvent() {
			setupCheckbox();
			// オプション変更イベント
			$('#option-select-checklist input[type=checkbox]').unbind('change');
			$('#option-select-checklist input[type=checkbox]').on('change', function () {
				var checked = 0;
				if ($(this).prop('checked')) checked = 1;
				checkOption($(this).next('label').attr("id").slice(24),checked);
			});

			// 詳細オプション変更イベント
			$('#sub-option01 input').unbind('change');
			$('#sub-option01 input').on('change', function () {
				var checked = 0;
				if ($(this).prop('checked')) checked = 1;
				var value = $(this).next('label').attr("id").slice(31).split('_');
				checkOptionDetail(parseInt(value[0]), parseInt(value[1]),checked);
			});
		}

		/* 間取り表示変更
		 * {int} floorNo 選択した階数
		 * return なし
		 */
		function changeMadori(floor_plan_id, floorNo) {
			OreLog.info('changeMadori');
			var code = selected_floor_plans[floorNo].floor_code;
			if (code == 0) {
				// 外壁
				dragRoom.DRAG.setFocusIndex(null);
				dragRoom.DRAG.draw();

				roomClick(floorNo, 0);

				return;
			}
			OreLog.debug('floor_cache', floor_cache);
			OreLog.debug('floor_plan_id', floor_plan_id);
			var data = floor_cache['plan' + floor_plan_id].data;
			var jstr = data.after.json;
			dragRoom.DRAG.floorNo = floorNo;

			if (jstr.length > 0) {
				clickRoom.loadData(jstr);
				dragRoom.DRAG.fitCanvas();
				dragRoom.DRAG.floorNo = floorNo;
                dragRoom.DRAG.setFocusLDK();
				roomClick(floorNo, dragRoom.DRAG.focusIndex);
				dragRoom.DRAG.draw();
			}
		}

		/* 部屋クリック時のイベント
		 * {int} floorNo 階数
		 * {int} itemIdx 部屋のID
		 * return なし
		 */
		function roomClickInstance(floorNo, itemIdx) {
			// 部屋名を表示
			if (floorNo != 0) {
				var name = '';
				if (dragRoom.DRAG.itemAr[itemIdx] != null)
					name = dragRoom.DRAG.itemAr[itemIdx].name;
				$('#option-select-checklist .option-select-title').html(name);

				// タブ切り替え
				$('.nav-tabs .active').removeClass('active');
				$('.nav-tabs > li.number' + floorNo).addClass('active');
			}
			else {
				$('#option-select-checklist .option-select-title').html('外装・その他');
			}

			// オプション詳細非表示
			$('#sub-option01').hide();

			updateOption(floorNo, itemIdx);
		}

		function updateOption(floorNo, itemIdx) {
			var floor_plan_id = floorPlanId(floorNo);

			// 階数と部屋のIDをPOSTしてパース情報取得
			var persListHtml = '';
			OreUtil.ajax('floor_plans/'+floor_plan_id+'/pers/'+itemIdx+'/'+taste_id, {}, function(data) {
				for (var i = 0; i < data.length; i++) {
					if (data[i].image == 'undefined') continue;

					persListHtml += '<li class="clist-main-list-item list-image">'
						+ '<img src="'+data[i].image+'">'
						+ '<a class="btn-enlarge image-preview" href="javascript:void(0);"></a>';
					+ '</li>';
				}

				$('ul#select-plan').html(persListHtml);
			},'get', {opacity:0});


			// 選択されているオプション一覧を取得
			OreUtil.ajax('rough_plans/'+rough_plan_id+'/floor/'+floorNo+'/room_index/'+itemIdx+'/options', {}, function(selectOptionList) {

				// 階数と部屋のIDをPOSTしてオプション情報取得
				OreUtil.ajax('floor_plans/'+floor_plan_id+'/room_index/'+itemIdx+'/options', {}, function(data) {

					var optionsHtml = '';
                    $('#option-select-checklist ul').html(optionsHtml);// 初期化

					// オプション情報から一覧を出力
					for (var i = 0; i < data.length; i++) {
						var optionId = data[i].option_id;
						OreUtil.ajax('floor_plans/'+floor_plan_id+'/room_index/'+itemIdx+'/options/'+optionId, {}, function(data2) {
							// 各オプションの詳細な内容取得
							var optionHtml = '';
							optionHtml += '<li>';
							optionHtml += '<input class="deco-cb deco-cb-orange" type="checkbox"';
							if (selectOptionList.indexOf(data2[0].option_id) >= 0) {
								optionHtml += ' checked';
							}
							optionHtml += '><label id="input-room-option-select'+data2[0].option_id+'">';
							optionHtml += data2[0].option_name;
							optionHtml += '</label>';
							if (data2.length > 1) {
								optionHtml += '<a class="open-sub-window" href="#sub-option01"';
								optionHtml += ' onClick="updateModalOptionDetails('+floorNo+',' + data2[0].option_id + ');"';
								optionHtml += '>（オプション）</a>';
							}
							optionHtml += '</li>';

							optionsHtml += optionHtml;
							$('#option-select-checklist ul').html(optionsHtml);
							initClickEvent();
						});

						// 画像一覧にオプション画像も追加
						if (data[i].option_image == 'undefined') continue;

						persListHtml += '<li class="clist-main-list-item list-image">'
							+ '<img src="'+data[i].option_image+'">'
							+ '<a class="btn-enlarge image-preview" href="javascript:void(0);"></a>';
						+ '</li>';
					}
					$('ul#select-plan').html(persListHtml);
				}, 'get', {opacity:0});
			}, 'get', {opacity:0});
		}

		/* オプションのチェック変更処理
		 * {int} optionId チェックしたオプションID
		 * {int} checked チェック状態
		 * return なし
		 */
		function checkOption(optionId, checked) {

			var itemIdx = dragRoom.DRAG.focusIndex;
			var floorNo = dragRoom.DRAG.floorNo;
			if (itemIdx == null) {
				itemIdx = 0;
				floorNo = 0;
			}
			// オプション詳細非表示
			$('#sub-option01').hide();

			OreUtil.ajax('rough_plans/'+rough_plan_id+'/floor/'+floorNo+'/room_index/'+itemIdx+'/options', {
				'id':optionId,
				'checked':checked,
				'type': 'option'
			}, function(result) {
				getPlanMoney();
			},'POST',{opacity:0});
		}
		function getPlanMoney() {
			OreUtil.ajax('rough_plans/'+rough_plan_id+'/monthly_amount', {},function(money) {
				var html = '<span class="info-wind-price">' + OreUtil.formatNum(money.amount) +  '</span>円';
				html += '<span class="text-gray">（' + OreUtil.formatNum(money.diff) + '円）</span>';
				$('.stage2 .price-area').html(html);
			}, 'get', {async:true});
		}

		/* オプション工事詳細のチェック変更処理
		 * {int} optionId チェックしたオプションID
		 * {int} options_construction_detail_id チェックしたオプション工事詳細ID
		 * {int} checked チェック状態
		 * return なし
		 */
		function checkOptionDetail(optionId, options_construction_detail_id, checked) {
			var itemIdx = dragRoom.DRAG.focusIndex;
			var floorNo = dragRoom.DRAG.floorNo;
			if (itemIdx == null) {
				itemIdx = 0;
				floorNo = 0;
			}
			OreUtil.ajax('rough_plans/'+rough_plan_id+'/floor/'+floorNo+'/room_index/'+itemIdx+'/options/', {
				'id':options_construction_detail_id,
				'checked':checked,
				'type': 'detail'
			}, function(result) {
				// 全て選択解除した場合
				var selectList=[];
				$('#sub-option01 input:checked').each(function(){
					selectList.push($(this).val());
				});
				if (selectList.length == 0) {
					$('#input-room-option-select'+optionId).siblings('input').prop('checked',false);
				}
				// オプションが未選択で何かを選択した場合
				if (!$('#input-room-option-select'+optionId).siblings('input').prop('checked') && checked) {
					$('#input-room-option-select'+optionId).siblings('input').prop('checked',true);
					var floorNo = dragRoom.DRAG.floorNo;
					if (dragRoom.DRAG.focusIndex == null) floorNo = 0;
					updateModalOptionDetails(floorNo, optionId);
				}

				OreUtil.ajax('rough_plans/'+rough_plan_id+'/monthly_amount', {},function(money) {
					$('.info-wind-price').html(money.amount.toLocaleString());
					$('.info-wind-price').siblings('.text-gray').html('（' + money.diff.toLocaleString() + '円）');
				});
			},'POST', {opacity:0});
		}


		/* オプションの工事詳細のモーダルを更新
		 * floorNo 階数
		 * optionId 表示するオプションID
		 * return なし
		 */
		function updateModalOptionDetailsInstance(floorNo, optionId) {
			var floor_plan_id = floorPlanId(floorNo);

			var itemIdx = dragRoom.DRAG.focusIndex;
			if (itemIdx == null) itemIdx = 0;
			OreUtil.ajax('rough_plans/'+rough_plan_id+'/floor/'+floorNo+'/room_index/'+itemIdx+'/options/'+optionId, {}, function(selectOptionList) {
				OreUtil.ajax('floor_plans/'+floor_plan_id+'/room_index/'+itemIdx+'/options/'+optionId, {}, function(data) {
					var optionListStr = "";
					var startFlg = false;
					var before_selection_condition = -1;
					var radioNameNo = 0;
					for (var i = 0; i < data.length; i++) {
						if (before_selection_condition != data[i].selection_condition ||
							data[i].selection_condition == 3) {
							if (!((before_selection_condition == 1 && data[i].selection_condition == 2) ||
								(before_selection_condition == 2 && data[i].selection_condition == 1))) {
								startFlg = $('#input-room-option-select'+optionId).siblings('input').prop('checked');
								if (before_selection_condition != -1) optionListStr += '</li>';
								optionListStr += '<li class="option-select-group">';
								if (data[i].selection_condition != 4 &&
									data[i].selection_condition != 9) radioNameNo++;
							}
						}
						before_selection_condition = data[i].selection_condition;

						if (data[i].selection_condition == 4 || data[i].selection_condition == 9) {// チェックボックス
							optionListStr += '<input class="deco-cb deco-cb-orange" type="checkbox"';
							if (selectOptionList.indexOf(data[i].options_construction_detail_id) >= 0 ) {
								optionListStr += ' checked';
							}
							optionListStr += '><label id="input-room-detail-option-select'+optionId+'_'+data[i].options_construction_detail_id+'">';

						}
						else {// ラジオボタン
							optionListStr += '<input class="deco-cb deco-rb-orange" type="radio" name="rb-option-select-00'+radioNameNo+'"';
							if (startFlg || selectOptionList.indexOf(data[i].options_construction_detail_id) >= 0) {
								optionListStr += ' checked';
							}
							optionListStr += '><label id="input-room-detail-option-select'+optionId+'_'+data[i].options_construction_detail_id+'">';

						}
						optionListStr += data[i].construction_details_name;
						optionListStr += '</label><br/>';
						startFlg = false;
					}
					optionListStr += '</li>';

					$('#sub-option01 ul').html(optionListStr);

					initClickEvent();
				}, 'get', {opacity:0});
			}, 'get', {opacity:0});
		}

		//現在のfloor_plan_idを取得
		function floorPlanId(floorNo) {
			var plans = $('.stage2').data('plans');
			return  plans[floorNo].floor_plan_id;
		}

	}
	function showConfirm(data) {
		var selector;
		if (plan_type === 0) {
			selector = '.stage3 ';
			var $options = $('.planning-confirm-option-list .row').empty();
			var len = data.options.length;
			for (var i = 0;i < len; i++) {
				$options.append('<div class="col-xs-4"><div class="planning-confirm-option-list-item">' + data.options[i].name + '</div></div>');
			}
			$('.stage3 input[type="checkbox"]').prop('checked', false);
			$('.stage3 .list-image img').attr('src', data.pers).attr('width', 172);
		} else {
			OreUtil.$apiModal.hide();
			selector = '.stage2 ';
			$('.stage2 input[name="title"]').val(data.title);
			$('.stage2 textarea[name="description"]').val(data.description);
			$('.stage2 input[name="renovation_title"]').val(data.renovation_title);
			$('.stage2 textarea[name="renovation_description"]').val(data.renovation_description);
			var $select = $('.stage2 select[name="trade_aspect"]');
			$('option:selected', $select).removeAttr('select');
			var exist = false;
			$('option', $select).each(function(){
				if ($(this).val() === data.trade_aspect) {
					exist = true;
					return false;
				}
			});
			if (!exist) {
				$select.append($('<option/>').val(data.trade_aspect).text(data.trade_aspect));
			}
			$select.val(data.trade_aspect);
			$('.stage2 input[type="checkbox"]').prop('checked', false);
			$('.stage2 .list-image img').attr('src', data.pers).attr('width', 172);

			// データをロード
			publish.setFloorCache(floor_cache);
			publish.setData(data);
			publish.show();

			//image.load(rough_plan_id);
		}
		$(selector + '.api-val').each(function () {
			var $this = $(this);
			var field = $this.data('field');
			if(typeof data[field] === 'undefined') {
				console.log('API:' + field + 'がありません。');// debug
				return;
			}
			if (field === 'construction_cost') {
				$this.text(OreUtil.formatNum(data[field] + data.tax) + '円');
			} else if ($(this).hasClass('price')) {
				$this.text(OreUtil.formatNum(data[field]) + '円');
			} else if ($(this).hasClass('format-num')) {
				$this.text(OreUtil.formatNum(data[field]));
			} else {
				$this.text(data[field]);
			}
		});
		var len = data.floor_plans.length;
		var $images = $('.regist-list-images').empty();
		for(var i = 0; i < len; i++) {

			var madoriData = floor_cache['plan' + data.floor_plans[i].floor_plan_id].data;
			if (madoriData.floor === 0) {
				continue;
			}

			var $div = $('<div/>').addClass('col-xs-6');
			$div.append('<span class="image-title">'+ madoriData.floor + 'F</span>');
			var html = '<div class="list-image">';
			html += '<img src="' + madoriData.after.image +'" alt="" width="333">';
			html += '<a class="btn-enlarge image-preview-with-compass" href="javascript:void(0);"></a>';
			if (madoriData.after.compass >= 0) {
				var deg = OreUtil.compassNumtoDeg(madoriData.after.compass);
				html += '<div class="compass02"><div class="compass-needle" style="transform: rotateZ('+deg+'deg);"></div></div>';
			}
			html += '</div>';
			$div.append(html);
			$images.append($div);
		}
	}
	function checkConfirm() {
		alert.hide();
		alert.clear_msg();
		var error = false;
		if (!$('.stage3 input[name="privacy_agree"]').prop('checked')) {
			alert.set_message('個人情報保護方針が 同意されていません。');
			error = true;
		}
		if (!$('.stage3 input[name="price_agree"]').prop('checked')) {
			alert.set_message('金額変動の可能性について 同意されていません。');
			error = true;
		}
		if (!$('.stage3 input[name="construction_agree"]').prop('checked')) {
			alert.set_message('工事が実施できない場合があることに 同意されていません。');
			error = true;
		}
		if (error) {
			var $box = $('#roughplan-kakutei');
			var $alert = $('.ore-alert')

			var offset = $box.position();
			var h0 = $box.height();
			var h1 = $alert.outerHeight();
			alert.set_position(offset.top + (h0 - h1) / 2 + 'px', '150px');
			alert.show();
			return;
		}
		OreUtil.ajax('confirm_rough_plans', {rough_plan_id: rough_plan_id}, function(data){
			changeStage(3, 4, function(){
				setReadOnly();
				OreUtil.$apiModal.hide();
			});
		}, 'post', {chained:true});
	}
	function sendFile() {
		var error = false;
		alert.hide();
		alert.clear_msg();
		var noFiles = [];
		var form = new FormData();
		$('.file-kakunin-shorui').each(function(){
			var $this = $(this);
			var file = $this.prop('files')[0];

			if (!file) {
				noFiles.push($this.data('label'));
			} else {
				form.append('document_file' + $this.data('num'), file);
			}
		});
		if (noFiles.length > 0) {
			alert.set_message('ファイルが選択されていません。');
			for (var i in noFiles) {
				alert.set_message('・' + noFiles[i]);
			}
			var $box = $('.plan-container-inner .btn-send-fax');
			var $alert = $('.ore-alert')

			var offset = $box.position();
			var h0 = $box.height();
			var h1 = $alert.outerHeight();
			alert.set_position(offset.top + (h0 - h1) / 2 + 'px', 0);
			alert.show();
			return;
		}
		form.append('rough_plan_id', rough_plan_id);
		form.append('type', 1);
		OreUtil.ajax('confirm_docs', form, function(data) {
			$('.stage6 .planning-wrap').hide();
			$('.stage6 .planning-wrap.complete-normal').show();
			changeStage(5,6,function(){
				OreUtil.$apiModal.hide();
			});
		}, 'post', {chained: true});


	}
	function setupCheckbox() {
		if (readOnly) {
			$('input[type="checkbox"],input[type="radio"]').attr('disabled', 'disabled');
			return;
		}
		// チェックボックスにidを振る
		var cb_count = 0;
		var rb_count = 0;
		$('input[type=checkbox]').each(function () {
			var id = 'cb_' + cb_count;
			cb_count++;
			if (!$(this).attr('id'))
				$(this).attr('id', id).next('label').attr('for', id);
		});
		$('input[type=radio]').each(function () {
			var id = 'rd_' + rb_count;
			rb_count++;
			$(this).attr('id', id).next('label').attr('for', id);
		});
	}
	function setReadOnly() {
		readOnly = true;
		$('input[type="checkbox"],input[type="radio"],select').attr('disabled', 'disabled');
		$('input[type="text"],textarea').attr('readonly', 'readonly');
		madori_obj.clicked = true;
		if (plan_type === 0) {
			$('.stage3 .box-item-body-utext a').hide();
		} else {
			$('.stage2 .box-item-body-utext a').hide();
		}
		$('.plan-container-inner.stage1 .request-madori').hide();
		
		if (typeof image !== 'undefined') {
			image.readonlyFunc();
		}
	}
}
var roomClick = function() {};
var updateModalOptionDetails = function() {};
