var OreSearch = function ($target, pathname, search)
{
	this.$target = $target;
	this.pathname = pathname;

	// TODO:デバッグ用にクエリ引数からパラメータを復元できるようにする
	this.params = typeof OreStrage === 'undefined' ? OreUtil.getParams : OreStrage.getParams;
	this.query = typeof search === 'undefined' ? '' : search;
}
OreSearch.prototype = {
	cleanQuery: function(query) {
		var arr = [];
		$.each(query.split('&'), function(i, param) {
			if (param.split('=')[1]) { arr.push(param); }
		});
		return arr.join('&');
	},
	search: function(not_moved) {
		var params = {};
		OreLog.debug(this.$target.serializeArray());
		$(this.$target.serializeArray()).each(function(j, v) {
			if (v.value === '') {
				return;
			}
			if (typeof(v.value) === 'undefined') {
				OreLog.warn(v.value+'is undefined');
				return;
			}
			if (typeof(params[v.name]) === 'undefined') {
				params[v.name] = v.value;
			} else {
				params[v.name] += ',' + v.value;
			}
		});
		OreLog.debug(params);

		if(not_moved) {
            // パラメータを渡して遷移する
            OreStrage.save(params);
            return params;
		}else{
            // パラメータを渡して遷移する
            OreStrage.move(this.pathname, params, this.query);
		}

	},
	format : function(src) {
		var params = [];
		for (var i in src) {
			var param = {};
			param.value = src[i];
			var name = decodeURIComponent(i);
			name = name.split(',');
			if (name.length <= 1) {
				continue;
			}
			param.ope = name[0];
			param.field = name[1];

			if (String(param.value).match(/,/) && param.field === 'madori_num') {
				OreLog.warn('opeを上書きする特殊なvalueが渡された'+param.field);
				var tmp = param.value.split(',');
				OreLog.debug(tmp[1]);
				param.ope = tmp[0];
				param.value = tmp[1];
			}
			OreLog.debug(param);

			if (typeof(param.field) === 'undefined' || typeof(param.ope) === 'undefined' || typeof(param.value) === 'undefined' ) {
				OreLog.warn(param);
				continue;
			}
			
			params.push(param);
		}
		OreLog.debug(params);
		return params;
	},
	formatQuery: function() {
		return this.format(this.params);
	},
	formatName: function() {
		var params = {};
		$(this.$target.serializeArray()).each(function(j, v) {
			params[v.name] = v.value;
		});
		OreLog.debug(params);
		return this.format(params);
	},
	revertInput: function () {
		for (var param in this.params) {
			var name = decodeURIComponent(param);
			var value = decodeURIComponent(String(this.params[param]).replace(/\+/g, '%20'));
			if (name.match(/^in,/) || name === 'preference') {
				var tmp = value.split(',');
				OreLog.debug(tmp);
				for (var t in tmp) {
					OreLog.debug($('[name="'+name+'"][value="'+tmp[t]+'"]'));
					$('[name="'+name+'"][value="'+tmp[t]+'"]').prop('checked', true);
				}
			} else {
				OreLog.debug(name+','+value);
				$('[name="'+name+'"]').val(value);
				$('[name="'+name+'"]').prop('checked', true);
			}
		}	
	}
}
