var OreTable = function (api, $target) {
	this.page = 1;		// ページ
	this.size = 10;		// 一ページの行数
	this.where = '';	// 検索条件
	this.is_owned = 0;
	this.api = api;			// API
	this.orderby = '[-id]';	// ソートキー
	this.sort_field = 'id';	// ソートするフィールド	
	this.sort_asc = false;	// ソート順
	this.sort_cols = [];	// ソートする列
	this.$target = $target;	// 出力先
	this.template = [];

	// 前回表示した内容をロード
	this.setStatus(OreStrage.getParams);
}
OreTable.prototype = {
	/*
	 * テーブルを生成する
	 * 
	 * done: 検索結果が得られた
	 * fail: 全件検索(whereが空)したにもかかわらず一件もヒットしなかった場合
	 */
	build : function () {
		// テーブル作成後なにかしたいかもしれない
		var dfd = $.Deferred();
		var ajax_param = {
			type: 'page',
			page: this.page,
			size: this.size,
			orderby: this.orderby,
			is_owned: this.is_owned
		};

		// TODO:whereには生のデータが入っているので忘れずエスケープする
		var where = OreUtil.makeWhere(this.where);
		if (where != '[]') {
			OreLog.debug('where='+where);
			ajax_param.where = where;
		}

		// get_typeが渡されたときも忘れずに
		if (typeof(this.get_type) != 'undefined') {
			OreLog.debug('get_type='+this.get_type);
			ajax_param.get_type = this.get_type;
		}

		// もっとスマートな方法ないかな？
		var self = this;
		OreUtil.ajax(this.api, ajax_param, function(data) {
			OreLog.info('検索が完了した');
			OreLog.debug(data);

			// 件数表示
			$('.search-result-header-title span.result_count').text(OreUtil.formatNum(data.totalElements));

			// ページャーを追加
			OreUtil.makePager($('.search-result-header-pager .pagination'),self.page, data.totalPages, 10);
			OreUtil.makePager($('.search-result-footer .pagination'),self.page, data.totalPages, 10);

			//ソート
			OreUtil.makeSortArrow(self.sort_cols, self.sort_field, self.sort_asc);

			// 表示件数
			$('.search-result-header-disp select').val(self.size);

			// 検索した結果を一覧表示
			self.$target.empty();
			for (var i in data.content)
			{
				var content = data.content[i];
				OreUtil.setClientName(content);
				OreLog.debug(content);
				var content_num = i;

				var str = '';
				// テンプレートオブジェクトを処理していく
				for (var obj of self.template)
				{
					if (typeof(obj) === "string") {
						// テキストはそのまま出力
						str += obj;
					} else {
						var tmp = [];
						var select = [];
						if (typeof obj.select !== 'undefined') {
							for (var sel of obj.select.split(','))
							{
								select.push(sel);
							}
						}
						if (typeof obj.keys !== 'undefined') {
							for (var key of obj.keys.split(','))
							{
								if (typeof(content[key]) === 'undefined') {
									OreLog.warn(key + ' undefined');
								}
								if (select.length === 0) {
									tmp.push(content[key]);
								} else {
									tmp.push(select[content[key]]);
								}
							}
							// セパレータとフォーマッタを使ってテキストを生成していく
							str += obj.formatter(tmp.join(obj.separator));
						}
						if (typeof obj.in !== 'undefined') {
							switch (obj.in) {
								case 'count':
									tmp = content_num;
									break;
								case 'page':
									tmp = self.page;
									break;
								default:
									OreLog.warn('未定義のコマンド:in');
									break;
							}
							str += tmp;
						}
					}
				}
				//OreLog.debug(str);
				self.$target.append(str);
			}

			// 物件一覧表示ごとに現在の設定をストレージに保存
			var params = {};
			$.extend(true, params, OreStrage.getParams);
			$.extend(true, params, self.getStatus());
			OreLog.debug('params', params);
			OreStrage.save(params);

			// 全件検索して一件も該当しなかった
			if (typeof ajax_param.where === 'undefined' && data.totalElements === 0)
			{
				OreLog.debug('reject', data);
				// 一件もヒットしなかった場合fail
				dfd.reject(data);
			} else {
				OreLog.debug('resolve', data);
				dfd.resolve(data);
			}
		});
		return dfd.promise();
	},
	changeOrder : function (selecter) {
		var field = selecter.data('field');
		this.sort_field = field;
		var asc = selecter.data('asc');
		this.sort_asc = asc;
		var fields = field.split(',');
		var orderby = '['
		// 最初のフィールドにはソート順のフラグを設定
		if (asc) {
			orderby += '"' + fields[0] +  '"';
		} else {
			orderby += '"-' + fields[0] +  '"';
		}
		// 残りのフィールドは普通に連結
		for (var i=1; i<fields.length; ++i) {
			orderby += ',"' + fields[i] +  '"';
		}
		orderby += ']';
		OreLog.debug('orderby = ' + orderby);
		this.orderby = orderby;


		OreLog.debug('field=' + field);
		OreLog.debug('asc=' + asc);
	},
	parse : function (template) {
		// 成形する
		var str = template.replace(/	/g, '');
		var splits = str.split(/\{|\}/g);
		OreLog.debug(splits);
		for (var i in splits) {
			if (!splits[i].match(/.+:".+"/)) {
				// ただのテキストはこっち
				this.template.push(splits[i]);
			} else {
				// 埋め込みオブジェクトはこっち
				var obj = (new Function('return ' + '{'+splits[i]+'}'))();
				// ソートカーソルの生成が宣言された
				if (typeof(obj.sort) !== 'undefined') {
					var sort_col = {
						$elm: $('th ' + obj.sort),
						field: obj.keys
					};
					this.sort_cols.push(sort_col);
				}
				// デフォルトのセパレータは''
				if (typeof(obj.separator) === 'undefined') {
					obj.separator = '';
				}
				// フォーマッタの作成
				// 通常はそのままテキストで返す
				obj.formatter = function (str) { return str };
				if (typeof(obj.type) !== 'undefined') {
					// TODO:各種出力タイプごとの実装は以下に行う
					if (obj.type === 'manyen') {
						// moneyフォーマット
						obj.formatter = function (str) { return OreUtil.formatNumMan(str);}
					} else if (obj.type === 'yen') {
						// moneyフォーマット
						obj.formatter = function (str) { return OreUtil.formatNum(str)+'円';}
					} else if (obj.type === 'number') {
						// numフォーマット
						obj.formatter = function (str) { return str === '' ? 0 : str;}
					} else if (obj.type === 'date') {
						// TODO:dateフォーマットを実装する
						
					}
				}
				// そのままプッシュ
				this.template.push(obj);
			}
		}
		OreLog.debug(this.template);
	},
	getStatus: function() {
		OreLog.info('OreTable.getStatus');
		var ret = {
			page: this.page,
			size: this.size,
			orderby: this.orderby,
			sort_field: this.sort_field,
			sort_asc: this.sort_asc,
		}
		return ret;
	},
	setStatus: function(params) {
		OreLog.info('OreTable.setStatus');
		this.page = typeof params.page ==='undefined' ? this.page : params.page;
		this.size = typeof params.size ==='undefined' ? this.size : params.size;
		this.orderby = typeof params.orderby ==='undefined' ? this.orderby : params.orderby;
		this.sort_field = typeof params.sort_field ==='undefined' ? this.sort_field : params.sort_field;
		this.sort_asc = typeof params.sort_asc ==='undefined' ? this.sort_asc : params.sort_asc;
	}
}
