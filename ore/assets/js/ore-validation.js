var OreValidationHelper = function() {
	//エラーメッセージ表示部分
	$('.form .validation').each(function(){
		var $span = $('<span/>').addClass('error-msg').hide();
		$(this).prepend($span);
	});
	this.errorStack = [];
};
OreValidationHelper.prototype = {
	preValidate: function(validations) {
		for (var i in validations) {
			var $elm = validations[i].$elm;
			if (typeof validations[i].valids !== 'undefined') {
				for(var j in validations[i].valids) {
					var valid = validations[i].valids[j];
					if (valid.type === 'required') {
						var $target = $elm.parents('td').prev();
						if ($target.find('span').length === 0) {
							$target.append('<span class="require-mark"></span>');
						}
					}
				}
			}
		}
	},
	validate: function(validations) {
		var data = {};
		var $form = validations[0].$elm.parents('form');
		var isError = false;

		this.errorStack = [];

		$('.error-msg', $form).hide();
		$('.validation', $form).removeClass('has-error');
		for (var i in validations) {
			var $elm = validations[i].$elm;
			var field = validations[i].field;
			var val;
			if ($elm.attr('type') === 'radio') {
				val = '';
				var $opt = $('input[name="' + $elm.attr('name') + '"]:checked');
				if (typeof $opt[0] !== 'undefined') {
					val = $elm.val();
				}
			} else {
				val = $elm.val();
			}
			if (typeof validations[i].mods !== 'undefined') {
				try {
					for(var j in validations[i].mods) {
						var mod = validations[i].mods[j];
						switch(mod.type) {
							case 'trim':
								val = this.modifyTrim(val);
								break;
							case 'hankaku_num':
								val = this.modifyHankakuNum(val);
								break;
							case 'to_yen':
								val = this.modifyToYen(val);
								break;
							default:
								//TODO 後で消してください。
								console.log('ng mod');
						}
					}
				} catch (e) {
					console.error('mod error');
					console.error(validations[i]);
					return false;
				}
			}
			if (typeof validations[i].valids !== 'undefined') {
				for(var j in validations[i].valids) {
					var valid = validations[i].valids[j];
					var error = false;
					switch (valid.type) {
						case 'required':
							if(!this.validateRequired(val)) {
								error = '必須項目です';
							}
							break;
						case 'date':
							if(!this.validateDate(val)) {
								error = '不正な日付です';
							}
							break;
						case 'postcode':
							if(!this.validatePostcode(val)){
								error = '不正な番号です';
							}
							break;
						case 'tel':
							if(!this.validateTel(val)) {
								error = '不正な番号です';
							}
							break;
						case 'email':
							if (!this.validateEmail(val)) {
								error = '不正なアドレスです';
							}
							break;
						case 'integer':
							val = val.replace(/,/g, '');
							if (!this.validateInteger(val)) {
								error = '整数で入力してください';
							}
							break;
						case 'number':
							val = val.replace(/,/g, '');
							if (!this.validateNumber(val)) {
								error = '数値で入力してください';
							}
							break;
						case 'equal':
							if (!this.validateEqual(val, data[valid.field])) {
								error = '一致しません';
							}
							break;
						case 'limit':
							if (!this.validateLimit(val, valid)) {
								error = '値を範囲内に収めてください';
							}
							break;
						case 'fdp':
							if (!this.validateFirstDecimalPlace(val)) {
								error = '小数第一位まで入力できます';
							}
							break;
						default:
							//TODO 後で消してください。
							console.log('ng valid');
					}
					if (error) {
						var $validation = $elm.parents('.validation').addClass('has-error');
						$('.error-msg', $validation).text(error).show();

						this.errorStack.push(valid.type);

						isError = true;
						break;
					}
				}
			}
			data[field] = val;
		}
		if (isError) {
			return false;
		} else {
			return data;
		}
	},
	validateRequired: function(val) {
		if (typeof val === 'undefined') {
			return false;
		}
		return (val.length > 0);
	},
	validateDate:function(val) {
		if (val === '') {
			return true;
		}
		var y,m,d;
		if (val.match(/^(\d+)[-\/](\d+)[-\/](\d+)$/) ||
			val.match(/^(\d{4})(\d{2})(\d{2})$/)
		) {
			y = RegExp.$1;
			m = RegExp.$2;
			d = RegExp.$3;
		} else {
			return false;
		}
		var dt= new Date(y,m-1,d);
		return (dt.getFullYear()==y && dt.getMonth()==m-1 && dt.getDate()==d);
	},
	validatePostcode: function(val) {
		if (val === '') {
			return true;
		}
		return val.match(/^(\d{3})-?(\d{4})$/);
	},
	validateTel: function(val) {
		if (val === '') {
			return true;
		}
		if((val.match(/-/g)||[]).length > 2) {
			return false;
		}
		if(!val.match(/^0[1-9]\d{1,3}-?\d{1,4}-?\d{4}$/)) {
			return false;
		}
		val = val.replace(/-/g, '');
		if (val.match(/^0[256789]0/)) {
			return (val.length === 11);
		} else {
			return (val.length === 10);
		}

	},
	validateEmail: function(val) {
		if (val === '') {
			return true;
		}
		return val.match(/^[A-Za-z0-9]+[\w\+\.-]+@[\w\.-]+\.\w{2,}$/);
	},
	validateInteger: function(val) {
		if (val === '') {
			return true;
		}
		return val.match(/^-?\d+$/)
	},
	validateNumber: function(val) {
		if (val === '') {
			return true;
		}
		return !isNaN(val);
	},
	validateFirstDecimalPlace: function(val) {
		if (val === '') {
			return true;
		}
		return val.match(/^\d+(\.\d)?$/)
	},
	validateEqual: function(val, val2) {
		return val === val2;
	},
	validateLimit: function(val, valid) {
		if (val === '') {
			return true;
		}
		var ret = true;
		if (valid.max !== 'undefined' && Number(val) > valid.max) {
			ret = false;
		}
		if (valid.min !== 'undefined' && Number(val) < valid.min) {
			ret = false;
		}
		if (valid.le !== 'undefined' && Number(val) <= valid.le) {
			ret = false;
		}
		return ret;
	},
	modifyTrim: function(val) {
		return val.trim();
	},
	//全角数字とハイフン・スラッシュを半角に変換
	modifyHankakuNum: function(val) {
		return OreUtil.modifyHankakuNum(val);
	},
	modifyToYen: function(val) {
		return String(val * 10000);
	}
};
$(function(){
	OreValidation = new OreValidationHelper();
});
//念の為
var OreValidation =  {
	validate: function() {
		return false;
	}
};
