var OreUtility = function(){
	this.loginUrl = '/ore/login.html';
	this.apiHost = 'https://oreapi-staging.orenoie.co.jp';
	this.apiUrl = this.apiHost + '/api/';
	this.token = null;
	this.loginStaffId = null;
	this.agentName = '';
	this.getParams = {};
	this.loadingStartTime = 300;
	this.ajaxFinished = true;
	//トークン取得
	if (typeof ORE_NO_LOGIN_CHECK === 'undefined' || !ORE_NO_LOGIN_CHECK) {
		var tokenPlace = localStorage.getItem('ore_token_place');
		if (tokenPlace !== 'session' && tokenPlace !== 'local') {
			console.log('API:Invalid tokenPlace'); // debug
			location.href = this.loginUrl;
			return;
		}
		var token = '';
		if (tokenPlace === 'session') {
			if(document.cookie != '') {
				var cookies = document.cookie.split('; ');
				for (var i in cookies) {
					var cookie = cookies[i].split('=');
					if (cookie[0] === 'ore_api_token') {
						if(decodeURIComponent(cookie[1]) === 'exist') {
							token = localStorage.getItem('ore_token');
							break;
						}
					}

				}
			}
		} else {
			token = window[tokenPlace + 'Storage'].getItem('ore_token');
		}
		if (!token) {
			console.log('API:Token missing'); // debug
			location.href = this.loginUrl;
			return;
		}
		this.token = token;
	}
	//管理者権限
	var adminFlg = localStorage.getItem('ore_admin_flg');
	if (adminFlg) {
		this.adminFlg = Number(adminFlg);
	} else {
		this.adminFlg = 0;
	}
	//不動産名
	var agentName = localStorage.getItem('ore_agent_name');
	if (agentName) {
		this.agentName = agentName;
	}
	//ログインスタッフID
	var staff_id  = localStorage.getItem('ore_login_staff_id');
	if (staff_id) {
		this.loginStaffId = staff_id;
	}
	//コントラクトネーム
	var contractName = localStorage.getItem('ore_contract_name');
	if (contractName) {
		this.contractName = contractName;
	}

	// ヘッダー自動書き換え
	$('.header-info-item').text(this.contractName + '：' + this.agentName);

	//GETパラメータ分離
	if (location.search) {
		var params = location.search.substring(1).split('&');
		for (var i in params) {
			var item = params[i].split('=', 2);
			if (item.length === 2) {
				this.getParams[item[0]] = decodeURIComponent(item[1]);
			}
		}
	}
	// ヘッダー修正
	$('.header').addClass('header-middle');
	$('.logo.logo-small').after('<div class="support-wrap"><span class="support"><img src="/ore/assets/images/support-tel.png" alt="OREサポートセンターはこちら"></span></div>');
	//オーバーレイ作成
	var $div = $('<div/>').attr('id', 'api-modal').css({
		'background-color': 'rgba(0,0,0, 0.5)',
		width: '100%',
		height: '100%',
		'z-index': 10000,
		position: 'fixed',
		top:0,
		left:0,
		display:'none'
	});
	var $img = $('<img/>').attr('src', '/ore/assets/images/loading-api.gif').css({
		position: 'absolute',
		display: 'block',
		left:'50%',
		top:'50%',
		'margin-left': '-101px',
		'margin-top': '-64px',
		width: '202px',
		height: '128px',
	});
	$('body').append($div.append($img));
	this.$apiModal = $div;

	//数値テキストボックス
	var valFunc = $.fn.val;
	$.fn.val = $.extend(function(){
		var $this = $(this);
		var num = $this.hasClass('format-num');
		var numOnly = $this.hasClass('num-only');
		var integer = $this.hasClass('integer');
		var noMinus = $this.hasClass('no-minus');
		if ((num || numOnly) && $this.attr('type') === 'text') {
			if (arguments.length > 0) {
				if (!num || arguments.length > 1) {
					return valFunc.apply(this, arguments);
				} else {
					arguments[0] = OreUtil.formatNum(arguments[0]);
					return valFunc.apply(this, arguments);
				}
			} else {
				var val = valFunc.apply(this, arguments);
				if (typeof val !== 'undefined') {
					val = val.trim();
					val = OreUtil.modifyHankakuNum(val);
					val = val.replace(/,/g, '');

					if(val.indexOf('.') > -1) {
						val = val.split('.');
						if (integer) {
							val = val[0];
						} else {
							val = val[0] + '.' + val[1];
						}
					}
					if (!noMinus && val.match(/^-/)) {
						val = '-' + val.replace(/[^\.0-9]/g, '');
					} else {
						val = val.replace(/[^\.0-9]/g, '');
					}
				}
				return val;
			}
		} else {
			return valFunc.apply(this, arguments);
		}

	}, valFunc);
};
$(function(){
	$('input.format-num[type="text"]').each(function(){
		var $this = $(this);
		$this.val($this.val());
	});
	$('input.format-num[type="text"],input.num-only[type="text"]').focus(function(){
		var $this = $(this);
		$this.val($this.val(), true);
	});
	$('input.format-num[type="text"],input.num-only[type="text"]').blur(function(){
		var $this = $(this);
		$this.val($this.val());
	});
});

OreUtility.prototype = {
	ajax: function (url, params, success, method, other) {
		var async = false;
		var chained = false;
		var sync = false;
		var error_func = null;
		this.$apiModal.css({opacity:1});
		if (typeof other !== 'undefined') {
			if (typeof other.async !== 'undefined') {
				async = other.async;
			}
			if (typeof other.chained !== 'undefined') {
				chained = other.chained;
			}
			if (typeof other.opacity !== 'undefined') {
				this.$apiModal.css({opacity:other.opacity});
			}
			if (typeof other.sync !== 'undefined') {
				sync = true;
			}
			if (typeof other.error_func !== 'undefined') {
				error_func = other.error_func;
			}
		}
		if (typeof method === 'undefined') {
			method = 'get';
		}
		method = method.toLowerCase();
		url = this.apiUrl + url;
		if (method === 'get') {
			if (Object.keys(params).length > 0) {
				var get_param = '';
				for (var i in params) {
					if (get_param !== '') {
						get_param += '&';
					}
					get_param += i + '=' + encodeURIComponent(params[i]);
				}
				url += '?' + get_param;
			}

		}
		var self = this;
		var ajax_param = {
			url: url,
			method: method,
			cache: false,
			headers: {
				Authorization:'Bearer ' + this.token
			},
			dataType: 'json',
			success: function(data){
				self.ajaxFinished = true;
				if (!chained && !async) {
					self.$apiModal.hide();
				}
				success(data);
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.debug(jqXHR);
				self.ajaxFinished = true;
				if (jqXHR.status === 401) {
					reauth();
					return;
				}
				self.$apiModal.hide();
				if (error_func) {
					error_func(jqXHR, defaultError);
				} else {
					defaultError();
				}
				function defaultError() {
					if (jqXHR.status === 400) {
						alert(textStatus+": "+errorThrown+'\n\n'+getProperties(JSON.parse(jqXHR.responseText)));
						function getProperties(obj) {
							var properties = '';
							for (var prop in obj){
								if (typeof obj[prop] === 'object') {
									properties += getProperties(obj[prop]);
								} else if (prop === 'messages') {
									properties += prop + ': ' + obj[prop] + '\n';
								}
							}
							return properties;
						}
					} else {
						alert(textStatus+": "+errorThrown);
					}
				}
			}
		};
		if (method !== 'get' && Object.keys(params).length > 0) {
			ajax_param.data = params;
		}
		if (method !== 'get' && params instanceof FormData) {
			// FormDataようにパラメータを修正
			ajax_param.dataType = 'text';
			ajax_param.processData  = false;
			ajax_param.contentType  = false;
			ajax_param.data = params;
		}
		if (!async) {
			this.ajaxFinished = false;
			var self = this;
			setTimeout(function() {
				if (self.ajaxFinished === false) {
					self.$apiModal.show();
				}
			}, this.loadingStartTime);
		}
		if (sync) {
			ajax_param.async = false;
		}
		var callCnt = 1;
		//ajax_param.headers = {Authorization:'Bearer aaa'};
		$.ajax(ajax_param);
		function reauth() {
			if (callCnt>1) {
				OreUtil.$apiModal.hide();
				alert('トークンの再取得に失敗しました。');
				location.href = OreUtil.loginUrl;
				return;
			}
			callCnt++;
			$.ajax({
				url: OreUtil.apiHost + '/login',
				type: "POST",
				headers: {
					"Authorization":"Basic " + btoa("ORE/Client4PasswordAccess:ORE/Clientsecret")
				},
				data: {
					grant_type:"refresh_token",
					refresh_token:localStorage.getItem('ore_refresh_token'),
					//scope:"oreapi"
				},
				dataType: 'json',
				success: function(data){
					OreUtil.saveToken(data, localStorage.getItem('ore_token_place'));
					ajax_param.headers = {Authorization:'Bearer ' + data.access_token};
					$.ajax(ajax_param);
				},
				error: function(jqXHR, textStatus, errorThrown){
					OreUtil.$apiModal.hide();
					console.log(jqXHR);
				}
			});

		}

	},
	saveToken: function (data, tokenPlace) {
		window.localStorage.setItem('ore_token_place', tokenPlace);
		if (tokenPlace === 'session') {
			document.cookie = 'ore_api_token=exist';
			document.cookie = 'ore_api_token2=' + data.access_token;

		}
		window.localStorage.setItem('ore_token', data.access_token);
		window.localStorage.setItem('ore_login_staff_id', data.staff_id);
		window.localStorage.setItem('ore_refresh_token', data.refresh_token);

		this.token = data.access_token;
	},
	isLogin: function () {
		if (this.token) {
			return true;
		} else {
			return false;

		}

	},
	formatNumMan: function(num, numOnly) {
		if (num === null || isNaN(num)) {
			return '';
		}
		num = Math.round(num / 10000);
		if (numOnly) {
			return this.formatNum(num);
		} else {
			return this.formatNum(num) + '万円';
		}
	},
	formatMadori: function(data) {
		var str = '';
		if (data.madori_num !== null) {
			str += data.madori_num;
		}
		if (data.madori_type1 !== null) {
			str += data.madori_type1;
		}
		if (data.madori_type2 !== null) {
			str += data.madori_type2;
		}
		return str;
	},
	formatNum: function (num) {
		var delimiter = '.',
			numString = num.toString(),
			targetNum;

		if(numString.indexOf(delimiter) > -1){
			targetNum = numString.split(delimiter);
		} else {
			targetNum = numString;
		}
		if(targetNum instanceof Array){
			return targetNum[0].replace(/(\d)(?=(\d{3})+$)/g , '$1,') + delimiter + targetNum[1];
		} else {
			return targetNum.replace(/(\d)(?=(\d{3})+$)/g , '$1,')
		}
	},
	escHtml: function(ch) {
		ch = ch.replace(/&/g,"&amp;") ;
		ch = ch.replace(/"/g,"&quot;") ;
		ch = ch.replace(/'/g,"&#039;") ;
		ch = ch.replace(/</g,"&lt;") ;
		ch = ch.replace(/>/g,"&gt;") ;
		return ch ;
	},
	makePager: function($elm, page, total, cols) {
		$elm.empty();
		if (total > 1) {
			var org_page = page;

			if (page > total) {
				page = total;
			}
			var pages = [page];
			var prev = page - 1;
			var next = page + 1;
			while (true) {
				if (prev > 0) {
					pages.unshift(prev);
					prev--;
				}
				if (pages.length >= cols) {
					break;
				}
				if (next <= total) {
					pages.push(next);
					next++;
				}
				if (pages.length >= cols || (prev < 1 && next > total)) {
					break;
				}

			}
			 if (total > cols) {
				 if (pages[0] > 1) {
					 pages[0] = -1;
				 }
				 if (pages[cols-1] < total) {
					 pages[cols-1] = -1;
				 }
			 }
			if (page > 1) {
				var $li, $a;
				$li = $('<li/>');
				$a = $('<a/>').attr('href', 'javascript:void(0);').attr('aria-label', 'prev').data('page', 1);
				$a.append($('<i class="icon icon-pager-start"></i>'));
				$li.append($a);
				$elm.append($li);
				$li = $('<li/>');
				$a = $('<a/>').attr('href', 'javascript:void(0);').attr('aria-label', 'prev').data('page', page - 1);
				$a.append($('<i class="icon icon-pager-prev"></i>'));
				$li.append($a);
				$elm.append($li);
			}
			for (var i in pages) {
				if (pages[i] > 0) {
					$li = $('<li/>');
					if (org_page === pages[i]) {
						$li.addClass('active');
					}
					$a = $('<a/>').attr('href', 'javascript:void(0);').data('page', pages[i]).text(pages[i]);
					$li.append($a);
					$elm.append($li);
				} else {
					$li = $('<li/>').html('<span>&#8230;</span>');
					$elm.append($li);
				}
			}
			if (page < total) {
				var $li, $a;
				$li = $('<li/>');
				$a = $('<a/>').attr('href', 'javascript:void(0);').attr('aria-label', 'prev').data('page', page + 1);
				$a.append($('<i class="icon icon-pager-next"></i>'));
				$li.append($a);
				$elm.append($li);
				$li = $('<li/>');
				$a = $('<a/>').attr('href', 'javascript:void(0);').attr('aria-label', 'prev').data('page', total);
				$a.append($('<i class="icon icon-pager-end"></i>'));
				$li.append($a);
				$elm.append($li);
			}
		}

	},
	makeSortArrow: function(items, field, asc) {
		for(var i in items) {
			var item = items[i];
			item.$elm.empty();
			var $a = $('<a/>').addClass('btn-sort')
				.attr('href', 'javascript:void(0);')
				.data('field', item.field)
				.html('<i class="icon icon-sort"></i>');
			if (field === item.field) {
				$a.addClass('active').data('asc', !asc);
				if (asc) {
					$a.addClass('asc');
				}
			} else {
				$a.data('asc', false);
			}
			item.$elm.append($a);
		}
	},
	getClientName:function(id, callback) {
		var name = sessionStorage.getItem('ore_client_name_' + id);
		if (name !== null) {
			callback(name);
			return;
		}
		var self = this;
		this.ajax('clients/' + id, {}, function(data){
			name = self.setClientName(data.content);
			callback(name);
		}, 'get', {async:true});
	},
	setClientName: function(client) {
		var	name = client.last_name + '　' + client.first_name;
		sessionStorage.setItem('ore_client_name_' + client.id, name);
		return name;
	},
	makeWhere: function(params) {
		var where = [];
		for (var i in params) {
			var param = params[i];
			var value = param.value.replace(/\\/g, '\\\\').replace(/"/g, '\\"');
			if (param.ope === 'like') {
				value = value.replace(/%/g, '\\%');
				value = '%' + value + '%';
			} else if (param.ope === 'in') {
				value = value.split(',');
			}
			where.push('{"field":"' + param.field + '","ope":"' + param.ope + '","value":"' + value + '"}');
		}
		if (where.length > 0) {
			return '[' + where.join(',') + ']';
		} else {
			return '';
		}
	},
	modifyHankakuNum: function(val) {
		val = val.replace(/[０-９]/g,
			function(tmp) {
				return String.fromCharCode(tmp.charCodeAt(0) - 0xFEE0);
			}
		);
		val = val.replace(/[ー―‐]/g, '-');
		val = val.replace(/／]/g, '/');
		return val;
	},
	postForm: function(url, data) {
        var $form = $('<form/>', {'action': url, 'method': 'post'});
        for(var key in data) {
                $form.append($('<input/>', {'type': 'hidden', 'name': key, 'value': data[key]}));
        }
        $form.appendTo(document.body);
        $form.submit();
	},
	makeRandomString: function(length) {
		// 生成する文字列に含める文字セット
		var c = "abcdefghijklmnopqrstuvwxyz0123456789";

		var cl = c.length;
		var r = "";
		for(var i=0; i<length; i++){
		  r += c[Math.floor(Math.random()*cl)];
		}
		return r;
	},
	sendResetMail: function(email) {
		OreLog.info('sendResetMail', this, email);
		var dfd = $.Deferred();

		// メールアドレスにパスワード変更メールを送信する
		$('#api-modal').show();
		$.ajax({
			url: this.apiUrl + 'staffs/reissue_password',
			type: "POST",
			data: {
				email:email,
			},
			dataType: 'json',
		}).done(function(data) {
			OreLog.info('メール送信成功');
			$('#api-modal').hide();
			dfd.resolve();
		}).fail(function(data) {
			OreLog.info('メール送信失敗');
			$('#api-modal').hide();
			dfd.reject();
		});
		return dfd.promise();
	},
	compassNumtoDeg: function(num) {
		return -num * 22.5;
	}
};
var OreUtil = new OreUtility();
