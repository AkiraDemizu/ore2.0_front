function ore_make_sidemenu() {
	var html = '\
				<div class="logo-area">\n\
					<div class="logo">\n\
						<a href="/ore/menu.html"><img src="/ore/assets/images/logo.png" alt="ORE Open Renovation Engine"></a>\n\
					</div>\n\
					<div class="sidemenu-btn sidemenu-close">\n\
						<span class="btn-close"></span>\n\
					</div>\n\
				</div>\n\
				<div class="row btn-area btn-area-regist">\n\
					<div class="col-xs-12"><a class="btn btn-block btn-orange" href="/ore/master/customer_add.html" onclick="OreStrage.clear();"><i class="icon icon-add"></i>顧客 新規登録</a></div>\n\
					<div class="col-xs-12"><a class="btn btn-block btn-orange" href="/ore/master/property_add.html" onclick="OreStrage.clear();"><i class="icon icon-add"></i>物件 新規登録</a></div>\n\
				</div>\n\
				<div class="link-list-wrap">\n\
					<ul class="link-list">\n\
						<li><a href="/ore/master/contract_list.html" onclick="OreStrage.clear();">契約物件一覧</a></li>\n\
						<li><a href="/ore/master/customer_list.html" onclick="OreStrage.clear();">顧客一覧</a></li>\n\
						<li><a href="/ore/master/property_list.html" onclick="OreStrage.clear();">自社登録物件一覧</a></li>\n\
					</ul>\n\
					<ul class="link-list">\n\
						<li><a href="/ore/master/staff_list.html" onclick="OreStrage.clear();">スタッフ一覧</a></li>\n\
						<li id="staff_add"><a href="/ore/master/staff_add.html" onclick="OreStrage.clear();">スタッフ新規登録</a></li>\n\
						<!--<li><a href="/ore/master/staff_edit.html" onclick="OreStrage.clear();">スタッフ編集・削除</a></li>-->\n\
					</ul>\n\
					<ul class="link-list">\n\
						<li><a href="/ore/publish/plan_list.html" onclick="OreStrage.clear();">掲載物件一覧</a></li>\n\
						<li><a href="/ore/master/info.html" onclick="OreStrage.clear();">契約・お支払い情報</a></li>\n\
					</ul>\n\
				</div>\n\
				<div class="sidemenu-bottom">\n\
					<a class="btn btn-gray logout" href="javascript:void(0);">ログアウト</a>\n\
					<div class="copyright">Copyright © オレの家 All rights Reserved.</div>\n\
				</div>\n';
	$('#sidemenu').html(html);
}
ore_make_sidemenu();
$(function(){
	if (OreUtil.adminFlg === 0) {
		$('#staff_add').hide();
	}

	$('#sidemenu a.logout').click(function(){
		window.localStorage.removeItem('ore_token_place');
		window.localStorage.removeItem('ore_token');
		window.sessionStorage.removeItem('ore_token');
		OreStrage.clear();
		location.href = '/ore/login.html';
	});

	$("body").click(function(e) {
		if (!($(e.target).hasClass('sidemenu') || $(e.target).hasClass('sidemenu-btn') || $(e.target).hasClass('sidemenu-btn-line'))) {
			$('#sidemenu').slidemenu('hide');
		}
	});
});
