var Strage = function() {
	//this.clear();
	OreLog.info('Strage');
	this.getParams = {};
	var item = localStorage.getItem(location.pathname);
	OreLog.debug('location.pathname', location.pathname);
	OreLog.debug('item', item);
	if (item !== null) {
		this.getParams = JSON.parse(item);
	}
	OreLog.debug('getParams', this.getParams);
}
Strage.prototype = {
	// 登録につかうキー(pathname)のリスト
	// プロジェクトごとに変えて使う
	pathname: {
		MASTER_CUSTOMER_EDIT: '/ore/master/customer_edit.html',
		MASTER_CUSTOMER_LIST: '/ore/master/customer_list.html',
		MASTER_STAFF_LIST: '/ore/master/staff_list.html',
		MASTER_PROPERTY_LIST: '/ore/master/property_list.html',
		PUBLISH_PROPERTY_LIST: '/ore/publish/property_list.html',
		PLANNING_CUSTOMER_SEARCH: '/ore/planning/customer_search.html',
		PLANNING_PROPERTY_SEARCH: '/ore/planning/property_search.html',
	},
	// pathnameをキーにparamsを保存
	push: function(pathname, params) {
		OreLog.info('Strage.push');
		OreLog.debug('pathname', pathname);
		OreLog.debug('params', params);
		
		localStorage.setItem(pathname, JSON.stringify(params));
	},
	// 保存後pathnameに遷移する
	move: function(pathname, params, search) {
		this.push(pathname, params);
		location.href = pathname + search;
	},
	// カレントページにparamsを保存する
	save: function(params) {
		this.push(location.pathname, params);
	},
	// ストレージをクリア
	clear: function() {
		OreLog.info('Strage.clear');
		for (var pathname in this.pathname) {
			// nullでクリアする
			localStorage.setItem(this.pathname[pathname], '[]');
		}
	}
}

var OreStrage = new Strage();
